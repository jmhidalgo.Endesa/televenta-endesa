@isTest
private class TV_CLS_Test_Factory_Test {
 
    @isTest static void createClientList_Test() {
        
        Test.startTest();
        List<Account> accountList = TV_CLS_Test_Factory.createClientList();
        insert accountList;
        Test.stopTest();  
        
        For( Account accountItem: accountList ){
            System.debug( 'HSYSTEM.DEBUG: accountItem.id: ' + accountItem.Id );
        }
                
        System.assertEquals( 9, accountList.size(), 'Deben haber 9 Clients.' );
    }
    
    @isTest static void createContactList_Test() {
        
        Test.startTest();
        List<Account> accountList = TV_CLS_Test_Factory.createClientList();
        insert accountList;
        Test.stopTest();  
        
        List<Contact> contactList = [ SELECT id, name
                                      FROM Contact
                                      WHERE name = :accountList[8].Name ]; 
        If ( !contactList.isEmpty() ){
            System.assert( contactList[0].Name == accountList[8].Name );
        } Else {
            System.assert( false );
        }
    }
    
    @isTest static void createLeadList_Test() {
        
        Test.startTest();
        List<Lead> leadList = TV_CLS_Test_Factory.createLeadList();
        insert leadList;
        Test.stopTest();
        
        For( Lead leadItem: leadList ){
            System.debug( 'HSYSTEM.DEBUG: leadItem.id: '+ leadItem.Id );
        }
        
        System.assertEquals( 9, leadList.size(), 'Deben haber 9 Leads.' );
    }
    
    @isTest static void createCampaign_Test() {
        
        Test.startTest();
        Campaign campaignItem = TV_CLS_Test_Factory.createCampaign();
        insert campaignItem;
        Test.stopTest();
        
        System.debug( 'HSYSTEM.DEBUG: campaignItem.id: '+ campaignItem.Id );
          
        System.assert( campaignItem != null );
    }
    
    @isTest static void createComunication_Test() {
        
        Test.startTest();
        Campaign campaignItem = TV_CLS_Test_Factory.createComunication();
        insert campaignItem;
        Test.stopTest();
        
        System.debug( 'HSYSTEM.DEBUG: campaignItem.id: '+ campaignItem.Id );
          
        System.assert( campaignItem != null );
    }
    
    @isTest static void createAddressList_Test() {
        
        Test.startTest();
        List<B2C_Address__c> addressList = TV_CLS_Test_Factory.createAddressList();
        insert addressList;
        Test.stopTest();  
        
        For( B2C_Address__c addressItem: addressList ){
            System.debug( 'HSYSTEM.DEBUG: addressItem.id: ' + addressItem.Id );
        }
                
        System.assertEquals( 9, addressList.size(), 'Deben haber 9 Address.' );
    }
    
    @isTest static void createServicePointList_Test() {
        
        Test.startTest();
        List<B2C_Service_Point__c> servicePointList = TV_CLS_Test_Factory.createServicePointList();
        insert servicePointList; 
        Test.stopTest();  

        For( B2C_Service_Point__c servicePointItem: servicePointList ){
            System.debug( 'HSYSTEM.DEBUG: servicePointItem.id: ' + servicePointItem.Id );
        }
                
        System.assertEquals( 9, servicePointList.size(), 'Deben haber 9 Service Point.' );
    }
    
    @isTest static void createCampaignMemberList_Test() {
        
        Test.startTest();
        ServicePointTriggerHandler.bypassTrigger = true;
        List<CampaignMember> campaignMemberList = TV_CLS_Test_Factory.createCampaignMemberList();
        insert campaignMemberList;
        ServicePointTriggerHandler.bypassTrigger = false;
        Test.stopTest();
        
        For( CampaignMember campaignMemberItem: campaignMemberList ){
            System.debug( 'HSYSTEM.DEBUG: campaignMemberItem.id: ' + campaignMemberItem.Id );
        }
          
        System.assertEquals( 9, campaignMemberList.size(), 'Deben haber 9 Service Point.' );
    }
    
    @isTest static void createInteration_Test() {
        
        ServicePointTriggerHandler.bypassTrigger = true;
        List<CampaignMember> campaignMemberList = TV_CLS_Test_Factory.createCampaignMemberList();
        insert campaignMemberList;
        
        Test.startTest();
        Task taskItem = TV_CLS_Test_Factory.createInteration( campaignMemberList[0] );
        insert taskItem;
        Test.stopTest();
        
        ServicePointTriggerHandler.bypassTrigger = false;
        
        System.debug( 'HSYSTEM.DEBUG: taskItem.id: '+ taskItem.Id );
          
        System.assert( taskItem != null );
    }
    
}