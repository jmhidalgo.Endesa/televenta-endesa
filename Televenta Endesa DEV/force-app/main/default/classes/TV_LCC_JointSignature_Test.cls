/**
 * @author			ATOS (ABS)
 * @description		Apex class for TV_LCC_JointSignature
 *
 * Edit Log:
 * @history			2020/04/10  |  ETV-3  |  ATOS (ABS)   |  Creation.
 * @history			2020/04/11  |  ETV-3  |  ATOS (ABS)   |  Continue creating all method class.
 */
@isTest
public with sharing class TV_LCC_JointSignature_Test {
  /**
   * @description		Function for insert data for test class
   */
  @testSetup
  static void setup() {
    NeedConfig__c needConfig = new NeedConfig__c();
    needConfig.NeedMethodSign__c = '10';
    needConfig.NeedChannel__c = '4';
    insert needConfig;

    Account account = new Account();
    account.LastName__c = 'TestNameSetUp';
    account.FirstName__c = 'TestNameSetUp';
    account.Identifier_Type__c = 'NIF';
    account.NIF_CIF_Customer_NIE__c = '36982412E';
    account.Email_con__c = 'test@gmail.com';
    account.Contact_method_cd__c = 'E-mail';
    account.Licensed__c = false;
    account.Cliente_External_Id__c = '34-ee';
    account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName()
      .get('Cliente')
      .getRecordTypeId();
    account.Physical_or_legal_cd__c = 'Physical Customer';
    account.MobilePhone1__c = '678689687';
    account.No_Phone_flg__c = false;
    account.No_Email_flg__c = false;
    account.Identifier_Number__c = 224346;
    account.Account_Type__c = 'Residential';
    account.Electronic_bill_flg__c = true;
    account.Duplicate_convenience_flg__c = true;
    account.Name = 'Autocalculado';
    insert account;

    Case case1 = new Case();
    case1.AccountId = account.Id;
    insert case1;

    B2C_CNAE__c cnae1 = new B2C_CNAE__c();
    cnae1.name = '9820';
    insert cnae1;

    final B2C_Address__c address = new B2C_Address__c();
    address.Premise_flg__c = true;
    address.Main_Address__c = false;
    address.Adress_active_flg__c = true;
    address.Country_cd__c = 'ES';
    address.Country_desc__c = 'ESPAÑA';
    address.Province_cd__c = '15';
    address.Province_text__c = 'A CORUÑA';
    address.County_cd__c = '030';
    address.County__c = 'A CORUÑA';
    address.City_cd__c = 'BER13108';
    address.City__c = 'A CORUÑA';
    address.INE_city_cd__c = '15030000101';
    address.Street_cd__c = 'BER0569533';
    address.Street__c = 'NELLE';
    address.INE_street_cd__c = '1503000423';
    address.Type_street__c = 'RD';
    address.Zipcode__c = '12345';
    address.Longitude__c = 0;
    address.Latitude__c = 0;
    address.Census_tract__c = '123';
    address.Street_number__c = '109';
    address.Point_duplicator_cd__c = 'A';
    address.Type_Point_lightener_cd__c = 'AFS';
    address.Point_lightener__c = 'S';
    address.Floor_cd__c = '4';
    address.Door__c = '2';
    address.Build__c = '1';
    address.Stairs__c = '3';
    address.Adress_lightener__c = 'asdf';
    address.Type_address_cd__c = 'Normalized';
    address.Reference_year__c = 2010;
    address.INE_county_cd__c = '030';
    address.INE_city_cd__c = '15030000101';
    address.UTM_x__c = '2';
    address.UTM_y__c = '2';
    address.Trust_level__c = 5;
    address.CUPS_13_gas__c = 'CRM0001391063';
    address.CUPS_13_elec__c = 'CRM0001312697';
    address.Description_adress__c = 'Descripcion calle';
    address.INE_state_cd__c = 'PR';
    address.DIR_external_CRM_id__c = '123456789012345';
    address.Formatedd_Address__c = 'Formatedd Address';
    address.DIR_external_account_id__c = '123456789012345';
    address.DIR_external_contact_id__c = '123456789012345';
    address.Account__c = account.Id;
    AddressTriggerHandler.bypassTrigger = true;
    insert address;
    AddressTriggerHandler.bypassTrigger = false;

    B2C_Service_Point__c sp = new B2C_Service_Point__c();
    sp.RecordTypeId = Schema.SObjectType.B2C_Service_Point__c.getRecordTypeInfosByName()
      .get('Gas')
      .getRecordTypeId();
    sp.Name = 'PENDIENTE_INFORMAR';
    sp.Business_line_cd__c = '02';
    sp.Tariff_cd__c = '31';
    sp.Annual_Consumption__c = 3000;
    sp.Reason_creation__c = 'No localizado';
    sp.Distribution_Group_Gas__c = 'Test';
    sp.DistribuitorGas_cd__c = '0222';
    sp.PostalCode__c = '12345';
    sp.Street__c = 'Falsa';
    sp.Street_number__c = '68';
    sp.country__c = 'ES';
    sp.CNAE__c = cnae1.Id;
    sp.Account_id__c = account.Id;
    sp.AddrId_Id__c = address.Id;
    ServicePointTriggerHandler.bypassTrigger = true;
    insert sp;
    ServicePointTriggerHandler.bypassTrigger = false;

    NE__Quote__c oferta = new NE__Quote__c();
    oferta.NE__AccountId__c = account.Id;
    oferta.Case__c = case1.Id;
    oferta.FI_NEQ_SEL_Movement_type__c = 'A';
    oferta.FI_NEQ_SEL_Flow_Controlling_Action__c = 'A';
    insert oferta;

    NE__Order__c configuration = new NE__Order__c();
    configuration.NE__Configuration_Type__c = 'New';
    configuration.NE__Configuration_SubType__c = 'Standard';
    configuration.NE__AccountId__c = account.Id;
    configuration.NE__BillAccId__c = account.Id;
    configuration.NE__ServAccId__c = account.Id;
    configuration.NE__Quote__c = oferta.id;
    insert configuration;

    final NE__Catalog__c ca = new NE__Catalog__c();
    insert ca;

    NE__Catalog_Item__c cata = new NE__Catalog_Item__c();
    cata.Ne__Catalog_Id__c = ca.id;
    insert cata;

    NE__Product__c product = new NE__Product__c();
    product.RecordTypeId = Schema.SObjectType.NE__Product__c.getRecordTypeInfosByName()
      .get('Standard')
      .getRecordTypeId();
    product.Type__c = 'Product';
    product.Code__c = 'TNS31';
    product.Name = 'Gas Endesa Mini';
    insert product;

    NE__Product__c product1 = new NE__Product__c();
    product1.RecordTypeId = Schema.SObjectType.NE__Product__c.getRecordTypeInfosByName()
      .get('Standard')
      .getRecordTypeId();
    product1.Type__c = 'Product';
    product1.Code__c = 'TNS32';
    product1.Name = 'Gas Endesa Mini1';
    insert product1;
    FI_TRG_NE_OrderItem_Handler.bypassTrigger = true;
    NE__OrderItem__c orderItem = new NE__OrderItem__c();
    orderItem.NE__OrderId__c = configuration.Id;
    orderItem.NE__Qty__c = 1;
    orderItem.NE__ProdId__c = product.Id;
    orderItem.NE__Account__c = account.Id;
    orderItem.NE__Status__c = 'Pending';
    orderItem.FI_CI_LKP_Quote__c = oferta.Id;

    orderItem.FI_CI_SEL_Business_Line__c = '02';
    orderItem.FI_CI_LKP_CUPS__c = sp.Id;
    orderItem.FI_CI_SEL_Company_holder__c = '20';
    orderItem.FI_CI_FLG_Skip_Validations__c = true;
    insert orderItem;

    NE__OrderItem__c orderItem1 = new NE__OrderItem__c();
    orderItem1.NE__OrderId__c = configuration.Id;
    orderItem1.NE__Qty__c = 1;
    orderItem1.NE__ProdId__c = product1.Id;
    orderItem1.NE__Account__c = account.Id;
    orderItem1.FI_CI_SEL_Business_Line__c = '01';
    orderitem1.FI_CI_SEL_Access_channel__c = '66';
    orderItem1.FI_CI_LKP_CUPS__c = sp.Id;
    orderItem1.FI_CI_SEL_Company_holder__c = '20';
    orderItem1.FI_CI_FLG_Electronicinvoice__c = true;
    orderItem1.FI_CI_FLG_Skip_Validations__c = true;
    insert orderItem1;
    
    NE__OrderItem__c orderItem2 = new NE__OrderItem__c();
    orderItem2.NE__OrderId__c = configuration.Id;
    orderItem2.NE__Qty__c = 1;
    orderItem2.NE__Status__c = 'In Review';
    orderItem2.NE__ProdId__c = product1.Id;
    orderItem2.NE__Account__c = account.Id;
    orderItem2.FI_CI_SEL_Business_Line__c = '01';
    orderitem2.FI_CI_SEL_Access_channel__c = '66';
    orderItem2.FI_CI_LKP_CUPS__c = sp.Id;
    orderItem2.FI_CI_SEL_Company_holder__c = '20';
    orderItem2.FI_CI_FLG_Electronicinvoice__c = true;
    orderItem2.FI_CI_FLG_Skip_Validations__c = true;
    orderItem2.FI_CI_LKP_Quote__c = oferta.Id;
    insert orderItem2;
  }
  /**
   * @description		Method for function getAccountId
   */
  @isTest
  static void testGetAccountId() {
    final List<Case> lstCase = [SELECT Id FROM Case LIMIT 1];
    if (!lstCase.isEmpty()) {
      String accountId = TV_LCC_JointSignature.getAccountId(lstCase[0].Id);
      System.assertEquals(
        '001',
        accountId.substring(0, 3),
        'AccountId Start for 001'
      );
    }
  }
  /**
   * @description		Method for function getAccountId
   */
  @isTest
  static void testGetAccountIdNull() {
      String accountId = TV_LCC_JointSignature.getAccountId('001');
      System.assertEquals(
        null,
        accountId,
        'AccountId is null'
      );
  }
  /**
   * @description		Method for function getLoginData
   */
  @isTest
  static void testGetLoginData() {
    WS_Info__mdt testMdt = new WS_Info__mdt();
    testMdt = TV_LCC_JointSignature.getLoginData();
    System.assertEquals(
      'TV_Genesys',
      testMdt.DeveloperName,
      'DeveloperName it´s TV_Genesys'
    );
  }
  /**
   * @description		Method for function getInteractions
   */
  @isTest
  static void testGetInteractions() {
    final List<Account> lstAcc = [
      SELECT Id
      FROM Account
      WHERE FirstName__c = 'TestNameSetUp' AND LastName__c = 'TestNameSetUp'
      LIMIT 1
    ];
    if (!lstAcc.isEmpty()) {
      Task taskInProgress = TV_LCC_JointSignature.getInteractions(lstAcc[0].Id);
      System.assertEquals(null, taskInProgress, 'There is task created');
    }
  }
  /**
   * @description		Method for function updTypeSign
   */
  @isTest
  static void testUpdTypeSign() {
    final List<Case> lstCase = [SELECT Id FROM Case LIMIT 1];
    if (!lstCase.isEmpty()) {
      TV_LCC_JointSignature.updTypeSign(lstCase[0].Id);
      System.assertEquals(1, lstCase.size(), 'List size should be 1');
    }
  }
  /**
   * @description		Method for function updTypeSign2
   */
  @isTest
  static void testUpdTypeSign2() {
    final List<Case> lstCase = [SELECT Id FROM Case LIMIT 1];
    if (!lstCase.isEmpty()) {
      TV_LCC_JointSignature.updTypeSign(lstCase[0].Id);
      System.assertEquals(1, lstCase.size(), 'List size should be 1');
    }
  }
  /**
   * @description		Method for function permissionOK
   */
  @isTest
  static void testPermissionOK() {
    final List<Case> lstCase = [SELECT Id FROM Case LIMIT 1];
    if (!lstCase.isEmpty()) {
      TV_LCC_JointSignature.WrapperPermission wrapPer = new TV_LCC_JointSignature.WrapperPermission();
      wrapPer = TV_LCC_JointSignature.permissionOK(lstCase[0].Id);
      System.assertEquals(
        false,
        wrapPer.channelOk,
        'Channel should be give false'
      );
      System.assertEquals(
        false,
        wrapPer.interactionInProgress,
        'interactionInProgress should be give false'
      );
    }
  }

  /**
   * @description		Method for function checkCIAndAddress
   */
  @isTest
  static void testCheckCIAndAddress() {
    TV_LCC_JointSignature.WrapperValidation wrapVal = new TV_LCC_JointSignature.WrapperValidation();
    final List<Case> lstCase = [SELECT Id FROM Case LIMIT 1];
    if (!lstCase.isEmpty()) {
      wrapVal = TV_LCC_JointSignature.checkCIAndAddress(lstCase[0].Id);
      System.assertEquals(
        '',
        wrapVal.validationOk,
        'validationOk should be give empty string'
      );
      System.assertEquals(
        true,
        wrapVal.statusKo,
        'statusKo should be give true'
      );
      System.assertEquals(
        0,
        wrapVal.differentAddress,
        'differentAddress should be give 0'
      );
    }
  }

  /**
   * @description		Method for function signCaseCIs
   */
  @isTest
  static void testSignCaseCIs() {
    final List<Case> lstCase = [SELECT Id FROM Case LIMIT 1];
    if (!lstCase.isEmpty()) {
      TV_LCC_JointSignature.signCaseCIs(lstCase[0].Id, '10');
      System.assertEquals(1, lstCase.size(), 'List size should be 1');
    }
  }

  /**
   * @description		Method for function getAllMethods
   */
  @isTest
  static void testGetAllMethods() {
    List<String> lstStr = TV_LCC_JointSignature.getAllMethods();
  }

  /**
   * @description		Method for function getLegalTextConga
   */
  @isTest
  static void testGetLegalTextConga() {
    TV_LCC_Util_LegalText.LegalTextWrapper wrapText = new TV_LCC_Util_LegalText.LegalTextWrapper();
    final List<Case> lstCase = [SELECT Id FROM Case LIMIT 1];
    if (!lstCase.isEmpty()) {
      wrapText = TV_LCC_JointSignature.getLegalTextConga(lstCase[0].Id);
    }
  }

  /**
   * @description		Method for function checkProfile
   */
  @isTest
  static void testCheckProfile() {
    Boolean permission = TV_LCC_JointSignature.checkProfile();
    system.assertEquals(false, permission, 'Should to be false');
  }

  /**
   * @description		Method for function reSigned
   */
  @isTest
  static void testReSigned() {
    final List<Case> lstCase = [SELECT Id FROM Case LIMIT 1];
    if (!lstCase.isEmpty()) {
      Date now = Date.today();
      TV_LCC_JointSignature.reSigned(lstCase[0].Id, now);
    }
  }
  /**
   * @description		Method for function reSigned2
   */
  @isTest
  static void testReSigned2() {
    final List<Case> lstCase = [SELECT Id FROM Case LIMIT 1];
    if (!lstCase.isEmpty()) {
      TV_LCC_JointSignature.reSigned(lstCase[0].Id, null);
    }
  }
}