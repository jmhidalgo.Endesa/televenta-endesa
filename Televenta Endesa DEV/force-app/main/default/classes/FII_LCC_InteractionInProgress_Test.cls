/**
 *   @author     Everis
 *   @since      April 18, 2019
 *   @desc       Test Class of FII_LCC_InteractionInProgress
 *   @history    April 18, 2019 - everis (JVC) - Create Apex Class 
 */
@isTest private class FII_LCC_InteractionInProgress_Test {
	
	private static final String CONTACT_LASTNAME = 'Pendiente de Informar';
	private static final String ACCOUNT_NIF = '85258421C';
	
	@testSetup static void setup(){
       	Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
		final List<Contact> listContact = new List<Contact>();
		final Account accTest  = UtilTest.createAccount( 'Test',  'Contacto',  'Contacto2', 'email@test.com','NIF',  '85258421C', 'Account',  false);
		insert accTest;
		//final Contact con = UtilTest.createContact();
		final Contact contest = UtilTest.createContact( accTest.Id,  'Contacto',  'Pendiente de Informar',  '655888777',  'email@test.com',  'NIF',  '75888923D');
		//listContact.add(con);
		listContact.add(contest);
		AccountTriggerHandler.bypassTrigger = true;
		insert listContact;
		// Interaction related only with client
		final Task interaction = FII_Util_GSM.createInteraction(contest.AccountId, null, null, 'Phone', 'interaction');
		System.debug('SSM: ' + interaction);
        final List<Contact> listCont = [Select Id,Name,LastName,FirstName from Contact limit 1];
		final List<Account> listAcc = [Select Id, FirstName__c from Account limit 1];
		System.debug('SSM listCont: ' + listCont);
        System.debug('SSM listAcc: ' + listAcc);
		insert interaction;
	}
	
	@isTest static void interactionInProgressFunctionality(){
		final Contact con = [SELECT Id, Name, LastName, AccountId FROM Contact WHERE LastName = :CONTACT_LASTNAME LIMIT 1];
		final Account acc = [SELECT Id, Name FROM Account WHERE NIF_CIF_Customer_NIE__c = :ACCOUNT_NIF LIMIT 1];
		
		Test.startTest();
		// Consultamos la interacción en curso
		Task intInProgress = FII_LCC_InteractionInProgress.getInteractionsInProgress(acc.Id);
		System.assertNotEquals(null, intInProgress);
		
		// Como la interacción en curso no tiene un contacto relacionado, el botón está deshabilitado
		Boolean disabled = FII_LCC_InteractionInProgress.disableButton(acc.Id, acc.Id, intInProgress);
		System.assertEquals(true, disabled);
		
		// Asignamos el contacto "Pendiente de Informar" y por tanto seguimos teniendo el botón deshabilitado
		FII_LCC_InteractionInProgress.setContactToInteraction(intInProgress.Id, con.Id);
		intInProgress = FII_LCC_InteractionInProgress.getInteractionsInProgress(acc.Id);
		disabled = FII_LCC_InteractionInProgress.disableButton(acc.Id, acc.Id, intInProgress);
		System.assertEquals(true, disabled);

		//Si el contacto ya no es el "Pendiente de Informar", entonces ya se habilita el botón
		con.LastName = 'Contacto OK';
		ContactTriggerHandler.bypassTrigger = true;
		update con;
		ContactTriggerHandler.bypassTrigger = false;
		intInProgress = FII_LCC_InteractionInProgress.getInteractionsInProgress(acc.Id);
		disabled = FII_LCC_InteractionInProgress.disableButton(acc.Id, acc.Id, intInProgress);
		System.assertEquals(false, disabled);
		Test.stopTest();
	}
	
	@isTest static void getContactRelated(){
		final Account acc = [SELECT Id, Name FROM Account WHERE NIF_CIF_Customer_NIE__c = :ACCOUNT_NIF LIMIT 1];
		final String contactsRelated = FII_LCC_InteractionInProgress.getContactRelated(acc.Id);
		System.assertNotEquals(null, contactsRelated);
	}
	 @isTest static void getTypeAndSubType(){
		Task task = new task(); 
        task.FII_ACT_FLG_SMSPending__c = true;
        task.FII_ACT_TXT_NLInfo__c='1';
        insert task;
        
		final List<String> typeAndSubType = FII_LCC_InteractionInProgress.getTypeAndSubType(task.FII_ACT_TXT_NLInfo__c);
		System.assertNotEquals(null, TypeAndSubType, '');
	}
    
    @isTest static void getTypeAndSubType2(){
		Task task = new task();
        task.FII_ACT_FLG_SMSPending__c = true;
        task.FII_ACT_TXT_NLInfo__c='80';
        insert task;
        
		final List<String> typeAndSubType = FII_LCC_InteractionInProgress.getTypeAndSubType(task.FII_ACT_TXT_NLInfo__c);
		System.assertNotEquals(null, TypeAndSubType,'');
	}
    
    @isTest static void testgetAutorizedOperations(){
        final String s = FII_LCC_InteractionInProgress.getAutorizedOperations();
		System.assertNotEquals(null, s);
    }
    
    @isTest static void testgetTitularOperations(){
        final String s = FII_LCC_InteractionInProgress.getTitularOperations();
		System.assertNotEquals(null, s);
    }
    
    @isTest static void hiddenButton_Test(){
        Test.startTest();
        Boolean marcadorView = FII_LCC_InteractionInProgress.hiddenButton();
        Test.stopTest();
        If ( marcadorView ){
			System.assert(true, 'El usuario no puede ver el marcador');           
        } else {
            System.assert(true, 'El usuario puede ver el marcador');
        }
    }
    
}