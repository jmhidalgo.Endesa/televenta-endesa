public with sharing class TV_LCC_ClassifyCallResult {
    
	@AuraEnabled
    public static WS_Info__mdt getLoginData(){
        if (Schema.sObjectType.WS_Info__mdt.isAccessible()){
            List<WS_Info__mdt> wsInfo = [SELECT DeveloperName, EndPoint__c, UserName__c, Password__c FROM WS_Info__mdt WHERE DeveloperName = 'TV_Genesys'];
            if(!wsInfo.isEmpty()){
                return wsInfo[0];
            }
        }
        return null;
    }
    
    @AuraEnabled
    public static List<TV_CallClassification__c> getPicklistData(){
		List<TV_CallClassification__c> listCallClassification = new List<TV_CallClassification__c>();
        if (Schema.sObjectType.TV_CallClassification__c.isAccessible()){
            listCallClassification =    [SELECT Id,
                                                TV_CCL_SEL_ContactType__c,
                                                TV_CCL_SEL_Aggrupation__c,
                                                TV_CCL_SEL_Typology__c,
                                                TV_CCL_TXT_Code__c,
                                                TV_CCL_FLG_NeedAdditionalChannel__c,
                                                TV_CCL_FLG_NeedAdditionalDate__c,
                                                TV_CCL_FLG_IsRecall__c,
                                                TV_CCL_FLG_IsSellOk__c
                                        FROM TV_CallClassification__c
                                        WHERE TV_CCL_FLG_IsVisible__c = true];
        }
        return listCallClassification;
    }
    
    @AuraEnabled
    public static Task getTaskData(String recordId){
        
        List<Task> listTask = new List<Task>();
        
        if (Schema.sObjectType.Task.isAccessible()){
            listTask =  [SELECT Id,
                                Status,
                                CompletedDateTime,
                                FII_ACT_LKP_Campaign__c,
                                CallObject,
                                TV_Campana_Marcador__c,
                                TV_Telefono_Marcado__c, 
                                TV_Login_Agent_Marcador__c,
                                TV_Result_Tip_Sf__c,
                                TV_Result_Tip_Mrc__c,
                                FII_ACT_TXT_CampaignMemberId__c,
                                FII_ACT_SEL_SubTipoTarea__c
                        FROM Task 
                        WHERE 
                            FII_ACT_TXT_Main_interaction__c = null AND
                            RecordType.Name ='Task Interaction' AND
                            (CompletedDateTime = NULL OR CompletedDateTime > :Datetime.now().addMinutes(-5)) AND
                            (FII_ACT_LKP_RelatedClient__c = :recordId OR TV_ACT_LKP_LeadID__c = :recordId) AND 
                            CreatedDate > :DateTime.now().addHours(-24)
                        ORDER BY CreatedDate DESC]; 
        }
        
        if(!listTask.isEmpty()){
            return listTask[0];
        }
        return null;
    }
    
    @AuraEnabled
    public static void updateSalesforceData(String taskId,
                                            String campaignMemberId,
                                    		String resultId,
                                      		String additionalInfo,
                                      		String recallAssignTo,
                                      		String recallDatetime,
                                      		String recallPhone,
                                      		String additionalDate,
                                            String additionalChannel,
                                            Boolean isSimulated){
		
        if (Schema.sObjectType.Task.isAccessible() && Schema.sObjectType.Task.isUpdateable()){
            List<Task> listTask = [SELECT Id FROM Task WHERE Id = :taskId];
            if(!listTask.isEmpty()){
                Task task = listTask[0];
                if(String.isNotBlank(resultId)){
                    task.TV_ACT_LKP_CallClassification__c = resultId;
                }
                if(String.isNotBlank(additionalInfo)){
                    task.TV_Informacion_Adicional__c = additionalInfo;
                }
                if(String.isNotBlank(recallAssignTo)){
                    task.TV_Asignarme_Reprogramacion__c = true;
                }
                if(String.isNotBlank(recallDatetime)){
                    task.TV_Fecha_hora_reprogramada__c = Datetime.valueOf(recallDatetime.replace('T',' ')).addHours(1);
                }
                if(String.isNotBlank(recallPhone)){
                    task.TV_Telefono_Alternativo__c = recallPhone;
                }   
                if(String.isNotBlank(additionalDate)){
                    task.TV_Campo_Adicional__c = additionalDate;
                }
                if(String.isNotBlank(additionalChannel)){
                    task.TV_Campo_Adicional__c = additionalChannel;
                }
                if(isSimulated){
                    task.status = 'Completed';
                }
                update task;
            }
        }
                            
                                                
        if (Schema.sObjectType.CampaignMember.isAccessible() && Schema.sObjectType.CampaignMember.isUpdateable()){
            List<CampaignMember> listCampaignMembers = [SELECT Id FROM CampaignMember WHERE Id = :campaignMemberId];
            if(!listCampaignMembers.isEmpty()){
                CampaignMember campaignMember = listCampaignMembers[0];
                if(String.isNotBlank(resultId)){
                    campaignMember.TV_CAMM_LKP_CallClassification__c = resultId;
                }
                update campaignMember;
            }
        }                               
    }
    
    @AuraEnabled
    public static void updateTaskResult(Id taskId, Boolean updatedToICWS, Boolean updatedInSF, Boolean technicalError){
        if (Schema.sObjectType.Task.isAccessible() && Schema.sObjectType.Task.isUpdateable()){
            List<Task> listTask = [SELECT Id FROM Task WHERE Id = :taskId];
            if(!listTask.isEmpty()){
                Task task = listTask[0];
                if(updatedToICWS != null){
                    task.TV_Result_Tip_Mrc__c = updatedToICWS;
                }
                if(updatedInSF != null){
                    task.TV_Result_Tip_Sf__c = updatedInSF;
                }
                if(technicalError != null){
                    task.TV_Result_Error__c = technicalError;
                }
                update task;
            }
        }
    }
    
    @AuraEnabled 
    public static Boolean validateVentaOK(Id taskId){
        Boolean validated = false;
        if(Schema.sObjectType.Task.isAccessible() && Schema.sObjectType.Case.isAccessible() && Schema.sObjectType.NE__Quote__c.isAccessible() && Schema.sObjectType.NE__OrderItem__c.isAccessible()){
            Set<Id> setCaseIds = new Set<Id>();
            List<Task> listTasks = [SELECT Id, WhatId, FII_ACT_TXT_Recruitment_interaction__c FROM Task WHERE Id = :taskId OR FII_ACT_TXT_Main_interaction__c = :taskId];
            Set<Id> setAddicionalTasksId = new Set<Id>();
            for(Task task : listTasks){
                if(String.isNotBlank(task.WhatId) && ((String)task.WhatId).startsWith('500')){
                    setCaseIds.add(task.WhatId);
                }
                if(String.isNotBlank(task.FII_ACT_TXT_Recruitment_interaction__c) && task.FII_ACT_TXT_Recruitment_interaction__c.startsWith('500')){
                    setCaseIds.add(task.FII_ACT_TXT_Recruitment_interaction__c);
                }
                if(String.isNotBlank(task.FII_ACT_TXT_Recruitment_interaction__c) && task.FII_ACT_TXT_Recruitment_interaction__c.startsWith('00T')){
                    setAddicionalTasksId.add(task.FII_ACT_TXT_Recruitment_interaction__c);
                }
            }
            
            if(!setAddicionalTasksId.isEmpty()){
                List<Task> listAddicionalTasks = [SELECT Id, WhatId, FII_ACT_TXT_Recruitment_interaction__c FROM Task WHERE Id IN :setAddicionalTasksId];
                for(Task task : listAddicionalTasks){
                    if(String.isNotBlank(task.WhatId) && ((String) task.WhatId).startsWith('500')){
                        setCaseIds.add(task.WhatId);
                    }
                    if(String.isNotBlank(task.FII_ACT_TXT_Recruitment_interaction__c) && task.FII_ACT_TXT_Recruitment_interaction__c.startsWith('500')){
                        setCaseIds.add(task.FII_ACT_TXT_Recruitment_interaction__c);
                    }
                }
            }
            
            List<Case> Caso = [SELECT Id FROM Case WHERE Id IN :setCaseIds AND Type = 'Need_03' ORDER BY createdDate DESC LIMIT 1];
            Set<String> allowedStatuses = new Set<String>{'Signature_pending', 'In Review', 'Pending_after_BO', 'Signed'};
            if(Caso.size() > 0){
                String idCaso = Caso[0].Id;
                List<id> quoteIds = new List<Id>();
                List<NE__Quote__c> quote = [SELECT Id FROM NE__Quote__c WHERE Case__c =:idCaso];
                for(NE__Quote__c q : quote){
                    quoteIds.add(q.Id);
                }
                if(quoteIds.size() > 0){
                    List<NE__OrderItem__c> configurationItems =[SELECT Id, NE__Status__c FROM NE__OrderItem__c WHERE FI_CI_LKP_Quote__c IN: quoteIds AND NE__Status__c != 'Cancelled' AND NE__Status__c != '07'];
                    if(configurationItems.size() > 0){
                        Boolean allSame = true;
                        String status = '';
                        for(NE__OrderItem__c co: configurationItems){
                            if(String.IsBlank(status)){
                            status = co.NE__Status__c;
                            }
                            if(status != co.NE__Status__c){
                                allSame = false;
                            }
                        }

                        if(allSame && allowedStatuses.contains(status)){
                            validated = true;
                        }

                    }
                }
            }
        }
    	return validated; 
    }
}