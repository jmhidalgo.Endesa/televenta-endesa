@isTest
public class FII_LCC_CustomerService_Test {

    @testSetup static void setup(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Account acc = new Account(Name = 'Autocalculado', Identifier_Type__c='NIF', FII_ACC_SEL_MarketerContracts__c='02', NIF_CIF_Customer_NIE__c ='31841666Y', FirstName__c = 'Acc', LastName__c = 'Test', Physical_or_legal_cd__c = 'Physical Customer', Account_language__c = 'Español', Contact_method_cd__c = 'SMS', MobilePhone1__c = '632145987', No_Phone_flg__c  = false, No_Email_flg__c = true);
        Account acc2 = new Account(Name = 'Endesa', Identifier_Type__c='NIF', FII_ACC_SEL_MarketerContracts__c='01', NIF_CIF_Customer_NIE__c ='35523522E', FirstName__c = 'Acc', LastName__c = 'Test', Physical_or_legal_cd__c = 'Physical Customer', Account_language__c = 'Español', Contact_method_cd__c = 'SMS', MobilePhone1__c = '632145987', No_Phone_flg__c  = false, No_Email_flg__c = true);
        Account acc3 = new Account(Name = 'Endesa', Identifier_Type__c='NIF', FII_ACC_SEL_MarketerContracts__c='03', NIF_CIF_Customer_NIE__c ='43636464J', FirstName__c = 'Acc3', LastName__c = 'Test', Physical_or_legal_cd__c = 'Physical Customer', Account_language__c = 'Inglés', Contact_method_cd__c = 'SMS', MobilePhone1__c = '632145987', No_Phone_flg__c  = false, No_Email_flg__c = true);
        
        AccountTriggerHandler.bypassTrigger = true;
        insert acc;
        insert acc2;
        insert acc3;
        AccountTriggerHandler.bypassTrigger = false;
        
        List<Contact> contacts = new List<Contact>();
        Contact cont1 = new Contact(FirstName = 'Cont', LastName = 'Tests999',Email='aaa@yaju.com', Contact_method_cd__c = 'SMS',AccountId = acc.Id, MobilePhone = '632145987', No_Phone_flg__c  = false, No_Email_flg__c  = false, Document_Type__c ='NIF', NIF_CIF_Customer_NIE__c ='73009311C', Language__c='Español');
        Contact cont2 = new Contact(FirstName = 'Cont', LastName = 'Tests888',Email='bbb@yaju.com', Contact_method_cd__c = 'SMS',AccountId = acc.Id, MobilePhone = '632145986', No_Phone_flg__c  = false, No_Email_flg__c  = false, Document_Type__c ='NIF', NIF_CIF_Customer_NIE__c ='25226402W', Language__c='Español');
        Contact cont3 = new Contact(FirstName = 'Cont', LastName = 'Tests777',Email='ccc@yaju.com', Contact_method_cd__c = 'SMS',AccountId = acc.Id, MobilePhone = '632145985', No_Phone_flg__c  = false, No_Email_flg__c  = false, Document_Type__c ='NIF', NIF_CIF_Customer_NIE__c ='31910068Y', Language__c='Catalán');
        Contact cont4 = new Contact(FirstName = 'Cont', LastName = 'Tests666',Email='ddd@yaju.com', Contact_method_cd__c = 'SMS',AccountId = acc2.Id, MobilePhone = '632145987', No_Phone_flg__c  = false, No_Email_flg__c  = false, Document_Type__c ='NIF', NIF_CIF_Customer_NIE__c ='70823317S', Language__c='Español');
        Contact cont5 = new Contact(FirstName = 'Cont5', LastName = 'Tests555',Email='eee@yaju.com', Contact_method_cd__c = 'SMS',AccountId = acc2.Id, MobilePhone = '632145987', No_Phone_flg__c  = false, No_Email_flg__c  = false, Document_Type__c ='NIF', NIF_CIF_Customer_NIE__c ='25272244M', Language__c='Catalán');
        Contact cont6 = new Contact(FirstName = 'Cont', LastName = 'Tests444',Email='fff@yaju.com', Contact_method_cd__c = 'SMS',AccountId = acc2.Id, MobilePhone = '632145987', No_Phone_flg__c  = false, No_Email_flg__c  = false, Document_Type__c ='NIF', NIF_CIF_Customer_NIE__c ='Y7971841D', Language__c='Catalán');
        
        contacts.add(cont1);
        contacts.add(cont2);
        contacts.add(cont3);
        contacts.add(cont4);
        contacts.add(cont5);
        contacts.add(cont6);
        
		ContactTriggerHandler.bypassTrigger = false;
        insert contacts;
		ContactTriggerHandler.bypassTrigger = false;
        
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true,
            B2C_DocumentType__c = '01',
            B2C_DocumentTypeSF__c = 'CIE');
        insert contentVersion;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 2];

        //create ContentDocumentLink  record
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = cont1.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'I';
        insert cdl;
        //create Templates
        List<APXTConga4__Conga_Template__c> lista = new List<APXTConga4__Conga_Template__c>();
        APXTConga4__Conga_Template__c spa = new APXTConga4__Conga_Template__c();
            spa.APXTConga4__Name__c='ES_ML_Consentimiento_RGPD';
        APXTConga4__Conga_Template__c spaXXI = new APXTConga4__Conga_Template__c();
            spaXXI.APXTConga4__Name__c='ES_MR_Consentimiento_RGPD';
        APXTConga4__Conga_Template__c cat = new APXTConga4__Conga_Template__c();
            cat.APXTConga4__Name__c='CA_ML_Consentimiento_RGPD';
        APXTConga4__Conga_Template__c eng = new APXTConga4__Conga_Template__c();
            eng.APXTConga4__Name__c='EN_ML_Consentimiento_RGPD';
        APXTConga4__Conga_Template__c endesa = new APXTConga4__Conga_Template__c();
            endesa.APXTConga4__Name__c='CA_MR_Consentimiento_RGPD';
        APXTConga4__Conga_Template__c xxi = new APXTConga4__Conga_Template__c();
            xxi.APXTConga4__Name__c='EN_MR_Consentimiento_RGPD';
        lista.add(spa);
        lista.add(spaXXI);
        lista.add(cat);
        lista.add(eng);
        lista.add(endesa);
        lista.add(xxi);
        insert lista;
        
    }
    
    @isTest static void standardBehaviorGSM() {
        Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='31841666Y'];
        Contact con = [Select Id from Contact where LastName = 'Tests999'];
        Task interaction = FII_Util_GSM.createInteraction(acc.Id, con.Id, null, 'Phone', 'interaction');
        Test.startTest();
            insert interaction;
            FII_LCC_CustomerService.standardBehaviorGSM(acc.Id, null, null, 'Test', 'TestAct', null, 'Completed', Label.FII_RT_TSK_ACTION);
        Test.stopTest();
    }
    
    @isTest static void getAccountOrLead() {
        Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='31841666Y'];
        Contact con = [Select Id from Contact where LastName = 'Tests999'];
        Task interaction = FII_Util_GSM.createInteraction(acc.Id, con.Id, null, 'Phone', 'interaction');
        insert interaction;
        Case need = FII_Util_GSM.createNeed(acc.Id, null, 'Test', null);
        insert need;
        Test.startTest();
        String res = FII_LCC_CustomerService.getAccountOrLead(need.Id);
        System.assertEquals(acc.Id, res);
        Test.stopTest();
    }
    
    @isTest static void itTestCreateNeed() {
        Account objAccount = [Select Id from Account where NIF_CIF_Customer_NIE__c ='31841666Y'];
        Test.startTest();
        Case objCase = FII_LCC_CustomerService.createNeed(objAccount.Id);
        // Las necesidades se crean sin tipo y se va modificando en función de sus actuaciones
        //System.assertNotEquals(null, objCase.Type);
														 
		 
        Test.stopTest();
    }

    @isTest static void getUrlGestionEC(){
        Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='31841666Y'];
        Contact cont = [Select Id from Contact where LastName = 'Tests999'];
        RecordType needRT = [Select Id from RecordType where DeveloperName = 'FII_RT_CAS_Need'];
        Security__mdt sec_params = [SELECT  IV_NEOL__c, KEY_NEOL__c, TEST_NEOL__c FROM Security__mdt WHERE DeveloperName='Security_NEOL'];
        //sec_params.TEST_NEOL__c=false;
        //update sec_params;        
        Test.startTest();
        FII_LCC_CustomerService.getUrlGestionEC(cont.id);
        Test.stopTest();
    }

    @isTest static void disabledButton() {
    Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='31841666Y'];
        Contact con = [Select Id from Contact where LastName = 'Tests999'];
        Task interaction = FII_Util_GSM.createInteraction(acc.Id, con.Id, null, 'Phone', 'interaction');
        insert interaction;
    Test.startTest();
    Task itInCourse = FII_Util_GSM.getInteractionInProgress(acc.Id);
    Boolean disabled = true;
    disabled = FII_LCC_CustomerService.disableButton(acc.Id, acc.Id, itInCourse);
    System.assertEquals(false, disabled);
    Test.stopTest();
  }
    
    @isTest static void getInteraction_Test() {
    Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='31841666Y'];
    Test.startTest();
        List<String> interact = new List<String>(2);
    interact = FII_LCC_CustomerService.getInteraction(acc.Id);
    Test.stopTest();
  }
    @isTest static void getClient(){
        Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='31841666Y'];
        Test.startTest();
        Account cuenta = FII_LCC_CustomerService.getClient(acc.id);
        //System.assertEquals('Autocalculado', cuenta.Name);
        Test.stopTest();
    }
    
    @isTest static void getCongaURL(){
        Contact con = [Select Id from Contact where LastName = 'Tests999'];
        Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
      insert  ind;
        Test.startTest();
        String congaURL = FII_LCC_CustomerService.getCongaURL(con.id, false); // comprobar el boolean
        Test.stopTest();
    }
    @isTest static void getCongaURLAcuse(){
        Contact con = [Select Id from Contact where LastName = 'Tests555'];
        Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
      insert  ind;
        Test.startTest();
        String congaURL = FII_LCC_CustomerService.getCongaURL(con.id, true); 
        Test.stopTest();
    }
    
    @isTest static void getLastFileBase64(){
        Contact con = [Select Id from Contact where LastName = 'Tests999'];
        Test.startTest();
        String lastFileBase64 = FII_LCC_CustomerService.getLastFileBase64(con.id, false);
        System.assertNotEquals(null, lastFileBase64);  //VGVzdCBDb250ZW50
        Test.stopTest();
    }

    @isTest static void spanishTemplate(){
        Contact con = [Select Id from Contact where LastName = 'Tests999'];
        Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
      insert  ind;
        
        Test.startTest();
        ind.FII_IND_SEL_Molestar__c='No Ofertas';
        update ind;
        String spain = FII_LCC_CustomerService.getCongaURL(con.id, false); // comprobar el boolean
        System.AssertNotEquals(spain, null);
        Test.stopTest();
    }

    @isTest static void englishTemplate(){
        Contact con = [Select Id from Contact where LastName = 'Tests888'];
        Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
      insert  ind;
        
        Test.startTest();
        ind.FII_IND_SEL_Molestar__c='No Ofertas';
        update ind;
        
        String english = FII_LCC_CustomerService.getCongaURL(con.id, false); // comprobar el boolean
        System.AssertNotEquals(english, null);
        Test.stopTest();
    }

    @isTest static void catTemplate(){
        Contact con = [Select Id from Contact where LastName = 'Tests777'];
        Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
      insert  ind;
        
        Test.startTest();
        ind.FII_IND_SEL_Molestar__c='No Ofertas';
        update ind;
        String catalan = FII_LCC_CustomerService.getCongaURL(con.id, false); 
        System.AssertNotEquals(catalan, null);
        Test.stopTest();
    }

    @isTest static void xxiTemplate(){
        Contact con = [Select Id from Contact where LastName = 'Tests444'];
        Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
      insert  ind;
        
        Test.startTest();
        ind.FII_IND_SEL_Molestar__c='No Ofertas';
        update ind;
        String catalan = FII_LCC_CustomerService.getCongaURL(con.id, true); 
        System.AssertNotEquals(catalan, null);
        Test.stopTest();
    }

    @isTest static void endesaTemplate(){
        Contact con = [Select Id from Contact where LastName = 'Tests666'];
        Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
        insert  ind;
        
        Test.startTest();
        ind.FII_IND_SEL_Molestar__c='No Ofertas';
        update ind;
        String catalan = FII_LCC_CustomerService.getCongaURL(con.id, true); 
        System.AssertNotEquals(catalan, null);
        Test.stopTest();
    }

    @isTest static void isTheEventForMe(){
        Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='31841666Y'];
        Contact con = [Select Id from Contact where LastName = 'Tests999'];
        Task interaction = FII_Util_GSM.createInteraction(acc.Id, con.Id, null, 'Phone', 'interaction');
        interaction.FII_ACT_LKP_RelatedClient__c = acc.Id;
        insert interaction;
        Test.startTest();
        FII_LCC_CustomerService.isTheEventForMe(con.id,acc.id,interaction);
        Test.stopTest();
    }
    
    @isTest static void deleteNeed() {
        Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='31841666Y'];
        Case casos = FII_Util_GSM.createNeed(acc.Id, null, 'Test Casos', null);
        insert casos;
        Case need = FII_Util_GSM.createNeed(acc.Id, null, 'Test Need', null);
        insert need;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument LIMIT 2];
        List<id> listDocumentsId = new list<id>();
        listDocumentsId.add(documents[0].id);
        Boolean isDocument = false;
                
        Test.startTest();
        String res = FII_LCC_CustomerService.deleteNeed(need.Id, listDocumentsId, isDocument, casos.id);
        System.assertEquals('OK', res);
        Test.stopTest();
    }
    
    @isTest static void caseAssign() {
        Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='43636464J'];
        Case casos = FII_Util_GSM.createNeed(acc.Id, null, 'TestCaseAssign', null);
        insert casos;
        List<String> listCaseId = new list<String>();
        listCaseId.add(''+casos.id);
        
        Test.startTest();
        FII_LCC_CustomerService.caseAssign(listCaseId);
        Test.stopTest();
    }
    
    @isTest static void getIdCase() {
        Account acc = [Select Id from Account where NIF_CIF_Customer_NIE__c ='43636464J'];
        String accountId = acc.id;
        Case need = FII_Util_GSM.createNeed(acc.Id, null, 'TestCaseAssign', null);
        insert need;
              
        Test.startTest();
        id res = FII_LCC_CustomerService.getIdCase(accountId, need.id);
        system.assertEquals(need.id, res);
        Test.stopTest();
     }

	//21/01/2020
    @isTest
    private static void getProfileUser_Test (){
        Test.startTest();
        String result = FII_LCC_CustomerService.getProfileUser();
        System.assertNotEquals(null, result);
        Test.stopTest();
        
    } 
    @isTest
    private static void getIsContractAllowed_Test (){
        Account acc = [SELECT Id FROM Account WHERE NIF_CIF_Customer_NIE__c ='43636464J'];
        Contact cont1 = [SELECT Id FROM Contact WHERE Email='aaa@yaju.com' LIMIT 1];
        acc.TitularContactHide__c = cont1.Id;
        update acc;
        Individual ind = new Individual(contact__c = cont1.Id,LastName = 'Test3',FII_IND_SEL_Molestar__c = 'Sí Ofertas');
        insert ind;
        AccountContactRelation relation = new AccountContactRelation(ContactId=cont1.Id,AccountId=acc.Id,roles='Autorizado'); 
        insert relation;
        Task interaction = FII_Util_GSM.createInteraction(acc.Id, cont1.Id, null, 'Phone', 'interaction');
        insert interaction;
        system.debug('indList test: '+ind);
        system.debug('interaction test: '+interaction);
        String accountId = acc.id;
        Test.startTest();
        String result = FII_LCC_CustomerService.getIsContractAllowed(accountId,interaction.Id);
        FII_LCC_CustomerService.getInteraction(acc.Id);
        System.assertEquals('OK', result);
        Test.stopTest();
        
    } 		
    
}