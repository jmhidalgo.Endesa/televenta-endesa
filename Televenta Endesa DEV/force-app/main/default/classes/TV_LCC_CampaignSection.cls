/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-21-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-21-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class TV_LCC_CampaignSection {
    
    @AuraEnabled
    public static DataWrapper getDataDos(  string campaignMemberId, string recordId, string url ){
        
        
        DataWrapper 				  dataWrapper = new DataWrapper();
        List<Task>					  taskList;
        List<Campaign> 				  CampaignList;
        List<CampaignMember>          campaignMemberList;
        List<B2C_GestionaAudience__c> gestionaAudienceList;
        List<B2C_Service_Point__c>	  servicePointList;
        List<Lead> 					  leadList;
        List<ContentDocumentLink>	  contentDocumentLinkList;
        List<ContentVersion> 		  contentVersionList;
        
        string campaignMemberIdARP = campaignMemberId;
               
        If( campaignMemberIdARP == null ){
            campaignMemberIdARP = getCampaignMember( recordId );
        }
        
        if (Schema.sObjectType.CampaignMember.isAccessible()) {
        
        
        	campaignMemberList = [ SELECT Id, ContactId, CampaignId, FII_CAMM_LKP_AccountMember__c, TV_Motivo_CI_Pendiente_Recuperacion__c,
                                                       		   LeadId, LeadOrContactId, PS1__c, TV_CI_Pendiente_Recuperacion__c,TV_LKP_Caso_Control_Calidad__c,
                                                               TV_LKP_Caso_Control_Calidad__r.caseNumber, TV_CI_Pdte_Recuperacion__c 
                                                    	FROM   CampaignMember 
                                                   		WHERE  Id = :campaignMemberIdARP ];
        
            If(  !( campaignMemberList.isEmpty() ) ){
                
                Id recId = recordId;

                If ( String.valueOf(recId.getSobjectType()) == 'Account' ){
                    dataWrapper.tipoInterlocutor = TV_CLS_Utils.obtenerTipoInterlocutor( recId );
                } Else If ( String.valueOf(recId.getSobjectType()) == 'Lead' ){
                    dataWrapper.tipoInterlocutor = 'No Cliente';
                }

                if (Schema.sObjectType.Task.isAccessible()) {
                	
                    taskList = [ SELECT Id, TV_ACT_FOR_ResultType__c, TV_Informacion_Adicional__c
                                 FROM   Task 
                                 WHERE  Status = 'Completed'
                                 AND    TV_Campana_Marcador__c != null
                                 AND 	FII_ACT_TXT_CampaignMemberId__c = :campaignMemberList[0].Id
                                 AND    RecordType.Name ='Task Interaction'
                                 ORDER BY CreatedDate DESC 
                                 LIMIT 1 ];
                    
                    If(  !( taskList.isEmpty() ) ){                  
                        dataWrapper.interation = taskList[0];
                    }
                    
                }
                
                 dataWrapper.callInProgressBool = true;
                    
                 If( campaignMemberList[0].CampaignId != null ){
                        
                        dataWrapper.campaignMember = campaignMemberList[0];
                        
                        if (Schema.sObjectType.Campaign.isAccessible()) {

                        	campaignList = [ SELECT Id, Name, Campaigndescription__c, TV_Lead_Origin__c, TV_Partner__c, 
                                        		TV_Colectivo__c,Audiencedescription__c, Campaignsubtype__c, 
                                        		FII_CAM_SEL_Subtype_2__c, TV_URL_Download__c
                                         	FROM 	Campaign
                                         	WHERE  Id = :campaignMemberList[0].CampaignId ];
                        
                          If(  ( !( campaignList.isEmpty() ) ) ){ 
                            dataWrapper.campaign = campaignList[0];
                                
                              	if (Schema.sObjectType.B2C_Service_Point__c.isAccessible()) {
                                    
                                    servicePointList = [ SELECT Id, Adress__c
                                                         FROM B2C_Service_Point__c
                                                         WHERE NAME = :campaignMemberList[0].PS1__c ];
                                    
                                    If( !servicePointList.isEmpty() ){
                                        
                                        dataWrapper.servicePoint = servicePointList[0];
                                        
                                    }
                                 }
                            
                            if (Schema.sObjectType.Lead.isAccessible()) {

                                leadList = [ SELECT Id, TV_Agent__c, TV_Provider_FV__c, TV_Location__c, Description
                                             FROM   Lead 
                                             WHERE  Id = :campaignMemberList[0].LeadId ];
                                
                                If( !leadList.isEmpty() ){
                                    
                                    dataWrapper.lead = leadList[0];                        
                                    dataWrapper.showLeadData = true;
                                    
                                } Else {
                                    
                                    dataWrapper.showLeadData = false;
                                    
                                }
                            }
                              
                            if (Schema.sObjectType.ContentDocumentLink.isAccessible()) {
                                
                            	contentDocumentLinkList = [ SELECT Id, LinkedEntityId, ContentDocumentId
 															FROM   ContentDocumentLink
															WHERE  LinkedEntityId = :campaignList[0].Id ];
                            
                                If( !contentDocumentLinkList.isEmpty() ){
    
                                    Set<Id> ids = new Set<Id>();
                                    For(ContentDocumentLink contentDocumentLinkItem: contentDocumentLinkList){
                                        ids.add(contentDocumentLinkItem.ContentDocumentId);
                                    }
                                    
                                    if (Schema.sObjectType.ContentVersion.isAccessible()) {
                                        
                                        contentVersionList =[ SELECT Id, ContentDocumentId, FII_CV_SEL_DocumentType__c
                                                                FROM ContentVersion
                                                                WHERE (ContentDocumentId In :ids) and (FII_CV_SEL_DocumentType__c ='Argumentario TV')
                                                                Order By CreatedDate Desc
                                                                Limit 1
                                                              ];
                                        If( !contentVersionList.isEmpty() ){                            
                                            dataWrapper.urlDocument = url.substring( 0, url.indexOf('.com/')+4 )+'/sfc/servlet.shepherd/document/download/'+contentVersionList[0].ContentDocumentId+'?operationContext=S1';
                                        }
                                    }
                                }
                            }
                        }
                      }
                    }
               
          } Else {
                
               dataWrapper.callInProgressBool = false; 
               dataWrapper.showLeadData = false;
          	}
    	}        

        
        dataWrapper.tooltipClientType = 'Cliente normal sin ninguna información adicional';
        
        return dataWrapper;
    }
    
    public class DataWrapper {
        @AuraEnabled
        public Campaign campaign;
        @AuraEnabled
        public CampaignMember campaignMember;
        @AuraEnabled
        public Lead lead;
        @AuraEnabled
        public Account account;
        @AuraEnabled
        public B2C_GestionaAudience__c gestionaAudience;
        @AuraEnabled
        public B2C_Service_Point__c servicePoint;
        @AuraEnabled
        public Task interation;
        @AuraEnabled
        public String tooltipClientType;
        @AuraEnabled
        public Boolean showLeadData;
        @AuraEnabled
        public Boolean callInProgressBool;
        @AuraEnabled
        public String tipoInterlocutor;
        @AuraEnabled
        public String urlDocument;
    }
    
    @AuraEnabled
    public static string getCampaignMember(Id recId){
        final String contactLastName = 'Pendiente de Informar';
        final String contactFirstName = 'Contacto';
        List<Contact> dummyContact;
        Task openedInteraction = FII_Util_GSM.getInteractionInProgress(recId);
        if (openedInteraction != null) {
            openedInteraction = (Task) FII_Util_GSM.mapApiNamePicklist(new List<Task>{openedInteraction})[0];
            
            if (openedInteraction.FII_ACT_TXT_Main_interaction__c == null){
                if (Schema.sObjectType.Contact.isAccessible()) {
                    dummyContact = [SELECT Id, FirstName, LastName FROM Contact WHERE FirstName =: contactFirstName AND LastName =: contactLastName LIMIT 1];
                    
                    if (!dummyContact.isEmpty() && openedInteraction.WhoId == dummyContact[0].Id) {
                        openedInteraction.WhoId = null;
                    }
                }
            }
            return openedInteraction.FII_ACT_TXT_CampaignMemberId__c;
        }
        else{
            return null;
        }
		
    }
    
}