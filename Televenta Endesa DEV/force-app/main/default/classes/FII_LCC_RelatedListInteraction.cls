/**
 * @author Everis
 * @since December 10, 2018 
 * @desc Class that connects the RelatedListInteraction component with Salesforce records
 * @history November 10, 2018 - everis (JJM) - Create Apex Class 
 *          January 23, 2019 - everis (LHZ) - Modify Apex, Optimization the queries
 *          March 08, 2019 - everis (JVC) - Refact using FII_Util_GSM
 */

public class FII_LCC_RelatedListInteraction {
	/** 
	 * @desc Searchs for the related main interaction tasks
	 * 
	 * @param needId: Id of the record shown in the page
	 * @param sobjectType: Account, Contact, Need, or Action
	 * @return the related main interaction tasks
	 */
	@AuraEnabled
	public static List<Task> getInteractionsRelatedList(Id recId){
		List<Task> interactions = FII_Util_GSM.consultRelatedInteractions(recId);
		interactions = FII_Util_GSM.mapApiNamePicklist(interactions);
		return interactions;

	}
	
	@AuraEnabled
	public static sObject getRecordPrimary(Id recId){
		return FII_Util_GSM.consultRecordPrimary(recId);
	}

	/*
	*============================================================================================================================================
	* @methodName               isTheEventForMe
	* @received parameters      Id myRecId, Id eventRecId, Task interactionInProgress
	* @return                   Boolean FII_Util_GSM.isTheEventForMe(myRecId, eventRecId, interactionInProgress)
	*============================================================================================================================================
	*/
	// CAMBIOS PARA GSM
	@AuraEnabled 
	public static Boolean isTheEventForMe (Id myRecId, Id eventRecId, Task oldInteraction){
		return FII_Util_GSM.isTheEventForMe(myRecId, eventRecId, oldInteraction);
	}

	/*
	*============================================================================================================================================
	* @methodName               isTeleventUser 
	* @return                   Boolean TV_CLS_Utils.getIsTeleventaProfile()
	*============================================================================================================================================
	*/
	@AuraEnabled 
	public static Boolean isTeleventUser(){

		return TV_CLS_Utils.getIsTeleventaProfile();
	}
    
    @AuraEnabled 
    public static Boolean hiddenButton(){
        
        List<User> userList;
        
        if(Schema.sObjectType.User.isAccessible()){
            userList = [ SELECT Id, CallCenterId 
                         FROM   User 
                         where id=: UserInfo.getUserId() ];
        }
        List<CallCenter> callCenterList;
        If( !userList.isEmpty() ){
            if(Schema.sObjectType.CallCenter.isAccessible()){
                callCenterList =  [ SELECT Id, name 
                                    FROM   CallCenter 
                                    WHERE  id = :userList[0].CallCenterId 
                                    AND    name = 'PureConnect for Salesforce Lightning'];
            }
        }
        return callCenterList.isEmpty();
	}

	
    
}