@isTest
public class FII_LCC_RelatedListAddress_Test {
    
    private static Account acc;
    private static B2C_Address__c direccion;
    private static B2C_Address__c direccion2;
    private static B2C_Billing_Profile__c bill;
    private static B2C_Service_Point__c serv;
    
    @TestSetup
    static void setup(){
        
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.CRM_OSCLI003Mock()); 
        Test.setMock(WebServiceMock.class, new UtilSoapMock.TBCGLI003Mock());
        Test.setMock(WebServiceMock.class, new UtilSoapMock.OSCLI003Mock());
        
        acc = UtilTest.createAccount();
        acc.NIF_CIF_Customer_NIE__c = '52620026M';
        insert acc;
        
        direccion = UtilTest.createAddress();
        direccion.Account__c = acc.Id;
        insert direccion;

        acc.Addr_id__c=direccion.id;
        update acc;
        
        direccion2 = UtilTest.createAddress();
        direccion2.Account__c = acc.Id;
        direccion2.DIR_external_CRM_id__c = '124';
        direccion2.City__c = 'TEST';
        insert direccion2;
        
        /*bill = UtilTest.createBillingProfile();
        bill.Adress__c = direccion.Id;
        insert bill;*/
        
    }
    
    private static void init () {
        acc = [SELECT Id from account limit 1];
        direccion = [SELECT Id, Name, Country_desc__c, Deprecated__c from B2C_Address__c WHERE Account__c = :acc.Id AND City__c != 'TEST'];
        direccion2 = [SELECT Id, Name, Country_desc__c, Deprecated__c from B2C_Address__c WHERE City__c = 'TEST'];
    }
    
    @isTest  
    static void returnListAddress_True_Test(){
        Test.startTest();
        init();
        List<B2C_Address__c> resp = FII_LCC_RelatedListAddress.returnListAddress(acc.Id, true);
        Test.stopTest();
    }
    
   @isTest 
    static void returnListAddress_False_Test(){
        Test.startTest();
        init();
        List<B2C_Address__c> resp = FII_LCC_RelatedListAddress.returnListAddress(acc.Id, false);
        Test.stopTest();
    }
    
    @isTest
    static void returnMainAddress_Test(){
        Test.startTest();
        init();
        FII_LCC_RelatedListAddress.returnMainAddress(direccion.id);
        Test.stopTest();

    }

    @isTest 
    static void deleteAddress_If_Test(){
        Test.startTest();
        init();
        String resp = FII_LCC_RelatedListAddress.deleteAddress(direccion.id);
        Test.stopTest();
    }
    
    @isTest 
    static void deleteAddress_Else_Test(){
        Test.startTest();
        init();
        String resp = FII_LCC_RelatedListAddress.deleteAddress(direccion2.id);
        Test.stopTest();
    }

    @isTest
    static void selectMainAddress(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        init();
        String resp = FII_LCC_RelatedListAddress.mainAddressApex(acc.id, direccion2.id);
        Test.stopTest();
    }

    @isTest
    static void selectMainAddressNull(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        init();
        String resp = FII_LCC_RelatedListAddress.mainAddressApex(acc.id, null);
        Test.stopTest();
    }

    @isTest
    static void standardGSM(){
        Test.startTest();
        init();
        Case cas = FII_LCC_RelatedListAddress.standardBehaviorGSM(acc.id, 'Atencion al cliente', null, null, null, null, 'Open', Label.FII_RT_TSK_ACTION);
        Test.stopTest();
    }
    
}