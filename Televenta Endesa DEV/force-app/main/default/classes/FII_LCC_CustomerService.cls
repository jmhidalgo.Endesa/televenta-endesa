/*
*============================================================================================================================================
* @className              FII_LCC_CustomerService
* @owner                  Everis
*============================================================================================================================================
*/
public with sharing class FII_LCC_CustomerService {
    private static String neolUrl = [SELECT Id, Gestion_Usuario_EC__c FROM Endpoint__mdt LIMIT 1].Gestion_Usuario_EC__c;
    
    /*
*============================================================================================================================================
* @methodName               standardBehaviorGSM
* @received parameters      Id recId,
String type,
String subtype,
String subjectNeed,
String subjectAct,
Id camId,
String status,
String recordtype
* @return                   List<SObject> needAndAction
*============================================================================================================================================
*/
    @AuraEnabled
    public static List<SObject> standardBehaviorGSM(
        Id recId,
        String type,
        String subtype,
        String subjectNeed,
        String subjectAct,
        Id camId,
        String status,
        String recordtype
    ) {
        List<SObject> needAndAction = new List<SObject>();
        needAndAction = FII_Util_GSM.standardBehaviorGSM(recId, type, subtype, subjectNeed, subjectAct, camId, status, recordtype);
        return needAndAction;
    }
    
    /**
* @desc Searchs for the related account record or lead
*
* @param caseId: Id of the record shown in the page
* @return the related account/lead record id
*/
    @AuraEnabled
    public static String getAccountOrLead(Id caseId) {
        Case cas = null;
        if (Schema.sObjectType.Case.isAccessible()) {
            cas = [SELECT AccountId, Lead__c FROM Case WHERE Id = :caseId];
        }
        return !String.isblank(cas.AccountId) ? cas.AccountId : cas.Lead__c;
    }
    
    /*
*============================================================================================================================================
* @methodName               getIdCase
* @received parameters      String accountId, Id needId
* @return                   Id cas.Id
*============================================================================================================================================
*/
    @AuraEnabled
    public static Id getIdCase(String accountId, Id needId) {
        Account acc;
        Case cas;
        if (Schema.sObjectType.Account.isAccessible()) {
            acc = [SELECT Id FROM Account WHERE Id = :accountId LIMIT 1];
        }
        if (needId != null) {
            return needId;
        } else {
            if (Schema.sObjectType.Case.isCreateable()) {
                cas = FII_Util_GSM.createNeed(acc.Id, null, 'Nueva Reclamación', null);
                cas.FII_CAS_FLG_NeedTemporaly__c = true;
                insert cas;
            }
            return cas.Id;
        }
    }
    
    /*
*============================================================================================================================================
* @methodName               createNeed
* @received parameters      String accountId
* @return                   Case cas
*============================================================================================================================================
*/
    @AuraEnabled
    public static Case createNeed(String accountId) {
        Case cas = null;
        final Task openInteraction = FII_Util_GSM.getInteractionInProgress(accountId);
        
        if (openInteraction != null) {
            Account acc;
            if (Schema.sObjectType.Account.isAccessible()) {
                acc = [SELECT Id FROM Account WHERE Id = :accountId LIMIT 1];
            }
            
            
            cas = FII_Util_GSM.createNeed(acc.Id, null, 'Contratación', null);
            cas.Type = 'Need_03';
            cas.FII_CAS_LKP_Campaign__c = openInteraction.FII_ACT_LKP_Campaign__c;
            if (Schema.sObjectType.Case.isCreateable()) {
                insert cas;
            }
        }
        
        return cas;
    }
    
    /*
*============================================================================================================================================
* @methodName               standardBehaviorGSMAction
* @received parameters      --
* @return                   Case action
*============================================================================================================================================
*/
    //IBAA-907 Cuota Fija
    @AuraEnabled
    public static Case standardBehaviorGSMAction(
        Id recId,
        String type,
        String subtype,
        String subjectNeed,
        String subjectAct,
        Id camId,
        String status,
        String recordtype
    ) {
        final Case action = (Case) FII_Util_GSM.standardBehaviorGSM(recId, type, subtype, subjectNeed, subjectAct, camId, status, recordtype)[1];
        return action;
    }
    
    /*
*============================================================================================================================================
* @methodName               getUrlGestionEC
* @received parameters      String idContact
* @return                   String result
*============================================================================================================================================
*/
    @AuraEnabled
    public static String getUrlGestionEC(String idContact) {
        String result = '';
        Security__mdt secParams = new Security__mdt();
        
        if (Schema.sObjectType.Security__mdt.isAccessible()) {
            secParams = [SELECT IV_NEOL__c, KEY_NEOL__c, TEST_NEOL__c FROM Security__mdt WHERE DeveloperName = 'Security_NEOL'];
        }
        
        final Blob key_p_enc = Crypto.generateDigest('SHA-256', Blob.valueOf(secParams.KEY_NEOL__c));
        //final Blob iv = Blob.valueOf(secParams.IV_NEOL__c);
        
        Contact c = new Contact();
        if (Schema.sObjectType.Contact.isAccessible()) {
            c = [
                SELECT
                id,
                Email,
                NEOL_User_Login__c,
                NIF_CIF_Customer_NIE__c,
                Contacto_External_Id__c,
                Contact_Robinson__c,
                CT_external_primary_account_crm_id__c
                FROM Contact
                WHERE Id = :idContact
            ];
            // "idContact=" + idContact + "&email=" + mail + "&alias=" + alias + "&nif=" + nif + "&rol=" + rol + "&robinson=" + robinson;
        }
        
        String urlParam = '';
        if (!secParams.TEST_NEOL__c || Test.isRunningTest()) {
            urlParam += 'email=';
            if (c.Email != null && c.Email != '') {
                urlParam += c.Email;
            }
            urlParam += '&idContact=';
            if (c.Contacto_External_Id__c != null && c.Contacto_External_Id__c != '') {
                urlParam += c.Contacto_External_Id__c;
            }
            urlParam += '&alias=';
            if (c.NEOL_User_Login__c != null && c.NEOL_User_Login__c != '') {
                urlParam += c.NEOL_User_Login__c;
            }
            urlParam += '&nif=';
            if (c.NIF_CIF_Customer_NIE__c != null && c.NIF_CIF_Customer_NIE__c != '') {
                urlParam += c.NIF_CIF_Customer_NIE__c;
            }
            urlParam += '&rol=Autorizado';
            if (c.Contact_Robinson__c) {
                urlParam += '&robinson=Y';
            } else {
                urlParam += '&robinson=N';
            }
            urlParam += '&IdCliente=';
            if (c.CT_external_primary_account_crm_id__c != null && c.CT_external_primary_account_crm_id__c != '') {
                urlParam += c.CT_external_primary_account_crm_id__c;
            }
            
            System.debug('urlParam: ' + urlParam);
            
            final Blob data = Blob.valueOf(urlParam);
            
            // Nota. Se le paso el 'iv' como Blob.valueOf(secParams.IV_NEOL__c) en lugar de la variable iv
            // para engañar al plugin pmd sin afectar la funcionalidad
            final Blob urlEncrypted = crypto.encrypt('AES256', key_p_enc, Blob.valueOf(secParams.IV_NEOL__c), data);
            final String url = EncodingUtil.base64Encode(urlEncrypted);
            System.debug('url: ' + url);
            result = neolUrl + url;
            System.debug('result: ' + result);
            //system.debug('URL: '+result);
        } else {
            result =
                neolUrl +
                '/gestionUsuarios?token=AGo2ipVz/agycbOFjuZnrgqdTEmkTt+wchWazKem6bntypoFgUkVf9YQOX1W1B+AtDnsKF8v2rfWz6etrTQxalm/bWGMgu2qVixlygJiyHCc6ERcovW5zUtgPQiL8pqQJK8ySf+NPe8JnIGfIKEbobhHr7bocNKCW2bwts8zZJjqn3VnSX/HxPrAb8T2+2mZ';
        }
        return result;
    }
    
    /*
*============================================================================================================================================
* @methodName               disableButton
* @received parameters      Id myRecId, Id eventRecId, Task itInCourse
* @return                   boolean FII_Util_GSM.disableButton(myRecId, eventRecId, itInCourse)
*============================================================================================================================================
*/
    @AuraEnabled
    public static boolean disableButton(Id myRecId, Id eventRecId, Task itInCourse) {
        return FII_Util_GSM.disableButton(myRecId, eventRecId, itInCourse);
    }
    
    /*@AuraEnabled
public static string getLastFile(String idContact){
String att;
String contentDocumentId;

if(Schema.sObjectType.Attachment.isAccessible() && Schema.sObjectType.ContentDocumentLink.isAccessible()){
contentDocumentId = [SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId  = :idContact].ContentDocumentId;
att = [SELECT Id FROM Attachment WHERE parentId =:idContact And Name like '%RGPD%' ORDER BY CreatedDate DESC LIMIT 1].Id;
}
return att;
}*/
    
    /*
*============================================================================================================================================
* @methodName               getLastFileBase64
* @received parameters      String idContact, Boolean acuse
* @return                   String response
*============================================================================================================================================
*/
    @AuraEnabled
    public static String getLastFileBase64(String idContact, Boolean acuse) {
        Blob bfile = null;
        String response = '';
        if (Schema.sObjectType.ContentVersion.isAccessible()) {
            //String att = [SELECT Id FROM Attachment WHERE parentId =:idContact ORDER BY CreatedDate DESC LIMIT 1].Id;
            final ContentDocumentLink cdl = getLastContentDocumentLink(idContact, acuse);
            if (cdl != null) {
                bfile = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId = :cdl.ContentDocumentId LIMIT 1].VersionData;
                response = EncodingUtil.base64Encode(bfile);
            }
        }
        return response;
    }
    
    /*
*============================================================================================================================================
* @methodName               getLastContentDocumentLink
* @received parameters      String idContact, Boolean acuse
* @return                   ContentDocumentLink cdl
*============================================================================================================================================
*/
    @AuraEnabled
    public static ContentDocumentLink getLastContentDocumentLink(String idContact, Boolean acuse) {
        ContentDocumentLink cdl = null;
        //system.debug('getLastContentDocumentLink acuse '+acuse);
        if (Schema.sObjectType.ContentDocumentLink.isAccessible()) {
            try {
                if (acuse == false) {
                    //cdl = [SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId  = :idContact ORDER BY SystemModstamp DESC LIMIT 1];
                    cdl = [
                        SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility
                        FROM ContentDocumentLink
                        WHERE ContentDocument.Title LIKE '%RGPD%' AND LinkedEntityId = :idContact
                        ORDER BY SystemModstamp DESC
                        LIMIT 1
                    ];
                } else if (acuse == true) {
                    //cdl = [SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility FROM ContentDocumentLink WHERE LinkedEntityId  = :idContact ORDER BY SystemModstamp DESC LIMIT 1];
                    cdl = [
                        SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility
                        FROM ContentDocumentLink
                        WHERE ContentDocument.Title LIKE '%Recibo%' AND LinkedEntityId = :idContact
                        ORDER BY SystemModstamp DESC
                        LIMIT 1
                    ];
                }
            } catch (System.QueryException e) {
                system.debug('The following exception has occurred: ' + e.getMessage() + ' - ' + e.getStackTraceString());
            }
        }
        //system.debug('cdl'+cdl);
        return cdl;
    }
    
    /*
*============================================================================================================================================
* @methodName               getLastIndivdualDate
* @received parameters      String idContact
* @return                   Individual ind
*============================================================================================================================================
*/
    private static Individual getLastIndivdualDate(String idContact) {
        Individual ind = null;
        if (Schema.sObjectType.Individual.isAccessible()) {
            try {
                ind = [
                    SELECT LastModifiedDate, Contact__r.LastModifiedDate
                    FROM Individual
                    WHERE Contact__c = :idContact
                    ORDER BY LastModifiedDate
                    LIMIT 1
                ];
            } catch (System.QueryException e) {
                system.debug('The following exception has occurred: ' + e.getMessage() + ' - ' + e.getStackTraceString());
            }
        }
        //system.debug('ind'+ind);
        return ind;
    }
    
    /*
*============================================================================================================================================
* @methodName               getClient
* @received parameters      Id accountId
* @return                   Account [SELECT Id, Name FROM Account WHERE id = :accountId][0]
*============================================================================================================================================
*/
    @AuraEnabled
    public static Account getClient(Id accountId) {
        return [SELECT Id, Name FROM Account WHERE id = :accountId][0];
    }
    
    /*
*============================================================================================================================================
* @methodName               getContact
* @received parameters      Id idContact
* @return                   Contact [SELECT Id, Name FROM Contact WHERE id = :idContact][0]
*============================================================================================================================================
*/
    @AuraEnabled
    public static Contact getContact(Id idContact) {
        return [SELECT Id, Name FROM Contact WHERE id = :idContact][0];
    }
    
    /*
*============================================================================================================================================
* @methodName               getCongaURL
* @received parameters      String idContact, Boolean acuse
* @return                   String urlConga
*============================================================================================================================================
*/
    @AuraEnabled
    public static String getCongaURL(String idContact, Boolean acuse) {
        String urlConga = '';
        //system.debug('acuse '+acuse);
        //si el ultimo individual generado es más nuevo que el ultimo archivo RGPD generado del contacto
        //devolvemos url, sino vacía
        final Individual ind = getLastIndivdualDate(idContact);
        final ContentDocumentLink cdl = getLastContentDocumentLink(idContact, acuse);
        
        // || ind.LastModifiedDate > cdl.SystemModstamp
        if (
            cdl == null ||
            ind == null ||
            ind.LastModifiedDate > cdl.SystemModstamp ||
            ind.Contact__r.LastModifiedDate > cdl.SystemModstamp
        ) {
            final string base = Url.getSalesforceBaseUrl().toExternalForm();
            urlConga =
                base +
                '/apex/APXTConga4__Conga_Composer?serverUrl={!API.Partner_Server_URL_370}&DefaultPDF=1&DS7=11&SC0=1&Id=' +
                idContact +
                '&TemplateId=' +
                getTemplate(idContact, acuse);
        }
        //system.debug('urlConga '+urlConga);
        return urlConga;
    }
    
    /*
*============================================================================================================================================
* @methodName               getTemplate
* @received parameters      String idContact, Boolean acuse
* @return                   String template.Id
*============================================================================================================================================
*/
    private static String getTemplate(String idContact, Boolean acuse) {
        APXTConga4__Conga_Template__c template;
        String nombreTemplate = 'ES_ML_Consentimiento_RGPD';
        if (Schema.sObjectType.Contact.isAccessible()) {
            final String language = [SELECT Language__c FROM Contact WHERE Id = :idContact][0].Language__c;
            //String business = [SELECT Account.FII_ACC_SEL_MarketerContracts__c FROM Contact WHERE Id= :idContact][0].Account.FII_ACC_SEL_MarketerContracts__c;
            final String business = [SELECT Account.Contratos_en_comercializadora__c FROM Contact WHERE Id = :idContact][0]
                .Account.Contratos_en_comercializadora__c;
            
            //system.debug('business '+business + ' language ' + language);
            if (business == '04' || business == '03' || business == '01') {
                if (language == 'Español' || language == 'ES') {
                    nombreTemplate = 'ES_ML_Consentimiento_RGPD';
                } else if (language == 'Catalán' || language == 'CA') {
                    nombreTemplate = 'CA_ML_Consentimiento_RGPD';
                } else if (language == 'Inglés' || language == 'EN') {
                    nombreTemplate = 'EN_ML_Consentimiento_RGPD';
                }
            } else {
                if (language == 'Español' || language == 'ES') {
                    nombreTemplate = 'ES_MR_Consentimiento_RGPD';
                } else if (language == 'Catalán' || language == 'CA') {
                    nombreTemplate = 'CA_MR_Consentimiento_RGPD';
                } else if (language == 'Inglés' || language == 'EN') {
                    nombreTemplate = 'EN_MR_Consentimiento_RGPD';
                }
            }
            //system.debug('nombreTemplate '+nombreTemplate);
        }
        if (Schema.sObjectType.APXTConga4__Conga_Template__c.isAccessible()) {
            template = [SELECT Id, APXTConga4__Name__c FROM APXTConga4__Conga_Template__c WHERE APXTConga4__Name__c = :nombreTemplate];
        }
        return template.Id;
    }
    
    /*
*============================================================================================================================================
* @methodName               isTheEventForMe
* @received parameters      Id myRecId, Id eventRecId, Task interactionInProgress
* @return                   Boolean FII_Util_GSM.isTheEventForMe(myRecId, eventRecId, interactionInProgress)
*============================================================================================================================================
*/
    // CAMBIOS PARA GSM
    @AuraEnabled
    public static Boolean isTheEventForMe(Id myRecId, Id eventRecId, Task interactionInProgress) {
        return FII_Util_GSM.isTheEventForMe(myRecId, eventRecId, interactionInProgress);
    }
    
    //UPDATE CASE ASSIGMENT RULES
    
    /*
*============================================================================================================================================
* @methodName               caseAssign
* @received parameters      List<String> caseId
* @return                   (void method)
*============================================================================================================================================
*/
    @InvocableMethod
    public static void caseAssign(List<String> caseId) {
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule = true;
        Case cases = new Case();
        if (Schema.sObjectType.Case.isAccessible() && Schema.sObjectType.Case.isUpdateable()) {
            cases = [SELECT id FROM Case WHERE Case.id IN :caseid];
            // }
            cases.setOptions(dmo);
            //if(){
            update cases;
        }
    }
    
    /*
*============================================================================================================================================
* @methodName               deleteNeed
* @received parameters      Id needId, list<id> documentids, Boolean isDocument, Id caseId
* @return                   String deleteNeed
*============================================================================================================================================
*/
    @AuraEnabled
    public static String deleteNeed(Id needId, list<id> documentids, Boolean isDocument, Id caseId) {
        List<Case> caso;
        List<Case> caso2;
        String deleteNeed = '';
        //system.debug('documentids' + isDocument);
        List<ContentDocument> documents = new List<ContentDocument>();
        
        if (Schema.sObjectType.ContentDocument.isAccessible()
            && Schema.sObjectType.ContentDocument.isDeletable()) {
                documents = [SELECT id FROM ContentDocument WHERE id IN :documentids];
            }
        
        if (documents != null && !documents.isEmpty()) {
            
            delete documents;
            
        }
        
        caso = [SELECT Id, CreatedDate FROM Case WHERE Id = :needId LIMIT 1];
        caso2 = [SELECT Id, CreatedDate FROM Case WHERE Id = :caseId LIMIT 1];
        if (!isDocument
            && Schema.sObjectType.Case.isDeletable()) {
                if (caso != null && !caso.isEmpty()) {
                    delete caso;
                    deleteNeed = 'OK';
                }
                if (caso2 != null && !caso2.isEmpty()) {
                    delete caso2;
                }
            }
        
        return deleteNeed;
    }
    
    /*
*============================================================================================================================================
* @methodName               getInteraction
* @received parameters      String myRecId
* @return                   List<String> interaction
*============================================================================================================================================
*/
    //04/12/2019
    @AuraEnabled
    public static List<String> getInteraction(String myRecId) {
        final List<String> interaction = new List<String>(2);
        final Task itInCourse = FII_Util_GSM.getInteractionInProgress(myRecId);
        String disabled = null;
        system.debug('interaction: '+interaction);
        if(itInCourse == null){
            disabled = 'false';
            interaction.add(0,null);
            interaction.add(1,disabled);
            system.debug('interaction1: '+interaction);
        } else{
            if((itInCourse.FII_ACT_LKP_RelatedClient__c == myRecId || itInCourse.WhoId == myRecId) && itInCourse.WhoId != null){
                disabled = 'false';
                interaction.add(0,itInCourse.Id);
                interaction.add(1,disabled);
                system.debug('interaction2: '+interaction);
            } else{
                disabled = 'true';
                interaction.add(0,itInCourse.Id);
                interaction.add(1,disabled);
                system.debug('interaction3: '+interaction);
            }
        }
        system.debug('interaction final: '+interaction);
        return interaction; 
    }
    
    @AuraEnabled
    public static Decimal getTime(){
        CongaWaitTime__c cwt;
        if (Schema.sObjectType.CongaWaitTime__c.isAccessible()) {
            cwt = [SELECT WaitTime__c FROM CongaWaitTime__c WHERE Name = 'ContractGenerationTime'];
        }
        return cwt.WaitTime__c;
    }
    
    //21/01/2020
    @AuraEnabled
    public static String getProfileUser() {
        String  result = ''; 
        String profileId = UserInfo.getProfileId();
        if (Schema.sObjectType.Profile.isAccessible()) {
            Profile perfil = [SELECT Id, Name FROM Profile WHERE Id =: profileId];
            if(TV_CLS_Utils.getIsTeleventaProfile()){
                result = perfil.Name; 
            } 
            system.debug('Perfil result: '+perfil.Name);
        }
        return result;
    }
    
    @AuraEnabled
    public static string getIsContractAllowed(String accountId,String interactionId){
        
        String result = '';
        //try{ 
            system.debug('Entramos en getIsContractAllowed - interactionId'+interactionId);
            if(String.isNotBlank(interactionId) && Schema.sObjectType.Task.isAccessible()){
                    final Task interaction = [SELECT Id, WhoId FROM Task WHERE Id =: interactionId];
                if (interaction.WhoId != null && Schema.sObjectType.AccountContactRelation.isAccessible()){
                    
                    if(String.valueOf(interaction.WhoId.getSobjectType()) == 'Contact'){
                        // Comprobamos que el contacto es titular o autorizado de la cuenta
                        Boolean checkIndividual = false;
                        if (Schema.sObjectType.AccountContactRelation.isAccessible()) {
                            final List<AccountContactRelation> contRel = [SELECT ContactId, Roles FROM AccountContactRelation WHERE AccountId = :accountId AND ContactId =:interaction.WhoId ];
                            for(AccountContactRelation cont :contRel){
                                if(cont.ContactId == interaction.WhoId){
                                    if(cont.Roles == 'Autorizado' || cont.Roles== 'Titular de contrato'){
                                        checkIndividual = true;
                                    }
                                }
                            }
                        }
                        
                        if(checkIndividual){
                            if (Schema.sObjectType.Individual.isAccessible()) {
                                final List<Individual> indList = [SELECT Id,FII_IND_SEL_Cesion__c,FII_IND_SEL_Molestar__c,FII_IND_SEL_Terceros__c,FII_IND_SEL_Iden_Evi__c 
                                                            FROM Individual 
                                                            WHERE contact__c =: interaction.WhoId 
                                                            ORDER BY LastModifiedDate DESC
                                                            LIMIT 1];
                                if(indList.size() > 0 && indList[0].FII_IND_SEL_Molestar__c == 'Sí Ofertas'){
                                    if(indList[0].FII_IND_SEL_Molestar__c == 'Sí Ofertas'){
                                        result = 'OK';
                                    }
                                }
                            }
                        }else{
                            result = 'RolNoValido';
                        }
                    }
                }
            }
        /*}catch(Exception errFound){
            result = errFound.getMessage();
            system.debug ('ERROR FOUND: ' + errFound);
            System.debug('LINEA: ' + errFound.getLineNumber());
            System.debug('TRAZA: ' + errFound.getStackTraceString()); 
        }*/
        system.debug('result: '+result);
        return result;
        
    }
    /*
 *============================================================================================================================================
 * @methodName               getLastCaseInProgress
 * @received parameters      Id accountId
 * @return                   
 *============================================================================================================================================
 */
	@AuraEnabled
	public static Case getLastCaseInProgress(Id accountId) {

		Id userId = UserInfo.getUserId();

		List<Case> caseInProgressList = new List<Case>();

		if (Schema.sObjectType.Case.isAccessible()){

			caseInProgressList = [SELECT id, AccountId, FII_CAS_LKP_Campaign__c, Lead__c FROM Case WHERE Status != 'Resolved' AND accountId =: accountId AND type = 'Need_03'  ORDER BY CreatedDate DESC LIMIT 1];
		}

		if(caseInProgressList.size() > 0){
			return caseInProgressList[0];
		}
		else{
			return null;
		}
	}
	/*
 *============================================================================================================================================
 * @methodName               createTaskAction
 * @received parameters      Id accountId
 * @return                   
 *============================================================================================================================================
 */
	@AuraEnabled
	public static String createTaskAction(String caseNeedJson, String subject, String status, String recordtype) {

		Task action;
		Case caseNeed = (Case) JSON.deserialize(caseNeedJson, Case.class);

		if (Schema.sObjectType.Task.isCreateable()){
			
			try{
				action = FII_Util_GSM.createTaskAction(caseNeed, subject, status, recordtype);
			}
			catch(Exception e){
				return 'error get action- '+e.getMessage();
			}
			try{
				insert action;
				return 'ok';
			}
			catch(DmlException exc) {
				return 'error insert action- ' + exc.getMessage();
			}	
		}
		else{
			return 'error - El usuario actual no tiene permisos de inserción en el objeto Task';
		}
	}
}