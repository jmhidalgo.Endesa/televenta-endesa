@isTest
private class TV_LCC_CampaignSection_Test {
    
    @isTest static void getDataDos_test(){
        ServicePointTriggerHandler.bypassTrigger = true;
        List<CampaignMember> campaignMemberList = TV_CLS_Test_Factory.createCampaignMemberList();
        insert campaignMemberList;
        Test.startTest();        
        TV_LCC_CampaignSection.DataWrapper dataWrapper = TV_LCC_CampaignSection.getDataDos(  campaignMemberList[0].id, campaignMemberList[0].FII_CAMM_LKP_AccountMember__c,'' );
        Test.stopTest();
        ServicePointTriggerHandler.bypassTrigger = false;
        System.debug(dataWrapper.showLeadData);
        System.assert( dataWrapper.showLeadData == true);
        
    }
    
        @isTest static void createInteration_Test() {
        
        ServicePointTriggerHandler.bypassTrigger = true;
        List<CampaignMember> campaignMemberList = TV_CLS_Test_Factory.createCampaignMemberList();
        insert campaignMemberList;
        
        Task taskItem = TV_CLS_Test_Factory.createInteration( campaignMemberList[0] );
        insert taskItem;
        
        Test.startTest();
		string campaignMemberId = TV_LCC_CampaignSection.getCampaignMember(taskItem.WhoId);
        Test.stopTest();
        
        ServicePointTriggerHandler.bypassTrigger = false;
		
        System.Assert( campaignMemberId != null );
    }
}