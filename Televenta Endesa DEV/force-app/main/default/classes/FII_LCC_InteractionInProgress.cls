/**
* @author      Everis
* @since       March 13, 2019 
* @desc        Class that connects the InteractionInProgress component with Salesforce records
* @history     March 13, 2019 - everis (JVC) - Create Apex Class 
*/
public With Sharing class FII_LCC_InteractionInProgress {
    
    @AuraEnabled
    public static Task getInteractionsInProgress(Id recId){
        System.debug('getInteractionsInProgress recId ' + recId);
        final String contactLastName = 'Pendiente de Informar';
        final String contactFirstName = 'Contacto';
        List<Contact> dummyContact;
        Task openedInteraction = FII_Util_GSM.getInteractionInProgress(recId);
        if (openedInteraction != null) {
            openedInteraction = (Task) FII_Util_GSM.mapApiNamePicklist(new List<Task>{openedInteraction})[0];
            
            if (openedInteraction.FII_ACT_TXT_Main_interaction__c == null){
                if (Schema.sObjectType.Contact.isAccessible()) {
                    dummyContact = [SELECT Id, FirstName, LastName FROM Contact WHERE FirstName =: contactFirstName AND LastName =: contactLastName LIMIT 1];
                    
                    if (!dummyContact.isEmpty() && openedInteraction.WhoId == dummyContact[0].Id) {
                        openedInteraction.WhoId = null;
                    }
                }
            }
        }
		system.debug('openedInteraction ' + openedInteraction);
        return openedInteraction;
    }
    
    /** 
* @desc the identifier in the paremtrica is searched, returning the type and subtype fields
* 
* @param idMotive: customer identifier
* @returns the type and subtype fields
*/
    
    @AuraEnabled
    public static List<String> getTypeAndSubType(String idMotive){
        
        final List<String> listTypeSubType = new List<String>();
        
        if(Schema.sObjectType.Interaction_LN__mdt.isAccessible()){
            final List<Interaction_LN__mdt> interLN = [Select id, type__c, subType__c from Interaction_LN__mdt where indicador__c=:idMotive Limit 1];
            
            if(interLN.size()>0){
                listTypeSubType.add(interLN[0].type__c);
                listTypeSubType.add(interLN[0].subType__c);
            }
            return listTypeSubType;
        }else{
            return null;
        }
    }
    
    
    @AuraEnabled
    public static boolean disableButton(Id myRecId, Id eventRecId, Task itInCourse){
        return FII_Util_GSM.disableButton(myRecId, eventRecId, itInCourse);
    }
    
    /** 
* @desc Searchs for the related contacts, using the AccountContactRelation object
* 
* @param accountId: Id of the record shown in the page
* @return the related contacts as an object with fields from both AccountContactRelation and Contact
*/
    @AuraEnabled
    public static String getContactRelated (Id accountId) {
        final List<ContactRole> contRol = new List<ContactRole>();
        if (Schema.sObjectType.AccountContactRelation.isAccessible()){
        final List<AccountContactRelation> contRel = [Select ContactId, Roles from AccountContactRelation where AccountId = :accountId];
        
        final Set<Id> contId = new Set<Id>();
        for(AccountContactRelation acr: contRel) {
            contId.add(acr.ContactId);
        }
        final Map<Id,Contact> cont = new Map<Id, Contact>([Select Id, Name from Contact where Id in :contId]);
        
        for(AccountContactRelation relCont : contRel) {
            //final Contact con = cont.get(relCont.ContactId);
            Contact con = cont.get(relCont.ContactId); //FIX
            ContactRole cr = new ContactRole();
            cr.cont_id = con.Id;
            cr.cont_Name = con.Name;
            cr.cont_Role = relCont.Roles;
            contRol.add(cr);
            }
        
        return JSON.serialize(contRol);
        }
        else {
            return null;
        	}
        
    }
    
    
    @AuraEnabled
    public static void setContactToInteraction(Id interactionId, Id contactId){
        FII_Util_GSM.setContact(interactionId, contactId);
    }
    
    @AuraEnabled
    public static String getAutorizedOperations(){
        Return [SELECT FII_CRO_TXT_Autorized__c FROM FII_ContactRole_Operations__mdt LIMIT 1][0].FII_CRO_TXT_Autorized__c;
    }
    
    @AuraEnabled
    public static String getTitularOperations(){
        Return [SELECT  FII_CRO_TXT_Titular__c FROM FII_ContactRole_Operations__mdt LIMIT 1][0].FII_CRO_TXT_Titular__c;
    }
    
    // CAMBIOS PARA GSM
    @AuraEnabled 
    public static Boolean isTheEventForMe (Id myRecId, Id eventRecId, Task interactionInProgress){
        return FII_Util_GSM.isTheEventForMe(myRecId, eventRecId, interactionInProgress);
    }
    
    @AuraEnabled 
    public static Boolean hiddenButton(){
        
        List<User> userList;
        
        if(Schema.sObjectType.User.isAccessible()){
            userList = [ SELECT Id, CallCenterId 
                         FROM   User 
                         where id=: UserInfo.getUserId() ];
        }
        List<CallCenter> callCenterList;
        If( !userList.isEmpty() ){
            if(Schema.sObjectType.CallCenter.isAccessible()){
                callCenterList =  [ SELECT Id, name 
                                    FROM   CallCenter 
                                    WHERE  id = :userList[0].CallCenterId 
                                    AND    name = 'PureConnect for Salesforce Lightning'];
            }
        }
        return callCenterList.isEmpty();
	}
    
    /** 
* @desc object with fields from both, AccountContactRelation and Contact
*/
    public class ContactRole {
        
        public Id cont_id;
        public String cont_Name;
        public String cont_Role;
        
    }
    
    @AuraEnabled
    public static Case standardBehaviorGSM(Id recId, String type, String subtype, String subjectNeed, String subjectAct, Id camId, String status, String recordtype){
        final Case need = (Case)FII_Util_GSM.standardBehaviorGSM(recId, type, subtype, subjectNeed, subjectAct, camId, status, recordtype)[0];
        return need;
    }
}