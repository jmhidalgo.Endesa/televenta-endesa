@isTest
public class TV_LCC_ClientSection_Test {

    @TestSetup
	static void makeData(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        List<Contact> listContacts = new List<Contact>();
        List<Account> listAccounts = new List<Account>();

		Contact con = UtilTest.createContact();
		con.Physical_or_legal_cd__c = 'Corporate Customer';   
		AccountTriggerHandler.bypassTrigger = true;
		insert con;
		AccountTriggerHandler.bypassTrigger = false;

		//Account acc = [SELECT Id FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        Account acc = UtilTest.createAccount();
		acc.TitularContactHide__c = con.Id;
        acc.FirstName__c = 'PruebaRGPD';
        acc.NIF_CIF_Customer_NIE__c = '44283092L';
		acc.Cliente_External_Id__c = '35-ee';
        acc.IInternal_payment_quality_index_comerc__c = 0.1;
        acc.Contratos_en_comercializadora__c = '01';
        listAccounts.add(acc);
		

        //Account sin trigger (sin contacto titular y este sin individual)
        Account acc2 = UtilTest.createAccount();
		acc2.TitularContactHide__c = con.Id;
        acc2.FirstName__c = 'PruebaRGPD2';
        acc2.NIF_CIF_Customer_NIE__c = '71708328D';
		acc2.Cliente_External_Id__c = '36-aa';
        acc2.IInternal_payment_quality_index_comerc__c = 3;
        acc2.Contratos_en_comercializadora__c = '02';
		listAccounts.add(acc2);

        //Account acc = [SELECT Id FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        Account acc3 = UtilTest.createAccount();
		acc3.TitularContactHide__c = con.Id;
        acc3.FirstName__c = 'Contacto';
        acc3.NIF_CIF_Customer_NIE__c = '44878946N';
		acc3.Cliente_External_Id__c = '37-ee';
        acc3.IInternal_payment_quality_index_comerc__c = 6;
        acc3.Contratos_en_comercializadora__c = '03';
        listAccounts.add(acc3);
        
        Account acc4 = UtilTest.createAccount();
		acc4.TitularContactHide__c = con.Id;
        acc4.FirstName__c = 'PruebaRGPD4';
        acc4.NIF_CIF_Customer_NIE__c = '70360782X';
		acc4.Cliente_External_Id__c = '38-aa';
        acc4.IInternal_payment_quality_index_comerc__c = 8;
        acc4.Contratos_en_comercializadora__c = '04';
		listAccounts.add(acc4);

        AccountTriggerHandler.bypassTrigger = true;
        insert listAccounts ;
        AccountTriggerHandler.bypassTrigger = false;


        final Contact contact = new Contact();
		contact.FirstName = 'Test3';
		contact.LastName = 'Pendiente de Informar';
		contact.Email = 'test4@gmail.com';
		contact.Work_email__c = 'test4@gmail.com';
		contact.Document_Type__c = 'NIF';
		contact.NIF_CIF_Customer_NIE__c = '91770772J';
		contact.Contact_method_cd__c='E-mail';
		contact.No_Phone_flg__c = true;
		contact.No_Email_flg__c = false;
		contact.Nationality_cd__c = 'ES';
		contact.Contact_Robinson__c = true;
		contact.Physical_or_legal_cd__c = 'Physical Customer';
		//contact.Phone='954676767';
		contact.Profession_cd__c='Lawyer';
		contact.Sex__c='Male';
        contact.AccountId = acc.Id;

        ContactTriggerHandler.bypassTrigger = true;
        listContacts.add(contact);
        
        ContactTriggerHandler.bypassTrigger = false;

        final Contact contact2 = new Contact();
		contact2.FirstName = 'Contacto';
		contact2.LastName = 'Pendiente de Informar';
		contact2.Email = 'test4@gmail.com';
		contact2.Work_email__c = 'test4@gmail.com';
		contact2.Document_Type__c = 'NIF';
		contact2.NIF_CIF_Customer_NIE__c = '92779508S';
		contact2.Contact_method_cd__c='E-mail';
		contact2.No_Phone_flg__c = true;
		contact2.No_Email_flg__c = false;
		contact2.Nationality_cd__c = 'ES';
		contact2.Contact_Robinson__c = true;
		contact2.Physical_or_legal_cd__c = 'Physical Customer';
		//contact.Phone='954676767';
		contact2.Profession_cd__c='Lawyer';
		contact2.Sex__c='Male';
        contact2.AccountId = acc3.Id;

        ContactTriggerHandler.bypassTrigger = true;
        listContacts.add(contact2);
        insert listContacts;
		ContactTriggerHandler.bypassTrigger = false;
        
        Task interaction = FII_Util_GSM.createInteraction(acc.Id, null, null, 'Phone', 'interaction');
        insert interaction;

		Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
        ind.DonotdisturbRGPD__c = 'Y';
    	insert  ind;  
        Test.stopTest();
	}

	@isTest
	private static void getData_Test (){
		Account acc = [SELECT Id,TitularContactHide__c FROM Account WHERE FirstName__c='PruebaRGPD' LIMIT 1];
        TV_LCC_ClientSection.DataWrapper result = TV_LCC_ClientSection.getData(acc.Id);
		Account acc2 = [SELECT Id,TitularContactHide__c FROM Account WHERE FirstName__c='PruebaRGPD2' LIMIT 1];
        TV_LCC_ClientSection.DataWrapper result2 = TV_LCC_ClientSection.getData(acc2.Id);
		Account acc3 = [SELECT Id,TitularContactHide__c FROM Account WHERE FirstName__c='Contacto' LIMIT 1];
        TV_LCC_ClientSection.DataWrapper result3 = TV_LCC_ClientSection.getData(acc3.Id);
		Account acc4 = [SELECT Id,TitularContactHide__c FROM Account WHERE FirstName__c='PruebaRGPD4' LIMIT 1];
        TV_LCC_ClientSection.DataWrapper result4 = TV_LCC_ClientSection.getData(acc4.Id);
	}
}