/**
*   @author   ATOS
*   @since    May 08, 2019
*   @desc     Controller of FII_LC_LeadToCustomer
*   @history  May 08, 2019 - ATOS (AFG) - Create Apex Class
*/
public with sharing class FI_LCC_LeadToCustomer {

    private class Lov {
        public String label;
        public string value;
    }


    public static String recordTypeClientID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();

    /**
     * @desc Find the documentNumber in Accounts, Contacts and SVE
     * @param documentNumber The Lead document
     * @return Custom object with the result
     */
    @AuraEnabled
    public static CustomerSearchWrapper findCustomerInAllSystems(String documentNumber) {
        CustomerSearchWrapper searchWrapper = new CustomerSearchWrapper();
        searchWrapper.recordTypeClientId = recordTypeClientID;
        List<Account> lAccounts = getAccountsByDocumentNumber(documentNumber);
        List<Contact> lContacts = getContactsByDocumentNumber(documentNumber);
        List<Map<String, String>> lCustomerDatas = new List<Map<String, String>>();

        for (Account acct : lAccounts) {
            Map<String, String> tmpMap = new Map<String, String>();
            tmpMap.put('Id', String.valueOf(acct.Id));
            tmpMap.put('Name', acct.Name);
            tmpMap.put('Type', Label.ACCOUNT_LABEL);
            lCustomerDatas.add(tmpMap);
        }
        for (Contact cnt : lContacts) {
            Map<String, String> tmpMap = new Map<String, String>();
            tmpMap.put('Id', String.valueOf(cnt.Id));
            tmpMap.put('Name', cnt.Name);
            tmpMap.put('Type', Label.CONTACT_LABEL);
            lCustomerDatas.add(tmpMap);
        }

        if (lCustomerDatas.isEmpty()) {
            String resultCallSVE = callSVECustomer(documentNumber, 'NIF');
             if ('ERROR'.equals(resultCallSVE)) {
                searchWrapper.errorCallSVE = true;
            } else {
                searchWrapper.clientInSVE = 'OK'.equals(resultCallSVE);
            }
        } else {
            searchWrapper.documentInOthersAccountOrContacts = true;
            searchWrapper.lCustomerDatas = lCustomerDatas;
        }
        return searchWrapper;
    }
    /**
     * @desc Find the Lead in BBDD by Id
     * @param leadId
     * @return The found Lead
     */
    @AuraEnabled
    public static Lead getLead(String leadId) {
        if(Schema.SObjectType.Lead.isAccessible()) {
            List<Lead> leads = [
                    SELECT Id, NIF_CIF_Customer_NIE__c, IsConverted, ConvertedAccountId, Account_Surname_Company__c,
                            Account_NIF_CIF_Customer_NIE__c, Name, Address_Lead__c, Account_Identifier_type_cd__c,
                            FirstName, LastName, Language_cd__c, FII_LEA_FLG_NIFDuplicated__c, Nacionality_cd__c,
                            Physical_Legal_person_cd__c, Email, No_email_flg__c, Account_Contact_method_cd__c, 
                            No_Phone_flg__c, MobilePhone, Phone, OtherPhone__c, Work_Phone__c, IndividualID,
                            TV_Rep_Physical_Legal_Person_cd__c, TV_Representant_DOC_Type__c, TV_Representant_Name__c, 
                            Account_RGPD_Datadisclosure__c, Account_RGPD_Donotdisturb__c, Account_RGPD_Offerings__c,
                            TV_Representant_NIF__c, TV_Representant_Surname__c, Identifier__c, Identifier_type_cd__c
                            FROM Lead
                            WHERE Id = :leadId
            ];
            if(leads != null && !leads.isEmpty()){ // MODIFICACION JAML 11/12/2019
                if(!(leads[0].No_Phone_flg__c)){
                    FII_Util_Lead.addPrefix(leads);
                }
                return leads[0];
            }
            else {return null;}
        }else{return null;}
        //return (leads != null && !leads.isEmpty()) ? leads[0] : null;     
    }

    /**
     * @desc Find the Lead in BBDD by Id
     * @param leadId
     * @return The found Lead
     */
    @AuraEnabled
    public static Task getInteractionInProgress(String leadId) {
        Task res = null;
        if(Schema.SObjectType.Task.isAccessible()) {
            List<Task> taskList = [SELECT Id, WhoId, FII_ACT_LKP_RelatedClient__c,
                                            TV_ACT_TXT_MarcadorCampaignId__c , CallObject
                                   FROM Task 
                                   WHERE WhoId = :leadId AND Status = 'Open' AND FII_ACT_TXT_Main_interaction__c = null];
        
            if(taskList != null && !taskList.isEmpty()){
                res = taskList[0];
            }
        }
        return res;    
    }

    @AuraEnabled
    public static Profile getProfileInfo(){
        Profile res = null;
        String profileId = UserInfo.getProfileId();
        List<Profile> profile = new List<Profile>();
        if(Schema.SObjectType.Profile.isAccessible()) {
            profile = [SELECT Id, Name FROM Profile WHERE Id =:profileId];
        }
        if(profile != null){
            res = profile.get(0);
        }
        return res;
    }
/*
    @AuraEnabled
    public static String getLoV(String objectName, String fieldApiname,Boolean nullRequired){
        final List<Lov> optionList = new List<Lov>();
        
        final Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
        final Map<String, Schema.SObjectField> fieldMap = gd.get(objectName.toLowerCase()).getDescribe().fields.getMap();
        
        final List<Schema.PicklistEntry> pickListValues = fieldMap.get(fieldApiname).getDescribe().getPickListValues();
        Lov opt = new Lov();
        opt.label = '';
        opt.value = '';
        optionList.add(opt);
        for (Schema.PicklistEntry pv : pickListValues) {
            opt = new Lov();
            opt.label = pv.getLabel();
            opt.value = pv.getValue();
            optionList.add(opt);
        }
        return JSON.serialize(optionList);
    }*/

    /**
     * @desc Convert Lead and related this with Account and the Account Contact
     * @param customer The Lead for conversion
     * @param accountId The Account for related
     * @return Map with result operations values
     */
    @AuraEnabled
    public static Map<String, String> converLead(Lead customer, Account sAccount) {

        //AccountTriggerHandler.bypassTrigger=true;
        AccountContactRelationTriggerHandler.bypassTrigger=true;
        ContactTriggerHandler.bypassTrigger=true;
        IndividualTriggerHandler.bypassTrigger=true;
        FII_TRG_Task_Handler.bypassTrigger=true;

        Map<String, String> customResults = new Map<String, String>();
        Database.LeadConvert leadconvert = new Database.LeadConvert();
        Id cntId = null;
        Savepoint sp = DataBase.setSavepoint();
       try {
            System.debug('Lead --> '+customer);
            System.debug('Account --> '+sAccount);
            if(Schema.SObjectType.Account.isAccessible() && Schema.SObjectType.Account.isCreateable() && Schema.SObjectType.Account.isUpdateable() 
                &&  Schema.SObjectType.AccountContactRelation.isAccessible() && Schema.SObjectType.AccountContactRelation.isUpdateable()
                && Schema.SobjectType.LeadStatus.isAccessible() && Schema.SObjectType.Individual.isAccessible()
                && Schema.SObjectType.CampaignMember.isAccessible() && Schema.sObjectType.Individual.isUpdateable() && Schema.sObjectType.CampaignMember.isUpdateable()){
                customer.MobilePhone = sAccount.MobilePhone1__c;
                customer.Account_NIF_CIF_Customer_NIE__c = customer.Identifier__c;
                customer.Account_Identifier_type_cd__c = customer.Identifier_type_cd__c;
                if(!customer.No_Phone_flg__c){
                    FII_Util_Lead.addPrefix(new List<Lead>{customer});  // MODIFICACION JAML 11/12/2019
                    System.debug('Antes de update customer ' + customer);
                    update customer;
                    System.debug('Despues de update customer ' + customer);
                }
                sAccount.Account_status__c = 'Potential';
                sAccount.Account_language__c = 'Español';
                sAccount.Contact_method_cd__c = customer.Account_Contact_method_cd__c;
                //System.debug('sAccount antes de insert sin reg');
                //System.debug('sAccount antes de insert --> '+sAccount);
                System.debug('Antes de insert sAccount ');
                insert sAccount;
                System.debug('Despues de insert sAccount ');
                //System.debug('sAccount despues de insert sin reg');
                //System.debug('sAccount despues de insert --> '+sAccount);
                leadconvert.setLeadId(customer.Id);
                // List<Contact> lContacts = [SELEC Id, Role__c, Contact_method_cd__c, New_email__c, Email FROM Contact WHERE AccountId=: sAccount.Id];
                // System.debug('lContacts --> '+lContacts);
                // 
                List<AccountContactRelation> lContactsRelations = [SELECT Id, ContactId, Contact.Contact_method_cd__c, Contact.New_email__c, Contact.Email, Contact.IndividualId,
                                                                        Contact.DatacessionRGPD__c, Contact.DonotdisturbRGPD__c, Contact.OfferingsRGPD__c,Contact.CommentsRGPD__c
                                                                   FROM AccountContactRelation 
                                                                   WHERE Roles = 'Titular de contrato' 
                                                                      AND AccountId = :sAccount.Id /*Contact.NIF_CIF_Customer_NIE__c = :sAccount.NIF_CIF_Customer_NIE__c*/ LIMIT 1];
                cntId = (lContactsRelations != null && !lContactsRelations.isEmpty()) ? lContactsRelations[0].ContactId : null;
                //lContactsRelations[0].Contact.CommentsRGPD__c = 
                if(lContactsRelations != null && lContactsRelations[0] != null && lContactsRelations[0].Contact != null){
                    lContactsRelations[0].Contact.DatacessionRGPD__c = customer.Account_RGPD_Datadisclosure__c == 'Si' ? 'Y' : 'N';
                    lContactsRelations[0].Contact.DonotdisturbRGPD__c = customer.Account_RGPD_Donotdisturb__c == 'Si' ? 'Y' : 'N';
                    lContactsRelations[0].Contact.OfferingsRGPD__c = customer.Account_RGPD_Offerings__c == 'Si' ? 'Y' : 'N';
                }
                System.debug('lContactsRelations --> '+lContactsRelations);
                leadconvert.setContactId(cntId);
                leadconvert.setAccountId(sAccount.Id);
                //List<LeadStatus> leads = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1];
                //leadconvert.setConvertedStatus(leads[0].MasterLabel);
                leadconvert.setConvertedStatus('Converted');
                leadconvert.setDoNotCreateOpportunity(true);
                System.debug('Antes de convertir --> '+sAccount);

                Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.AllowSave = true;
                synchronizeInteraction(customer.Id, sAccount.Id, cntId);
                //synchronizeInteraction(customer.Id, sAccount.Id, sAccount.NIF_CIF_Customer_NIE__c);
                Database.LeadConvertResult leadconverts = Database.convertLead(leadconvert,dml);
                System.debug('Despues de convertir --> '+sAccount);
                System.debug('leadconverts -> ' + leadconverts);
                if (leadconverts.isSuccess() && Schema.SObjectType.Account.isAccessible() && Schema.SObjectType.AccountContactRelation.isAccessible() ) {
                    System.debug('Proceso de conversion del lead correcto');
                    customResults.put('status', 'SUCCESS');
                    //customResults.put('IdAccount', String.valueOf(sAccount.Id));
                    customResults.put('IdAccount', leadconverts.getAccountId());
                    /*Account sAccount2 = [SELECT Id, Proveniente_de_lead__c, FI_ACC_LKP_ParentLead__c,
                            FI_ACC_FLG_BYPASS_VR__c FROM Account WHERE Id =  :sAccount.Id];
                    sAccount2.Proveniente_de_lead__c = true;
                    sAccount2.FI_ACC_LKP_ParentLead__c = leadconverts.leadId;
                    sAccount2.FI_ACC_FLG_BYPASS_VR__c = true;
                    System.debug('Account 2 --> '+sAccount2);*/

                    System.debug('Lead 2 --> '+customer);

                    //sAccount.Id = leadconverts.getAccountId();
                    //sAccount.Proveniente_de_lead__c = true;
                    //sAccount.FI_ACC_LKP_ParentLead__c = customer.Id;
                    sAccount.FI_ACC_FLG_BYPASS_VR__c = true;
                    System.debug('sAccount.Proveniente_de_lead__c --> '+sAccount.Proveniente_de_lead__c);
                    System.debug('sAccount.FI_ACC_LKP_ParentLead__c --> '+leadconverts.getLeadId());
                    System.debug('Account_RGPD_Datadisclosure__c --> '+customer.Account_RGPD_Datadisclosure__c);
                    

                    if(String.isNotBlank(customer.Account_RGPD_Datadisclosure__c)){
                        //sAccount.DatacessionRGPD__c = customer.Account_RGPD_Datadisclosure__c.replace('í', 'i');
                        if(customer.Account_RGPD_Datadisclosure__c == 'Si'){
                            sAccount.DatacessionRGPD__c = 'Y';
                        } else if(customer.Account_RGPD_Datadisclosure__c == 'No'){
                            sAccount.DatacessionRGPD__c = 'N';
                        }
                        
                    }
                    if(String.isNotBlank(customer.Account_RGPD_Donotdisturb__c)){
                        //sAccount.DonotdisturbRGPD__c = customer.Account_RGPD_Donotdisturb__c.replace('í', 'i');
                        if(customer.Account_RGPD_Donotdisturb__c == 'Si'){
                            sAccount.DonotdisturbRGPD__c = 'Y';
                        } else if(customer.Account_RGPD_Donotdisturb__c == 'No'){
                            sAccount.DonotdisturbRGPD__c = 'N';
                        }
                        //sAccount.DonotdisturbRGPD__c = customer.Account_RGPD_Donotdisturb__c;
                    }
                    if(String.isNotBlank(customer.Account_RGPD_Offerings__c)){
                        //sAccount.OfferingsRGPD__c = customer.Account_RGPD_Offerings__c.replace('í', 'i');
                        //sAccount.OfferingsRGPD__c = customer.Account_RGPD_Offerings__c;
                        if(customer.Account_RGPD_Offerings__c == 'Si'){
                            sAccount.OfferingsRGPD__c = 'Y';
                        } else if(customer.Account_RGPD_Offerings__c == 'No'){
                            sAccount.OfferingsRGPD__c = 'N';
                        }
                    }

                    //if(String.isNotBlank(customer.Account_RGPD_Datadisclosure__c)){
                        //if(customer.Account_RGPD_Datadisclosure__c == 'Y'){
                            //sAccount.RGPD_Datadisclosure__c = customer.Account_RGPD_Datadisclosure__c.replace('í', 'i');
                            System.debug('Antes de sAccount.RGPD_Datadisclosure__c ');
                            sAccount.RGPD_Datadisclosure__c = 'Si';
                            System.debug('Despues de sAccount.RGPD_Datadisclosure__c ' + sAccount.RGPD_Datadisclosure__c);
                        //} else if(customer.Account_RGPD_Datadisclosure__c == 'N'){
                            //sAccount.RGPD_Datadisclosure__c = customer.Account_RGPD_Datadisclosure__c.replace('í', 'i');
                            //sAccount.RGPD_Datadisclosure__c = 'No';
                        //}
                    //}
                    /*
                    if(String.isNotBlank(customer.Account_RGPD_Offerings__c)){
                        if(customer.Account_RGPD_Offerings__c == 'Y'){
                            //sAccount.RGPD_Datadisclosure__c = customer.Account_RGPD_Datadisclosure__c.replace('í', 'i');
                            sAccount.RGPD_Datadisclosure__c = 'Si';
                        } else if(customer.Account_RGPD_Offerings__c == 'N'){
                            //sAccount.RGPD_Datadisclosure__c = customer.Account_RGPD_Datadisclosure__c.replace('í', 'i');
                            sAccount.RGPD_Datadisclosure__c = 'No';
                        }
                    }*/
                    if(String.isNotBlank(customer.Account_RGPD_Offerings__c)){
                        //sAccount.RGPD_Offerings__c = customer.Account_RGPD_Offerings__c.replace('í', 'i');
                        sAccount.RGPD_Offerings__c = customer.Account_RGPD_Offerings__c.replace('í', 'i');
                    }
/*
                    System.debug('customer.IndividualId ' + customer.IndividualId);
                    Individual ind = null;
                    if(customer.IndividualId != null){
                        if(Individual.SObjectType.getDescribe().isAccessible()){
                            ind = [SELECT Id, DatacessionRGPD__c, DonotdisturbRGPD__c, OfferingsRGPD__c,
                                            FII_IND_SEL_Cesion__c, FII_IND_SEL_Molestar__c, FII_IND_SEL_Clientes__c
                                   FROM Individual 
                                   WHERE Id = :customer.IndividualId limit 1];
                            
                        }
                    } else if(lContactsRelations[0].Contact.IndividualId != null){
                        if(Individual.SObjectType.getDescribe().isAccessible()){
                         ind = [SELECT Id, DatacessionRGPD__c, DonotdisturbRGPD__c, OfferingsRGPD__c,
                                         FII_IND_SEL_Cesion__c, FII_IND_SEL_Molestar__c, FII_IND_SEL_Clientes__c
                                FROM Individual 
                                WHERE Id = :lContactsRelations[0].Contact.IndividualId limit 1];
                                
                     }
                    } else {
                        ind = new Individual();
                        ind.FirstName = customer.FirstName.toUpperCase();
                        ind.LastName = customer.LastName.toUpperCase();
                        insert ind;
                    }

                    ind.DatacessionRGPD__c = customer.Account_RGPD_Datadisclosure__c == 'Si' ? 'Y' : 'N';
                    ind.DonotdisturbRGPD__c = customer.Account_RGPD_Donotdisturb__c == 'Si' ? 'Y' : 'N';
                    ind.OfferingsRGPD__c = customer.Account_RGPD_Offerings__c == 'Si' ? 'Y' : 'N';

                    ind.FII_IND_SEL_Cesion__c = ind.DatacessionRGPD__c == 'Y' ? 'Si' : 'No';
                    ind.FII_IND_SEL_Molestar__c = ind.DonotdisturbRGPD__c == 'Y' ? 'Sí Ofertas' : 'No Ofertas';
                    ind.FII_IND_SEL_Clientes__c = ind.OfferingsRGPD__c == 'Y' ? 'Si' : 'No';

                    if(Schema.sObjectType.Individual.isUpdateable()){
                        System.debug('Individual ind antes de update ' + ind);

                        update ind;

                        System.debug('Individual ind despues de update ' + ind);
                    }*/

                    //System.debug('Account 2 --> '+sAccount);
                    //update sAccount2;
                    //dml = new Database.DMLOptions();
                    //dml.DuplicateRuleHeader.AllowSave = true;
                    //Database.update(sAccount2,dml);
                    //Database.update(sAccount,dml);
                    //Modificacion JAML 11/12/2019
                    List<AccountContactRelation> contactRelacionado = null;
                     if(cntId == null){
                        contactRelacionado  = [SELECT Id, ContactId, Contact.Contact_method_cd__c, Contact.New_email__c, 
                                                        Contact.Email, Roles, Contact.IndividualId
                                                FROM AccountContactRelation WHERE AccountId = :sAccount.Id LIMIT 1];
                        contactRelacionado[0].Roles = 'Titular de contrato';
                        //contactRelacionado[0].Contact.IndividualId = customer.IndividualId;
                        //contactRelacionado[0].Contact.IndividualId = ind.Id;
                        System.debug('contactRelacionado[0].Contact.IndividualId ' + contactRelacionado[0].Contact.IndividualId);
                        cntId = contactRelacionado[0].ContactId;
                        contactRelacionado[0].Contact.DatacessionRGPD__c = customer.Account_RGPD_Datadisclosure__c == 'Si' ? 'Y' : 'N';
                        contactRelacionado[0].Contact.DonotdisturbRGPD__c = customer.Account_RGPD_Donotdisturb__c == 'Si' ? 'Y' : 'N';
                        contactRelacionado[0].Contact.OfferingsRGPD__c = customer.Account_RGPD_Offerings__c == 'Si' ? 'Y' : 'N';
                        contactRelacionado[0].Contact.Proveniente_de_lead__c = true;

                        update contactRelacionado[0];
                        System.debug('contactRelacionado[0].Contact.IndividualId despues del update ' + contactRelacionado[0].Contact.IndividualId);

                    } else {
                        //lContactsRelations[0].Contact.IndividualId = customer.IndividualId;
                        //lContactsRelations[0].Contact.IndividualId = ind.Id;
                        lContactsRelations[0].Contact.DatacessionRGPD__c = customer.Account_RGPD_Datadisclosure__c == 'Si' ? 'Y' : 'N';
                        lContactsRelations[0].Contact.DonotdisturbRGPD__c = customer.Account_RGPD_Donotdisturb__c == 'Si' ? 'Y' : 'N';
                        lContactsRelations[0].Contact.OfferingsRGPD__c = customer.Account_RGPD_Offerings__c == 'Si' ? 'Y' : 'N';
                        lContactsRelations[0].Contact.Proveniente_de_lead__c = true;

                        System.debug('lContactsRelations[0].Contact.IndividualId ' + lContactsRelations[0].Contact.IndividualId);
                        update lContactsRelations[0];
                        System.debug('lContactsRelations[0].Contact.IndividualId  despues del update ' + lContactsRelations[0].Contact.IndividualId);

                    }
                    //Account cuenta = [SELECT Id, Email_con__c, MobilePhone1__c FROM Account WHERE Id =:sAccount.Id LIMIT 1];
                    //Contact contacto = [SELECT Id, Email, MobilePhone FROM Contact WHERE Id = :contactRelacionado[0].ContactId LIMIT 1];
                    //cuenta.TitularContactHide__c = contacto.Id;
                    sAccount.TitularContactHide__c = cntId;

                    System.debug('cntId ' + cntId);

                    System.debug('customer.IndividualId ' + customer.IndividualId);
                    Individual ind = null;
                    //if((lContactsRelations != null && lContactsRelations[0].Contact.IndividualId != null) || (contactRelacionado != null && contactRelacionado[0].Contact.IndividualId != null)){
                    if(cntId != null){
                        ind = [SELECT Id, DatacessionRGPD__c, DonotdisturbRGPD__c, OfferingsRGPD__c,
                                        FII_IND_SEL_Cesion__c, FII_IND_SEL_Molestar__c, FII_IND_SEL_Clientes__c
                               FROM Individual 
                               WHERE Contact__c = :cntId limit 1];
                        System.debug('Primer if individual ind ' + ind);
                        
                    } else if(customer.IndividualId != null){
                        ind = [SELECT Id, DatacessionRGPD__c, DonotdisturbRGPD__c, OfferingsRGPD__c,
                                        FII_IND_SEL_Cesion__c, FII_IND_SEL_Molestar__c, FII_IND_SEL_Clientes__c
                               FROM Individual 
                               WHERE Id = :customer.IndividualId limit 1];
                        System.debug('Segundo if individual ind ' + ind);

                    } else {
                        ind = new Individual();
                        ind.FirstName = customer.FirstName.toUpperCase();
                        ind.LastName = customer.LastName.toUpperCase();
                        insert ind;
                        System.debug('Tercer if individual ind ' + ind);
                    }

                    
                    List<AccountContactRelation> accCntRelAut = [SELECT Id, ContactId, Contact.Contact_method_cd__c, Contact.New_email__c,Contact.Email, Roles, Contact.IndividualId 
                                                                 FROM AccountContactRelation 
                                                                 WHERE AccountId = :sAccount.Id and Roles = 'Autorizado'];

                    System.debug('Contactos autorizados ' + accCntRelAut);

                    if(accCntRelAut != null && accCntRelAut.size() > 0){
                        List<Account> listAcc = [select toLabel(LicensedRol__c) from Account where Id = : sAccount.Id];
                        System.debug('listAcc ' + listAcc);
                        String rol = listAcc != null ? listAcc.get(0).LicensedRol__c : '';
                        System.debug('rol ' + rol);
                        accCntRelAut.get(0).Roles = rol;
                        update accCntRelAut;
                    }

                    ind.DatacessionRGPD__c = customer.Account_RGPD_Datadisclosure__c == 'Si' ? 'Y' : 'N';
                    ind.DonotdisturbRGPD__c = customer.Account_RGPD_Donotdisturb__c == 'Si' ? 'Y' : 'N';
                    ind.OfferingsRGPD__c = customer.Account_RGPD_Offerings__c == 'Si' ? 'Y' : 'N';

                    ind.FII_IND_SEL_Cesion__c = ind.DatacessionRGPD__c == 'Y' ? 'Si' : 'No';
                    ind.FII_IND_SEL_Molestar__c = ind.DonotdisturbRGPD__c == 'Y' ? 'Sí Ofertas' : 'No Ofertas';
                    ind.FII_IND_SEL_Clientes__c = ind.OfferingsRGPD__c == 'Y' ? 'Si' : 'No';

                    //ind.Contact__c = cntId;
                    System.debug('Individual ind antes de update ' + ind);

                    update ind;

                    System.debug('Individual ind despues de update ' + ind);

                    // Marcador: Se envía al marcador el id del lead, cuando los lookups de acc y contact están vacíos. 
                    // Por lo tanto, rellenando los lookups de cuenta y contacto, el marcador ya no recibirá el id del lead.
                    List<CampaignMember> listCampMemb = [SELECT Id, LeadId, FII_CAMM_LKP_AccountMember__c,
                                                                ContactId, TV_LKP_Contacto_Referido__c
                                                         FROM CampaignMember
                                                         WHERE LeadId = :customer.Id];

                    if(listCampMemb != null && listCampMemb.size() > 0){
                        //listCampMemb[0].LeadId = null;
                        //listCampMemb[0].FII_CAMM_LKP_AccountMember__c = sAccount != null ? sAccount.Id : null;
                        listCampMemb[0].TV_LKP_Cliente_Referido__c = sAccount != null ? sAccount.Id : null;
                        //listCampMemb[0].ContactId = cntId != null ? cntId : null;
                        listCampMemb[0].TV_LKP_Contacto_Referido__c = cntId != null ? cntId : null;
                        update listCampMemb[0];
                    }

                    dml = new Database.DMLOptions();
                    dml.DuplicateRuleHeader.AllowSave = true;
                    //Database.update(cuenta,dml);
                    System.debug('sAccount antes de update final ' + sAccount);
                    
                    Database.update(sAccount,dml);

                    sAccount.Proveniente_de_lead__c = true;
                    sAccount.FI_ACC_LKP_ParentLead__c = customer.Id;

                    update sAccount;

                }
            }else{
                customResults.put('status', 'ERROR');
                customResults.put('errormsg', Label.ERROR_ACCOUNT_ACCESS);
            }
        } catch(System.DmlException e) {
            customResults.put('status', 'ERROR');
            customResults.put('errormsg', e.getDmlMessage(0));
            System.debug('Convert Lead Error: ' + e);

            Database.rollback(sp);
        } catch(System.NullPointerException e) {
            customResults.put('status', 'ERROR');
            customResults.put('errormsg', e.getMessage());
            System.debug('Convert Lead Error: ' + e);

            Database.rollback(sp);
        }

       /*FJDM } catch (Exception e) {
            customResults.put('status', 'ERROR');
            customResults.put('errormsg', e.getMessage());
            Database.rollback(sp);
        }*/

        AccountTriggerHandler.bypassTrigger=false;
        AccountContactRelationTriggerHandler.bypassTrigger=false;
        ContactTriggerHandler.bypassTrigger=false;
        IndividualTriggerHandler.bypassTrigger=false;
        FII_TRG_Task_Handler.bypassTrigger=false;

        System.debug('customResults ' + customResults);
        return customResults;
    }

    public static void synchronizeInteraction(Id leadId, Id accountId, Id cntId){
        System.debug('FI_LCC_LeadToCustomer entrada synchronizeInteraction accountId ' + accountId);
        System.debug('synchronizeInteraction leadId ' + leadId);
        System.debug('synchronizeInteraction cntId ' + cntId);
        List<Task> taskList = [SELECT Id, WhoId, Contact__c, FII_ACT_LKP_RelatedClient__c FROM Task WHERE WhoId =: leadId AND Status = 'Open' AND FII_ACT_TXT_Main_interaction__c = null];
        List<AccountContactRelation> accCntRelList = [SELECT Id, Roles FROM AccountContactRelation WHERE ContactId = :cntId AND AccountId = :accountId];
        System.debug('FI_LCC_LeadToCustomer taskList ' + taskList);
        /*List<AccountContactRelation> lContactsR = [SELEC Id, ContactId, Contact.Contact_method_cd__c, Contact.New_email__c, Contact.Email, Roles
                                                   FROM AccountContactRelation 
                                                   WHERE AccountId = :sAccount.Id AND Roles = 'Titular de contrato' LIMIT 1];*/
        //List<Contact> lContacts = getContactsByDocumentNumber(nif); 
        //System.debug('FI_LCC_LeadToCustomer lContacts ' + lContacts);                                         
        for(Task t : taskList){
            t.FII_ACT_LKP_RelatedClient__c = accountId;
            /*if(lContacts != null && lContacts.size() > 0){
                t.WhoId = lContacts.get(0).Id;
            }*/
            t.WhoId = cntId;
            t.Contact__c = cntId;
            t.FII_ACT_TXT_Role__c = accCntRelList.size() > 0 ? accCntRelList.get(0).Roles : '';
        }
        System.debug('FI_LCC_LeadToCustomer update taskList');
        update taskList;
    }

    @AuraEnabled
    public static WS_Info__mdt getLoginData(){
        List<WS_Info__mdt> wsInfo = [SELECT DeveloperName, EndPoint__c, UserName__c, Password__c FROM WS_Info__mdt WHERE DeveloperName = 'TV_Genesys'];
        if(!wsInfo.isEmpty()){
            return wsInfo[0];
        }
        return null;
    }
    /**
     * @desc Find the Account by Document number and recordType
     * @param documentNumber The document
     * @return The list of founds Accounts
     */
    private static List<Account> getAccountsByDocumentNumber(String documentNumber) {
        if(Schema.SObjectType.Account.isAccessible()){
            return [
                    SELECT Id, Name
                            FROM Account
                            WHERE NIF_CIF_Customer_NIE__c = :documentNumber
                            AND IsDeleted = FALSE WITH SECURITY_ENFORCED
            ];
        }else{return null;}
    }
    /**
     * @desc Find the Contact by Document
     * @param documentNumber The document
     * @return The list of founds Contacts
     */
    private static List<Contact> getContactsByDocumentNumber(String documentNumber) {
        if(Schema.SObjectType.Contact.isAccessible() && Schema.SObjectType.AccountContactRelation.isAccessible()){
            List<AccountContactRelation> lContactsRelations = [SELECT Id, ContactId, Contact.Contact_method_cd__c
                    FROM AccountContactRelation WHERE Roles = 'Titular de contrato' AND Contact.NIF_CIF_Customer_NIE__c = :documentNumber LIMIT 1];
            return (lContactsRelations != null && !lContactsRelations.isEmpty())?[
                    SELECT Id, Name, AccountId
                            FROM Contact
                            WHERE NIF_CIF_Customer_NIE__c = :documentNumber
                            AND IsDeleted = FALSE WITH SECURITY_ENFORCED
            ]:new List<Contact>();
        }else{return null;}
    }
    /**
     * @desc Find the Client in SVE by document number
     * @param numIdentif The document number of the client
     * @return Any String with the posible values (Error, OK, KO)
     *          ERROR: Fail in SVE callout
     *          OK: The client exists in SVE
     *          KO: The client no exists in SVE
     */
    public static String callSVECustomer(String numIdentif, String tipIdentf) {
        String responseSVE = 'ERROR';
        
        String numUpper = numIdentif.toUpperCase();
        Account acc = new Account();
        acc.NIF_CIF_Customer_NIE__c = numUpper;
        acc.Identifier_Type__c = tipIdentf;
        adaptadorSVEWS contextWS = new adaptadorSVEWS();
        Map<String, Object> mapSVE = contextWS.locateCustomerSVETBCGSC005(acc);
        if (mapSVE != null) {
            xmlnsEndesaComWsdlEverestCrossTbcg.Cliente_element clien = (xmlnsEndesaComWsdlEverestCrossTbcg.Cliente_element) mapSVE.get('cliente');
            if (clien != null) {
                if (clien.TipoIndentificador != null) {
                    responseSVE = 'OK';
                } else {
                    responseSVE = 'KO';
                }
            }
        }
        /*
        //VYF ATOS - Se ha cambiado de WSDL TBCGSC005 por TBCGSC003 donde se añade un parametro Existe (SE COMENTAN ESTOS CAMBIOS)
        Map<String, Object> mapSVE = contextWS.locateCustomerSVETBCGSC003(acc);
        if (mapSVE != null) {
            xmlnsEndesaComWsdlEverestCrossTbcg.Cliente_element CLIEN = (xmlnsEndesaComWsdlEverestCrossTbcg.Cliente_element) mapSVE.get('cliente');
            if (CLIEN != null) {
                if (CLIEN.TipoIndentificador != null && CLIEN.Existe == 'Y') {
                    responseSVE = 'OK';
                } else {
                    responseSVE = 'KO';
                }
            }
        }*/
        return responseSVE;
    }

    /**
     * Custom class for the result for search the client
     * errorCallSVE: The callout to SVE return ERROR value
     * clientInSVE: if is true the call to SVE returned OK if it is false returned KO
     * documentInOthersAccountOrContacts: If is true exists Accounts or Contacts with the same document number of the client
     * lCustomerDatas: Contains the Datas of the Accounts or Contacts founds if documentInOthersAccountOrContacts is true
     * recordTypeClientId: Contains the recorType for the new Account
     */
    public class CustomerSearchWrapper {
        @AuraEnabled
        public Boolean errorCallSVE;
        @AuraEnabled
        public Boolean clientInSVE;
        @AuraEnabled
        public Boolean documentInOthersAccountOrContacts;
        @AuraEnabled
        public List<Map<String, String>> lCustomerDatas;
        @AuraEnabled
        public String recordTypeClientId;
    }
}