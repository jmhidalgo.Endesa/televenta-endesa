//////////////// START TEST everis - (CCM) 26/03/2019 //////////////// 
@isTest
public class SummaryKOCaseTest {
    
    /**
	* Nota, se comentan llamadas a validarFinalizarCaso porque eso ya lo hace un process builder,
	* gatillado al cambiar el estado a 'Resolved' del Case
	*/
        
    @testSetup
    public static void setup(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());

		final FII_Job__c job = new FII_Job__c();
		job.Name = 'Puesto Trabajo Pruebas';
		job.FII_PT_Canal__c = '54';
		insert job;

		final FII_UserJobRelation__c usj = new FII_UserJobRelation__c();
		usj.FII_UJR_LKP_Job__c = job.Id;
		usj.FII_UJR_LKP_User__c = UserInfo.getUserId();
		insert usj;

        Account account = new Account();
        account.Name = 'TestName';
        account.LastName__c='TestName';
        account.FirstName__c = 'TestName';
        account.Identifier_Type__c = 'NIF';
        account.NIF_CIF_Customer_NIE__c = '22753890J';
        account.Email_con__c = 'uno@dos.com';
        account.Contact_method_cd__c = 'SMS';
        account.Licensed__c = false;
        account.Cliente_External_Id__c = 'sgh-test-case-1';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
        account.Physical_or_legal_cd__c = 'Physical Customer';
        account.Account_language__c = 'Español';
        account.MobilePhone1__c = '654076192';
        account.No_Phone_flg__c = false;
        account.No_Email_flg__c = false;
        account.Identifier_Number__c = 224346;
        account.Account_status__c = 'Active';
        account.Account_Type__c = 'Residential';
        account.Electronic_bill_flg__c = true;
        account.Duplicate_convenience_flg__c = true;
        account.Name = 'SGH - Test CASE 1';
        insert account;
        
        B2C_CNAE__c cnae1 = new B2C_CNAE__c();
        cnae1.name = '9820';
        insert cnae1;
        
        B2C_Address__c dir = UtilTest.createAddress();
        dir.Adress_active_flg__c = true;
        dir.Name = 'Test1';
        dir.Account__c = account.id;
        dir.City__c = 'BARCELONA';
        dir.City_cd__c = 'CZZ00034';
        dir.Country_cd__c = 'ES';
        dir.County__c = 'BARCELONA';
        dir.County_cd__c = '019';
        dir.Premise_flg__c = true;
        dir.Province_text__c = 'BARCELONA';
        dir.Province_cd__c = '08';
        dir.Zipcode__c = '08003';
        dir.Street_number__c = '20';
        insert dir;
        
        account.Addr_id__c = dir.id;
        update account;
        
        B2C_Service_Point__c ps = new B2C_Service_Point__c();
        ps.Account_id__c = account.id;
        ps.AddrId_Id__c = dir.id;
        ps.CNAE__c = cnae1.id;
        ps.RecordTypeId = Schema.SObjectType.B2C_Service_Point__c.getRecordTypeInfosByName().get('Electricidad').getRecordTypeId();
        ps.Name = 'ES0021000021623755VA0F';
        ps.CUPS_Consulted_SVE__c = 'ES0021000021623755VA0F';
        ps.Business_line_cd__c = '01';
        ps.Distribution_Group_Elec__c='test';
        ps.DistribuitorElec_cd__c='0217';
        ps.Tariff_cd__c = '001';
        ps.Power_period_1_Kw__c = 123;
        ps.Tension_Type_cd__c = 'TRI';
        ps.Tension_cd__c = '06';
        ps.Power_Max_CIE__c = 123;
        ps.Meass_Equip_property__c = 'C';
        ps.Power_period_1_Kw__c=12;
        ps.Tariff_cd__c='003';
        ps.Reason_creation__c = 'No localizado';
        ps.Power_control_mode_cd__c='2';
        ps.Power_period_1_Kw__c=3.3;
        ps.Power_period_2_Kw__c=16;
        ps.Power_period_3_Kw__c=12;
        ps.Power_period_4_Kw__c=12;
        ps.Power_period_5_Kw__c=12;
        ps.Power_period_6_Kw__c=12;
        ps.PostalCode__c = '12345';
        ps.Account_id__c = account.id;
        ps.AddrId_Id__c = dir.id;
        ps.Power_Max_CIE__c = 30;
        ps.Street__c = 'bbb';
        ps.Street_number__c = '4';
        ps.Power_Ascribed_Ext__c = 0;
        ps.CNAE__c = cnae1.id;
        ps.Power_Ascribed_Ext__c = 1;
        ps.Meass_Equip_property__c = 'C';
		ServicePointTriggerHandler.bypassTrigger = true;
        insert ps;
		ServicePointTriggerHandler.bypassTrigger = false;
        
        B2C_Billing_Profile__c datosPago = UtilTest.createBillingProfile();
        datosPago.Account__c = account.Id;
        datosPago.CC_external_CRM_id__c = 'atr-ext-id-123';
        datosPago.Account_Payment_Holder__c = account.Id;
        insert datosPago;
        
        Contact con1 = new Contact();
        con1.FirstName = 'Test12';
        con1.LastName = 'Test22';
        con1.Email = 'tes2t@gmail.com';
        con1.Work_email__c = 'tes2t@gmail.com';
        con1.Document_Type__c = 'NIF';
        con1.NIF_CIF_Customer_NIE__c = '96640713A';
        con1.Contact_method_cd__c='E-mail';
        con1.No_Phone_flg__c = false;
        con1.Phone = '954076188';
        con1.No_Email_flg__c = false;
        con1.Nationality_cd__c = 'ES';
        con1.Contact_Robinson__c = true;
        con1.Language__c = 'Español';
        con1.AccountId = account.id;
        con1.Physical_or_legal_cd__c = 'Physical Customer';
        insert con1;
        
        NE__Quote__c ql1 = new NE__Quote__c();
        ql1.NE__AccountId__c = account.Id;
        NE__Quote__c ql2 = new NE__Quote__c();
        ql2.NE__AccountId__c = account.Id;
        final List<NE__Quote__c> qlInsert = new List<NE__Quote__c>{ql1,ql2};
        NEQuoteLineTriggerHandler.bypassTrigger = true;
        insert qlInsert;
        NEQuoteLineTriggerHandler.bypassTrigger = false;
        
        NE__Order__c conf = new NE__Order__c();
        conf.RecordTypeId = Schema.SObjectType.NE__Order__c.getRecordTypeInfosByDeveloperName().get('Quote').getRecordTypeId();
        conf.NE__AccountId__c = account.Id;
        conf.NE__Quote__c = ql1.Id;
        insert conf;
        
        NE__Product__c product = new NE__Product__c();
        product.RecordTypeId = Schema.SObjectType.NE__Product__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        product.Type__c = 'Product';
        product.Code__c = 'TEE2D';
        product.Name='TEE2D - TEMPO SIEMPRE GANAS';
        insert product;
        
        NE__OrderItem__c ci = new NE__OrderItem__c();
        ci.FI_CI_DAT_SalesDate__c = Date.Today();
        ci.FI_CI_FLG_Configured__c = true;
        ci.FI_CI_FLG_Critical_movement__c = true;
        ci.FI_CI_FLG_DeferredShipping__c = true;
        ci.FI_CI_FLG_Electronicinvoice__c = true;
        ci.FI_CI_FLG_Validaciones_OK__c = true;
        ci.FI_CI_LKP_Address__c = dir.Id;
        ci.FI_CI_LKP_CNAE__c = cnae1.Id;
		ci.FI_CI_LKP_CNAE_to_hire__c = cnae1.Id;
		ci.FI_CI_LKP_CUPS__c = ps.Id;
		ci.FI_CI_LKP_PaytmenDatas__c = datosPago.Id;
		ci.FI_CI_LKP_Quote__c = ql1.Id;
		ci.FI_CI_LKP_Related_Contact__c = con1.Id;
        ci.FI_CI_NUM_Design_Power__c = 3.3;
        ci.FI_CI_NUM_Maximum_period_power__c = 3.3;
        ci.FI_CI_NUM_PowerToHire1kW__c = 3.3;
        ci.FI_CI_SEL_Access_channel__c = '54';
        ci.FI_CI_SEL_ActionType__c = '01';
        ci.FI_CI_SEL_Business_Line__c = '01';
        ci.FI_CI_SEL_Company_holder__c = '20';
        ci.FI_CI_SEL_ContractType__c = '01';
        ci.FI_CI_SEL_Current_ATR_Rate__c = '001';
		ci.FI_CI_SEL_Current_hourly_discrimination__c = 'Sin DH';
		ci.FI_CI_SEL_Current_Power_control_mode__c = '1';        
		ci.FI_CI_SEL_Current_Tension_cd__c = '02';
		ci.FI_CI_SEL_Current_Tension_Type_cd__c = 'MONO';
		ci.FI_CI_SEL_DateModelEfect__c = 'A';        
        ci.FI_CI_SEL_Distribuidora__c = '0024';
		ci.FI_CI_SEL_FacturationPeriod__c = '02';
        ci.FI_CI_SEL_Hourly_discrimination_to_hire__c = 'Con DH';
		ci.FI_CI_SEL_MeasureEquipmentToHireProp__c = 'C';
        ci.FI_CI_SEL_Nationality__c = 'ES';
        ci.FI_CI_SEL_PhysicalOrLegalCd__c = 'Physical Customer';
        ci.FI_CI_SEL_PowerControlMode_ToHire__c = '1';
        ci.FI_CI_SEL_PrintingLanguage__c = 'ES';
        ci.FI_CI_SEL_Province__c = '08';
        ci.FI_CI_SEL_RateATRTollGroup__c = '004';
        ci.FI_CI_SEL_SelfConsumption_ToHire__c = '00';
        ci.FI_CI_SEL_ShippingAddressType__c = 'Client';
		ci.FI_CI_SEL_Subtype__c = 'AD1';
        ci.FI_CI_SEL_Tension_ToHire__c = '02';
        ci.FI_CI_SEL_TensionTypeToHire__c = 'MONO';
		ci.FI_CI_SEL_Type__c = 'A';
		ci.FI_CMP_SEL_Commodity__c = 'Luz';
		ci.FI_CMP_SEL_Opt_Energy__c = 'SI';
		ci.FI_CMP_SEL_Opt_Power__c = 'NO';
        ci.NE__Account__c = account.Id;
        ci.NE__Action__c = 'Add';
        ci.NE__Billing_Account__c = account.Id;
        ci.NE__Generate_Asset_Item__c = true;
        ci.NE__OrderId__c   = conf.Id;
        ci.NE__Picked_By__c = 'Human';
        ci.NE__ProdId__c = product.Id;
        ci.NE__Qty__c = 1;
        ci.NE__Service_Account__c = account.Id;
        ci.NE__Status__c = Label.FI_CI_STATUS_PENDING;
        
        FI_TRG_NE_OrderItem_Handler.bypassTrigger = true;
        insert ci;
        FI_TRG_NE_OrderItem_Handler.bypassTrigger = false;
        Test.stopTest();
    }
    
    @Istest
    public static void KO() {
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__Quote__c ql1 = [SELECT Id FROM NE__Quote__c LIMIT 1];
        Account acc1 = [SELECT Id, Name, Identifier_Type__c, Nationality_cd__c, Cliente_External_Id__c, NIF_CIF_Customer_NIE__c, Physical_or_legal_cd__c, NE__Active__c, Last_update_of_default_risk_date__c FROM Account LIMIT 1];
        Contact con1 = [SELECT Id FROM Contact LIMIT 1];
        
        Case caseNeed = new Case();
        caseNeed.AccountId = acc1.Id;
        caseNeed.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FII_RT_CAS_Need').getRecordTypeId();
        caseNeed.ContactId = con1.Id;
        insert caseNeed;

        Case caseCC = new Case();
        caseCC.AccountId = acc1.Id;
        caseCC.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Caso_General_Control_de_Calidad').getRecordTypeId();
        caseCC.FI_CAS_LKP_CaseNeed__c = caseNeed.Id;
        caseCC.FI_CAS_FLG_Control_de_calidad__c = true;
        caseCC.OwnerId = UserInfo.getUserId();
        insert caseCC;
        
        ql1.Case__c = caseNeed.id;
        ql1.FI_NEQ_LKP_QaCase__c = caseCC.id;
        ql1.Apex_Context__c = true;
        NEQuoteLineTriggerHandler.bypassTrigger = true;
        update ql1;
        NEQuoteLineTriggerHandler.bypassTrigger = false;
        
        caseCC.Status = Label.FI_CAS_STATUS_RESOLVED;
        caseCC.FI_CAS_SEL_resultado__c = 'KO';

        SummaryKOCase.updateCaseCC(caseCC, acc1);

        Case caseVerify = [SELECT Id, Status FROM Case WHERE Id =: caseCC.Id];
        System.assertEquals(Label.FI_CAS_STATUS_PENDING, caseVerify.Status, 'El estado del caso no es el esperado');
        
        Test.stopTest();
    }
    
    @isTest
    public static void KOConRegistro() {
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        NE__Quote__c ql1 = [SELECT Id FROM NE__Quote__c LIMIT 1];
        Account acc1 = [SELECT Id, Name, Identifier_Type__c, Nationality_cd__c, Cliente_External_Id__c, NIF_CIF_Customer_NIE__c, Physical_or_legal_cd__c, NE__Active__c, Last_update_of_default_risk_date__c FROM Account LIMIT 1];
        Contact con1 = [SELECT Id FROM Contact LIMIT 1];
        
        Case caseNeed = new Case();
        caseNeed.AccountId = acc1.Id;
        caseNeed.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FII_RT_CAS_Need').getRecordTypeId();
        caseNeed.ContactId = con1.Id;
        insert caseNeed;

        Case caseCC = new Case();
        caseCC.AccountId = acc1.Id;
        caseCC.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Caso_General_Control_de_Calidad').getRecordTypeId();
        caseCC.FI_CAS_LKP_CaseNeed__c = caseNeed.Id;
        caseCC.FI_CAS_FLG_Control_de_calidad__c = true;
        caseCC.OwnerId = UserInfo.getUserId();
        insert caseCC;
        
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Status = 'Open';
        t.Priority = 'Normal';
        t.WhatId = caseCC.id;
        t.Stage__c = 'Control Visual - IG';
        t.Quality_error__c = 'INEXISTENCIA DE CONTRATO';
        t.FI_ACT_LKP_OrderItem__c = ci.Id;
        t.Quality_control_result__c = 'KO';
        insert t;
        
        ql1.Case__c = caseNeed.id;
        ql1.FI_NEQ_LKP_QaCase__c = caseCC.id;
        ql1.Apex_Context__c = true;
        NEQuoteLineTriggerHandler.bypassTrigger = true;
        update ql1;
        NEQuoteLineTriggerHandler.bypassTrigger = false;
        
        caseCC.Status = Label.FI_CAS_STATUS_RESOLVED;
        caseCC.FI_CAS_SEL_resultado__c = 'KO';

        SummaryKOCase.updateCaseCC(caseCC, acc1);
        
        Case caseVerify = [SELECT Id, Status FROM Case WHERE Id =: caseCC.Id];
        System.assertEquals(caseVerify.Status, Label.FI_CAS_STATUS_RESOLVED);
        
        Test.stopTest();
    }
    
    @isTest
    public static void OKsinTodosOK() {
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        NE__Quote__c ql1 = [SELECT Id FROM NE__Quote__c LIMIT 1];
        Account acc1 = [SELECT Id, Name, Identifier_Type__c, Nationality_cd__c, Cliente_External_Id__c, NIF_CIF_Customer_NIE__c, Physical_or_legal_cd__c, NE__Active__c, Last_update_of_default_risk_date__c FROM Account LIMIT 1];
        Contact con1 = [SELECT Id FROM Contact LIMIT 1];

        Case caseNeed = new Case();
        caseNeed.AccountId = acc1.Id;
        caseNeed.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FII_RT_CAS_Need').getRecordTypeId();
        caseNeed.ContactId = con1.Id;
        insert caseNeed;

        Case caseCC = new Case();
        caseCC.AccountId = acc1.Id;
        caseCC.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Caso_CCPP_Control_Calidad').getRecordTypeId();
        caseCC.FI_CAS_LKP_CaseNeed__c = caseNeed.Id;
        caseCC.FI_CAS_FLG_Control_de_calidad__c = true;
        caseCC.OwnerId = UserInfo.getUserId();
        insert caseCC;
        
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Status = 'Open';
        t.Priority = 'Normal';
        t.WhatId = caseCC.id;
        t.Stage__c = 'Control Visual - IG';
        t.FI_ACT_LKP_OrderItem__c = ci.Id;
        t.Quality_control_result__c = 'OK';
        insert t;
        
        ql1.Case__c = caseNeed.id;
        ql1.FI_NEQ_LKP_QaCase__c = caseCC.id;
        ql1.Apex_Context__c = true;
        NEQuoteLineTriggerHandler.bypassTrigger = true;
        update ql1;
        NEQuoteLineTriggerHandler.bypassTrigger = false;
        
        caseCC.status = Label.FI_CAS_STATUS_RESOLVED;
        caseCC.FI_CAS_SEL_resultado__c = 'OK';
                
        SummaryKOCase.updateCaseCC(caseCC, acc1);

        Case caseVerify = [SELECT Id, Status FROM Case WHERE Id =: caseCC.Id];
        System.assertEquals(Label.FI_CAS_STATUS_PENDING, caseVerify.Status, 'El estado del caso no es el esperado');
        
        Test.stopTest();
    }
    
    @isTest
    public static void generateQACase_Test() {
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__Quote__c ql1 = [SELECT Id, Access_channel__c FROM NE__Quote__c LIMIT 1];
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        Contact con1 = [SELECT FirstName, LastName, Email, Work_email__c, Document_Type__c, NIF_CIF_Customer_NIE__c, Contact_method_cd__c, No_Phone_flg__c, Phone,
                        No_Email_flg__c, Nationality_cd__c, Contact_Robinson__c, Language__c, AccountId, Physical_or_legal_cd__c FROM Contact LIMIT 1];
        Account acc1 = [SELECT Id FROM Account LIMIT 1];
        
        Case caseNeed = new Case();
        caseNeed.AccountId = acc1.Id;
        caseNeed.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FII_RT_CAS_Need').getRecordTypeId();
        caseNeed.ContactId = con1.Id;
        insert caseNeed;

        ql1.Case__c = caseNeed.id;
        ql1.Apex_Context__c = true;
        NEQuoteLineTriggerHandler.bypassTrigger = true;
        update ql1;
        NEQuoteLineTriggerHandler.bypassTrigger = false;

        Map<Id, NE__Quote__c> mapQuotes = new Map<Id, NE__Quote__c>();
        Map<Id, NE__OrderItem__c> mapConfigItem = new Map<Id, NE__OrderItem__c>();        
        mapQuotes.put(ql1.Id, ql1);
        mapConfigItem.put(ci.Id, ci);
        
        Case qaCase = SummaryKOCase.generateQACase(mapConfigItem, caseNeed, con1);
        
        System.assertNotEquals(qaCase, null);
        Test.stopTest();
    }
    
    @isTest
    public static void nuevoMetodo() {
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        NE__Quote__c ql1 = [SELECT Id FROM NE__Quote__c LIMIT 1];
        Account acc1 = [SELECT Id, Name, Identifier_Type__c, Nationality_cd__c, Cliente_External_Id__c, NIF_CIF_Customer_NIE__c, Physical_or_legal_cd__c, NE__Active__c, Last_update_of_default_risk_date__c FROM Account LIMIT 1];
        Contact con1 = [SELECT Id FROM Contact LIMIT 1];

        Case caseNeed = new Case();
        caseNeed.AccountId = acc1.Id;
        caseNeed.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FII_RT_CAS_Need').getRecordTypeId();
        caseNeed.ContactId = con1.Id;
        insert caseNeed;

        Case caseCC = new Case();
        caseCC.AccountId = acc1.Id;
        caseCC.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Caso_General_Control_de_Calidad').getRecordTypeId();
        caseCC.FI_CAS_LKP_CaseNeed__c = caseNeed.Id;
        caseCC.FI_CAS_FLG_Control_de_calidad__c = true;
        caseCC.OwnerId = UserInfo.getUserId();
        insert caseCC;
        
        Task t = new Task();
        t.OwnerId = UserInfo.getUserId();
        t.Status = 'Open';
        t.Priority = 'Normal';
        t.WhatId = caseCC.id;
        t.Stage__c = 'Control Visual - IG';
        t.Quality_error__c = 'INEXISTENCIA DE CONTRATO';
        t.FI_ACT_LKP_OrderItem__c = ci.Id;
        t.Quality_control_result__c = 'KO';
        insert t;
        
        ql1.Case__c = caseNeed.id;
        ql1.FI_NEQ_LKP_QaCase__c = caseCC.id;
        ql1.Apex_Context__c = true;
        NEQuoteLineTriggerHandler.bypassTrigger = true;
        update ql1;
        NEQuoteLineTriggerHandler.bypassTrigger = false;
        
        caseCC.Status = Label.FI_CAS_STATUS_RESOLVED;
        caseCC.FI_CAS_SEL_resultado__c = 'KO';

        SummaryKOCase.updateCaseCC(caseCC, acc1);

        Case caseVerify = [SELECT Id, Status FROM Case WHERE Id =: caseCC.Id];
        System.assertEquals(caseVerify.Status, Label.FI_CAS_STATUS_RESOLVED);

        Test.stopTest();
    }
    
    @isTest
    public static void newMethod() {
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        NE__Quote__c ql1 = [SELECT Id FROM NE__Quote__c LIMIT 1];
        Account acc1 = [SELECT Id FROM Account LIMIT 1];
        Contact con1 = [SELECT Id FROM Contact LIMIT 1];

        Case caseNeed = new Case();
        caseNeed.AccountId = acc1.Id;
        caseNeed.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FII_RT_CAS_Need').getRecordTypeId();
        caseNeed.ContactId = con1.Id;
        insert caseNeed;

        Case caseCC = new Case();
        caseCC.AccountId = acc1.Id;
        caseCC.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Caso_General_Control_de_Calidad').getRecordTypeId();
        caseCC.FI_CAS_LKP_CaseNeed__c = caseNeed.Id;
        caseCC.FI_CAS_FLG_Control_de_calidad__c = true;
        caseCC.OwnerId = UserInfo.getUserId();
        insert caseCC;
        
        Task task = new Task();
        task.OwnerId = UserInfo.getUserId();
        task.Status = 'Open';
        task.Priority = 'Normal';
        task.WhatId = caseCC.id;
        task.Stage__c = 'Control Visual - IG';
        task.Quality_error__c = 'None';
        task.FI_ACT_LKP_OrderItem__c = ci.Id;
        task.Quality_control_result__c = 'OK';
        insert task;
        
        ql1.Case__c = caseNeed.id;
        ql1.FI_NEQ_LKP_QaCase__c = caseCC.id;
        ql1.Apex_Context__c = true;
        NEQuoteLineTriggerHandler.bypassTrigger = true;
        update ql1;
        NEQuoteLineTriggerHandler.bypassTrigger = false;
        
        caseCC.Status = Label.FI_CAS_STATUS_RESOLVED;
        caseCC.FI_CAS_SEL_resultado__c = 'KO';

        SummaryKOCase.updateCaseCC(caseCC, acc1);
        
        Case caseVerify = [SELECT Id, Status FROM Case WHERE Id =: caseCC.Id];
        System.assertEquals(caseVerify.Status, 'Pending');
        
        Test.stopTest();
    }
    
    @isTest
    public static void checkCaseConditionsTest(){
        
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        NE__Quote__c ql1 = [SELECT Id, NE__Status__c FROM NE__Quote__c LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        
        Case c = new Case();
        c.AccountId = acc.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Caso TF Papel Físico').getRecordTypeId();
        insert c;

        Map<String, String> auxQuoteStatus = new Map<String, String>();
        auxQuoteStatus.put(ql1.Id, 'withoutInc');
        
        List<NE__OrderItem__c> configItems = new List<NE__OrderItem__c>();
		ci.FI_CI_FLG_Skip_Validations__c = true;
        configItems.add(ci);
        
        SummaryKOCase.checkCaseConditions(auxQuoteStatus, false, c, configItems, acc);
        
        Case caseAfterUpdate = [SELECT Id, Status FROM Case LIMIT 1];
        System.assertEquals(caseAfterUpdate.Status, 'Pending');
        
        Test.stopTest();
    }
    
    @isTest
    public static void checkCaseConditionsTestPending(){
        
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        NE__Quote__c ql1 = [SELECT Id, NE__Status__c FROM NE__Quote__c LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        
        Case c = new Case();
        c.AccountId = acc.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Caso TF Papel Físico').getRecordTypeId();
        insert c;
        
        Map<String, String> auxQuoteStatus = new Map<String, String>();
        auxQuoteStatus.put(ql1.Id, 'recoverPending');
        
        List<NE__OrderItem__c> configItems = new List<NE__OrderItem__c>();
		ci.FI_CI_FLG_Skip_Validations__c = true;
        configItems.add(ci);
        
        SummaryKOCase.checkCaseConditions(auxQuoteStatus, true, c, configItems, acc);
        
        String caseStatus = [SELECT Id, Status FROM Case WHERE Id = :c.Id].Status;
        System.assertEquals(caseStatus, 'Pending');
        
        Test.stopTest();
    }
    
    @isTest
    public static void checkCaseConditionsTestRecoveryPending(){
        
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        NE__Quote__c ql1 = [SELECT Id, NE__Status__c FROM NE__Quote__c LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];

        Case caseNeed = new Case();
        caseNeed.AccountId = acc.Id;
        caseNeed.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FII_RT_CAS_Need').getRecordTypeId();
        insert caseNeed;
        
        Case c = new Case();
        c.AccountId = acc.Id;
        c.FI_CAS_LKP_CaseNeed__c = caseNeed.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Caso_General_Control_de_Calidad').getRecordTypeId();
        insert c;
        
        Map<String, String> auxQuoteStatus = new Map<String, String>();
        auxQuoteStatus.put(ql1.Id, 'ko');
        
        List<NE__OrderItem__c> configItems = new List<NE__OrderItem__c>();
 		ci.FI_CI_FLG_Skip_Validations__c = true;
        configItems.add(ci);
        
        SummaryKOCase.checkCaseConditions(auxQuoteStatus, false, c, configItems, acc);
        
        String caseStatus = [SELECT Id, Status FROM Case WHERE Id = :c.Id].Status;
        System.assertEquals(caseStatus, 'Pending');
        
        Test.stopTest();
    }
    
    @isTest
    public static void checkCaseConditionsTestRecoveryOk(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        NE__Quote__c ql1 = [SELECT Id, NE__Status__c FROM NE__Quote__c LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        
        Case c = new Case();
        c.AccountId = acc.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Caso_General_Control_de_Calidad').getRecordTypeId();
        c.FI_CAS_SEL_resultado__c = 'KO';
        insert c;
        
        Map<String, String> auxQuoteStatus = new Map<String, String>();
        auxQuoteStatus.put(ql1.Id, 'Ok');
        
        List<NE__OrderItem__c> configItems = new List<NE__OrderItem__c>();
		ci.FI_CI_FLG_Skip_Validations__c = true;
        configItems.add(ci);
        
        SummaryKOCase.checkCaseConditions(auxQuoteStatus, false, c, configItems, acc);
        SummaryKOCase.checkCaseConditions(auxQuoteStatus, true, c, configItems, acc);
        
        String caseStatus = [SELECT Id, Status FROM Case WHERE Id = :c.Id].Status;
        System.assertEquals(caseStatus, 'Pending');
        
        Test.stopTest();
    }
    
    @isTest
    public static void checkCaseConditionsTestOneQuoteOk() {
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        
        NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
		ci.FI_CI_FLG_Skip_Validations__c = true;
        ci.NE__Status__c = 'Cancelled';
        update ci;
        
        NE__Quote__c ql1 = [SELECT Id, NE__Status__c FROM NE__Quote__c LIMIT 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        
        Case c = new Case();
        c.AccountId = acc.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Caso_General_Control_de_Calidad').getRecordTypeId();
        c.FI_CAS_SEL_resultado__c = 'OK';
        insert c;
        
        Map<String, String> auxQuoteStatus = new Map<String, String>();
        auxQuoteStatus.put(ql1.Id, 'ok');
        
        List<NE__OrderItem__c> configItems = new List<NE__OrderItem__c>();
		ci.FI_CI_FLG_Skip_Validations__c = true;
        configItems.add(ci);
        
        SummaryKOCase.checkCaseConditions(auxQuoteStatus, false, c, configItems, acc);
        
        String caseStatus = [SELECT Id, Status FROM Case WHERE Id = :c.Id].Status;
        System.assertEquals(caseStatus, 'Pending');
        
        Test.stopTest();
    }
    
    @isTest
    public static void generateCQCaseTest() {
        Test.startTest();
        Account acc1 = [SELECT Id FROM Account LIMIT 1];
        final Case c = new Case();
        c.AccountId = acc1.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FII_RT_CAS_Need').getRecordTypeId();
        insert c;
        
        final NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        SummaryKOCase.generateCQCase(c, ci, null, 'FII_RT_CAS_Need', null);
        Test.stopTest();
    }
    
    
    @isTest
    public static void checkMatrixTest() {
        Test.startTest();
        Account acc1 = [SELECT Id FROM Account LIMIT 1];
        final Case c = new Case();
        c.AccountId = acc1.Id;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('FII_RT_CAS_Need').getRecordTypeId();
        insert c;
        
        final List<Third_matrix__mdt> matrix = [SELECT Id,
	        									       Business_line__c,
													   Case_type__c,
													   Channel__c,
													   Company_Holder__c,
													   Critical_movement__c,
													   Destination_product__c,
													   Entitlement_Name__c,
													   Language__c,
													   MasterLabel,
													   Movement_type__c,
													   Product_origin__c,
													   Product_type__c,
													   Remunerable__c,
													   SubChannel__c,
													   Subtype_of_movement__c,
													   Supplier__c
        								  		FROM Third_matrix__mdt];

        final NE__OrderItem__c ci = (NE__OrderItem__c) Utilities.getConfigurationItem([SELECT Id FROM NE__OrderItem__c LIMIT 1].Id);
        ci.FI_CI_SEL_Access_channel__c = matrix.get(0).Channel__c;
        ci.FI_CI_TXT_PDS_Ocaps_TF__c = matrix.get(0).SubChannel__c;
        ci.FI_CI_SEL_Access_channel__c = matrix.get(0).Channel__c;
        ci.FI_CI_SEL_Business_Line__c = matrix.get(0).Business_line__c;
        ci.FI_CI_SEL_Supplier__c = matrix.get(0).Supplier__c;
        ci.FI_CI_SEL_Type__c = matrix.get(0).Movement_type__c;
        ci.FI_CI_SEL_Subtype__c = matrix.get(0).Subtype_of_movement__c;
		ci.FI_CI_FLG_Skip_Validations__c = true;

        Map<Id, NE__OrderItem__c> configurationItem = new Map<Id, NE__OrderItem__c>();
        configurationItem.put(ci.Id, ci);
        Third_matrix__mdt mtdmatrix = SummaryKOCase.checkMatrix(configurationItem, null);
        
        Test.stopTest();    
    }
}
//////////////// END TEST everis - (CCM) 26/03/2019 ////////////////