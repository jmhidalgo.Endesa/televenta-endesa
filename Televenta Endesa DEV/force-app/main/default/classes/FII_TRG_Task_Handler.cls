/**
* @author Everis
* @since November 20, 2018 
* @desc Actions over tasks when created 
* @history November 20, 2018 - everis (LJG) - Create Apex Class 
* @history January 16, 2019 - everis (LJG) - Modify Apex Class (development of before insert method, relating case needs with interactions)
* @history February 26, 2019 - everis (JVC) - Refact Apex Class
* @history August 19, 2019 - everis (JHG) - Add Quenda methods on before insert
* @history      2020/03/26  |  COS-83  |  ATOS (ABS)  |  Vinculate case to interacction in second line.
*/
public without sharing class FII_TRG_Task_Handler implements TriggerInterface{
    
    @TestVisible public static Boolean bypassTrigger = false;
    private static final String CONTACT_LASTNAME = 'Pendiente de Informar';
    private static final String CONTACT_FIRSTNAME = 'Contacto';
    private static final Id IDRT_ACT_INTERACTION = FII_Util_MasterData_RecordTypes.getByDevName(Label.FII_RT_TSK_INTERACTION).Id;
    public void beforeInsert (List<SObject> newItems) {
        
        if (bypassTrigger) { 
            System.debug('BYPASS TRIGGER BeforeInsert Task');
            return; 
        }

        // START COS-83
        Map<Integer,Id> mapIndexTaskIdAccount = new Map<Integer,Id>();
        Map<Id,Integer> mapIdAccountIndexTask = new Map<Id,Integer>();
        Map<Integer,Id> mapIndexTaskIdCase = new Map<Integer,Id>();
        // END COS-83
        
        //STAR TELEVENT
        //Set<Id>                contactSetId        = new Set<Id>();
        Set<Id>                accountSetId        = new Set<Id>();
        List<AccountContactRelation>   contactList        = new List<AccountContactRelation>();
        CampaignMember            campaignMemberItem;
        List<CampaignMember>       campaignMemberList    = new List<CampaignMember>();
        List<FII_UserJobRelation__c>   userJobRelationList    = new List<FII_UserJobRelation__c>();
        List<TV_CallClassification__c> callClassificationList;
		if(Schema.sObjectType.TV_CallClassification__c.isAccessible()){        
        	callClassificationList = [ SELECT Id, TV_CCL_TXT_Code__c from TV_CallClassification__c where TV_CCL_TXT_Code__c != null];
        }
        //END TELEVE 

        List<Task> mainInteCCDDList = new List<Task>();
        
        for(Task t : (List <Task>) newItems){
            //START TELEVENT
          if( ( t.TV_Campana_Marcador__c != null ) ){
                t.FII_ACT_SEL_Channel__c         = 'Phone';
          t.FII_ACT_SEL_Interaction_Category__c = 'Interaction';
          t.FII_ACT_SEL_Interaction_Type__c     = 'Input';
                   
                If ( t.TV_ACT_TXT_ResultCodeMarcador__c == null ){
                    t.Status  = 'Open';
                    t.Subject = 'Task Interaction: '+t.Subject;
                } else {
                    t.Subject                 = 'Informatica Cloud Interaction';
                    t.Status                  = 'Completed';
                    t.FII_TSK_TXT_MediumPresentationCI__c = 'IVR';
          t.FII_ACT_TXT_Type__c                 = 'Call';

                    If ( !callClassificationList.isEmpty() ){//4
                        For ( TV_CallClassification__c callClassificationItem: callClassificationList ){//3
                            If( callClassificationItem.TV_CCL_TXT_Code__c == t.TV_ACT_TXT_ResultCodeMarcador__c ){//1
                                t.TV_ACT_LKP_CallClassification__c            = callClassificationItem.Id;
                            }//1
                        }//3
                    }//4  
                }
                If ( t.FII_ACT_TXT_CampaignMemberId__c != null ){//2
                    campaignMemberItem     = new CampaignMember();
                    campaignMemberItem.Id  = t.FII_ACT_TXT_CampaignMemberId__c;                    
                    If ( !callClassificationList.isEmpty() ){//4
                        If ( t.TV_ACT_TXT_ResultCodeMarcador__c != null ){
                            campaignMemberItem.TV_CAMM_LKP_CallClassification__c = t.TV_ACT_LKP_CallClassification__c;
                        }
                    }//4
                    campaignMemberItem.TV_TXT_AgrupadorCampana__c = t.TV_ACT_TXT_AgrupadorCampana__c;
                    If(campaignMemberItem.TV_CAMM_LKP_CallClassification__c != null || campaignMemberItem.TV_TXT_AgrupadorCampana__c != null ){
                        campaignMemberList.add( campaignMemberItem );
                    }                    
                }//2
                   
                If ( t.Contact__c != null && t.TV_ACT_LKP_LeadID__c != null ){
                    t.WhoId = t.Contact__c;
                    //contactSetId.add(t.Contact__c);
                    accountSetId.add(t.FII_ACT_LKP_RelatedClient__c);
                } else if( t.Contact__c != null && t.TV_ACT_LKP_LeadID__c == null ){
                    t.WhoId = t.Contact__c;
                    //contactSetId.add(t.Contact__c);
                    accountSetId.add(t.FII_ACT_LKP_RelatedClient__c);
                } else if( t.Contact__c == null && t.TV_ACT_LKP_LeadID__c != null ){
                    t.WhoId = t.TV_ACT_LKP_LeadID__c;
                }                     
            }
            //END TELEVENT 
            
            if(t.FII_ACT_SEL_Channel__c == 'CCPP' && t.FII_ACT_TXT_FederationId__c != null && t.FII_ACT_TXT_Main_interaction__c == null && t.RecordTypeId == IDRT_ACT_INTERACTION){
                mainInteCCDDList.add(t);

            }
            // START COS-83
            if (Schema.sObjectType.Task.isAccessible()){
                List<Task> lstAux = new List<Task>();
                lstAux = (List <Task>) newItems;
                if(!mapIndexTaskIdAccount.ContainsKey(lstAux.indexOf(t)) && t.FII_ACT_TXT_Service__c == 'VENTA CRUZADA' && t.FII_ACT_LKP_RelatedClient__c != null){
                    mapIndexTaskIdAccount.put(lstAux.indexOf(t) ,t.FII_ACT_LKP_RelatedClient__c);
                    mapIdAccountIndexTask.put(t.FII_ACT_LKP_RelatedClient__c,lstAux.indexOf(t));
                }
            }
            // END COS-83
        }
        
        //START TELEVENT
        if ( Schema.sObjectType.CampaignMember.isUpdateable() ) {
            If ( !campaignMemberList.isEmpty() ){
                update campaignMemberList;
            }
        }
        Id userId = UserInfo.getUserId();
        if(Schema.sObjectType.FII_UserJobRelation__c.isAccessible()){
        	userJobRelationList = [ SELECT id, FII_UJR_LKP_Job__r.FII_PT_Platform__c, 
                                           FII_UJR_LKP_Job__r.FII_PT_Provider__c, FII_UJR_LKP_User__c
                                    FROM   FII_UserJobRelation__c
                                    Where  FII_UJR_LKP_User__c = :userId
                                    Order  By createddate desc
                                  ];
            If( !userJobRelationList.isEmpty() ){
                for(Task taskItem :  (List <Task>) newItems ){
                    for(FII_UserJobRelation__c userJobRelationItem: userJobRelationList){
                        taskItem.FII_ACT_TXT_Platform__c = userJobRelationItem.FII_UJR_LKP_Job__r.FII_PT_Platform__c;
                        taskItem.TV_ACT_TXT_Provider__c  = userJobRelationItem.FII_UJR_LKP_Job__r.FII_PT_Provider__c;
                    }
                }
            }
        }
        //If( !contactSetId.isEmpty() ){
        If( !accountSetId.isEmpty() ){
            if(Schema.sObjectType.AccountContactRelation.isAccessible()){
                contactList =[SELECT ContactId, Roles, AccountId
                              FROM   AccountContactRelation
                              WHERE  AccountId IN :accountSetId and Roles = 'Titular de contrato'
                             ];
                If( !contactList.isEmpty() ){
                    for(Task taskItem : (List <Task>) newItems){
                        for(AccountContactRelation contactItem: contactList){
                            If( taskItem.FII_ACT_LKP_RelatedClient__c == contactItem.AccountId ){
                                taskItem.FII_ACT_TXT_Role__c = contactItem.Roles;
                                taskItem.Contact__c = contactItem.ContactId;
                                taskItem.whoid = contactItem.ContactId;
                            }
                        }
                    }
                }
            }
        }
        //END TELEVENT
        //
        // START COS-83
        if(Schema.sObjectType.case.isAccessible()){
            List<Case> lstCase = [SELECT Id,AccountId FROM Case WHERE AccountId in  :mapIndexTaskIdAccount.values() AND CreatedDate = TODAY AND Subject ='Venta OPA' order by CreatedDate desc];
            for(Case currentCase:lstCase){
                if(!mapIndexTaskIdCase.containsKey(mapIdAccountIndexTask.get(currentCase.AccountId))){
                    mapIndexTaskIdCase.put(mapIdAccountIndexTask.get(currentCase.AccountId),currentCase.Id);
                }
            }
        }
        if (Schema.sObjectType.Task.isAccessible()){
            for(Task t : (List <Task>) newItems){
                List<Task> lstAux = new List<Task>();
                lstAux = (List <Task>) newItems;
                if(mapIndexTaskIdCase.containsKey(lstAux.indexOf(t))){
                    t.WhatId = mapIndexTaskIdCase.get(lstAux.indexOf(t));
                }
            }
        }

        // END COS-83

        if(!mainInteCCDDList.isEmpty() && Schema.sObjectType.Contact.isAccessible()){
            Contact dummyContact = [SELECT NIF_CIF_Customer_NIE__c, Id, LastName, FirstName, AccountId, Account.FirstName__c FROM Contact WHERE LastName =: CONTACT_LASTNAME AND FirstName =: CONTACT_FIRSTNAME AND Account.FirstName__c =: CONTACT_FIRSTNAME LIMIT 1][0];
            map<String, Task> mapaOwnerTask = new map<String, Task>();
            map<String, Task> mapaDNITask = new map<String, Task>();
            map<String, Task> mapaTypeTask = new map<String, Task>();
            Map<String, Id> translateOwnerMap = new Map<String, Id>();
            Map<String, Id> relationNIFwithIdMap = new Map<String, Id>();
            Map<String, String> translateTypeMap = new Map<String, String>();
            List<String> picklistOptions = new List<String>();
            List<Task> taskToUpdate = new List<Task>();
            
            for(Task t : mainInteCCDDList){
                mapaOwnerTask.put(t.FII_ACT_TXT_FederationId__c, t);
                if(t.FII_ACT_TXT_Document__c != null && t.FII_ACT_TXT_Document__c.length() > 0 && t.FII_ACT_TXT_Document__c != ''){
                    mapaDNITask.put(t.FII_ACT_TXT_Document__c, t);
                }
                mapaTypeTask.put(t.FII_ACT_TXT_NLInfo__c, t);
            }
            
            for(Schema.PicklistEntry v : Task.Status.getDescribe().getPicklistValues()){
                picklistOptions.add(v.getValue());
            }
            

            if(Schema.sObjectType.Interaction_LN__mdt.isAccessible() && !mapaTypeTask.KeySet().isEmpty() && mapaTypeTask.KeySet().size() != 0){
                for(Interaction_LN__mdt intLN : [SELECT id, type__c, subType__c, indicador__c FROM Interaction_LN__mdt WHERE indicador__c IN: mapaTypeTask.Keyset()]){
                    translateTypeMap.put(intLN.indicador__c, intLN.type__c);
                }
            }

            // Get federation Identifier to transalate 
            if(Schema.sObjectType.User.isAccessible() && !mapaOwnerTask.KeySet().isEmpty() && mapaOwnerTask.KeySet().size() != 0){
                for(User u : [SELECT FederationIdentifier, Id FROM User WHERE FederationIdentifier IN: mapaOwnerTask.KeySet()]){
                    translateOwnerMap.put(u.FederationIdentifier, u.Id);
                }
            }
            
            // Get account nif to translate
            if(Schema.sObjectType.Account.isAccessible() && !mapaDNITask.KeySet().isEmpty() && mapaDNITask.KeySet().size() != 0){
                for(Account c : [SELECT NIF_CIF_Customer_NIE__c, Id FROM Account WHERE NIF_CIF_Customer_NIE__c IN: mapaDNITask.KeySet()]){
                    relationNIFwithIdMap.put(c.NIF_CIF_Customer_NIE__c, c.Id);
                }    
            }
            
            for(Task t : mainInteCCDDList){
                t.FII_ACT_LKP_RelatedClient__c = relationNIFwithIdMap.get(t.FII_ACT_TXT_Document__c);
                t.OwnerId = translateOwnerMap.get(t.FII_ACT_TXT_FederationId__c);
                t.tipo__c = translateTypeMap.get(t.FII_ACT_TXT_NLInfo__c);
                t.FII_ACT_TXT_QuendaResult__c = '';
                if(t.tipo__c == null){
                    t.FII_ACT_TXT_QuendaResult__c = 'El campo Tipo no se ha informado correctamente. ';
                    t.tipo__c = 'No Válido';
                } 
                if(!picklistOptions.contains(t.Status)){
                    t.FII_ACT_TXT_QuendaResult__c += 'El campo Estado no se ha informado correctamente. ';
                    t.Status = 'Cita creada';
                }
                if (t.WhoId == null) {
                    t.WhoId = dummyContact.Id;
                }
                if(t.FII_ACT_TXT_QuendaResult__c == ''){
                    t.FII_ACT_TXT_QuendaResult__c = 'La interacción se ha generado correctamente.';
                }
            }           
        }
    }
    
    public void beforeUpdate (Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        if (bypassTrigger) { 
            System.debug('BYPASS TRIGGER BeforeUpdate Task');
            return; 
        }

       List<Task> newTasks = (List<Task>) newItems.values();
       newTasks = FII_Util_GSM.completeTasks ((Map<Id,Task>)oldItems, newTasks );
        
        // START - everis (LHZ) - 2019/06/26 IBBA's 411 - 858 - 861 - 366
        FII_Util_GSM.sendSurvey((Map<Id,Task>) newItems, (Map<Id,Task>) oldItems);
        // END - everis (LHZ) - 2019/06/26 - IBBA's 411 - 858 - 861 - 366
        //START - @Campañas - Everis - Modificado 04-2020 - Añadida llamadas a lógica para encuestas Modelo 1 
        system.debug('Rub ♣♣♣♣♣♣♣♣♣♣♣♣ TRG TASK HANDLER BEFORE UPDATE');
        /* DESCOMENTAR EN UAT - INICIO
        FIIE_Util_Surveys.sendSurveyInteraction((Map<Id,Task>) newItems, (Map<Id,Task>) oldItems);

        system.debug('Rub ♣♣♣♣♣♣♣♣♣♣♣♣ TRG TASK HANDLER BEFORE UPDATE - sendSurveyWacom');
        FIIE_Util_Surveys.prepareSurveyWacom((Map<Id,Task>) newItems);
		  DESCOMENTAR EN UAT - FIN */
        //END - @Campañas - Everis

        // START - IBAA-837 (Quenda)
        List<Task> mainInteCCPPList = new List<Task>(); 
        List<Task> mainInteCATList = new List<Task>();
        Contact dummyContact;
        Map<Id, Task> mapOldTask = (map<Id, Task>) oldItems;
                
        for(Task t : (List <Task>) newTasks){
            if (t.FII_ACT_SEL_Channel__c == 'CCPP' && t.RecordTypeId == IDRT_ACT_INTERACTION && t.FII_ACT_TXT_Main_interaction__c == null){
                mainInteCCPPList.add(t);
            } else if(t.FII_ACT_SEL_Channel__c == 'Phone' && t.RecordTypeId == IDRT_ACT_INTERACTION && t.FII_ACT_TXT_Main_interaction__c == null){
                mainInteCATList.add(t);
            }
            // Cambiamos RT de Pago por Tarjeta cuando el estado cambie a Pdte. Cliente - 28/11/2019
            if (t.RecordTypeId == FII_Util_MasterData_RecordTypes.getByDevName(Label.FII_RT_TSK_CARD_PAYMENT).Id && t.Status == 'Pendiente') {
                t.RecordTypeId = FII_Util_MasterData_RecordTypes.getByDevName(Label.FII_RT_TSK_CardPaymentRequested).Id;
            }
            // Cambiamos RT de Pago por Tarjeta cuando el estado cambie a Pdte. Cliente - 28/11/2019
        }

        // Comprobamos si vamso a necesitar realizar la query del contacto dummy
        if((!mainInteCCPPList.isEmpty() || !mainInteCATList.isEmpty()) && Schema.sObjectType.Contact.isAccessible()){
            Boolean dummyNeeded = false;
            for(Task t : mainInteCCPPList){
                if(t.WhoId == null){
                    dummyNeeded= true;
                    break;
                }
            }
            if(!dummyNeeded){
            for(Task t : mainInteCATList){
                    if(t.WhoId == null){
                        dummyNeeded= true;
                        break;
                    }
                } 
            }
            if(dummyNeeded){
                if(Schema.sObjectType.Contact.isAccessible()){ 
                	dummyContact = [SELECT NIF_CIF_Customer_NIE__c, Id, LastName, FirstName, AccountId, Account.FirstName__c FROM Contact WHERE LastName =: CONTACT_LASTNAME AND FirstName =: CONTACT_FIRSTNAME AND Account.FirstName__c =: CONTACT_FIRSTNAME LIMIT 1][0];
                }
            }
        }

        Map<String, Interaction_LN__mdt> translateTypeMap = new Map<String, Interaction_LN__mdt>();

        if(!mainInteCATList.isEmpty() && Schema.sObjectType.Contact.isAccessible()){
            Map<String, String> mapaIndicadorNL = new Map<String, String>();
            map<String, String> mapaTypeTaskCAT = new map<String, String>();

            for(Task t : mainInteCATList){
                String etiquetaAux = '';
                if(t.FII_ACT_TXT_NLInfo__c != null && t.FII_ACT_TXT_NLInfo__c != '' && t.FII_ACT_TXT_NLInfo__c != mapOldTask.get(t.Id).FII_ACT_TXT_NLInfo__c){
                    for(string charLN : t.FII_ACT_TXT_NLInfo__c.split('|')){
                        if(charLN.isNumeric()){ 
                            etiquetaAux += charLN; 
                        }else if(etiquetaAux != ''){ 
                            break; 
                        }
                    }
                    mapaIndicadorNL.put(t.FII_ACT_TXT_NLInfo__c, etiquetaAux);
                    mapaTypeTaskCAT.put(t.Id, etiquetaAux);
                }
            }

            if(Schema.sObjectType.Interaction_LN__mdt.isAccessible() && !mapaTypeTaskCAT.KeySet().isEmpty() && mapaTypeTaskCAT.KeySet().size() != 0){
                for(Interaction_LN__mdt intLN : [SELECT id, type__c, subType__c, indicador__c FROM Interaction_LN__mdt WHERE indicador__c IN: mapaTypeTaskCAT.Values()]){
                    translateTypeMap.put(intLN.indicador__c, intLN);
                }
            }

            if(!translateTypeMap.KeySet().isEmpty() && translateTypeMap.Size() != 0 && !mapaIndicadorNL.isEmpty() && mapaIndicadorNL.size() != 0) {
                for(Task t : mainInteCATList){
                    t.tipo__c = translateTypeMap.get(mapaIndicadorNL.get(t.FII_ACT_TXT_NLInfo__c)).type__c;
                    t.subTipo__c = translateTypeMap.get(mapaIndicadorNL.get(t.FII_ACT_TXT_NLInfo__c)).subType__c;
                    if(t.WhoId == null){
                        t.WhoId = dummyContact.Id;
                    }
                }
            }   
        }

        if(!mainInteCCPPList.isEmpty() && Schema.sObjectType.Contact.isAccessible()){
            map<String, Task> mapaTypeTask = new map<String, Task>();
            List<Task> taskToUpdateAgent = new List<Task>();
            List<Task> taskToUpdateType = new List<Task>();
            map<String, Task> mapaOwnerTask = new map<String, Task>();
            List<User> relatedOwners = new List<User>();
            Map<String, Id> translateOwnerMap = new Map<String, Id>();
            Map<Id, Task> oldTasks = (map<Id, Task>) oldItems;
                
            for(Task t : mainInteCCPPList){
                if(t.FII_ACT_LKP_RelatedClient__c != null && oldTasks.get(t.Id).FII_ACT_LKP_RelatedClient__c == null){
                    t.WhoId = null;
                }else if((t.FII_ACT_LKP_RelatedClient__c == null && oldTasks.get(t.Id).FII_ACT_LKP_RelatedClient__c != null && oldTasks.get(t.Id).WhoId == null) 
                        || (t.WhoId == null && t.FII_ACT_LKP_RelatedClient__c == null)){
                    t.WhoId = dummyContact.Id;
                }
                if(t.FII_ACT_TXT_FederationId__c != oldTasks.get(t.Id).FII_ACT_TXT_FederationId__c){
                    taskToUpdateAgent.add(t);
                    mapaOwnerTask.put(t.FII_ACT_TXT_FederationId__c, t);
                }
                if(t.FII_ACT_TXT_NLInfo__c != oldTasks.get(t.Id).FII_ACT_TXT_NLInfo__c){
                    taskToUpdateType.add(t);
                    mapaTypeTask.put(t.FII_ACT_TXT_NLInfo__c, t);   
                }
            }
                
            if(Schema.sObjectType.User.isAccessible() && !mapaOwnerTask.KeySet().isEmpty() && mapaOwnerTask.KeySet().size() != 0){
                relatedOwners = [SELECT FederationIdentifier, Id FROM User WHERE FederationIdentifier IN: mapaOwnerTask.KeySet()];
            }
            if(relatedOwners.size() > 0){
                for(User u : (List <User>) relatedOwners){
                    translateOwnerMap.put(u.FederationIdentifier, u.Id);
                }
            }
                
            if(Schema.sObjectType.Interaction_LN__mdt.isAccessible() && !mapaTypeTask.KeySet().isEmpty() && mapaTypeTask.KeySet().size() != 0){
                for(Interaction_LN__mdt intLN : [SELECT id, type__c, subType__c, indicador__c FROM Interaction_LN__mdt WHERE indicador__c IN: mapaTypeTask.Keyset()]){
                    translateTypeMap.put(intLN.indicador__c, intLN);
                }
            }

            for(Task t : (List <Task>) taskToUpdateAgent){
                t.OwnerId = translateOwnerMap.get(t.FII_ACT_TXT_FederationId__c);
            }
            for(Task t : (List <Task>) taskToUpdateType){
                t.tipo__c = translateTypeMap.get(t.FII_ACT_TXT_NLInfo__c).type__c;
                //t.subTipo__c = translateTypeMap.get(t.FII_ACT_TXT_NLInfo__c).subType__c;
            }
        }
        // END - IBAA-837 (Quenda)
        System.debug(newItems.values());
    }

    public void beforeDelete (List<sObject> oldRecordsList , Map<Id, SObject> oldItems) {
        if (bypassTrigger) { 
            System.debug('BYPASS TRIGGER BeforeDelete Task');
            return; 
        }
    }
    
    /** 
    * @desc Manage actuations inserted and her related interacctions
    * 
    * @param 
    * @param 
    */
    public void afterInsert (List<sObject> newRecordsList , Map<Id, SObject> newItems){
        if (bypassTrigger) { 
            System.debug('BYPASS TRIGGER AfterInsert Task');
            return; 
        }
        
        CampaignMember      campaignMemberItem;
        List<CampaignMember> campaignMemberList = new List<CampaignMember>();
        
        for(Task t: (List<Task>)newRecordsList){
      If ( t.FII_ACT_TXT_CampaignMemberId__c != null ){//2
                campaignMemberItem     = new CampaignMember();
                
                campaignMemberItem.Id  = t.FII_ACT_TXT_CampaignMemberId__c;
                campaignMemberItem.TV_TXT_ActivityId__c = t.Id;
                
                campaignMemberList.add( campaignMemberItem );
            }
    }
        
        If ( !campaignMemberList.isEmpty() ){
            update campaignMemberList;
        }

        // Check if the Recordtype of Task is action
        // Set of identifiers of new actions
        Set<Id> actionTaskId = new Set<Id>();
        actionTaskId = FII_Util_GSM.getActionTaskId(newRecordsList);
        
        if(!actionTaskId.isEmpty()){
            // Map of related needs of actions
            Map<Id, Case> relatedNeedsMap = new Map<Id, Case>();
            // 
            List<Id> relatedClientsOrLeadsId = new List<Id>();
            // 
            Map<Id, List<Task>> interactionsInProgressMap = new Map<Id, List<Task>>();
            // 
            List<List<SObject>> caseAndTasks = new List<List<SObject>>();
            // List of needs that need to be updated to relate them to interactions
            List<Case> needCasesToUpdate = new List<Case>();
            // List of interactions that should be inserted or updated
            List<Task> interactionsToUpsert = new List<Task>();
            // List of interactions that should be inserted
            List<Task> interactionsToInsert = new List<Task>();
            // List of new actions
            List<Task> actionTaskList = new List<Task>();
            
            actionTaskList = FII_Util_GSM.getActionTaskList(actionTaskId);
            
            relatedNeedsMap = FII_Util_GSM.getRelatedNeeds(actionTaskList);
            // Calculate if exist interacction in progress for new actions (task)
            relatedClientsOrLeadsId = FII_Util_GSM.getRelatedClientsOrLeads(actionTaskList);
            interactionsInProgressMap = FII_Util_GSM.getInteractionsInProgressBulk(relatedClientsOrLeadsId, true);
            
            // Section to manage the actions (associate actuation with open interaction, if there is)
            caseAndTasks = FII_Util_GSM.actionsWithOpenedInteraction(actionTaskList, interactionsInProgressMap, relatedNeedsMap);
            needCasesToUpdate.addAll((List<Case>)caseAndTasks[0]);
            interactionsToInsert.addAll(FII_Util_GSM.createNewChildInteractions(caseAndTasks[1], interactionsInProgressMap));
            interactionsToUpsert.addAll((List<Task>)caseAndTasks[2]);
            interactionsToUpsert.addAll((List<Task>)caseAndTasks[3]);
            
            interactionsToUpsert.addAll(interactionsToInsert);
            
            if(interactionsToUpsert.size() > 0){
                if(Schema.sObjectType.Task.isCreateable() && Schema.sObjectType.Task.isUpdateable()){
                    upsert interactionsToUpsert;
                }   
            }
            
            // When a Task Action is inserted, the status of the need is updated if necessary
            needCasesToUpdate = FII_Util_GSM.updateNeedStatus(actionTaskList, needCasesToUpdate, relatedNeedsMap.Values());
            
            if(needCasesToUpdate.size() > 0){
                if(Schema.sObjectType.Case.isUpdateable()){
                    update needCasesToUpdate;
                }
            }
        }
    }
    
    /** 
    * @desc 
    * 
    * @param 
    * @param 
    */
    public void afterUpdate (Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        if (bypassTrigger) { 
            System.debug('BYPASS TRIGGER AfterUpdate Task');
            return; 
        }
        Map<Id, Task> newTaskItems = new Map<Id, Task>();
        Map<Id, Task> oldTaskItems = new Map<Id, Task>();
        
        CampaignMember      campaignMemberItem;
        List<CampaignMember> campaignMemberList = new List<CampaignMember>();
        
        for(Task t : (List <Task>)newItems.values()){
      If ( t.FII_ACT_TXT_CampaignMemberId__c != null ){//2
                campaignMemberItem     = new CampaignMember();
                
                campaignMemberItem.Id  = t.FII_ACT_TXT_CampaignMemberId__c;
                campaignMemberItem.TV_TXT_ActivityId__c = t.Id;
                
                campaignMemberList.add( campaignMemberItem );
            }
    }
        
        If ( !campaignMemberList.isEmpty() ){
            update campaignMemberList;
        }
        
        for(Task t : (List <Task>)newItems.values()){
            newTaskItems.put(t.Id,t);
        }
        for(Task t : (List <Task>)oldItems.values()){
            oldTaskItems.put(t.Id,t);
        }

        // Section to manage tasks (actions) that have changed status
        List<Task> taksStatusChangedList = new List<Task>();
        for(Task t :(List<Task>)newItems.values()){
            if(newTaskItems.get(t.Id).Status != oldTaskItems.get(t.Id).Status){
                taksStatusChangedList.add(t);
            }
        }
        // Set of identifiers of new actions
        Set<Id> actionTaskId = FII_Util_GSM.getActionTaskId(taksStatusChangedList);
        
        if(!actionTaskId.isEmpty()){
            List<Task> actionTaskList = FII_Util_GSM.getActionTaskList(actionTaskId);
            List<Case> relatedNeedsList = FII_Util_GSM.getRelatedNeeds(actionTaskList).values();
            List<Case> needsToUpdate = FII_Util_GSM.updateNeedStatus(actionTaskList, new List<Case>(), relatedNeedsList);
            
            if(!needsToUpdate.isEmpty()){
                if(Schema.sObjectType.Case.isUpdateable()){
                    update needsToUpdate;
                }
            }
        }
    }
    
    public void afterDelete (Map<Id, SObject> oldItems) {
        if (bypassTrigger) { 
            System.debug('BYPASS TRIGGER AfterDelete Task');
            return; 
        }
    }
    
    public void afterUndelete(List<sObject> newRecordsList , Map<Id, sObject> newItems) {
        if (bypassTrigger) { 
            System.debug('BYPASS TRIGGER AfterUndelete Task');
            return; 
        }
    }
}