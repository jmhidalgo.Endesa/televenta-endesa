/**
*   @author     Everis
*   @since      March 26, 2019
*   @desc       Test Class of FII_LCC_Account_Edit
*   @history    March 26, 2019 - everis (FHO) - Create Apex Class 
*               May 30, 2019 - everis (JHG) - Añadidos métodos test para las modificaciones de la IBAA-853
*/
@isTest
public with sharing class FII_LCC_Account_Edit_Test {
    
    /*
 *============================================================================================================================================
 * @methodName               makeData
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
    @TestSetup
    static void makeData(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.setMock(WebServiceMock.Class , new UtilSoapMock.SICLI008Mock());
        final Contact con = UtilTest.createContact();   
        insert con;
        
        Account acc = [SELECT Id, Name FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        acc.TitularContactHide__c = con.Id;
        upsert acc;

        final Task interaction = FII_Util_GSM.createInteraction(acc.Id, con.Id, null, 'Phone', 'interaction');
        insert interaction;
        
        Case caseCreate = new Case();
        caseCreate.AccountId = acc.Id;
        caseCreate.ContactId = con.Id;
        insert caseCreate;
    }
    
    /*
 *============================================================================================================================================
 * @methodName               datosAccount
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
    @isTest
    private static Account datosAccount (){
        
        final Account acc = [SELECT Id,TitularContactHide__c, Name, Physical_or_legal_cd__c, FirstName__c, LastName__c, NIF_CIF_Customer_NIE__c, Identifier_Type__c FROM Account WHERE FirstName__c='TestName' LIMIT 1];
        System.assert(acc != null);
        return acc;  
    
    }

    /*
 *============================================================================================================================================
 * @methodName               datosContact
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
    @isTest
    private static Contact datosContact (){
        
        final Contact con = [SELECT Id, LastName, FirstName, Contact_method_cd__c, Nationality_cd__c, Phone, No_Phone_flg__c,Email FROM Contact WHERE FirstName ='Test1' LIMIT 1];
        System.assert(con != null);  
        return con; 

    }
    
    /*
 *============================================================================================================================================
 * @methodName               getInformationTest
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
    @isTest
    private static void getInformationTest (){
        
        final Account acc = FII_LCC_Account_Edit_Test.datosAccount();
        List<sObject> objetos = new List<sObject>();
        objetos = FII_LCC_Account_Edit.getInformation(acc.Id);
        System.assert(objetos.size() > 0);    
    }
    
    /*
 *============================================================================================================================================
 * @methodName               updateClientAndRelatedTest
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
    @isTest
    private static void updateClientAndRelatedTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.setMock(WebServiceMock.Class , new UtilSoapMock.SICLI008Mock());
        FII_LCC_Account_Edit.ResponseUpdateClientAndRelated response = new FII_LCC_Account_Edit.ResponseUpdateClientAndRelated();
        Account accountItem = FII_LCC_Account_Edit_Test.datosAccount();
        Contact contactItem = FII_LCC_Account_Edit_Test.datosContact();
        Individual individualItem = [SELECT Id, FII_IND_SEL_Cesion__c, FII_IND_SEL_Molestar__c, FII_IND_SEL_Iden_Evi__c, FII_IND_SEL_Clientes__c, FII_IND_SEL_Terceros__c FROM Individual WHERE contact__c =: contactItem.Id];
        
        accountItem.Name = '';
        accountItem.FirstName__c = '';
        accountItem.LastName__c = '';
        
        response = FII_LCC_Account_Edit.updateClientAndRelated(accountItem, contactItem, individualItem);
        System.assertEquals(response.type, 'error');
        accountItem = FII_LCC_Account_Edit_Test.datosAccount();
        /*contactItem.Phone = '000000000';
        response = FII_LCC_Account_Edit.updateClientAndRelated(accountItem, contactItem, individualItem);
        System.assert(response.type == 'error');
        contactItem = FII_LCC_Account_Edit_Test.datosContact();*/
        individualItem.FII_IND_SEL_Cesion__c = 'Valor no válido';
        response = FII_LCC_Account_Edit.updateClientAndRelated(accountItem, contactItem, individualItem);
        System.assert(response.type == 'error');  
        individualItem = [SELECT Id, FII_IND_SEL_Cesion__c, FII_IND_SEL_Molestar__c, FII_IND_SEL_Iden_Evi__c, FII_IND_SEL_Clientes__c, FII_IND_SEL_Terceros__c FROM Individual WHERE contact__c =: contactItem.Id];
        
        accountItem.Name = 'PRUEBA PRUEBA';
        accountItem.FirstName__c = 'PRUEBA';
        accountItem.LastName__c = 'PRUEBA';
        contactItem.MobilePhone = '687654321';
        contactItem.No_Phone_flg__c = false;
        response = FII_LCC_Account_Edit.updateClientAndRelated(accountItem, contactItem, individualItem);
        
        System.assertEquals(response.type, 'success');   
        Test.stopTest();
    }

    /*
 *============================================================================================================================================
 * @methodName               updateClientAndRelated2Test
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
    @isTest
    private static void updateClientAndRelated2Test(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.setMock(WebServiceMock.Class , new UtilSoapMock.SICLI008Mock());
        FII_LCC_Account_Edit.ResponseUpdateClientAndRelated response = new FII_LCC_Account_Edit.ResponseUpdateClientAndRelated();
        Account accountItem = FII_LCC_Account_Edit_Test.datosAccount();
        Contact contactItem = FII_LCC_Account_Edit_Test.datosContact();
        final Individual individualItem = null;
        
        accountItem.Name = '';
        accountItem.FirstName__c = '';
        accountItem.LastName__c = '';
        response = FII_LCC_Account_Edit.updateClientAndRelated(accountItem, contactItem, individualItem);
        System.assertEquals(response.type, 'error');
        accountItem = FII_LCC_Account_Edit_Test.datosAccount();

        /*contactItem.Phone = '000000000';
        response = FII_LCC_Account_Edit.updateClientAndRelated(accountItem, contactItem, individualItem);
        System.assert(response.type == 'error');
        contactItem = FII_LCC_Account_Edit_Test.datosContact();*/

        //individualItem.FII_IND_SEL_Cesion__c = 'Valor no válido';
        response = FII_LCC_Account_Edit.updateClientAndRelated(accountItem, contactItem, individualItem);
        System.assert(response.type == 'success');   
        //individualItem = [SELECT Id, FII_IND_SEL_Cesion__c, FII_IND_SEL_Molestar__c, FII_IND_SEL_Iden_Evi__c, FII_IND_SEL_Clientes__c, FII_IND_SEL_Terceros__c FROM Individual WHERE contact__c =: contactItem.Id];
        accountItem.Name = 'PRUEBA PRUEBA';
        accountItem.FirstName__c = 'PRUEBA';
        accountItem.LastName__c = 'PRUEBA';
        contactItem.MobilePhone = '687654321';
        contactItem.No_Phone_flg__c = false;
        response = FII_LCC_Account_Edit.updateClientAndRelated(accountItem, contactItem, individualItem);
        System.assertEquals(response.type, 'success');     
        Test.stopTest();
    }

/*
 *============================================================================================================================================
 * @methodName               checkAdminProfileTest
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
    @isTest
    private static void checkAdminProfileTest (){
        
        Boolean esSupervisorOAdmin;
        esSupervisorOAdmin = FII_LCC_Account_Edit.checkAdminProfile();     

        System.assertNotEquals(null, esSupervisorOAdmin);  
    }
    
    /*
 *============================================================================================================================================
 * @methodName               standardBehaviorGSMTest
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
    @isTest
    public static void standardBehaviorGSMTest(){
        Test.startTest();
        final Account accountItem = FII_LCC_Account_Edit_Test.datosAccount();
        final Id recId = accountItem.Id;
        final String type = 'Atencion al cliente';
        final String subtype = null;
        final String subjectNeed = 'Alta de Cliente';
        final String subjectAct = 'Alta de Cliente';
        final Id camId = null;
        final String status = 'Completed';
        final String recordtype = System.Label.FII_RT_TSK_ACTION;
        
        final Case caseItem = FII_LCC_Account_Edit.standardBehaviorGSM(recId, type, subtype, subjectNeed, subjectAct, camId, status, recordtype);
        
        FII_LCC_Account_Edit.getInformation(caseItem.Id);
        System.assert(caseItem.Id != null);
        Test.stopTest();
    }
    
    /*
 *============================================================================================================================================
 * @methodName               getObjectsTypeTest
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
    @isTest
    private static void getObjectsTypeTest (){
        
        final String test = FII_LCC_Account_Edit.getObjectsType();
        System.assertNotEquals('', test);
        
    }  
    
	/*
 *============================================================================================================================================
 * @methodName               getProfileUserTest
 * @received parameters      --
 * @return                   (void method)
 *============================================================================================================================================
 */
 
	@isTest
    private static void getProfileUserTest (){
        
        String result = FII_LCC_Account_Edit.getProfileUser();
        System.assertNotEquals(null, result);
        
    } 
    
}