/**
*   @author     Everis
*   @since      March 26, 2019
*   @desc       Test Class of FII_LCC_RelatedIndividualInfo
*   @history    March 26, 2019 - everis (FHO) - Create Apex Class 
*/
@isTest
public with sharing class FII_LCC_RelatedIndividualInfo_Test {

	@TestSetup
	static void makeData(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Test.startTest();
        List<Contact> listContacts = new List<Contact>();
        List<Account> listAccounts = new List<Account>();

        final Profile p = [SELECT ID FROM Profile WHERE Name = 'Asesor Experto' LIMIT 1];

        User u = new User();
        u.Username = 'televentaUser@test.com';
        u.Email = 'email2@test.com';
        u.FirstName = 'firstName2';
        u.LastName = 'lastName2';
        u.CommunityNickname = 'communityNickname2';
        u.ProfileId = p.Id;
        u.Alias = 'alias2'; 
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'es_ES';  
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'en_US';
        u.isActive = true;
        insert u;
        
		Contact con = UtilTest.createContact();
		con.Physical_or_legal_cd__c = 'Corporate Customer';   
		AccountTriggerHandler.bypassTrigger = true;
		insert con;
		AccountTriggerHandler.bypassTrigger = false;

		//Account acc = [SELECT Id FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        Account acc = UtilTest.createAccount();
		acc.TitularContactHide__c = con.Id;
        acc.FirstName__c = 'PruebaRGPD';
        acc.NIF_CIF_Customer_NIE__c = '44283092L';
		acc.Cliente_External_Id__c = '35-ee';
        listAccounts.add(acc);
		
        //Account sin trigger (sin contacto titular y este sin individual)
        Account acc2 = UtilTest.createAccount();
		acc2.TitularContactHide__c = con.Id;
        acc2.FirstName__c = 'PruebaRGPD2';
        acc2.NIF_CIF_Customer_NIE__c = '71708328D';
		acc2.Cliente_External_Id__c = '36-aa';
        
		listAccounts.add(acc2);
        

        //Account acc = [SELECT Id FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        Account acc3 = UtilTest.createAccount();
		acc3.TitularContactHide__c = con.Id;
        acc3.FirstName__c = 'Contacto';
        acc3.NIF_CIF_Customer_NIE__c = '44878946N';
		acc3.Cliente_External_Id__c = '37-ee';
        listAccounts.add(acc3);
        AccountTriggerHandler.bypassTrigger = true;
        insert listAccounts ;
		AccountTriggerHandler.bypassTrigger = false;
        
        final Contact contact = new Contact();
		contact.FirstName = 'Test3';
		contact.LastName = 'Pendiente de Informar';
		contact.Email = 'test4@gmail.com';
		contact.Work_email__c = 'test4@gmail.com';
		contact.Document_Type__c = 'NIF';
		contact.NIF_CIF_Customer_NIE__c = '91770772J';
		contact.Contact_method_cd__c='E-mail';
		contact.No_Phone_flg__c = true;
		contact.No_Email_flg__c = false;
		contact.Nationality_cd__c = 'ES';
		contact.Contact_Robinson__c = true;
		contact.Physical_or_legal_cd__c = 'Physical Customer';
		//contact.Phone='954676767';
		contact.Profession_cd__c='Lawyer';
		contact.Sex__c='Male';
        contact.AccountId = acc.Id;

        ContactTriggerHandler.bypassTrigger = true;
        listContacts.add(contact);
        
        ContactTriggerHandler.bypassTrigger = false;

        final Contact contact2 = new Contact();
		contact2.FirstName = 'Contacto';
		contact2.LastName = 'Pendiente de Informar';
		contact2.Email = 'test4@gmail.com';
		contact2.Work_email__c = 'test4@gmail.com';
		contact2.Document_Type__c = 'NIF';
		contact2.NIF_CIF_Customer_NIE__c = '92779508S';
		contact2.Contact_method_cd__c='E-mail';
		contact2.No_Phone_flg__c = true;
		contact2.No_Email_flg__c = false;
		contact2.Nationality_cd__c = 'ES';
		contact2.Contact_Robinson__c = true;
		contact2.Physical_or_legal_cd__c = 'Physical Customer';
		//contact.Phone='954676767';
		contact2.Profession_cd__c='Lawyer';
		contact2.Sex__c='Male';
        contact2.AccountId = acc3.Id;

        
        listContacts.add(contact2);
        ContactTriggerHandler.bypassTrigger = true;
        insert listContacts;
        ContactTriggerHandler.bypassTrigger = false;

        Task interaction = FII_Util_GSM.createInteraction(acc.Id, null, null, 'Phone', 'interaction');
        insert interaction;

		Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
    	insert  ind;  
        Test.stopTest();

        final Account account1 = new Account();
        account1.Name = 'TestName';
        account1.LastName__c='TestName';
        account1.FirstName__c = 'TestName';
        account1.Identifier_Type__c = 'NIF';
        account1.NIF_CIF_Customer_NIE__c = '54701693B';
        account1.Email_con__c = 'test@gmail.com';
        account1.Contact_method_cd__c = 'E-mail';
        account1.Licensed__c = false;
        account1.Cliente_External_Id__c = '22--34-ee';
        account1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account').getRecordTypeId();
        account1.Physical_or_legal_cd__c = 'Physical Customer';
        account1.No_Phone_flg__c = true;
        account1.No_Email_flg__c = false;
        account1.Identifier_Number__c = 224346;
        account1.Account_Type__c = 'Residential';
        account1.Electronic_bill_flg__c = true;
        account1.Duplicate_convenience_flg__c = true;
        account1.Account_language__c = 'Español';

        AccountTriggerHandler.bypassTrigger = true;
        insert account1;
		AccountTriggerHandler.bypassTrigger = false;
        
        final Contact contact1 = new Contact();
        contact1.FirstName = 'Test Contact Name';
        contact1.LastName = 'LastName';
        contact1.Email = 'test1@gmail.com';
        contact1.Work_email__c = 'test1@gmail.com';
        contact1.Document_Type__c = 'NIF';
        contact1.NIF_CIF_Customer_NIE__c = '69753554M';
        contact1.Contact_method_cd__c='E-mail';
        contact1.No_Phone_flg__c = true;
        contact1.No_Email_flg__c = false;
        contact1.Nationality_cd__c = 'ES';
        contact1.Contact_Robinson__c = true;
        contact1.Physical_or_legal_cd__c = 'Physical Customer';
        contact1.Profession_cd__c='Lawyer';
        contact1.Sex__c='Male';
        contact1.AccountId=account1.Id;
        ContactTriggerHandler.bypassTrigger=true;
        insert contact1;
        ContactTriggerHandler.bypassTrigger=false;
          
            

      
	}

	@isTest
	private static Account datosAccount (){
		Account acc = [SELECT Id,TitularContactHide__c FROM Account WHERE FirstName__c='PruebaRGPD' LIMIT 1];
		return acc;    
	}

	@isTest
	private static Contact datosContact (){
		Contact con = [SELECT Id FROM Contact WHERE FirstName ='Test1' LIMIT 1];
		return con;    
	}
    // List<AccountContactRelation> accContRel = [SELECT AccountId,contactId,Roles FROM AccountContactRelation WHERE AccountId =: acc.Id];

    @isTest
	private static void getIndividualNoTitularContactHideTest(){
        Test.startTest();
        Account acc2 = [SELECT Id,TitularContactHide__c FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        Contact con = [SELECT Id FROM Contact WHERE FirstName ='Test Contact Name' LIMIT 1];
        AccountContactRelation accRelationCont = new AccountContactRelation();
        accRelationCont.AccountId=acc2.Id;
        accRelationCont.contactId=con.Id;
        accRelationCont.Roles='Titular de contrato';
        insert accRelationCont;

        Individual ind1 = new Individual();
        ind1.LastName = 'LastName';
        IndividualTriggerHandler.bypassTrigger=true;
        insert  ind1; 
        IndividualTriggerHandler.bypassTrigger=false; 

        con.IndividualId=ind1.Id;
        ContactTriggerHandler.bypassTrigger=true;
        update con;
        ContactTriggerHandler.bypassTrigger=false;

        List<Individual> individuo = FII_LCC_RelatedIndividualInfo.getIndividual(acc2.Id);
        System.assertEquals(1, individuo.size());
        Test.stopTest();
    }
   
    @isTest
	private static void getIndividualNoIndividualTest(){
        Test.startTest();
        Account acc2 = [SELECT Id,TitularContactHide__c FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        Contact con = [SELECT Id FROM Contact WHERE FirstName ='Test Contact Name' LIMIT 1];
        acc2.TitularContactHide__c=con.Id;
        update acc2;
        List<Individual> individuo = FII_LCC_RelatedIndividualInfo.getIndividual(acc2.Id);
        System.assertEquals(1, individuo.size());
        Test.stopTest();
    }

       
    @isTest
	private static void getIndividualAccNoIndividualTest(){
        Test.startTest();
        Account acc2 = [SELECT Id,TitularContactHide__c FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        Contact con = [SELECT Id FROM Contact WHERE FirstName ='Test Contact Name' LIMIT 1];
        acc2.TitularContactHide__c=con.Id;
        update acc2;

        Individual ind1 = new Individual();
        ind1.LastName = 'LastName';
        IndividualTriggerHandler.bypassTrigger=true;
        insert  ind1; 
        IndividualTriggerHandler.bypassTrigger=false; 

        con.IndividualId=ind1.Id;
        ContactTriggerHandler.bypassTrigger=true;
        update con;
        ContactTriggerHandler.bypassTrigger=false;

        List<Individual> individuo = FII_LCC_RelatedIndividualInfo.getIndividual(acc2.Id);
        System.assertEquals(1, individuo.size());
        Test.stopTest();
    }

    @isTest
    private static void getIndividualAccNoIndividualIdTest(){
        Test.startTest();
        Boolean result=false;
        Account acc2 = [SELECT Id,TitularContactHide__c FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        Contact con = [SELECT Id FROM Contact WHERE FirstName ='Test Contact Name' LIMIT 1];
        acc2.TitularContactHide__c=con.Id;
        update acc2;
        Individual ind1 = new Individual();
        ind1.Contact__c = con.Id;
        ind1.LastName = 'LastName';
        IndividualTriggerHandler.bypassTrigger=true;
        insert  ind1; 
        IndividualTriggerHandler.bypassTrigger=false; 
        List<Individual> individuo = FII_LCC_RelatedIndividualInfo.getIndividual(acc2.Id);
        
        Contact conResult= [SELECT Id,IndividualId FROM Contact WHERE FirstName ='Test Contact Name' LIMIT 1];
        if(conResult.IndividualId!=null)result=true;
        System.assertEquals(true, result);
        Test.stopTest();
    }

    @isTest
	private static void getIndividualConNoIndividualTest(){
        Test.startTest();
        Contact con = [SELECT Id FROM Contact WHERE FirstName ='Test Contact Name' LIMIT 1];

        Individual ind1 = new Individual();
        ind1.LastName = 'LastName';
        IndividualTriggerHandler.bypassTrigger=true;
        insert  ind1; 
        IndividualTriggerHandler.bypassTrigger=false; 

        con.IndividualId=ind1.Id;
        ContactTriggerHandler.bypassTrigger=true;
        update con;
        ContactTriggerHandler.bypassTrigger=false;

        List<Individual> individuo = FII_LCC_RelatedIndividualInfo.getIndividual(con.Id);
        System.assertEquals(1, individuo.size());
        Test.stopTest();
    }
    @isTest
    private static void getIndividualConNoIndividualIdTest(){
        Test.startTest();
        Boolean result=false;
        Contact con = [SELECT Id FROM Contact WHERE FirstName ='Test Contact Name' LIMIT 1];
    
        Individual ind1 = new Individual();
        ind1.Contact__c = con.Id;
        ind1.LastName = 'LastName';
        IndividualTriggerHandler.bypassTrigger=true;
        insert ind1; 
        IndividualTriggerHandler.bypassTrigger=false; 
        List<Individual> individuo = FII_LCC_RelatedIndividualInfo.getIndividual(con.Id);
        
        Contact conResult= [SELECT Id,IndividualId FROM Contact WHERE FirstName ='Test Contact Name' LIMIT 1];
        if(conResult.IndividualId!=null)result=true;
        System.assertEquals(true, result);
        Test.stopTest();
    }

	@isTest
	private static void getPhysicalTest(){
		Contact con = FII_LCC_RelatedIndividualInfo_Test.datosContact();
		boolean juridico = FII_LCC_RelatedIndividualInfo.getPhysical(con.Id);
		System.assert(juridico);
        
        Account acc = FII_LCC_RelatedIndividualInfo_Test.datosAccount();
        juridico = FII_LCC_RelatedIndividualInfo.getPhysical(acc.Id);
        System.assert(juridico);
	}

	@isTest
	private static void saveTest() {
		Contact con = FII_LCC_RelatedIndividualInfo_Test.datosContact();
		Individual ind = [SELECT Id FROM Individual WHERE Contact__c =: con.Id LIMIT 1];
		FII_LCC_RelatedIndividualInfo.save(ind);
		System.assert(true);
	}
    
    @isTest
    private static void getObjectsTypeTest(){
        FII_LCC_RelatedIndividualInfo.getObjectsType();
    }
    
    @isTest
    private static void getInteractionsInProgressTest(){
        
        Account acc = FII_LCC_RelatedIndividualInfo_Test.datosAccount();
        
        Task intInProgress = FII_LCC_RelatedIndividualInfo.getInteractionsInProgress(acc.Id);
        System.assertNotEquals(null, intInProgress);
    }

    
    @isTest 
    private static void isTheEventForMeTest(){
        Account acc = FII_LCC_RelatedIndividualInfo_Test.datosAccount();
        Test.startTest();
        Boolean isTheEventForMeFlag = FII_LCC_RelatedIndividualInfo.isTheEventForMe(acc.Id, acc.Id, null);
        system.assertEquals(isTheEventForMeFlag, true);
        Test.stopTest();
    } 
    
    @isTest
    private static void standardBehaviorGSMTest(){
        Account accountItem = FII_LCC_RelatedIndividualInfo_Test.datosAccount();
        id recId = accountItem.id;
        String type = 'Atencion al cliente';
        String subtype = null;
        String subjectNeed = 'Modificación RGPD';
        String subjectAct = 'Modificación RGPD';
        Id camId = null;
        String status = 'Completed';
        String recordtype = System.Label.FII_RT_TSK_ACTION;
        Case caseItem = FII_LCC_RelatedIndividualInfo.standardBehaviorGSM(recId, type, subtype, subjectNeed, subjectAct, camId, status, recordtype);
    }

    @IsTest
    private static void createdIndividualTest() {
        Test.startTest();
		final Contact con = [SELECT Id FROM Contact WHERE NIF_CIF_Customer_NIE__c = '91770772J'];
        final Individual result = FII_LCC_RelatedIndividualInfo.createdIndividual(con.Id);
        Test.stopTest();

        System.assertNotEquals(null, result, 'Fallo al crear individual');
    }

    @isTest
    private static void testGetProfileTeleventa (){

        user usuario = [SELECT id FROM User WHERE userName = 'televentaUser@test.com'];
        test.startTest();
        system.runAs(usuario){
            Boolean result = FII_LCC_RelatedIndividualInfo.getIsTeleventaProfile();
            system.assertEquals(true, result);
        } 
        test.stopTest();
    }
    @isTest
    private static void testobtainTextsRGPD (){

        Account acc = FII_LCC_RelatedIndividualInfo_Test.datosAccount();
        test.startTest();

        FII_LCC_RelatedIndividualInfo.textsInfoWrapper result = FII_LCC_RelatedIndividualInfo.obtainTextsRGPD(acc.Id);
        system.assertEquals(true, result.textoInicial != '','textoInicial vacio');
        system.assertEquals(true, result.textoNoMolestar != '','textoNoMolestar vacio');
        system.assertEquals(true, result.textoOfrecimientos != '','textoOfrecimientos vacio');
        system.assertEquals(true, result.textoCesion != '','textoCesion vacio');
        system.assertEquals(true, result.textoNoCliente != '','textoNoCliente vacio');

        test.stopTest();
    }
}