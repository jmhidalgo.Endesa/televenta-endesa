@isTest
public with sharing class TV_CLS_Utils_Test {
    @TestSetup
    static void makeData(){
        List<Contact> listContacts = new List<Contact>();
        List<Account> listAccounts = new List<Account>();

        Test.startTest();
        List<String> listValuesTeveltaProfiles = new List<String>();
        Set<String> setValues = new Set<String>();
        String textToSplit = Label.TV_Perfiles_Televenta;
        String character = ',';
        if(!String.isBlank(textToSplit) && !String.isBlank(character)){
            setValues= new Set<String>(textToSplit.split(character));
        }
        for(String value :setValues){
            listValuesTeveltaProfiles.add(value);
        }

        final Profile p = [SELECT ID FROM Profile WHERE Name IN: listValuesTeveltaProfiles LIMIT 1];

        User u = new User();
        u.Username = 'televentaUser@test.com';
        u.Email = 'email2@test.com';
        u.FirstName = 'firstName2';
        u.LastName = 'lastName2';
        u.CommunityNickname = 'communityNickname2';
        u.ProfileId = p.Id;
        u.Alias = 'alias2'; 
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'es_ES';  
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'en_US';
        u.isActive = true;
        insert u;
            
		Contact con = UtilTest.createContact();
		con.Physical_or_legal_cd__c = 'Corporate Customer';   
		AccountTriggerHandler.bypassTrigger = true;
		insert con;
		AccountTriggerHandler.bypassTrigger = false;

		//Account acc = [SELECT Id FROM Account WHERE FirstName__c = 'TestName' LIMIT 1];
        Account acc = UtilTest.createAccount();
		acc.TitularContactHide__c = con.Id;
        acc.FirstName__c = 'PruebaRGPD';
        acc.NIF_CIF_Customer_NIE__c = '44283092L';
		acc.Cliente_External_Id__c = '35-ee';
        listAccounts.add(acc);
		

        //Account sin trigger (sin contacto titular y este sin individual)
        Account acc2 = UtilTest.createAccount();
		acc2.TitularContactHide__c = con.Id;
        acc2.FirstName__c = 'PruebaRGPD2';
        acc2.NIF_CIF_Customer_NIE__c = '71708328D';
		acc2.Cliente_External_Id__c = '36-aa';
        AccountTriggerHandler.bypassTrigger = true;
		listAccounts.add(acc2);
        AccountTriggerHandler.bypassTrigger = false;

        insert listAccounts ;

        final Contact contact = new Contact();
		contact.FirstName = 'Test3';
		contact.LastName = 'Pendiente de Informar';
		contact.Email = 'test4@gmail.com';
		contact.Work_email__c = 'test4@gmail.com';
		contact.Document_Type__c = 'NIF';
		contact.NIF_CIF_Customer_NIE__c = '91770772J';
		contact.Contact_method_cd__c='E-mail';
		contact.No_Phone_flg__c = true;
		contact.No_Email_flg__c = false;
		contact.Nationality_cd__c = 'ES';
		contact.Contact_Robinson__c = true;
		contact.Physical_or_legal_cd__c = 'Physical Customer';
		//contact.Phone='954676767';
		contact.Profession_cd__c='Lawyer';
		contact.Sex__c='Male';
        contact.AccountId = acc.Id;

        ContactTriggerHandler.bypassTrigger = true;
        listContacts.add(contact);
        ContactTriggerHandler.bypassTrigger = false;
        insert listContacts;

		Individual ind = new Individual();
        ind.Contact__c = con.Id;
        ind.LastName = 'Test2';
        insert  ind;  
        
        //B2C_Address__c b2cAddress = generateAddress(acc.Id);
        //insert b2cAddress;

        Asset asset = new Asset();
        asset.Name= 'LOONEY 1';
        asset.AccountId  = acc2.Id;
        asset.ParentId = NULL; 
        asset.NE__Status__c ='Inactive';
        asset.FI_ASS_SEL_HolderCompany__c = '20';
        asset.NE__EndDate__c = system.now();
        //asset.FI_ASS_LKP_Address__r = b2cAddress;
        asset.FI_ASS_LKP_Service_Point__c = null;
        asset.AccountId = acc2.Id;
        insert asset;

        Test.stopTest();
    } 

    static B2C_Address__c generateAddress(Id clientId){
        B2C_Address__c b2cAddress = new B2C_Address__c();
        b2cAddress.Name = 'LOONEY AVENUE';
        b2cAddress.Premise_flg__c = true;
        b2cAddress.Main_Address__c = false;
        b2cAddress.Adress_active_flg__c = true;
        b2cAddress.Country_cd__c = 'ES';
        b2cAddress.Country_desc__c = 'ESPAÑA';
        b2cAddress.Province_cd__c = '15';
        b2cAddress.Province_text__c = 'A CORUÑA';
        b2cAddress.County_cd__c = '030';
        b2cAddress.County__c = 'A CORUÑA';
        b2cAddress.City_cd__c = 'BER13108';
        b2cAddress.City__c = 'A CORUÑA';
        b2cAddress.INE_city_cd__c = '15030000101';
        b2cAddress.Street_cd__c = 'BER0569533';
        b2cAddress.Street__c = 'NELLE';
        b2cAddress.INE_street_cd__c = '1503000423';
        b2cAddress.Type_street__c = 'RD';
        b2cAddress.Zipcode__c = '12345';
        b2cAddress.Longitude__c = 0;
        b2cAddress.Latitude__c = 0;
        b2cAddress.Census_tract__c = '123';
        b2cAddress.Street_number__c = '109';
        b2cAddress.Point_duplicator_cd__c = 'A';
        b2cAddress.Type_Point_lightener_cd__c = 'AFS';
        b2cAddress.Point_lightener__c = 'S';
        b2cAddress.Floor_cd__c = '4';
        b2cAddress.Door__c = '2';
        b2cAddress.Build__c = '1';
        b2cAddress.Stairs__c  = '3';
        b2cAddress.Adress_lightener__c = 'asdf';
        b2cAddress.Type_address_cd__c = 'Normalized';
        b2cAddress.Reference_year__c = 2010;
        b2cAddress.INE_county_cd__c = '030';
        b2cAddress.INE_city_cd__c = '15030000101';
        b2cAddress.UTM_x__c = '2';
        b2cAddress.UTM_y__c = '2';
        b2cAddress.Trust_level__c = 5;
        b2cAddress.CUPS_13_gas__c = 'CRM0001391063';
        b2cAddress.CUPS_13_elec__c = 'CRM0001312697';
        b2cAddress.Description_adress__c = 'Descripcion calle';
        b2cAddress.INE_state_cd__c = 'PR';
        b2cAddress.DIR_external_CRM_id__c = '123456789012345';
        b2cAddress.Formatedd_Address__c = 'Formatedd Address';
        b2cAddress.DIR_external_account_id__c = '123456789012345';
        b2cAddress.DIR_external_contact_id__c = '123456789012345';
        b2cAddress.Account__c = clientId;
        return b2cAddress;
    }

    @isTest
    private static void testGetProfileTeleventa (){

        user usuario = [SELECT id FROM User WHERE userName = 'televentaUser@test.com'];
        test.startTest();
        system.runAs(usuario){
            Boolean result = TV_CLS_Utils.getIsTeleventaProfile();
            system.assertEquals(true, result);
        } 
        test.stopTest();
    }
    @isTest
    private static void testobtenerTipoInterlocutor (){

        user usuario = [SELECT id FROM User WHERE userName = 'televentaUser@test.com'];
        Account acc = [SELECT id FROM Account WHERE NIF_CIF_Customer_NIE__c = '71708328D'];
        test.startTest();
        system.runAs(usuario){
            String result = TV_CLS_Utils.obtenerTipoInterlocutor(acc.ID);
            system.assertNotEquals('', result);
        } 
        test.stopTest();
    }
    
    @isTest
    private static void testestadoRGPDMotivoLicito (){

        user usuario = [SELECT id FROM User WHERE userName = 'televentaUser@test.com'];
        Contact cont = [SELECT Id FROM COntact WHERE Email = 'test@gmail.com' LIMIT 1];
        test.startTest();
        system.runAs(usuario){
            String result = TV_CLS_Utils.estadoRGPDMotivoLicito(cont.Id);
            system.assertNotEquals('', result);
        } 
        test.stopTest();
    }
    
}