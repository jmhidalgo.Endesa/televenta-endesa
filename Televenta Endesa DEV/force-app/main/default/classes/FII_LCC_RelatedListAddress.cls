public with sharing class FII_LCC_RelatedListAddress {

    @AuraEnabled
    public static List<B2C_Address__c> returnListAddress(String clientId, Boolean obs){
        List<B2C_Address__c> listaDirecciones; 
        if(Schema.sObjectType.B2C_Address__c.isAccessible()){

            if(!obs) {
            listaDirecciones = [SELECT Id, Name, Country_desc__c,City__c,Zipcode__c,tolabel(Province_cd__c), Premise_flg__c, Deprecated__c
                            FROM B2C_Address__c
                            WHERE Account__c = :clientId AND Deprecated__c = false];
            }
            else {
                listaDirecciones = [SELECT Id, Name, Country_desc__c,City__c,Zipcode__c,tolabel(Province_cd__c), Premise_flg__c, Deprecated__c
                            FROM B2C_Address__c
                            WHERE Account__c = :clientId];
            }
        }
        return listaDirecciones;
    }

    @AuraEnabled
    public static Case standardBehaviorGSM(Id recId, String type, String subtype, String subjectNeed, String subjectAct, Id camId, String status, String recordtype){
        Case need = (Case)FII_Util_GSM.standardBehaviorGSM(recId, type, subtype, subjectNeed, subjectAct, camId, status, recordtype)[0];
        return need;
    }

    @AuraEnabled
    public static boolean disableButton(Id myRecId, Id eventRecId, Task itInCourse){
        return FII_Util_GSM.disableButton(myRecId, eventRecId, itInCourse);
    }

    @AuraEnabled 
	public static Boolean isTheEventForMe (Id myRecId, Id eventRecId, Task interactionInProgress){
		return FII_Util_GSM.isTheEventForMe(myRecId, eventRecId, interactionInProgress);
	}
    
    @AuraEnabled
    public static String returnMainAddress(String id){
        try{
            Id mainAddress;
            if(Schema.sObjectType.Account.isAccessible()){
                mainAddress=[SELECT Addr_id__c FROM Account WHERE id =:id][0].Addr_id__c;
            }
            if (mainAddress == null) mainAddress='';
            return mainAddress;

        }catch(exception e){
            return '';
        }

    }

    @AuraEnabled
    public static string mainAddressApex(Id accountid, Id addressid){ 
        try{
            if(Schema.sObjectType.Account.isAccessible() && Schema.sObjectType.Account.isUpdateable()){
                Account acc =[SELECT Id, Addr_id__c FROM Account WHERE Id =:accountid];
                final String oldAdd = acc.Addr_id__c;
                acc.Addr_id__c = addressid;
                update acc;

                if(Schema.sObjectType.B2C_Address__c.isAccessible() && Schema.sObjectType.B2C_Address__c.isUpdateable()){
                    final List<String> addIds = new List<String>{oldAdd, addressid};
                    final Map<Id, B2C_Address__c> addMap = new Map<Id, B2C_Address__c>([SELECT Id, Main_Address__c FROM B2C_Address__c WHERE Id IN :addIds]);
                    addMap.get(oldAdd).Main_Address__c = false;
                    addMap.get(addressid).Main_Address__c = true;
                    update addMap.values();
                }
            }
            return '0';
        }catch(exception e){
            System.debug('MYERROR ' + e);
            return ''+e;
        }
    }

    @AuraEnabled
    public static String deleteAddress(Id id){
        
        B2C_Address__c add=[SELECT id FROM B2C_Address__c WHERE id=:id];
         //IBAA-281 (ARL) Validaciones
        
        List<Account> listDicPrinc = new List<Account>();
        List<B2C_Billing_Profile__c> listDatosPago = new List<B2C_Billing_Profile__c>();
        List<B2C_Service_Point__c> listPuntoSum = new List<B2C_Service_Point__c>();
        if(Schema.sObjectType.Account.isAccessible()){
            listDicPrinc=[SELECT id,Addr_id__c FROM Account WHERE Addr_id__c =:id];
        }
        if(Schema.sObjectType.B2C_Billing_Profile__c.isAccessible()){
            listDatosPago=[SELECT id,Adress__c FROM B2C_Billing_Profile__c WHERE Adress__c =:id];
        }
        if(Schema.sObjectType.B2C_Service_Point__c.isAccessible()){
            listPuntoSum=[SELECT id,AddrId_Id__c FROM B2C_Service_Point__c WHERE AddrId_Id__c =:id];
        }
        String error='';
        for(Account acc:listDicPrinc)
            if(acc.Addr_id__c==add.id){
                error+='-Dependen direcciones fiscales/principales.';
                break;
            }
        for(B2C_Billing_Profile__c bp:listDatosPago){
            if(bp.Adress__c==add.id) {
                error+='-Dependen datos de pago.';
                break;
            }
        }
        for(B2C_Service_Point__c sp:listPuntoSum){
            if(sp.AddrId_Id__c==add.id){
                error+='-Dependen puntos de suministro.';
                break;
            }
        }
        if(error!='') {
            return error;
        }
        else{
            add.Deprecated__c=true;
            add.FII_DIR_FLG_Deprecated__c =true;
            update add;
            error='0';
        }
            
        System.debug('error'+error);
        return error;
    }
    
    /***  TELEVENTA  ***/
    @AuraEnabled
    public static Boolean getIsTeleventaProfile(){
		return TV_CLS_Utils.getIsTeleventaProfile();
    }
}