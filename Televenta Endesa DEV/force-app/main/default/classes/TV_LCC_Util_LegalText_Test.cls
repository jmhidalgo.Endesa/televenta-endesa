@isTest
public class TV_LCC_Util_LegalText_Test {

    @testSetup
    public static void testSetup() {
        NeedConfig__c needConfig = new NeedConfig__c();
        needConfig.NeedMethodSign__c = '10';
        needConfig.NeedChannel__c = '4';
        insert needConfig;

        Account account = new Account();
        account.LastName__c='TestNameSetUp';
        account.FirstName__c = 'TestNameSetUp';
        account.Identifier_Type__c = 'NIF';
        account.NIF_CIF_Customer_NIE__c = '36982412E';
        account.Email_con__c = 'test@gmail.com';
        account.Contact_method_cd__c = 'E-mail';
        account.Licensed__c = false;
        account.Cliente_External_Id__c = '34-ee';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
        account.Physical_or_legal_cd__c = 'Physical Customer';
        account.MobilePhone1__c='666666666';
        account.No_Phone_flg__c = false;
        account.No_Email_flg__c = false;
        account.Identifier_Number__c = 224346;
        account.Account_Type__c = 'Residential';
        account.Electronic_bill_flg__c = true;
        account.Duplicate_convenience_flg__c = true;
        account.Name = 'Autocalculado';
        insert account;

        Contact cont = new Contact(FirstName = 'Cont', LastName = 'TestsQuery', Contact_method_cd__c = 'SMS',AccountId = account.Id, MobilePhone = '632145987', No_Phone_flg__c  = false, No_Email_flg__c  = true, Document_Type__c ='NIF', NIF_CIF_Customer_NIE__c ='73009311C');
        cont.Physical_or_legal_cd__c = 'Physical Customer';
        insert cont;

        account.TitularContactHide__c = cont.Id;
        update account;
        /*/AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = account.Id;
        acr.ContactId = cont.Id;
        acr.Roles = 'Titular de contrato';
        insert acr;/*/
        AccountContactRelation a = [SELECT Id,AccountId,ContactId FROM AccountContactRelation][0];
        System.debug('TTTT:' + a.AccountId + ',' + a.ContactId);

        Case case1 = new Case();
        case1.AccountId = account.Id;
        case1.ContactId = cont.Id;
        insert case1;

        B2C_CNAE__c cnae1 = new B2C_CNAE__c();
        cnae1.name = '9820';
        cnae1.Segment__c = 'Doméstico';
        insert cnae1;      
        
        final B2C_Address__c address = new B2C_Address__c();
        address.Premise_flg__c = true;
        address.Main_Address__c = false;
        address.Adress_active_flg__c = true;
        address.Country_cd__c = 'ES';
        address.Country_desc__c = 'ESPAÑA';
        address.Province_cd__c = '15';
        address.Province_text__c = 'A CORUÑA';
        address.County_cd__c = '030';
        address.County__c = 'A CORUÑA';
        address.City_cd__c = 'BER13108';
        address.City__c = 'A CORUÑA';
        address.INE_city_cd__c = '15030000101';
        address.Street_cd__c = 'BER0569533';
        address.Street__c = 'NELLE';
        address.INE_street_cd__c = '1503000423';
        address.Type_street__c = 'RD';
        address.Zipcode__c = '12345';
        address.Longitude__c = 0;
        address.Latitude__c = 0;
        address.Census_tract__c = '123';
        address.Street_number__c = '109';
        address.Point_duplicator_cd__c = 'A';
        address.Type_Point_lightener_cd__c = 'AFS';
        address.Point_lightener__c = 'S';
        address.Floor_cd__c = '4';
        address.Door__c = '2';
        address.Build__c = '1';
        address.Stairs__c  = '3';
        address.Adress_lightener__c = 'asdf';
        address.Type_address_cd__c = 'Normalized';
        address.Reference_year__c = 2010;
        address.INE_county_cd__c = '030';
        address.INE_city_cd__c = '15030000101';
        address.UTM_x__c = '2';
        address.UTM_y__c = '2';
        address.Trust_level__c = 5;
        address.CUPS_13_gas__c = 'CRM0001391063';
        address.CUPS_13_elec__c = 'CRM0001312697';
        address.Description_adress__c = 'Descripcion calle';
        address.INE_state_cd__c = 'PR';
        address.DIR_external_CRM_id__c = '123456789012345';
        address.Formatedd_Address__c = 'Formatedd Address';
        address.DIR_external_account_id__c = '123456789012345';
        address.DIR_external_contact_id__c = '123456789012345';
        address.Account__c=account.Id;
        AddressTriggerHandler.bypassTrigger = true;
        insert address;
        AddressTriggerHandler.bypassTrigger = false;      

        B2C_Service_Point__c sp = new B2C_Service_Point__c();
        sp.RecordTypeId = Schema.SObjectType.B2C_Service_Point__c.getRecordTypeInfosByName().get('Gas').getRecordTypeId();
        sp.Name = 'PENDIENTE_INFORMAR';
        sp.Business_line_cd__c = '02';
        sp.Tariff_cd__c = '31';
        sp.Annual_Consumption__c = 3000;
        sp.Reason_creation__c='No localizado';
        sp.Distribution_Group_Gas__c='Test';
        sp.DistribuitorGas_cd__c='0222';
        sp.PostalCode__c = '12345';
        sp.Street__c = 'Falsa';
        sp.Street_number__c = '68';
        sp.country__c = 'ES';
        sp.CNAE__c = cnae1.Id;
        sp.Account_id__c = account.Id;
        sp.AddrId_Id__c = address.Id;

        B2C_Service_Point__c spE = new B2C_Service_Point__c();
        spE.RecordTypeId = Schema.SObjectType.B2C_Service_Point__c.getRecordTypeInfosByName().get('Electricidad').getRecordTypeId();
        spE.Name = 'PENDIENTE_INFORMAR';
        spE.Business_line_cd__c = '01';
        spE.Tariff_cd__c = '006';
        spE.Annual_Consumption__c = 3000;
        spE.Reason_creation__c='No localizado';
        spE.Distribution_Group_Gas__c='Test';
        spE.DistribuitorGas_cd__c='0222';
        spE.PostalCode__c = '12345';
        spE.Street__c = 'Falsa';
        spE.Street_number__c = '68';
        spE.country__c = 'ES';
        spE.CNAE__c = cnae1.Id;
        spE.Account_id__c = account.Id;
        spE.AddrId_Id__c = address.Id;
        ServicePointTriggerHandler.bypassTrigger = true;
        insert new List<B2C_Service_Point__c>{sp, spE};
        ServicePointTriggerHandler.bypassTrigger = false;


        NE__Quote__c oferta = new NE__Quote__c();
        oferta.NE__AccountId__c = account.Id;
        oferta.Case__c = case1.Id;
        oferta.FI_NEQ_SEL_Movement_type__c = 'A';
        oferta.FI_NEQ_SEL_Flow_Controlling_Action__c = 'A';
        oferta.FI_NEQ_LKP_CUPS_Gas__c = sp.Id;
        oferta.FI_NEQ_FLG_Digital_Invoice_Quote__c = true;

        NE__Quote__c ofertaE = new NE__Quote__c();
        ofertaE.NE__AccountId__c = account.Id;
        ofertaE.Case__c = case1.Id;
        ofertaE.FI_NEQ_SEL_Movement_type__c = 'A';
        ofertaE.FI_NEQ_SEL_Flow_Controlling_Action__c = 'A';
        ofertaE.FI_NEQ_LKP_CUPS_Electric__c = spE.Id;
        ofertaE.FI_NEQ_FLG_Digital_Invoice_Quote__c = true;
        insert new List<NE__Quote__c>{oferta, ofertaE};

        NE__Order__c configuration = new NE__Order__c();
        configuration.NE__Configuration_Type__c = 'New';
        configuration.NE__Configuration_SubType__c = 'Standard';
        configuration.NE__AccountId__c = account.Id;
        configuration.NE__BillAccId__c = account.Id;
        configuration.NE__ServAccId__c = account.Id;
        configuration.NE__Quote__c = oferta.id;
        insert configuration;

        final NE__Catalog__c ca = new NE__Catalog__c();
        insert ca;

        NE__Catalog_Item__c cata = new NE__Catalog_Item__c();
        cata.Ne__Catalog_Id__c= ca.id;
        insert cata;

        NE__Product__c product = new NE__Product__c();
        product.RecordTypeId = Schema.SObjectType.NE__Product__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        product.Type__c = 'Product';
        product.Code__c = 'TNS31';
        product.Name='Gas Endesa Mini';
        product.FI_CMP_FLG_Allowed_Price_Annex__c = true;

        NE__Product__c product1 = new NE__Product__c();
        product1.RecordTypeId = Schema.SObjectType.NE__Product__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        product1.Type__c = 'Product';
        product1.Code__c = 'TNS32';
        product1.Name='Gas Endesa Mini1';
        product.FI_CMP_FLG_Allowed_Price_Annex__c = true;

        FI_CongaParametrization__c c1 = new FI_CongaParametrization__c();
        c1.Name = 'Test1';
        c1.FI_COP_TXT_Conga_Code__c = 'TNS31';
        c1.FI_COP_TXT_Conga_Real_Name__c = 'Test';
        c1.FI_COP_TXT_Conga_Commercial_Name__c = 'Test';
        c1.FI_COP_DAT_Conga_Start_Date__c = Date.today();
        c1.FI_COP_SEL_Conga_Language__c = 'Español';
        c1.FI_COP_NUM_Conga_Component_Sequence__c = 1;
        
        FI_CongaParametrization__c c2 = new FI_CongaParametrization__c();
        c2.Name = 'Test2';
        c2.FI_COP_TXT_Conga_Code__c = 'TNS32';
        c2.FI_COP_TXT_Conga_Real_Name__c = 'Test';
        c2.FI_COP_TXT_Conga_Commercial_Name__c = 'Test';
        c2.FI_COP_DAT_Conga_Start_Date__c = Date.today();
        c2.FI_COP_SEL_Conga_Language__c = 'Español';
        c2.FI_COP_NUM_Conga_Component_Sequence__c = 2;
        
        insert new List<FI_CongaParametrization__c>{c1, c2};
            
        NE__Product__c productD = new NE__Product__c();
        productD.RecordTypeId = Schema.SObjectType.NE__Product__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        productD.Type__c = 'Product';
        productD.Code__c = 'DESC';
        productD.Name = 'Descuento';

        NE__Product__c productE = new NE__Product__c();
        productE.RecordTypeId = Schema.SObjectType.NE__Product__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
        productE.Type__c = 'Product';
        productE.Code__c = 'ELEC';
        productE.Name = 'Descuento';
        insert new List<NE__Product__c>{product, product1, productD, productE};

        FI_TRG_NE_OrderItem_Handler.bypassTrigger = true;
        NE__OrderItem__c orderItem = new NE__OrderItem__c();
        orderItem.NE__OrderId__c = configuration.Id;
        orderItem.NE__Qty__c = 1;
        orderItem.NE__ProdId__c=product.Id;
        orderItem.NE__Account__c = account.Id;
        orderItem.NE__Status__c = 'Pending';
        orderItem.FI_CI_LKP_Quote__c = oferta.Id;
        orderItem.FI_CMP_SEL_Commodity__c = 'Gas';
        orderItem.FI_CI_SEL_Business_Line__c = '02';
        orderItem.FI_CI_LKP_CUPS__c = sp.Id;
        orderItem.FI_CI_SEL_Type__c = 'A';
        orderItem.FI_CI_SEL_Subtype__c = 'CTN';
        orderItem.FI_CI_SEL_Company_holder__c = '20';
        orderItem.FI_CI_FLG_Skip_Validations__c = true;
        orderItem.FI_CI_TXT_Product_number__c = 'TNS31';
        orderItem.FI_CI_FLG_Quota12__c = true;
        insert orderItem;

        NE__Order_Item_Attribute__c oa = new NE__Order_Item_Attribute__c();
        oa.Name = 'Código Descuento CRM';
        oa.NE__Value__c = 'DESC';
        oa.NE__Order_Item__c = orderItem.Id;
        insert oa;

        NE__OrderItem__c orderItem1 = new NE__OrderItem__c();
        orderItem1.NE__OrderId__c = configuration.Id;
        orderItem1.NE__Qty__c = 1;
        orderItem1.NE__ProdId__c=product1.Id;
        orderItem1.NE__Account__c = account.Id;
        orderItem1.FI_CI_LKP_Quote__c = oferta.Id;
        orderItem1.FI_CI_SEL_Type__c = 'A';
        orderItem1.FI_CI_SEL_Subtype__c = 'CC';
        orderItem1.FI_CI_SEL_Business_Line__c = '02';
        orderItem1.FI_CMP_SEL_Commodity__c = 'Gas';
        orderitem1.FI_CI_SEL_Access_channel__c = '66';
        orderItem1.FI_CI_LKP_CUPS__c = sp.Id;
        orderItem1.FI_CI_SEL_Company_holder__c = '20';
        orderItem1.FI_CI_FLG_Electronicinvoice__c = true;
        orderItem1.FI_CI_FLG_Skip_Validations__c = true;
        orderItem1.FI_CI_TXT_Product_number__c = 'TNS32';
        orderItem1.FI_CI_FLG_Quota12__c = true;
        insert orderItem1;

        
        NE__Order_Item_Attribute__c oa1 = new NE__Order_Item_Attribute__c();
        oa1.Name = 'Código Descuento CRM';
        oa1.NE__Value__c = 'DESC';
        oa1.NE__Order_Item__c = orderItem1.Id;
        insert oa1;

        NE__OrderItem__c orderItem2 = new NE__OrderItem__c();
        orderItem2.NE__OrderId__c = configuration.Id;
        orderItem2.NE__Qty__c = 1;
        orderItem2.NE__ProdId__c = productD.Id;
        orderItem2.FI_CI_SEL_Type__c = 'A';
        orderItem2.FI_CI_SEL_Subtype__c = 'AD';
        orderItem2.NE__Parent_Order_Item__c = orderItem1.Id;
        orderItem2.NE__Account__c = account.Id;
        orderItem2.FI_CI_SEL_Access_channel__c = '66';
        orderItem2.FI_CI_SEL_Company_holder__c = '20';
        orderItem2.FI_CI_FLG_Electronicinvoice__c = true;
        orderItem2.FI_CI_FLG_Skip_Validations__c = true;
        insert orderItem2;

        NE__Order_Item_Attribute__c oa2 = new NE__Order_Item_Attribute__c();
        oa2.Name = 'Código Descuento CRM';
        oa2.NE__Value__c = 'DESC';
        oa2.NE__Order_Item__c = orderItem2.Id;
        insert oa2;

        NE__OrderItem__c orderItemE = new NE__OrderItem__c(); 
        orderItemE.NE__OrderId__c = configuration.Id;
        orderItemE.NE__Qty__c = 1;
        orderItemE.NE__ProdId__c = productE.Id;
        orderItemE.NE__Account__c = account.Id;
        orderItemE.FI_CI_LKP_Quote__c = ofertaE.Id;
        orderItemE.FI_CI_SEL_Type__c = 'A';
        orderItemE.FI_CI_SEL_Subtype__c = 'CC';
        orderItemE.FI_CI_SEL_Business_Line__c = '01'; 
        orderItemE.FI_CMP_SEL_Commodity__c = 'Luz';
        orderItemE.FI_CI_SEL_Access_channel__c = '66';
        orderItemE.FI_CI_LKP_CUPS__c = spE.Id;
        orderItemE.FI_CI_SEL_Company_holder__c = '20';
        orderItemE.FI_CI_FLG_Electronicinvoice__c = true;
        orderItemE.FI_CI_FLG_Skip_Validations__c = true;
        orderItemE.FI_CI_TXT_Product_number__c = 'ELEC';
        insert orderItemE;

        
        NE__Order_Item_Attribute__c oaE = new NE__Order_Item_Attribute__c();
        oaE.Name = 'Código Descuento CRM';
        oaE.NE__Value__c = 'DESC';
        oaE.NE__Order_Item__c = orderItemE.Id;
        insert oaE;

        Integer order = 1;
        List<TV_LegalTextGeneric__c> listGeneralText = new List<TV_LegalTextGeneric__c>();

        TV_LegalTextGeneric__c t1 = new TV_LegalTextGeneric__c();
        t1.TV_LTG_SEL_Type__c = 'PUNTO_SUMINISTRO';
        t1.TV_LTG_TXT_Text__c = 'a';
        t1.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t1);
        order++;

        TV_LegalTextGeneric__c t2 = new TV_LegalTextGeneric__c();
        t2.TV_LTG_SEL_Type__c = 'INICIO';
        t2.TV_LTG_SEL_Subtype__c = 'NIF';
        t2.TV_LTG_TXT_Text__c = 'a';
        t2.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t2);
        order++;

        TV_LegalTextGeneric__c t3 = new TV_LegalTextGeneric__c();
        t3.TV_LTG_SEL_Type__c = 'PERSONA';
        t3.TV_LTG_SEL_Subtype__c = 'AUTORIZADO';
        t3.TV_LTG_TXT_Text__c = 'a';
        t3.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t3);
        order++;

        TV_LegalTextGeneric__c t4 = new TV_LegalTextGeneric__c();
        t4.TV_LTG_SEL_Type__c = 'PRODUCTO';
        t4.TV_LTG_TXT_Text__c = 'a';
        t4.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t4);
        order++;

        TV_LegalTextGeneric__c t5 = new TV_LegalTextGeneric__c();
        t5.TV_LTG_SEL_Type__c = 'FIN_OFERTA';
        t5.TV_LTG_SEL_Subtype__c = '';
        t5.TV_LTG_TXT_Text__c = 'a';
        t5.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t5);
        order++;

        TV_LegalTextGeneric__c t6 = new TV_LegalTextGeneric__c();
        t6.TV_LTG_SEL_Type__c = 'EFACTURA';
        t6.TV_LTG_SEL_Subtype__c = 'SI';
        t6.TV_LTG_TXT_Text__c = 'a';
        t6.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t6);
        order++;

        TV_LegalTextGeneric__c t7 = new TV_LegalTextGeneric__c();
        t7.TV_LTG_SEL_Type__c = 'IBAN';
        t7.TV_LTG_TXT_Text__c = 'a';
        t7.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t7);
        order++;

        TV_LegalTextGeneric__c t8 = new TV_LegalTextGeneric__c();
        t8.TV_LTG_SEL_Type__c = 'PROMO';
        t8.TV_LTG_TXT_Text__c = 'a';
        t8.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t8);
        order++;

        TV_LegalTextGeneric__c t9 = new TV_LegalTextGeneric__c();
        t9.TV_LTG_SEL_Type__c = 'DISTRIBUIDORA';
        t9.TV_LTG_SEL_Subtype__c = 'NIF';
        t9.TV_LTG_TXT_Text__c = 'a';
        t9.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t9);
        order++;

        TV_LegalTextGeneric__c t10 = new TV_LegalTextGeneric__c();
        t10.TV_LTG_SEL_Type__c = 'DERECHOS_ENDESA';
        t10.TV_LTG_SEL_Subtype__c = 'NIF';
        t10.TV_LTG_TXT_Text__c = 'a';
        t10.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t10);
        order++;

        TV_LegalTextGeneric__c t11 = new TV_LegalTextGeneric__c();
        t11.TV_LTG_SEL_Type__c = 'BONO_SOCIAL';
        t11.TV_LTG_TXT_Text__c = 'a';
        t11.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t11);
        order++;
        
        TV_LegalTextGeneric__c t12 = new TV_LegalTextGeneric__c();
        t12.TV_LTG_SEL_Type__c = 'FIN_TXL';
        t12.TV_LTG_TXT_Text__c = 'a';
        t12.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t12);
        order++;
        
        TV_LegalTextGeneric__c t13 = new TV_LegalTextGeneric__c();
        t13.TV_LTG_SEL_Type__c = 'CUOTADOCE';
        t13.TV_LTG_TXT_Text__c = 'a';
        t13.TV_LTG_NUM_Order__c = order;
        listGeneralText.add(t13);
        order++;

        insert listGeneralText;

        TV_LegalTextProduct__c p1 = new TV_LegalTextProduct__c();
        p1.TV_LTP_TXT_ProductCode__c = 'TNS31';
        p1.TV_LTP_TXT_Text__c = 'a';
        p1.TV_LTP_FLG_Active__c = true;

        TV_LegalTextProduct__c p2 = new TV_LegalTextProduct__c();
        p2.TV_LTP_TXT_ProductCode__c = 'TNS32';
        p2.TV_LTP_TXT_Text__c = 'a';
        p2.TV_LTP_FLG_Active__c = true;

        TV_LegalTextProduct__c p3 = new TV_LegalTextProduct__c();
        p3.TV_LTP_TXT_ProductCode__c = 'DESC';
        p3.TV_LTP_TXT_Text__c = 'a';
        p3.TV_LTP_FLG_Active__c = true;
        
        TV_LegalTextProduct__c p4 = new TV_LegalTextProduct__c();
        p4.TV_LTP_TXT_ProductCode__c = 'ELEC';
        p4.TV_LTP_TXT_Text__c = 'a';
        p4.TV_LTP_FLG_Active__c = true;
        
        TV_LegalTextProduct__c p5 = new TV_LegalTextProduct__c();
        p5.TV_LTP_TXT_ProductCode__c = 'ELEC';
        p5.TV_LTP_SEL_Type__c = 'ATRIBUTO';
        p5.TV_LTP_TXT_Text__c = 'Más opciones';
        p5.TV_LTP_FLG_Active__c = true;

        insert new List<TV_LegalTextProduct__c>{p1, p2, p3, p4, p5};
            
        APXTConga4__Conga_Template__c template = new APXTConga4__Conga_Template__c();
        template.APXTConga4__Name__c = 'TV_TextoLegal';
        insert template;
        
        APXTConga4__Conga_Merge_Query__c  mergeQuery = new APXTConga4__Conga_Merge_Query__c();
        mergeQuery.APXTConga4__Name__c = 'tvlt';
        mergeQuery.APXTConga4__Query__c = 'select id from contact limit 1';
        mergeQuery.FI_COQ_TXT_ExternalId__c = 'tvlt';
        insert mergeQuery;
    }

    @isTest 
    public static void testGetLegalTextAndUrlConga(){
        Test.startTest();
            B2C_Billing_Profile__c pm = new B2C_Billing_Profile__c ();
            pm.IBAN__c = 'ES5700813489496621277682';
            insert pm;
            
            List<NE__OrderItem__c> listCIs = [SELECT  	Id, 
                                                      	FI_CI_LKP_CUPS__c, 
                                                      	FI_CI_LKP_CUPS__r.PostalCode__c,
                                                      	FI_CI_SEL_Business_Line__c,
                                              			FI_CI_NUM_PowerToHire1kW__c,
                                              			FI_CI_NUM_PowerToHire2kW__c,
                                              			FI_CI_NUM_PowerToHire3kW__c
                                              FROM NE__OrderItem__c];
            for(NE__OrderItem__c ci : listCIs){
                ci.FI_CI_LKP_PaytmenDatas__c = pm.Id;
            }
            update listCIs;
            
            List<Case> listCase = [SELECT Id FROM Case LIMIT 1];
        	TV_LCC_Util_LegalText.getLegalTextAndUrlConga(listCase[0].Id);
        	TV_LCC_Util_LegalText.getLegalTextAndUrlConga(null);
            
        	TV_LCC_Util_LegalText.isValidPostalCode('0000');
        	TV_LCC_Util_LegalText.isValidPostalCode('00000');
        	TV_LCC_Util_LegalText.isIGIC('35000');
        	TV_LCC_Util_LegalText.isIGIC('00000');
        	TV_LCC_Util_LegalText.isIPSI('51000');
        	TV_LCC_Util_LegalText.isIPSI('00000');
        	TV_LCC_Util_LegalText.maxPower(new List<Decimal>{1, 2});
        	TV_LCC_Util_LegalText.getAttributeValue([SELECT Id,Name FROM NE__Order_Item_Attribute__c], 'Test');
        	TV_LCC_Util_LegalText.calculatePriceWithTaxes(listCIs[0], 'Test', '10');
        
        Test.stopTest();
    }

}