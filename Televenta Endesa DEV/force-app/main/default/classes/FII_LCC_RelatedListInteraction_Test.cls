/**
	* @author Everis
	* @since November 22, 2018 
	* @desc Test of FII_LCC_RelatedListInteraction
	* @history November 22, 2018 - everis (JVC) - Create Test Apex Class 
*/
@isTest
private class FII_LCC_RelatedListInteraction_Test {
	
	private static final Id IDRT_CAS_ACTION = FII_Util_MasterData_RecordTypes.getByDevName(Label.FII_RT_CAS_ACTION).Id;
	
	private static final String CONTACT_LASTNAME = 'Test2';
	private static final String ACCOUNT_NIF = '85258421C';
	private static final String LEAD_EMAIL = 'test@test.com';
	private static final String NEED_SUBJECT = 'NEED';
	private static final String ACTION_CASE_SUBJECT = 'ACTION CASE';

	@testSetup static void setup(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
		final Contact con = UtilTest.createContact();
		AccountTriggerHandler.bypassTrigger = true;
		insert con;
		AccountTriggerHandler.bypassTrigger = false;
		final Lead newLead = UtilTest.createLead();
		insert newLead;

		// Need related with client and contact
		Case need = FII_Util_GSM.createNeed(con.AccountId, null, NEED_SUBJECT, null);
		FII_TRG_Case_Handler.bypassTrigger = true;
		insert need;
		FII_TRG_Case_Handler.bypassTrigger = false;
		
		// Interaction related with client and contact
		final Task interaction1 = FII_Util_GSM.createInteraction(con.AccountId, con.Id, null, 'Phone', 'interaction1');
		FII_TRG_Task_Handler.bypassTrigger = true;
		insert interaction1;
		FII_TRG_Task_Handler.bypassTrigger = false;
		need.FII_CAS_TXT_Opened_interaction__c = interaction1.Id;
		need.ContactId = con.Id;
		need.Origin = 'Phone';
		FII_TRG_Case_Handler.bypassTrigger = true;
		update need;
		FII_TRG_Case_Handler.bypassTrigger = false;

		// Interactions in progress
		final Map<Id,List<Task>> interactionsInProgressMap = FII_Util_GSM.getInteractionsInProgressBulk(new List<Id>{con.AccountId}, true);

		// Action Case related with need
		Case actionCase = need.clone();
		actionCase.ParentId = need.Id;
		actionCase.RecordTypeId = IDRT_CAS_ACTION;
		actionCase.Subject = ACTION_CASE_SUBJECT;
		FII_TRG_Case_Handler.bypassTrigger = true;
		insert actionCase;
		FII_TRG_Case_Handler.bypassTrigger = false;
		// Child Interaction of Action case related with need
		final Task interactionOfActionCase = FII_Util_GSM.createNewChildInteractions(new List<CAse>{actionCase}, interactionsInProgressMap)[0];
		FII_TRG_Task_Handler.bypassTrigger = true;
		insert interactionOfActionCase;
		FII_TRG_Task_Handler.bypassTrigger = false;

		// Action Task related with need
		Task actionTask = FII_Util_GSM.createTaskAction(need, 'Modificación de datos', 'Completed', Label.FII_RT_TSK_ACTION);
		actionTask.WhoId = con.Id;
		actionTask.FII_ACT_SEL_Channel__c = 'Phone';
		FII_TRG_Task_Handler.bypassTrigger = true;
		insert actionTask;
		FII_TRG_Task_Handler.bypassTrigger = false;
		// Child Interaction of Action Task related with need
		final Task interactionOfActionTask = FII_Util_GSM.createNewChildInteractions(new List<Task>{actionTask}, interactionsInProgressMap)[0];
		FII_TRG_Task_Handler.bypassTrigger = true;
		insert interactionOfActionTask;
		FII_TRG_Task_Handler.bypassTrigger = false;
	}
	
	@isTest static void getInteractionsRelatedList(){
		final Contact con = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = :CONTACT_LASTNAME LIMIT 1];
		final Account acc = [SELECT Id, Name FROM Account WHERE NIF_CIF_Customer_NIE__c = :ACCOUNT_NIF LIMIT 1];
		final Lead newLead = [SELECT Id, Name FROM Lead WHERE Email = :LEAD_EMAIL LIMIT 1];
		Test.startTest();
		final List<Task> interactionsOfClient = FII_LCC_RelatedListInteraction.getInteractionsRelatedList(acc.Id);
		final List<Task> interactionsOfContact = FII_LCC_RelatedListInteraction.getInteractionsRelatedList(con.Id);
		final List<Task> interactionsOfLead = FII_LCC_RelatedListInteraction.getInteractionsRelatedList(newLead.Id);
		System.assertEquals(interactionsOfClient.size(), 1);
		System.assertEquals(interactionsOfContact.size(), 1);
		System.assertEquals(interactionsOfLead.size(), 0);
		Test.stopTest();
	}

	@isTest static void getRecordPrimary() {
		final Contact con = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = :CONTACT_LASTNAME LIMIT 1];
		final Account acc = [SELECT Id, Name FROM Account WHERE NIF_CIF_Customer_NIE__c = :ACCOUNT_NIF LIMIT 1];
		final Lead newLead = [SELECT Id, Name FROM Lead WHERE Email = :LEAD_EMAIL LIMIT 1];
		Test.startTest();
		SObject obj;
		obj = FII_LCC_RelatedListInteraction.getRecordPrimary(acc.Id);
		System.assertEquals((String)obj.get(Schema.Account.Name), acc.Name);
		obj = FII_LCC_RelatedListInteraction.getRecordPrimary(con.Id);
		System.assertEquals((String)obj.get(Schema.Contact.Name), con.Name);
		obj = FII_LCC_RelatedListInteraction.getRecordPrimary(newLead.Id);
		System.assertEquals((String)obj.get(Schema.Lead.Name), newLead.Name);
		Test.stopTest();
	}
    
    @isTest static void isTheEventForMe_Test(){
        final Contact con = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = :CONTACT_LASTNAME LIMIT 1];
		final Account acc = [SELECT Id, Name FROM Account WHERE NIF_CIF_Customer_NIE__c = :ACCOUNT_NIF LIMIT 1];
		final Task interactionClient = [SELECT id, WhatId FROM Task WHERE Subject = 'Interacción: interaction1' LIMIT 1];
        Test.startTest();
        final Boolean isTheEventForMeFlag = FII_LCC_RelatedListInteraction.isTheEventForMe(acc.Id, acc.Id, interactionClient);
        system.assertEquals(isTheEventForMeFlag, true);
        Test.stopTest();
    }
    
    @isTest static void hiddenButton_Test(){
        Test.startTest();
        Boolean marcadorView = FII_LCC_RelatedListInteraction.hiddenButton();
        Test.stopTest();
        If ( marcadorView ){
			System.assert(true, 'El usuario no puede ver el marcador');           
        } else {
            System.assert(true, 'El usuario puede ver el marcador');
        }
    }
}