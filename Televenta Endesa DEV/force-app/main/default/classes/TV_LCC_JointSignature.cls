/**
 * @author			ATOS (ABS)
 * @description		Controller clas for lightning component TV_LC_JointSignature
 *
 * Edit Log:
 * @history			2020/04/04  |  IBTV-168  |  ATOS (ABS)   |  Creation.
 * @history			2020/04/05  |  IBTV-168  |  ATOS (ABS)   |  Add funcionality to class.
 * @history			2020/04/06  |  IBTV-168  |  ATOS (ABS)   |  Add funcionality to class.
 */
public without sharing class TV_LCC_JointSignature {
  /**
   * @description		Get account id from case
   * @param			    caseId id of case
   * @return			  return account id
   */
  @AuraEnabled
  public static String getAccountId(String caseId) {
    List<Case> currentCase = [
      SELECT Id, AccountId
      FROM Case
      WHERE Id = :caseId
      LIMIT 1
    ];
    if (!currentCase.isEmpty()) {
      return currentCase[0].AccountId;
    } else {
      return null;
    }
  }

  /**
   * @description		function for check profile
   * @return			true if profile is Verificador else return false
   */
  @AuraEnabled
  public static Boolean checkProfile(){
    Id profileId = userinfo.getProfileId();
    List<Profile> lstProfile = [SELECT Id, Name FROM Profile WHERE Id =:profileId LIMIT 1];
    if(!lstProfile.isEmpty()){
      if(lstProfile[0].Name == 'Verificador FOR'){
        return true;
      }
    }
    return false;
  }

  /**
   * @description		Class for resign
   * @param			  caseId id of case.
   * @return			retrurn true or false for say if updated CIs or not.
   */
  @AuraEnabled
  public static String reSigned(String caseId,Date dateSelected){
    if(dateSelected == null){
      return 'EMPTY';
    }
    final List<NE__Quote__c> lstQuote = [
      SELECT Id
      FROM NE__Quote__c
      WHERE Case__c = :caseId
    ];
    final List<Id> quoteIds = new List<Id>();
    for (NE__Quote__c currentQuote : lstQuote) {
      if (!quoteIds.contains(currentQuote.Id)) {
        quoteIds.add(currentQuote.Id);
      }
    }
    List<NE__OrderItem__c> lstCI = [
      SELECT Id
      FROM NE__OrderItem__c
      WHERE
        FI_CI_LKP_Quote__c IN :quoteIds
        AND NE__Status__c = 'In Review'
        AND FI_CI_SEL_Subtype__c != null
        AND FI_CI_SEL_SignatureType__c = '10'
    ];
    
    if (!lstCI.isEmpty()) {
      for (NE__OrderItem__c currentCI : lstCI) {
        currentCI.FI_CI_DAT_SignatureDate__c = dateSelected;
        currentCI.FI_CI_FLG_Skip_Validations__c = true;
      }
      if (
        Schema.sObjectType.NE__OrderItem__c.isUpdateable() && !lstCI.isEmpty()
      ) {
        update lstCI;
        return 'TRUE';
      }
    }
    return 'FALSE';
  }
  /**
   * @description		sign case and CIs
   * @param			  caseId case id
   * @param       selectedSign selected sign
   * @return      return SIGNED if there is CI signed, return ERROR if there is some error, return SUCCESS if all worked fine.
   */
  @AuraEnabled
  public static String signCaseCIs(String caseId, String selectedSign) {
    final List<NE__Quote__c> lstQuote = [
      SELECT Id
      FROM NE__Quote__c
      WHERE Case__c = :caseId
    ];
    final List<Id> quoteIds = new List<Id>();
    for (NE__Quote__c currentQuote : lstQuote) {
      if (!quoteIds.contains(currentQuote.Id)) {
        quoteIds.add(currentQuote.Id);
      }
    }
    List<NE__OrderItem__c> lstCI = [
      SELECT Id
      FROM NE__OrderItem__c
      WHERE
        FI_CI_LKP_Quote__c IN :quoteIds
        AND (NE__Status__c = 'Signed'
        OR NE__Status__c = 'Signature_pending')
        AND FI_CI_SEL_Subtype__c != null
    ];
    if (!lstCI.isEmpty()) {
      return 'SIGNED';
    }

    Case caseToUpdate = new Case(
      id = caseId,
      TV_SEL_SignatureType__c = selectedSign
    );
    if (Schema.sObjectType.Case.isUpdateable()) {
      update caseToUpdate;
    }

    lstCI = [
      SELECT Id
      FROM NE__OrderItem__c
      WHERE
        FI_CI_LKP_Quote__c IN :quoteIds
        AND (NE__Status__c = 'Pending'
        OR NE__Status__c = 'Pending_after_BO')
        AND FI_CI_SEL_Subtype__c != null
    ];
    for (NE__OrderItem__c currentCI : lstCI) {
      currentCI.FI_CI_DAT_SignatureDate__c = System.today();
      currentCI.FI_CI_FLG_Skip_Validations__c = true;
      currentCI.FI_CI_SEL_SignatureType__c = selectedSign;
    }
    if (
      Schema.sObjectType.NE__OrderItem__c.isUpdateable() && !lstCI.isEmpty()
    ) {
      update lstCI;
      return 'SUCCESS';
    }
    return 'ERROR';
  }

  /**
   * @description		Get login data
   * @return			  return record of parametric.
   */
  @AuraEnabled
  public static WS_Info__mdt getLoginData() {
    List<WS_Info__mdt> wsInfo = [
      SELECT DeveloperName, EndPoint__c, UserName__c, Password__c
      FROM WS_Info__mdt
      WHERE DeveloperName = 'TV_Genesys'
    ];
    if (!wsInfo.isEmpty()) {
      return wsInfo[0];
    }
    return null;
  }
  /**
   * @description		get interaction in progress
   * @param			    accountId id account
   * @return		  	return task.
   */
  @AuraEnabled
  public static Task getInteractions(Id accountId) {
    return FII_LCC_InteractionInProgress.getInteractionsInProgress(accountId);
  }
  /**
   * @description		update type sign
   * @param			    caseId case id
   */
  @AuraEnabled
  public static void updTypeSign(Id caseId) {
    Case caseToUpdate = new Case(id = caseId, TV_SEL_SignatureType__c = '');
    if (Schema.sObjectType.Case.isUpdateable()) {
      update caseToUpdate;
    }
    final List<NE__Quote__c> lstQuote = [
      SELECT Id
      FROM NE__Quote__c
      WHERE Case__c = :caseId
    ];
    final List<Id> quoteIds = new List<Id>();
    for (NE__Quote__c currentQuote : lstQuote) {
      if (!quoteIds.contains(currentQuote.Id)) {
        quoteIds.add(currentQuote.Id);
      }
    }
    List<NE__OrderItem__c> lstCI = [
      SELECT Id
      FROM NE__OrderItem__c
      WHERE
        FI_CI_LKP_Quote__c IN :quoteIds
        AND (NE__Status__c = 'Pending'
        OR NE__Status__c = 'Pending_after_BO')
        AND FI_CI_SEL_Subtype__c != null
    ];
    for (NE__OrderItem__c currentCI : lstCI) {
      currentCI.FI_CI_SEL_SignatureType__c = '';
      currentCI.FI_CI_FLG_Skip_Validations__c = true;
    }
    if (
      Schema.sObjectType.NE__OrderItem__c.isUpdateable() && !lstCI.isEmpty()
    ) {
      update lstCI;
    }
  }

  /**
   * @description		Function for know if channel is ok or not.
   * @param caseId  case id
   * @return			return true if is ok chanel, in other case return false.
   */
  @AuraEnabled
  public static WrapperPermission permissionOK(Id caseId) {
    WrapperPermission wrapPermission = new WrapperPermission();
    NeedConfig__c needConfig = NeedConfig__c.getOrgDefaults();
    String allChannels = needConfig.NeedChannel__c;
    List<String> lstApiName = new List<String>();
    String channel = '';
    if (allChannels.indexOf('#') == -1) {
      lstApiName.add(allChannels);
    } else {
      List<String> lstAux = allChannels.split('#');
      for (String currentChannel : lstAux) {
        if (!lstApiName.contains(currentChannel)) {
          lstApiName.add(currentChannel);
        }
      }
    }
    List<FII_UserJobRelation__c> lstUJR = [
      SELECT Id, FII_UJR_LKP_Job__c
      FROM FII_UserJobRelation__c
      WHERE FII_UJR_LKP_User__c = :UserInfo.getUserId()
    ];
    if (lstUJR.size() == 1) {
      List<FII_Job__c> lstJob = [
        SELECT Id, FII_PT_Canal__c
        FROM FII_Job__c
        WHERE Id = :lstUJR[0].FII_UJR_LKP_Job__c
      ];
      if (lstJob.size() == 1) {
        channel = lstJob[0].FII_PT_Canal__c;
      }
    }
    if (channel == '') {
      wrapPermission.channelOk = false;
    } else {
      if (lstApiName.contains(channel)) {
        wrapPermission.channelOk = true;
      } else {
        wrapPermission.channelOk = false;
      }
    }
    Task interactionInProgress = FII_LCC_InteractionInProgress.getInteractionsInProgress(
      caseId
    );
    if (interactionInProgress == null) {
      wrapPermission.interactionInProgress = false;
    } else {
      wrapPermission.interactionInProgress = true;
    }
    return wrapPermission;
  }

  /**
   * @description		Function for check CI and Address
   * @param			    recordId case id
   * @return			return Wrapper with result of validations
   */
  @AuraEnabled
  public static WrapperValidation checkCIAndAddress(Id recordId) {
    WrapperValidation wrapperVal = new WrapperValidation();
    List<NE__Quote__c> lstQuote = [
      SELECT Id
      FROM NE__Quote__c
      WHERE Case__c = :recordId
    ];
    List<Id> quoteIds = new List<Id>();
    for (NE__Quote__c currentQuote : lstQuote) {
      if (!quoteIds.contains(currentQuote.Id)) {
        quoteIds.add(currentQuote.Id);
      }
    }
    List<NE__OrderItem__c> lstCI = [
      SELECT Id, FI_CI_FLG_Validaciones_OK__c
      FROM NE__OrderItem__c
      WHERE
        FI_CI_LKP_Quote__c IN :quoteIds
        AND NE__Status__c NOT IN ('Pending', 'Pending_after_BO', 'Cancelled')
        AND FI_CI_SEL_Subtype__c != null
    ];
    if (lstCI.size() == 0) {
      wrapperVal.statusKo = false;
    } else {
      wrapperVal.statusKo = true;
    }
    lstCI = [
      SELECT Id, FI_CI_FLG_Validaciones_OK__c, Name
      FROM NE__OrderItem__c
      WHERE
        FI_CI_LKP_Quote__c IN :quoteIds
        AND NE__Status__c IN ('Pending', 'Pending_after_BO')
        AND FI_CI_SEL_Subtype__c != null
    ];
    if (lstCI.size() == 0) {
      wrapperVal.statusKo = true;
    }
    wrapperVal.validationOk = '';
    for (NE__OrderItem__c currentCI : lstCI) {
      if (currentCI.FI_CI_FLG_Validaciones_OK__c == false) {
        wrapperVal.validationOk =
          wrapperVal.validationOk +
          currentCI.Name +
          ', ';
      }
    }
    if (wrapperVal.validationOk.indexOf(', ') != -1) {
      wrapperVal.validationOk = wrapperVal.validationOk.substring(
        0,
        wrapperVal.validationOk.length() - 2
      );
    }
    lstCI = [
      SELECT Id, FI_CI_LKP_Address__c
      FROM NE__OrderItem__c
      WHERE
        FI_CI_LKP_Quote__c IN :quoteIds
        AND (NE__Status__c = 'Pending'
        OR NE__Status__c = 'Pending_after_BO')
        AND FI_CI_SEL_Subtype__c != null
    ];
    List<Id> lstAddress = new List<Id>();
    for (NE__OrderItem__c currentCI : lstCI) {
      if (
        !lstAddress.contains(currentCI.FI_CI_LKP_Address__c) &&
        String.isNotBlank(currentCI.FI_CI_LKP_Address__c)
      ) {
        lstAddress.add(currentCI.FI_CI_LKP_Address__c);
      }
    }
    wrapperVal.differentAddress = lstAddress.size();
    return wrapperVal;
  }
  /**
   * @description		Get legal text and url Conga
   * @param			    caseId case id
   * @return			  return wrapper from legal text
   */
  @AuraEnabled
  public static TV_LCC_Util_LegalText.LegalTextWrapper getLegalTextConga(
    Id caseId
  ) {
    TV_LCC_Util_LegalText.LegalTextWrapper response = new TV_LCC_Util_LegalText.LegalTextWrapper();
    response = TV_LCC_Util_LegalText.getLegalTextAndUrlConga(caseId);
    return response;
  }

  /**
   * @description		Function for get all Method Signature
   * @return			return all methods
   */
  @AuraEnabled
  public static List<String> getAllMethods() {
    List<String> allMethods = new List<String>();
    NeedConfig__c needConfig = NeedConfig__c.getOrgDefaults();
    String methods = needConfig.NeedMethodSign__c;
    if (methods.indexOf('#') == -1 && methods.indexOf('@') != -1) {
      List<String> lstMetAux = methods.split('@');
      allMethods.add(lstMetAux[0] + '#' + lstMetAux[1]);
    } else {
      List<String> lstApiName = new List<String>();
      List<String> lstAux = methods.split('#');
      for (String currentMethod : lstAux) {
        if (!lstApiName.contains(currentMethod)) {
          lstApiName.add(currentMethod);
        }
      }

      Schema.DescribeFieldResult fieldResult = Case.TV_SEL_SignatureType__c.getDescribe();
      List<Schema.PicklistEntry> pickListList = fieldResult.getPicklistValues();
      for (Schema.PicklistEntry pickListValue : pickListList) {
        if (
          pickListValue.isActive() &&
          lstApiName.contains(pickListValue.getValue())
        ) {
          allMethods.add(
            pickListValue.getValue() +
            '#' +
            pickListValue.getLabel()
          );
        }
      }
    }
    return allMethods;
  }

  /**
   * @author			ATOS (ABS)
   * @description		Wrapper for check if button should be activated
   *
   * Edit Log:
   * @history			2020/04/08  |  IBTV-168  |  ATOS (ABS)   |  Creation.
   */
  public class WrapperPermission {
    @AuraEnabled
    /**
     * @description		Variable Boolean for check if channel is OK
     */
    public Boolean channelOk { get; set; }
    @AuraEnabled
    /**
     * @description		Variable Boolean for check if there is interaction in progress
     */
    public Boolean interactionInProgress { get; set; }
  }

  /**
   * @author			ATOS (ABS)
   * @description		Wrapper class for save result of validations
   *
   * Edit Log:
   * @history			2020/04/04  |  IBTV-168  |  ATOS (ABS)   |  Creation.
   */
  public class WrapperValidation {
    @AuraEnabled
    /**
     * @description		Variable Boolean for check CIs are validated
     */
    public String validationOk { get; set; }
    @AuraEnabled
    /**
     * @description		Variable Boolean for check Status pending sign
     */
    public Boolean statusKo { get; set; }
    @AuraEnabled
    /**
     * @description     Variable Boolean for check different Addresses
     */
    public Integer differentAddress { get; set; }
  }
}