/**
 * @author Everis
 * @since March 22, 2019 
 * @desc Class that connects the RelatedIndividualInfo component with Salesforce records
 * @history March 22, 2019 - everis (JJM) - Create Apex Class 
 *          
 *          
 */

public with sharing class FII_LCC_RelatedIndividualInfo {
	/** 
	 * @desc Searchs for the related main individual
	 * 
	 * @param needId: Id of the record shown in the page
	 * @param sobjectType: ComtactId
	 * @return the related main individual
	 */    
    @AuraEnabled
    public static List<Individual> getIndividual(Id idRecord) {   
        List<Individual> ind = null; 
        Id contactId = null;
        Contact indContact = null;

        switch on String.valueOf(idRecord.getSobjectType()){
            when 'Account' {
                //if (Schema.sObjectType.Individual.isAccessible() && Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.Account.isAccessible()){
                Account acc = null;

                if (Schema.sObjectType.Account.isAccessible()) {
                    acc = [SELECT Id, TitularContactHide__c FROM Account WHERE Id =: idRecord LIMIT 1][0];
                    contactId = acc.TitularContactHide__c;
                }

                if (contactId == null && Schema.sObjectType.AccountContactRelation.isAccessible()) {
                    final List<AccountContactRelation> accContRel = [SELECT AccountId, contactId, Roles FROM AccountContactRelation WHERE AccountId =: acc.Id];
                    
                        if(accContRel!=null && accContRel.size()>0 ){
                            Boolean existCont=false;
                        List<Account> accToUpd = new List<Account>();

                            for(AccountContactRelation aContRel : accContRel){
                                if(aContRel.Roles=='Titular de contrato'){
                                    existCont=true;
                                    acc.TitularContactHide__c=aContRel.contactId;
                                    contactId=aContRel.contactId;
                                accToUpd.add(acc);
                                // DML EN LOOP
                                /*
                                    AccountContactRelationTriggerHandler.bypassTrigger=true;
                                    update acc;
                                    AccountContactRelationTriggerHandler.bypassTrigger=false;
                                */
                            } 
                                } 

                        if (Schema.sObjectType.Account.isUpdateable() && accToUpd.size() > 0) {
                            AccountContactRelationTriggerHandler.bypassTrigger=true;
                            update accToUpd[accToUpd.size()-1]; // Actualiza el ultimo indice porque el account es siempre el mismo
                            AccountContactRelationTriggerHandler.bypassTrigger=false;
                            }
                        else {
                            throw new AuraHandledException('Insufficient privileges on AccountContactRelation (Not updateable)');
                        }

                        if (existCont == false && Schema.sObjectType.AccountContactRelation.isUpdateable()) {
                                accContRel[0].Roles='Titular de contrato';
                                contactId=accContRel[0].contactId;
                                update accContRel;
                            }
				   
                        }                 
                    } 

                    if(contactId!=null){
                    if (Schema.sObjectType.Individual.isAccessible() && Schema.sObjectType.Contact.isAccessible()) {
                    ind = [SELECT Id,Contact__c, FII_IND_SEL_Terceros__c, FII_IND_SEL_Cesion__c, FII_IND_SEL_Iden_Evi__c, FII_IND_SEL_Clientes__c, FII_IND_SEL_Molestar__c  
                            FROM Individual 
                            WHERE  (Id IN (SELECT IndividualId
                                        FROM Contact 
                                        WHERE Id =: contactId))
                                        ];                   
                    }       
                }

                    if(ind!= null && !ind.IsEmpty()){
                    if (ind[0].Contact__c == null && Schema.sObjectType.Individual.isUpdateable()) {
                              ind[0].Contact__c=contactId;
                                IndividualTriggerHandler.bypassTrigger=true;
                                update ind[0];
                                IndividualTriggerHandler.bypassTrigger=false;
                                avoidRecursive.isStartContact=false;
                                avoidRecursive.isStartIndividual = false;
                        } 
                    }else if(contactId!=null){
                    if (Schema.sObjectType.Contact.isAccessible() && Schema.sObjectType.Individual.isAccessible()) {
                        indContact = [SELECT Id,IndividualId, (SELECT Id,Contact__c FROM Individuals__r ) FROM Contact WHERE Id =: contactId];
                    }
                    
                        if (indContact.IndividualId == null && !indContact.Individuals__r.isEmpty()){
                        if (Schema.sObjectType.Contact.isUpdateable()) {
                            indContact.IndividualId = indContact.Individuals__r[0].Id;
                            ind.add(indContact.Individuals__r[0]);                
                            ContactTriggerHandler.bypassTrigger=true;
                            update indContact;
                            ContactTriggerHandler.bypassTrigger=false;
                            avoidRecursive.isStartContact=false;
                            avoidRecursive.isStartIndividual = false;
                        }
                    }   
                }         
                //}         
            }
            when 'Contact' {
                if(Schema.sObjectType.Individual.isAccessible() && Schema.sObjectType.Contact.isAccessible()){
                    ind = [SELECT Id, Contact__c, FII_IND_SEL_Terceros__c, FII_IND_SEL_Cesion__c, FII_IND_SEL_Iden_Evi__c, FII_IND_SEL_Clientes__c, FII_IND_SEL_Molestar__c  
                            FROM Individual 
                            WHERE Id IN (SELECT IndividualId   //Id
                            FROM Contact 
                            WHERE Id = :idRecord)]; //id
                    contactId = idRecord;
                }
                    
                    if(ind!= null && !ind.IsEmpty()){
                    if (ind[0].Contact__c == null && Schema.sObjectType.Individual.isUpdateable()) {
                              ind[0].Contact__c=contactId;
                                IndividualTriggerHandler.bypassTrigger=true;
                                update ind[0];
                                IndividualTriggerHandler.bypassTrigger=false;
                                avoidRecursive.isStartContact=false;
                                avoidRecursive.isStartIndividual = false;
                        }
							  
                    }else if(idRecord!=null){
                    if (Schema.sObjectType.Individual.isAccessible() && Schema.sObjectType.Contact.isAccessible()) {
                         indContact = [SELECT Id,IndividualId, (SELECT Id,Contact__c FROM Individuals__r) FROM Contact WHERE Id =: idRecord];
                    }
                        
                         if (indContact.IndividualId == null && !indContact.Individuals__r.isEmpty()) {
                        if (Schema.sObjectType.Contact.isUpdateable()) {
                            indContact.IndividualId = indContact.Individuals__r[0].Id;
                            ind.add(indContact.Individuals__r[0]);
                            ContactTriggerHandler.bypassTrigger=true;
                            update indContact;
                            ContactTriggerHandler.bypassTrigger=false;
                            avoidRecursive.isStartContact=false;
                            avoidRecursive.isStartIndividual = false;
                        }
                    }   
                }
                //}
            }
        }  
        
        if(ind!=null && !ind.IsEmpty()){
            return ind;
        }else{
            //si no tiene lo crea!
            Individual indTemp = createdIndividual(contactId);
            
            if(indTemp != null){
                ind.add(indTemp);
            }
            
            return ind;

        }
        //return ind;
    }
    
    @AuraEnabled
    public static boolean getPhysical(String idRecord) {
        boolean isJuridico = false;
        Contact cont;

        if(idRecord.startsWith('003')){
            if(Schema.sObjectType.Contact.isAccessible()){
                cont = [SELECT Id,IndividualId, Physical_or_legal_cd__c   
                                FROM Contact 
                                WHERE Id = :idRecord];
            }
        } else if(idRecord.startsWith('001')) {
            if(Schema.sObjectType.Account.isAccessible()) {
                cont = [SELECT Id, IndividualId, Physical_or_legal_cd__c FROM Contact WHERE Id IN (SELECT TitularContactHide__c FROM Account WHERE Id =: idRecord)];
            }
        }   
        if (cont.Physical_or_legal_cd__c == 'Corporate Customer') {
            isJuridico = true;
        }
        
        return isJuridico;
    }

    @AuraEnabled
    public static string save(Individual ind){
        if(Schema.sObjectType.Individual.isUpdateable()){ 
        	try{
            	update(ind);
                return 'SUCCESS';  
        	}catch(dmlException e){
            	return e.getMessage().substringAfterLast(', ');
        	}
        }
        return 'No tiene permisos para modificar un registro de la entidad Individual.';
    } 

    @AuraEnabled
    public static Case standardBehaviorGSM(Id recId, String type, String subtype, String subjectNeed, String subjectAct, Id camId, String status, String recordtype){
        Case need = (Case)FII_Util_GSM.standardBehaviorGSM(recId, type, subtype, subjectNeed, subjectAct, camId, status, recordtype)[0];
        return need;
    } 

    @AuraEnabled 
    public static Boolean isTheEventForMe (Id myRecId, Id eventRecId, Task interactionInProgress){
        return FII_Util_GSM.isTheEventForMe(myRecId, eventRecId, interactionInProgress);
    }

    @AuraEnabled
	public static Task getInteractionsInProgress(Id recId){
		Task openedInteraction = FII_Util_GSM.getInteractionInProgress(recId);
        
		if(openedInteraction != null){
			openedInteraction = (Task)FII_Util_GSM.mapApiNamePicklist(new List<Task>{openedInteraction})[0];
		}
        
		return openedInteraction;
	}

    @AuraEnabled
    public static String getObjectsType() {
        // Passing the sObject name in the method, it could be multiple objects too
        return getDescribedObjects( new List<String>{'Individual'} );    
    }

    private static String getDescribedObjects( List<String> lstSObjectType ) {
    	
        // Globally desribe all the objects 
        Map<String, SObjectType> globalDescribe = Schema.getGlobalDescribe(); 
        // Create a JSON string with object field labels and picklist values
        String allObjJSON = '{';
        
        // Iterate over the list of objects and describe each object  
        for( String sObjectType : lstSObjectType ) {
            
            if( allObjJSON != '{' ){
            	allObjJSON += ', ';
            }

            allObjJSON += '"' + sObjectType + '": ';
            DescribeSObjectResult describeResult = globalDescribe.get(sObjectType).getDescribe();
            Map<String, Schema.SObjectField> desribedFields = describeResult.fields.getMap();
            String objJSON = '{';
            
            for( String fieldName :  desribedFields.keySet() ) {
                
                // Descirbe the field 
                Schema.SObjectField field = desribedFields.get( fieldName );
                Schema.DescribeFieldResult f = field.getDescribe();	   
                if( objJSON != '{' ){
                    objJSON += ', ';    
                }
                // Get the field label and append in the JSON string
                objJSON += '"' + f.getName() + '": ' + '{ "label" : "' + f.getLabel() + '"';
                
                // if it's a picklist field then also add the picklist options
                if( field.getDescribe().getType() == Schema.DisplayType.PICKLIST ){
                    
                    List <Schema.PicklistEntry> picklistValues = field.getDescribe().getPickListValues();
                	List<String> pickListOptions = new List<String>();
                    pickListOptions.add('{ "label": "-- Ninguno --", "value": null }');
                    
                    for (Schema.PicklistEntry pe : picklistValues) { 
                        
                        pickListOptions.add('{ "label": "' + pe.getLabel() + '", "value": "' + pe.getValue() + '" }');
                    
                    }
                    
                    objJSON += ', "picklistOptions": [' + String.join(pickListOptions, ', ') + ']';   
                }
                objJSON += '}';
            }
            objJSON += '}';
            
            allObjJSON += objJSON;
        }
        
        // Close the object in the JSON String
        allObjJSON += '}';
                
        return allObjJSON;
    }
    
    @AuraEnabled
    public static boolean getPhysicalOrLegal(String idRecord) {
        boolean personType = true;
        if(Schema.sObjectType.Account.isAccessible()){
            Account acc = [SELECT Physical_or_legal_cd__c   
                           FROM Account 
                           WHERE Id = :idRecord];
            
            if (acc.Physical_or_legal_cd__c == 'Corporate Customer') {
                personType = false;
            }
        }
        
        return personType;
    }
    
    @AuraEnabled
    public static Individual  createdIndividual(Id contactId){
        List<Contact> contlist = new List<Contact>();
        if(contactId != null ){
            if(String.valueOf(contactId.getSobjectType()) == 'Contact'){
                contlist = [ SELECT Id, individualId, CT_external_primary_account_crm_id__c, AccountId, Account.NIF_CIF_Customer_NIE__c, CommentsRGPD__c, DatacessionRGPD__c, DonotdisturbRGPD__c, OfferingsRGPD__c,
                            LastName, FirstName, Contacto_External_Id__c,No_Phone_flg__c ,MobilePhone,OtherPhone__c,Phone,No_Email_flg__c ,New_email__c,Email
                            FROM Contact WHERE Id = :contactId LIMIT 1];
            }else if(String.valueOf(contactId.getSobjectType()) == 'Account'){
                Id titularContactId  = [SELECT TitularContactHide__c FROM Account WHERE Id =: contactId LIMIT 1].TitularContactHide__c;
                if(titularContactId!=null ){
                    contlist = [ SELECT Id, individualId, CT_external_primary_account_crm_id__c, AccountId, Account.NIF_CIF_Customer_NIE__c, CommentsRGPD__c, DatacessionRGPD__c, DonotdisturbRGPD__c, OfferingsRGPD__c,
                                    LastName, FirstName, Contacto_External_Id__c,No_Phone_flg__c ,MobilePhone,OtherPhone__c,Phone,No_Email_flg__c ,New_email__c,Email
                                FROM Contact WHERE AccountId =:titularContactId LIMIT 1];
                }
            }

            if(!contlist.isEmpty()){
                Contact cont = contlist[0];
                Boolean flag=false;

                if((cont.MobilePhone!=null || cont.OtherPhone__c !=null || cont.Phone !=null) && cont.No_Phone_flg__c==true){
                    cont.No_Phone_flg__c=false;
                    flag=true;
                }

                if((cont.MobilePhone==null && cont.OtherPhone__c ==null && cont.Phone ==null) && cont.No_Phone_flg__c==false){
                    cont.No_Phone_flg__c=true;
                    flag=true;
                }

                if((cont.New_email__c!=null || cont.Email!=null) && cont.No_Email_flg__c==true){
                    cont.No_Email_flg__c=false;
                    flag=true;
                }

                if((cont.New_email__c==null && cont.Email==null) && cont.No_Email_flg__c==false){
                    cont.No_Email_flg__c=true;
                    flag=true;
                }

                if(flag &&
                Schema.sObjectType.Contact.isUpdateable()){
                    ContactTriggerHandler.bypassTrigger=true;
                    update cont;
                    ContactTriggerHandler.bypassTrigger=false;
                    avoidRecursive.isStartContact = false;
                }

				if(Schema.sObjectType.Individual.isCreateable() &&
                  Schema.sObjectType.AccountContactRelation.isAccessible()){
                Individual newIndividual= new Individual();
                    newIndividual.CommentsRGPD__c = cont.CommentsRGPD__c;
                    newIndividual.DatacessionRGPD__c = cont.DatacessionRGPD__c;
                    newIndividual.DonotdisturbRGPD__c = cont.DonotdisturbRGPD__c;
                    newIndividual.OfferingsRGPD__c = cont.OfferingsRGPD__c;

                    newIndividual.FII_IND_SEL_Cesion__c = cont.DatacessionRGPD__c == 'Y' ? 'Si' : 'No';
                    newIndividual.FII_IND_SEL_Molestar__c = cont.DonotdisturbRGPD__c == 'Y' ? 'Sí Ofertas' : 'No Ofertas';
                    newIndividual.FII_IND_SEL_Clientes__c = cont.OfferingsRGPD__c == 'Y' ? 'Si' : 'No';

                    newIndividual.Contact__c = cont.Id;
                    newIndividual.LastName = cont.LastName;
                    newIndividual.FirstName = cont.FirstName;
                    newIndividual.CRM_contacto_external_id__c = cont.Contacto_External_Id__c;

               
                List<AccountContactRelation> acrList = [SELECT Id, Roles, AccountId, ContactId, NIF_Contact__c,Account.NIF_CIF_Customer_NIE__c 
                                            FROM AccountContactRelation 
                                            WHERE Roles =: 'Titular de contrato' AND AccountId = :cont.AccountId LIMIT 1];
                
                if(!acrList.isEmpty()){
                    AccountContactRelation acr = acrList[0];
                    if (acr.Account.NIF_CIF_Customer_NIE__c == acr.NIF_Contact__c) { 
                        newIndividual.CRM_cliente_external_id__c = cont.CT_external_primary_account_crm_id__c;  
                    } 
                }
            
                try{
                    insert newIndividual;
                    System.debug('newIndividual ' + newIndividual);
                    return newIndividual;
                }catch(DmlException de){
                    throw new AuraException(de.getMessage());
                    
                }catch(QueryException qe){
                    throw new AuraException(qe.getMessage());
                }
            }
            }

            return null;
        }else{
            return null;
        }
    }

    /***  TELEVENTA  ***/
    @AuraEnabled
    public static Boolean getIsTeleventaProfile(){
        return TV_CLS_Utils.getIsTeleventaProfile();
    }

    @AuraEnabled
    public static Account getAccountData(String AccountId){
        return [SELECT Id, Name, Electronic_bill_flg__c, Email_con__c, Email_validated__c,FII_ACC_FOR_EmailContact__c,FII_ACC_FOR_Validate_Email__c,TitularContactHide__c,Birthdate__c,FII_ACC_NUM_Active_Contracts_EE__c,Addr_id__c FROM Account WHERE Id =: AccountId LIMIT 1];
    }
    @AuraEnabled
    public static Contact getContactData(String contactId){ 
        return [SELECT Id, DonotdisturbRGPD__c,OfferingsRGPD__c,DatacessionRGPD__c FROM Contact WHERE Id =: contactId LIMIT 1];
    }
    @AuraEnabled
    public static Lead getLeadData(String leadId){ 
        return [SELECT Id, Account_RGPD_Datadisclosure__c,Account_RGPD_Donotdisturb__c,Account_RGPD_Offerings__c,Account_RGPD_NoClientes__c FROM Lead WHERE Id =: leadId LIMIT 1];
    }
    
    @AuraEnabled
    public static textsInfoWrapper obtainTextsRGPD(Id recordId, String interactionId){
        textsInfoWrapper dataWrapper;
        String textoIncial = '';
        String textoIncialPorDefecto = 'Presentación de la oferta, se hace referencia a la grabacion de la llamada. Si el cliente pregunta de dónde ha obtenido sus datos: se debe mencionar el origen de la toma de consentimiento';
        String noMolestar = '';
        String ofrecimientos = '';
        String cesion = '';
        String noCliente = '';
        Id accountId;
        Contact cont = new Contact ();
        try{
            switch on String.valueOf(recordId.getSobjectType()){
                when 'Account', 'Contact' {

                        if(String.valueOf(recordId.getSobjectType()) == 'Contact'){
                            List<Contact> contList = [SELECT id, accountId FROM Contact WHERE id =: recordId LIMIT 1];

                            if(!contList.isEmpty()){
                                recordId = contList[0].accountId;
                            }
                        }
                        Account acc = getAccountData(recordId);
                        system.debug('acc.TitularContactHide__c: '+acc.TitularContactHide__c);
                        cont = getContactData(acc.TitularContactHide__c);
    
                        //Tipo interlocutor 
                        String tipoInterlocutor = TV_CLS_Utils.obtenerTipoInterlocutor(recordId);
                        String estadoRGPD = TV_CLS_Utils.estadoRGPDMotivoLicito(cont.Id);
                        Boolean contratosEnVigor = (tipoInterlocutor == 'Cliente' ? true : (acc.FII_ACC_NUM_Active_Contracts_EE__c != 0 ? true : false));
                        String provincia = '';
                        system.debug('interactionId: '+interactionId);
                        if(String.isNotBlank(interactionId)){
                            String campaignMemberId = [SELECT Id, FII_ACT_TXT_CampaignMemberId__c FROM Task WHERE id=:interactionId].FII_ACT_TXT_CampaignMemberId__c;
                            system.debug('campaignMemberId: '+campaignMemberId);
                            if(String.isNotBlank(campaignMemberId)){
                                List<CampaignMember> campaignMemberList = [ SELECT Id, ContactId, CampaignId, FII_CAMM_LKP_AccountMember__c, 
                                                                                LeadId, LeadOrContactId, PS1__c, TV_CI_Pendiente_Recuperacion__c,
                                                                                TV_Motivo_CI_Pendiente_Recuperacion__c
                                                                            FROM   CampaignMember 
                                                                            WHERE  Id = :campaignMemberId ]; 
                                system.debug('campaignMemberList: '+campaignMemberList);
                                if(campaignMemberList.size()> 0){
                                    List<B2C_Service_Point__c> servicePointList = [ SELECT Id, AddrId_Id__r.Id
                                                                                    FROM B2C_Service_Point__c
                                                                                    WHERE NAME = :campaignMemberList[0].PS1__c LIMIT 1];
                                    system.debug('servicePointList: '+servicePointList);
                                    if(servicePointList.size() > 0){
                                        provincia = obtenerProvincia(servicePointList[0].AddrId_Id__r.Id);
                                        system.debug('provincia por la int: '+provincia);
                                    }     
                                }
                            }

                            system.debug('tipoInterlocutor: '+tipoInterlocutor);
                            system.debug('estadoRGPD: '+estadoRGPD);
                            system.debug('contratosEnVigor: '+contratosEnVigor);
                            system.debug('provincia: '+provincia);
                            List<Textos_iniciales_RGPD__mdt> textoInicialRGPD = [SELECT Id,TV_TXT_Motivo_Licito__c FROM Textos_iniciales_RGPD__mdt 
                                                                                    WHERE (TV_TXT_Tipo_Accion__c =: tipoInterlocutor AND 
                                                                                        TV_TXT_RGPD_No_Molestar__c =: estadoRGPD AND
                                                                                        TV_FLG_Contratos_en_vigor__c =: contratosEnVigor AND
                                                                                        TV_TXT_Provincia__c =: provincia)
                                                                                    LIMIT 1];
                            system.debug('textoInicialRGPD: '+textoInicialRGPD);
                    
                            if(textoInicialRGPD.size() > 0){
                                textoIncial = textoInicialRGPD[0].TV_TXT_Motivo_Licito__c;
                                // Sustituimos 'xxx' por el nombre del cliente
                                if(textoIncial.contains('soy xxx')){
                                    textoIncial = textoIncial.replace('xxx', UserInfo.getName());
                                }
                                else if(textoIncial.contains('xxx')){
                                    textoIncial = textoIncial.replace('xxx', acc.Name);
                                }
                            }else{
                                textoIncial = textoIncialPorDefecto;
                            }
                        }
                        /*if(String.isBlank(interactionId) || String.isBlank(provincia)){
                            provincia = obtenerProvincia(acc.Addr_id__c);
                            system.debug('provincia SIN encontrar la int: '+provincia);
                        }
                        system.debug('tipoInterlocutor: '+tipoInterlocutor);
                        system.debug('estadoRGPD: '+estadoRGPD);
                        system.debug('contratosEnVigor: '+contratosEnVigor);
                        system.debug('provincia: '+provincia);
                        List<Textos_iniciales_RGPD__mdt> textoInicialRGPD = [SELECT Id,TV_TXT_Motivo_Licito__c FROM Textos_iniciales_RGPD__mdt 
                                                                                WHERE TV_TXT_Tipo_Accion__c =: tipoInterlocutor AND 
                                                                                    TV_TXT_RGPD_No_Molestar__c =: estadoRGPD AND
                                                                                    TV_FLG_Contratos_en_vigor__c =: contratosEnVigor AND
                                                                                    TV_TXT_Provincia__c =: provincia
                                                                                LIMIT 1];
                        system.debug('textoInicialRGPD: '+textoInicialRGPD);
                
                        if(textoInicialRGPD.size() > 0){
                            textoIncial = textoInicialRGPD[0].TV_TXT_Motivo_Licito__c;
                            // Sustituimos 'xxx' por el nombre del cliente
                            if(textoIncial.contains('soy xxx')){
                                textoIncial = textoIncial.replace('xxx', UserInfo.getName());
                            }
                            else if(textoIncial.contains('xxx')){
                                textoIncial = textoIncial.replace('xxx', acc.Name);
                            }
                        }else{
                            textoIncial = 'No se ha podido recuperar el texto del motivo lícito.';
                        }*/

                }
                /*when 'Contact' {
                    cont = getContactData(recordId);
                    textoIncial = 'Texto Motivo licito Contacto';
                }*/
                when 'Lead' {
                    textoIncial = textoIncialPorDefecto;
                }
            }
			
            noMolestar = 'No desea recibir comunicaciones sobre productos o servicios ofrecidos por Endesa Energía en el ámbito de la actividad energética.';
            ofrecimientos = 'Consiente que le ofrezcan productos y servicios personalizados de Endesa Energía o terceros relacionados con energía, hogar, seguros, automoción, servicios financieros y ocio que se adapten a sus necesidades.';
            cesion = 'Consiente que sus datos sean compartidos con empresas del Grupo Endesa o terceras empresas de los sectores indicados anteriormente con esa misma finalidad.';
            noCliente = 'Consiente recibir ofertas personalizadas en el ámbito de la actividad energética que puedan ser de su interés, mientras no sea cliente de Endesa.';
            dataWrapper = new textsInfoWrapper(textoIncial,noMolestar,ofrecimientos,cesion,noCliente);
        }catch(Exception errFound){
            system.debug ('ERROR FOUND: ' + errFound);
            System.debug('LINEA: ' + errFound.getLineNumber());
            System.debug('TRAZA: ' + errFound.getStackTraceString());  
        }
        return dataWrapper;
    }

	private static String obtenerProvincia (String addressId){
        String result = 'CAT';
        if(String.isNotBlank(addressId)){
            B2C_Address__c direccion = [SELECT Id,Province_cd__c FROM B2C_Address__c WHERE Id =: addressId];
            if(direccion.Province_cd__c != null){
                // Provincias de cataluña: Barcelona - 08 / Girona - 17 / LLeida - 25 / Tarragona - 43
                if(direccion.Province_cd__c != '08' && direccion.Province_cd__c != '17' && direccion.Province_cd__c != '25' && direccion.Province_cd__c != '43'){
                    result = 'NO CAT';
                }
            } 
        }
        return result;
    }

    public class textsInfoWrapper{
        @AuraEnabled
        public String textoInicial; 
        @AuraEnabled
        public String textoNoMolestar; 
        @AuraEnabled
        public String textoOfrecimientos;
        @AuraEnabled
        public String textoCesion;
        @AuraEnabled
        public String textoNoCliente;

        textsInfoWrapper(String inicial, String noMolestar,String ofrecimientos,String cesion,String noCliente){
            this.textoInicial = inicial;
            this.textoNoMolestar = noMolestar;
            this.textoOfrecimientos = ofrecimientos;
            this.textoCesion = cesion;
            this.textoNoCliente = noCliente;
        }
    }
}