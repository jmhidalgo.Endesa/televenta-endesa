@isTest
public class FII_LCC_ContractingServPointServerTest {
    
    @testSetup 
    static void setup() {
        Test.startTest(); 
        AccountTriggerHandler.bypassTrigger = true;
        Account client = generateAccount();
        insert client;
        
       
        B2C_Address__c b2cAddress = generateAddress(client.Id);
        insert b2cAddress;
       // client.Addr_id__c = b2cAddress.Id;
       // update client;
            
        B2C_CNAE__c cnae1 = generateCNAE();
        insert cnae1;
        
        ServicePointTriggerHandler.bypassTrigger = true;
        B2C_Service_Point__c servicePoint = generateServicePoint(client.Id, b2cAddress.Id, cnae1.Id);  
        servicePoint.AddrId_Id__c = b2cAddress.Id;
        servicePoint.AddrId_Id__r = b2cAddress;
        servicePoint.Account_id__c = client.Id;
        insert servicePoint;
        
        Contract cont = generateContract(servicePoint.Account_id__c , servicePoint.Id, servicePoint.AddrId_Id__c );
        cont.B2C_Address_id__c = b2cAddress.Id;
        cont.CUPS__c = servicePoint.Name;
        insert cont;
        
        NE__Quote__c req = new NE__Quote__c();
        //req.Supply_point__c = servicePoint.Id;
        req.NE__Status__c = 'Draft';
        req.Subtype__c = 'CT';
        req.FI_NEQ_SEL_Movement_Type__c = 'A';
        req.FI_NEQ_SEL_Flow_Controlling_Action__c = 'A'; 
        //req.Address__c = b2cAddress.Id;
        insert req;
        
        FI_TRG_Asset_Handler.bypassTrigger = true;
        Asset asset = new Asset();
        asset.Name= 'LOONEY 1';
        asset.AccountId  = client.Id;
        asset.ParentId = NULL; 
        asset.NE__Status__c ='Active';
        asset.FI_ASS_LKP_Address__r = b2cAddress;
        asset.FI_ASS_LKP_Service_Point__c = null;
        asset.AccountId = client.Id;
        insert asset;

        Asset asset2 = new Asset();
        asset2.Name= 'LOONEY 2';
        asset2.AccountId  = client.Id;
        asset2.ParentId = NULL; 
        asset2.NE__Status__c ='Active';
        //asset2.FI_ASS_LKP_Service_Point__c = servicePoint.Id;
        //b2c_service_point__c puntosum = new b2c_service_point__c();
        //puntosum.AddrId_Id__c = servicePoint.AddrId_Id__c;
        //asset2.FI_ASS_LKP_Service_Point__r  = servicePoint;
        //asset2.FI_ASS_LKP_Service_Point__r.AddrId_Id__c = servicePoint.AddrId_Id__c;
        insert asset2;
        
        Asset asset3 = new Asset();
        asset3.Name= 'LOONEY 3';
        asset3.AccountId  = client.Id;
        asset3.ParentId = NULL; 
        asset3.NE__Status__c ='Active';
        asset3.Account = client;
        asset3.Account.Addr_id__r = b2cAddress;
        //asset3.Account.Addr_id__c = client.Addr_id__c;
        
        insert asset3;
        
        Asset asset4 = new Asset();
        asset4.Name= 'LOONEY 4';
        asset4.AccountId  = client.Id;
        asset4.ParentId = NULL; 
        asset4.NE__Status__c ='Active';
        asset4.FI_ASS_LKP_Service_Point__c = null;
        insert asset4;
        Test.stopTest();
        /*B2C_Service_Point__c  addressTest = generateServicePoint('a1x1q0000006vJQAAY');
        insert addressTest;*/
        Asset asset5 = new Asset();
        asset5.Name= 'LOONEY 2';
        asset5.AccountId  = client.Id;
        asset5.ParentId = NULL; 
        asset5.NE__Status__c ='Active';
        asset5.FI_ASS_LKP_Service_Point__c = servicePoint.Id;
        asset5.FI_ASS_SEL_BusinessLine__c = '03';
        //b2c_service_point__c puntosum = new b2c_service_point__c();
        //puntosum.AddrId_Id__c = servicePoint.AddrId_Id__c;
        //asset2.FI_ASS_LKP_Service_Point__r  = servicePoint;
        //asset2.FI_ASS_LKP_Service_Point__r.AddrId_Id__c = servicePoint.AddrId_Id__c;
        insert asset5;
        
        
        Map<String, Integer> mapCheckSupUspcExpected = new Map<String, Integer>();
        //List<Contract> lContract = new List<Contract>();
        
        //List<B2C_Service_Point__c> lServicePoint = new List<B2C_Service_Point__c>();
        
        /*
        ServicePointTriggerHandler.byPassTrigger = true;
        B2C_Service_Point__c servicePoint1 = generateServicePoint(client.Id, b2cAddress.Id, cnae1.Id);
        insert servicePoint1;
        lServicePoint.add(servicePoint1);
        B2C_Service_Point__c servicePoint2 = generateServicePoint(client.Id, b2cAddress.Id, cnae1.Id);
        insert servicePoint2;
        lServicePoint.add(servicePoint2);
        B2C_Service_Point__c servicePoint3 = generateServicePoint(client.Id, b2cAddress.Id, cnae1.Id);
        insert servicePoint3;
        lServicePoint.add(servicePoint3);
        B2C_Service_Point__c servicePoint4 = generateServicePoint(client.Id, b2cAddress.Id, cnae1.Id);
        insert servicePoint4;
        lServicePoint.add(servicePoint4);
        B2C_Service_Point__c servicePoint5 = generateServicePoint(client.Id, b2cAddress.Id, cnae1.Id);
        insert servicePoint5;
        lServicePoint.add(servicePoint5);
        B2C_Service_Point__c servicePoint6 = generateServicePoint(client.Id, b2cAddress.Id, cnae1.Id);
        insert servicePoint6;
        lServicePoint.add(servicePoint6);
        ServicePointTriggerHandler.byPassTrigger = false;
        */
        /*
        for(Integer i = 0; i <= 6; ++i){
            B2C_Service_Point__c servicePoint = generateServicePoint(client.Id, b2cAddress.Id, cnae1.Id);
            //servicePoint.Name = 'ES00315001'+i+'3571503FJ0F';
            lServicePoint.add(servicePoint);
        }
        insert lServicePoint;
        */
        /*
        for(B2C_Service_Point__c servicePoint : lServicePoint){
            Contract cont = generateContract(servicePoint.Account_id__c , servicePoint.Id, servicePoint.AddrId_Id__c );
            lContract.add(cont);
        }
        insert lContract;
        */
        
    }
    /*
    @isTest static void searchByAddressTest() {
        Test.startTest();
        FII_LCC_ContractingServPointServer.searchForIds('LOONEY',System.Label.Address);
        Test.stopTest();
    }
    
    @isTest static void searchByAddressDifferentCUPSTest() {
        Test.startTest();
        Contract cont = [SELECT CUPS__c FROM Contract WHERE CUPS__c = 'PENDIENTE_INFORMAR'];
        cont.CUPS__c = 'ES0031500143571503FJ0F';
        update cont;
        FII_LCC_ContractingServPointServer.searchForIds('LOONEY',System.Label.Address);
        Test.stopTest();
    }
    
    @isTest static void searchByCUPSTest() {
        Test.startTest();
        FII_LCC_ContractingServPointServer.searchForIds('PENDIENTE',System.Label.CUPS);
        Test.stopTest();
    }
    
    @isTest static void searchByContractTest() {
        List<Contract> lContract = [SELECT Id, ContractNumber FROM Contract WHERE Product__c = 'Acme Gun'];
        Test.startTest();
        FII_LCC_ContractingServPointServer.searchForIds(lContract.get(0).ContractNumber,System.Label.Contract);
        Test.stopTest();
    }
    
    @isTest static void searchByAllTest() {       
        Test.startTest();
        FII_LCC_ContractingServPointServer.searchForIds('LOONEY',System.Label.All);       
        Test.stopTest();
    }
    */
    
    @isTest static void getAddrNameTest() { 
        Test.startTest();
      List<B2C_Address__c> listtest=[  SELECT id FROM B2C_Address__c limit 1 ];
      String TestId = listtest[0].id;
     String test1= FII_LCC_ContractingServPointServer.getAddrName(TestId);
        System.debug('Test1 +'+test1 );
        System.assertNotequals(test1,null,'test Vacio');
        Test.stopTest();
        
    }   
    @isTest static void getAddressesByAccountIdTest() {
        Test.startTest();
        List<Account> idtest = [select id from account limit 1];
        Id ids = idtest[0].id;
        List<B2C_Address__c> listatest = FII_LCC_ContractingServPointServer.getAddressesByAccountId(ids);
        System.debug('listatest ok' +listatest);
        System.assert(!listatest.isEmpty(),'Lista vacia');
        Test.stopTest();
    }
    
    @isTest static void getFieldSetTest() {
        Test.startTest();
        Id recordTypeId = FII_LCC_ContractingServPointServer.getRecordTypeId('Electricidad','B2C_Service_Point__c');
        List<RecordSetWrapper> fieldSetTest = FII_LCC_ContractingServPointServer.readFieldSet('ContractingServicePointElectricCreate', 'B2C_Service_Point__c');
        system.debug('FieldSetTest ---------- ' + fieldSetTest);
        Test.stopTest();
        System.assertEquals(fieldSetTest[0].required, false );
    }
    
    @isTest static void callSVEPSTest() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.TBCCEX028Mock('OK'));
        String callSvepsTest = FII_LCC_ContractingServPointServer.callSVEPS('ES0031406110526001ZC0F');
        system.debug('CallSvepsTest ----------' + callSvepsTest);
        Test.stopTest();
        System.assertEquals(callSvepsTest, '' );

    }
    
    @isTest static void checkSupplyPointsInB2BTest(){
        Test.startTest();
        Set<String> uspcSet = new Set<String>();
        uspcSet.add('ES0021000003988704AH0F00000000000000000000');//El malo
        uspcSet.add('ES0021000003988704AH0F');//Cups Formato correcto no SVE
        uspcSet.add('ES0031405429112004XP0F');//Cups si SVE no contrato
        uspcSet.add('ES0031405918892001NL0F');//Cups si SVE si contrato
        uspcSet.add(' ');
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.TBCCEX028Mock('OK'));
        Map<String, Integer> mapTest = FII_LCC_ContractingServPointServer.checkSupplyPointsInB2B(uspcSet);
        system.debug('mapTest ---------- ' + mapTest);
        Test.stopTest();
        System.assertNotEquals(mapTest.isEmpty(), true );
        System.assertEquals(mapTest.containsKey('ES0021000003988704AH0F'), true);
    }
    
    @isTest static void getAddressesByAnythingTest(){
        Test.startTest();
        String searchText = 'LOON';
        FII_LCC_ContractingServPointServer.getAddressesByAnything(searchText);
        searchText = 'PENDIENTE_INFORMAR';
        List<B2C_Address__c> adrListTest = FII_LCC_ContractingServPointServer.getAddressesByAnything(searchText);
        system.debug('adrListTest ---------- ' + adrListTest);
        Test.stopTest();
        //System.assertEquals(adrListTest[0].name, 'LOONEY AVENUE' );
    }
    
    @isTest static void getConfigurationItemInProgressTest(){
       Test.startTest();
        B2C_Service_Point__c servPointList = [SELECT Id, toLabel(Business_line_cd__c) FROM B2C_Service_Point__c WHERE Business_line_cd__c != null LIMIT 1];

        String psId = servPointList.Id;
        String psBL = servPointList.Business_line_cd__c;

        Boolean supplyPointInProgress = FII_LCC_ContractingServPointServer.getConfigurationItemInProgress(psId, psBL);
        System.assertEquals(false, supplyPointInProgress );
        Test.stopTest();
      
    }
    
    @isTest static void getRecordsByAddressNameTest(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.TBCCEX028Mock('OK'));
        Test.startTest();
        String searchText = 'LOONEY AVENUE';
        B2C_Address__c addr1 =[  SELECT id FROM B2C_Address__c where Name = 'LOONEY AVENUE' limit 1 ];
        FII_LCC_ContractingServPointServer.getRecordsByAddressId(addr1.Id);
        FII_LCC_ContractingServPointServer.getRecordsByAddressName(searchText);
        
        TableRowWrapper tb = new TableRowWrapper();
        system.debug('tb ---------- ' + tb);
        Test.stopTest();
        System.assertEquals(tb.Asset, null );
    }
    
    @isTest static void getRecordsByAccountIdTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.TBCCEX028Mock('OK'));        
        List<Account> accountId = [select Id from Account limit 1];
        system.debug('accountId ++ ' + accountId );
        List<TableRowWrapper> listTest = FII_LCC_ContractingServPointServer.getRecordsByAccountId(accountId[0].Id);
        Test.stopTest();
        System.assert(!listTest.isEmpty(),'Lista sin Registros');
        // TODO FIXME
        //System.assertEquals(listTest[0].Asset.Name, 'LOONEY 2' );
    }
    
    /*
    @isTest static void hasContractActiveInSVETest() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.TBCCEX028Mock('OK'));
        List<String> lStrings = new List<String>();
        lStrings.add('ES0031406110526001ZC0F');
        FII_LCC_ContractingServPointServer.hasContractActiveInSVE(JSON.serialize(lStrings));
        Test.stopTest();
    }*/
    
    @isTest static void checkDistributorTest() {
        Test.startTest();
        Boolean checkTest = FII_LCC_ContractingServPointServer.checkDistributor('ES0021000003988704AH0F', '01');
        system.debug('checkTest-------------------------------------' + checkTest);
        System.assertEquals(checkTest, true );
        Test.stopTest();
    }
    
    @isTest static void testWrapper() { 
        Test.startTest();
      	List<B2C_Service_Point__c> listtest =[SELECT id, Name FROM B2C_Service_Point__c limit 1 ];
        List<Asset> lasset = [SELECT Id FROM Asset LIMIT 1];
        Map<String, Integer> uspcMap = new Map<String, Integer>();
        Map<Id,List<Asset>> assetsWithSupplyPoint = new Map<Id,List<Asset>>();
        List<Asset> assetsWithoutSupplyPoint = new List<Asset>();
        Map<Id,Boolean> inProgressSupplyPointMap = new Map<Id,Boolean>();
        Map<Id, List<Asset>> assetsNEWithSupplyPoint  = new Map<Id,List<Asset>>();
        FII_LCC_ContractingServPointServer.getTableRowWrapper(listtest, uspcMap, assetsWithSupplyPoint, assetsWithoutSupplyPoint, inProgressSupplyPointMap,assetsNEWithSupplyPoint);
        assetsWithSupplyPoint.put(listtest.get(0).Id, lasset);
        FII_LCC_ContractingServPointServer.getTableRowWrapper(listtest, uspcMap, assetsWithSupplyPoint, assetsWithoutSupplyPoint, inProgressSupplyPointMap,assetsNEWithSupplyPoint);
        Test.stopTest();
  
    }   
    @Istest static void getRecordsByCUPS(){
        Test.startTest();
        List<B2C_Service_Point__c> listtest =[SELECT id, Name FROM B2C_Service_Point__c limit 1 ];
        List<Asset> lasset = [SELECT Id FROM Asset LIMIT 1];
        FII_LCC_ContractingServPointServer.getRecordsByCUPS('PENDIENTE_INFORMAR');
        Test.stopTest();
    }
    
    static Account generateAccount(){
        Account client = new Account();
        client.Name = 'ACME';
        client.Contact_method_cd__c = 'SMS';
        client.LastName__c = 'ARTEFACTS';
        client.Identifier_Type__c = 'N';
        client.LicensedPhysical_or_legal__c = 'Corporate customer';
        client.Physical_or_legal_cd__c = 'Corporate Customer';
        client.NIF_CIF_Customer_NIE__c = 'ES48625582V';
        client.MobilePhone1__c = '619139025';
        client.Email_con__c = 'shop@acme.net';
        client.Account_language__c = 'Español';
        return client;
    }
    
    static B2C_Address__c generateAddress(Id clientId){
        B2C_Address__c b2cAddress = new B2C_Address__c();
        b2cAddress.Name = 'LOONEY AVENUE';
        b2cAddress.Premise_flg__c = true;
        b2cAddress.Main_Address__c = false;
        b2cAddress.Adress_active_flg__c = true;
        b2cAddress.Country_cd__c = 'ES';
        b2cAddress.Country_desc__c = 'ESPAÑA';
        b2cAddress.Province_cd__c = '15';
        b2cAddress.Province_text__c = 'A CORUÑA';
        b2cAddress.County_cd__c = '030';
        b2cAddress.County__c = 'A CORUÑA';
        b2cAddress.City_cd__c = 'BER13108';
        b2cAddress.City__c = 'A CORUÑA';
        b2cAddress.INE_city_cd__c = '15030000101';
        b2cAddress.Street_cd__c = 'BER0569533';
        b2cAddress.Street__c = 'NELLE';
        b2cAddress.INE_street_cd__c = '1503000423';
        b2cAddress.Type_street__c = 'RD';
        b2cAddress.Zipcode__c = '12345';
        b2cAddress.Longitude__c = 0;
        b2cAddress.Latitude__c = 0;
        b2cAddress.Census_tract__c = '123';
        b2cAddress.Street_number__c = '109';
        b2cAddress.Point_duplicator_cd__c = 'A';
        b2cAddress.Type_Point_lightener_cd__c = 'AFS';
        b2cAddress.Point_lightener__c = 'S';
        b2cAddress.Floor_cd__c = '4';
        b2cAddress.Door__c = '2';
        b2cAddress.Build__c = '1';
        b2cAddress.Stairs__c  = '3';
        b2cAddress.Adress_lightener__c = 'asdf';
        b2cAddress.Type_address_cd__c = 'Normalized';
        b2cAddress.Reference_year__c = 2010;
        b2cAddress.INE_county_cd__c = '030';
        b2cAddress.INE_city_cd__c = '15030000101';
        b2cAddress.UTM_x__c = '2';
        b2cAddress.UTM_y__c = '2';
        b2cAddress.Trust_level__c = 5;
        b2cAddress.CUPS_13_gas__c = 'CRM0001391063';
        b2cAddress.CUPS_13_elec__c = 'CRM0001312697';
        b2cAddress.Description_adress__c = 'Descripcion calle';
        b2cAddress.INE_state_cd__c = 'PR';
        b2cAddress.DIR_external_CRM_id__c = '123456789012345';
        b2cAddress.Formatedd_Address__c = 'Formatedd Address';
        b2cAddress.DIR_external_account_id__c = '123456789012345';
        b2cAddress.DIR_external_contact_id__c = '123456789012345';
        b2cAddress.Account__c = clientId;
        return b2cAddress;
    }
    
    static B2C_CNAE__c generateCNAE (){
        B2C_CNAE__c cnae1 = new B2C_CNAE__c();
        cnae1.name = '9820';
        
        return cnae1;
    }
    
    static B2C_Service_Point__c generateServicePoint(Id clientId, Id addressId, Id cnaeId){
        B2C_Service_Point__c ps = new B2C_Service_Point__c();
        ps.RecordTypeId = Schema.SObjectType.B2C_Service_Point__c.getRecordTypeInfosByName().get('Electricidad').getRecordTypeId(); 
        ps.Name = 'PENDIENTE_INFORMAR';
        ps.CUPS_Consulted_SVE__c = '';
        ps.Business_line_cd__c = '01'; 
        ps.Distribution_Group_Elec__c='test';
        ps.DistribuitorElec_cd__c='0217';      
        ps.Power_period_1_Kw__c = 123;
        ps.Tension_Type_cd__c = 'TRI';
        ps.Tension_cd__c = '06';
        ps.Power_Max_CIE__c = 123;
        ps.Meass_Equip_property__c = 'C';
        ps.Power_period_1_Kw__c=12;
        ps.Tariff_cd__c='003';
        ps.Power_control_mode_cd__c='2';
        ps.Power_period_1_Kw__c=3.3;
        ps.Power_period_2_Kw__c=16;
        ps.Power_period_3_Kw__c=12;
        ps.Power_period_4_Kw__c=12;
        ps.Power_period_5_Kw__c=12;
        ps.Power_period_6_Kw__c=12;
        ps.PostalCode__c = '12345';
        ps.Power_Max_CIE__c = 20;
        ps.Region__c = 'Cataluña';
        ps.Account_id__c = clientId;
        ps.Street__c = 'TestStreet';
        ps.Street_number__c = '1';
        ps.CNAE__c = cnaeId;
        ps.Power_Ascribed_Ext__c = 1;
        ps.Reason_creation__c = 'No localizado';
        ps.Interruptible__c = '02';
        return ps;
    }
    
    static Contract generateContract(Id clientId, Id servicePointId, Id addressId){
        Contract cont = new Contract();
        cont.Tariff__c = '3.1';        
        cont.CustomerSignedDate = Date.newInstance(2018, 11, 20);
        cont.Product__c = 'Acme Gun';
        cont.Contract_Type_cd__c = '01';
        cont.Status = 'Active';
        cont.CUPS__c = 'ES02221200006145670F';
        cont.AccountId = clientId;
        cont.B2C_Service_PointId__c = servicePointId;
        cont.B2C_Address_id__c = addressId;
        return cont;
    }
    
}