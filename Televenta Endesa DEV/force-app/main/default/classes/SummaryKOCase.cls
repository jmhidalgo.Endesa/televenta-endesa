/****************************************************************************************************************************************/
/* SummaryKOCase
* @Author: sgonzalezh@ayesa.com
* @Vendor: Ayesa
* @Date: 2018/07/02
* @Module: COSMOS
* @LastChange		2019/02/22  10:00    JARG: Comentar llamadas a GTB
/****************************************************************************************************************************************/
public with sharing class SummaryKOCase {
    
    public static final Set<String> STATUS_QCASE_FINAL = new Set<String> {
        Label.FI_CAS_STATUS_RESOLVED,
            Label.FI_CAS_STATUS_RESOLVED_CANCELFRONT,
            Label.FI_CAS_STATUS_RESOLVED_ENDESA,
            Label.FI_CAS_STATUS_RESOLVED_RELCASE
            };
                
                /**
* Genera las incidencias restantes de un Configuration Item según un conjunto de etapas de Caso.
*
* @param caseObj: objeto Caso de Control de Calidad
* @param ci: objeto ConfigurationItem para el que crear las incidencias
* @param stagesCase: lista de etapas asociadas al RT del Caso
* @param ciIncList: lista de incidencias actuales del ConfigurationItem
* @param ciNewIncMap: mapa de incidencias actuales del ConfigurationItem <IdCI+Etapa,Incidencia>	
*/
                private static Map<String,Task> createTaskCC(Case caseObj, NE__OrderItem__c ci, List<String> stagesCase, List<Task> ciIncList, Map<String,Task> ciNewIncMap){
                    Map<String,Task> ciNewIncMapRet = ciNewIncMap;
                    if(ciNewIncMapRet == null){
                        ciNewIncMapRet = new Map<String,Task>();
                    }
                    
                    // Obtener etapas con incidencia asociada
                    Set<String> stagesWithInc = new Set<String>();
                    if (ciIncList != null){
                        for(Task inc : ciIncList){
                            stagesWithInc.add(inc.Stage__c);
                        }
                    }
                    
                    // Generar incidencias para las etapas sin incidencia asociada
                    for(String stage : stagesCase){
                        if(!stagesWithInc.contains(stage) && !ciNewIncMapRet.containsKey(ci.Id + stage)){
                            Task tk = new Task();
                            tk.Subject = 'Case: '+caseObj.CaseNumber+ ' - Control Calidad';
                            tk.WhatId = caseObj.Id;
                            tk.OwnerId = caseObj.OwnerId;
                            tk.FI_ACT_LKP_OrderItem__c = ci.Id;
                            tk.Stage__c = stage;
                            tk.Quality_error__c = 'KO por etapa anterior';
                            tk.Quality_control_result__c = 'KO';
                            tk.Quality_control_option__c = 'Audit';
                            tk.Status = Label.FI_TSK_STATUS_COMPLETED;
                            ciNewIncMapRet.put(ci.Id + stage, tk);
                        }
                    }
                    return ciNewIncMapRet;
                }


                /**
    * Método para que el estado Resuelto Motivo: Formalización automática del caso de TV pase el flujo del Resuelto OK de Formalización
    *
    * @param List<Case> newLstCases: lista de casos del trigger 
    * @param Map<Id, Case> oldLstCases: mapa de casos del trigger 
     */
     @InvocableMethod(label='Update Cases TV')
     public static void updateCaseCCTV (List<Id> idCases) {
         List<Case> lstCases = Utilities.getObject(idCases);
         Set<Id> idClients = new Set<Id>();
         for (Case itCase : lstCases) {
             idClients.add(itCase.AccountId);
         }
         final Map<Id, Account> mapClient = new Map<Id, Account>([SELECT Id, Name, Identifier_Type__c, Nationality_cd__c, Cliente_External_Id__c, NIF_CIF_Customer_NIE__c, Physical_or_legal_cd__c, NE__Active__c, Last_update_of_default_risk_date__c, Debtor__c FROM Account WHERE Id IN :idClients]);
         Map<Id,List<NE__OrderItem__c>> mapListCICaseCC = Utilities.getMapListCICases(idCases);
         for (Case newCase : lstCases) {
             Account acc = mapClient.get(newCase.AccountId);
             SummaryKOCase.checkCaseConditions(new Map<String, String>{'quoteStatus' => 'ok'}, false, newCase, mapListCICaseCC.get(newCase.Id), acc);
         }
     }
    
    
    /**
* Revisa y crea incidencias relacionadas con un caso cuando este pasa a OK o KO.
*
* @param caseCC: objeto Caso de Control de Calidad
*/
    public static void updateCaseCC(Case caseCC, Account client){
        // El Caso debe ser un caso de Control de Calidad
        if(caseCC.FI_CAS_FLG_Control_de_calidad__c && (caseCC.FI_CAS_SEL_resultado__c == 'KO' || caseCC.FI_CAS_SEL_resultado__c == 'OK')
        || caseCC.Status == 'Resuelto Motivo: Formalización automática'){
            Map<Id,NE__OrderItem__c> updateCIMap = new Map<Id,NE__OrderItem__c>(); //Mapa de ConfigurationItems a actualizar
            Map<String, Task> insertIncMap = new Map<String, Task>(); //Lista de nuevas incidencias a insertar
            
            // Obtener etapas correspondientes al RT del Caso actual
            String devName = [SELECT DeveloperName FROM RecordType WHERE Id = :caseCC.RecordTypeId].DeveloperName;
            List<FI_Picklist_RecordType_Values__mdt> stageCaseAux = [SELECT FI_PRTV_TXT_PicklistValue__c
                                                                     FROM FI_Picklist_RecordType_Values__mdt
                                                                     WHERE FI_PRTV_TXT_RecordTypeDevN__c  = :devName];
            List<String> stagesCase = new List<String>();
            for(FI_Picklist_RecordType_Values__mdt pick : stageCaseAux){
                stagesCase.add(pick.FI_PRTV_TXT_PicklistValue__c);
            }
            if(stagesCase.isEmpty()){
                caseCC.Information__c = 'No se ha configurado ninguna etapa de control de calidad para el tipo de registro de Caso actual.';
                caseCC.Status = Label.FI_CAS_STATUS_PENDING;
                caseCC.FI_CAS_SEL_resultado__c = null; // Para que no se caiga
                update caseCC;
            }
            else {
                // Obtener KOs no recuperables
                List<KO_routing__mdt> kosNoRecover = new List<KO_routing__mdt>();
                if (Schema.sObjectType.KO_routing__mdt.isAccessible()){
                    kosNoRecover = [SELECT Id,
                                    Action__c,
                                    Recoverable__c,
                                    Description_KO__c,
                                    Channel__c,
                                    Subchannel__c,
                                    Provider__c
                                    FROM KO_routing__mdt
                                    WHERE Recoverable__c = 'No'];
                }
                // Obtener Ofertas incluidas en el Caso
                List<NE__Quote__c> caseQuotesList = (List<NE__Quote__c>) Utilities.getObject(new NE__Quote__c(), 'FI_NEQ_LKP_QaCase__c', caseCC.Id);
                
                // Obtener Líneas de Oferta (ConfigurationItems) incluidas en el Caso
                Map<Id,List<NE__OrderItem__c>> mapListCICaseCC = Utilities.getMapListCICase(caseCC.Id);
                
                // Obtener incidencias incluidas en el Caso de Control de Calidad agrupadas por ConfigurationItem
                Map<Id,List<Task>> mapListTaskCaseCC = Utilities.getMapListTaskCaseByCI(caseCC.Id);
                
                Boolean quotesWithoutInc = false; // Algun ConfigurationItem sin incidencias-etapa
                Boolean quotesOK = false; // Alguna Oferta Ok
                Boolean quotesPendingRec = false; // Alguna Oferta Pdte.Recuperación
                Boolean quotesPendingInc = false; //Algun ConfigurationItem sin todas las inc
                Boolean quotesCancel = false; //Alguna Quote cancelada
                Boolean ciWithoutInc = false; 
                
                Map<String, String> auxQuoteStatus = new Map<String, String> ();
                List<NE__OrderItem__c> cancelledCIs = new List<NE__OrderItem__c>();
                
                // Recorrer Ofertas del Caso
                if (caseQuotesList != null) {
                    for(NE__Quote__c q : caseQuotesList){  
                        Boolean ciOneOK = false; // Algun ConfigurationItem Ok
                        Boolean ciOnePendRec = false; // Algun ConfigurationItem Pdte.Recuperación
                        Boolean ciOneCancel = false; // Algun ConfigurationItem Cancelado
                        Boolean ciPendingInc = false; //Algun CI sin etapas terminadas
                        
                        // Recorrer Configuration Items de la Oferta
                        for(NE__OrderItem__c ci : mapListCICaseCC.get(q.Id)){
                            if (ci.NE__Status__c != Label.FI_CI_STATUS_FORMALIZED){
                                Integer incCountKO = 0; // Nº de inciencias-etapa KO
                                Integer incCountOK = 0; // Nº de inciencias-etapa OK
                                Integer incCountKORec = 0; // Nº de inciencias-etapa KO Recuperable
                                Boolean incNoRecu = false; // Existen incidencias-etapa KO No Recuperables
                                
                                if(ci.NE__Status__c != Label.FI_CI_STATUS_CANCEL){
                                    List<Task> incList = mapListTaskCaseCC.get(ci.Id);
                                    // Si no hay incidencias en un Configuration Item: Informar al usuario que tiene que evaluar al menos una etapa
                                    if(incList == null || incList.isEmpty()){
                                        ciWithoutInc = true;
                                        break;
                                    }
                                    
                                    // Recorrer incidencias generadas del Configuration Item
                                    for(Task inc : incList){
                                        // Si la incidencia-etapa es KO: contabilizar los KO y cuántos de éstos no son Recuperables
                                        if(inc.Quality_control_result__c == 'KO'){
                                            incCountKO++;
                                            incCountKORec++;
                                            // Si el KO no es recuperable: no se contabiliza como KO Recuperable
                                            for(KO_routing__mdt ko : kosNoRecover){
                                                if(ko.Channel__c == ci.FI_CI_SEL_Access_channel__c && ko.Description_KO__c == inc.Quality_error__c){
                                                    incCountKORec--;
                                                    incNoRecu = true;
                                                    break;
                                                }
                                            }
                                        }
                                        // Si la incidencia-etapa es OK: contabilizamos los OK
                                        else if(inc.Quality_control_result__c == 'OK'){
                                            incCountOK++;
                                        }
                                    }// End for(Task inc
                                    
                                    // Actualización de estado de Configuration Item:
                                    // Todas las etapas OK: Validado OK
                                    if(incCountOK == stagesCase.size()){
                                        ci.FI_CI_FLG_Control_Calidad_OK__c = true;
                                        ciOneOK = true;
                                    }
                                    // Etapas pendientes:
                                    else if(incCountOK > 0 && incCountOK < stagesCase.size() && incCountKORec == 0 && incCountKO == 0){
                                        ciPendingInc = true;
                                    }
                                    // Alguna incidencia KO:
                                    else if(incCountKO > 0){
                                        // Alguna incidencia KO recuperable: Pdte. Recuperación
                                        if(incCountKORec > 0 && (!incNoRecu)){
                                            ci.NE__Status__c = Label.FI_CI_STATUS_REC_PENDING;
                                            ci.FI_CI_FLG_Requiere_CC__c = false;
                                            ciOnePendRec = true;
                                            insertIncMap = createTaskCC(caseCC, ci, stagesCase, mapListTaskCaseCC.get(ci.Id), insertIncMap);
                                        }
                                        // Ninguna incidencia KO no recuperable: Cancelada
                                        else {
                                            ci.NE__Status__c = Label.FI_CI_STATUS_CANCEL;
                                            ci.FI_CI_SEL_CancelReason__c = Label.FI_CI_CANCREASON_QUALITYCTRL;
                                            ciOneCancel = true;
                                            cancelledCIs.add(ci);
                                        }
                                    }
                                    updateCIMap.put(ci.Id,ci);
                                }
                            } else {
                                ciOneOK = true;
                            }
                        }// End for(NE__OrderItem__c
                        
                        if(ciOneOK){
                            //EL CI ESTA OK
                            if(auxQuoteStatus.containsKey(q.Id) && auxQuoteStatus.get(q.Id) == 'ok'){
                                auxQuoteStatus.put(q.Id, 'ok');
                            }
                            else if(!auxQuoteStatus.containsKey(q.Id)){
                                auxQuoteStatus.put(q.Id, 'ok');
                            }
                        } 
                        
                        if(ciOnePendRec){
                            //EL CI tiene etapas ptes de recuperacion
                            auxQuoteStatus.put(q.Id, 'recoverPending');
                            for(NE__OrderItem__c configItem : mapListCICaseCC.get(q.Id)){
                                if(configItem.NE__Status__c != Label.FI_CI_STATUS_REC_PENDING 
                                   && configItem.NE__Status__c != Label.FI_CI_STATUS_FORMALIZED && configItem.NE__Status__c != Label.FI_CI_STATUS_CANCEL){
                                       configItem.NE__Status__c = Label.FI_CI_STATUS_REC_PENDING;
                                       configItem.FI_CI_FLG_Requiere_CC__c = false;
                                       // Generar incidencias restantes para KO
                                       insertIncMap = createTaskCC(caseCC, configItem, stagesCase, mapListTaskCaseCC.get(configItem.Id), insertIncMap);
                                       updateCIMap.put(configItem.Id, configItem);
                                   }
                            }
                        }
                        
                        if (ciPendingInc){
                            //EL CI tiene etapas sin evaluar
                            if (!auxQuoteStatus.containsKey(q.Id) || (auxQuoteStatus.containsKey(q.Id) && auxQuoteStatus.get(q.Id) != 'recoverPending')){
                                auxQuoteStatus.put(q.Id, 'withoutInc');    
                            }
                        }
                        System.debug('ciOneCancel ' + ciOneCancel);
                        if (ciOneCancel) {
                            //EL CI esta cancelado 
                            for(NE__OrderItem__c configItem : cancelledCIs){
                                insertIncMap = createTaskCC(caseCC, configItem, stagesCase, mapListTaskCaseCC.get(configItem.Id), insertIncMap);
                                configItem.NE__Status__c = Label.FI_CI_STATUS_CANCEL;
                                configItem.FI_CI_SEL_CancelReason__c = Label.FI_CI_CANCREASON_QUALITYCTRL;
                                updateCIMap.put(configItem.Id, configItem);
                            }
                        }
                        
                    } ///end for quotes
                }
                checkCaseConditions(auxQuoteStatus, ciWithoutInc, caseCC, updateCIMap.values(), client);
                
                if(!insertIncMap.isEmpty()){
                    insert insertIncMap.values();
                }
            }
        }
    }
    
    /**
* Comprueba los diferentes estados de las quotes para cerrar el caso o dejarlo pendiente
* Tambien cuando el caso se cierra como Ok, se pasan las validaciones de formalizacion.
*/
    @testVisible
    private static void checkCaseConditions(Map<String, String> auxQuoteStatus, Boolean ciWithoutInc, Case caseCC, List<NE__OrderItem__c> configItems, Account client){
        Boolean canUpdateCIs = true; //Para saber si actualizar los CIs o no.
        system.debug('Entrada checkCaseConditions ' + configItems);
        Boolean oneQuoteOk = false; //Al menos una quote OK
        Boolean oneQuotePending = false; //Al menos una quote Pending
        Boolean oneQuoteKo = false; //Al menos una quote KO
        Boolean oneQuotePteRecover = false; //Al menos una quote pte recuperacion
        
        for (String quoteId : auxQuoteStatus.keySet()){
            String quoteStatus = auxQuoteStatus.get(quoteId);
            if (quoteStatus == 'ko'){
                oneQuoteKo = true;
            } else if (quoteStatus == 'ok') {
                oneQuoteOk = true;
            } else if (quoteStatus == 'withoutInc'){
                oneQuotePending = true;
            } else if (quoteStatus == 'recoverPending'){
                oneQuotePteRecover = true;
            }
        }
        if(ciWithoutInc && !oneQuotePteRecover){ //Si no tiene incidencias
            caseCC.Information__c = 'No se puede cerrar el caso porque existen líneas de oferta sin etapas del control de calidad evaluadas.';
            caseCC.Status = Label.FI_CAS_STATUS_PENDING;
            caseCC.FI_CAS_SEL_resultado__c = null;
            update caseCC;
            canUpdateCIs = false;
        }
        else if(oneQuotePending){
            //Si tiene pendientes por hacer
            //No se puede cerrar aun pendientes.
            caseCC.Information__c = 'No se puede cerrar el caso porque existen líneas de oferta sin etapas del control de calidad evaluadas.';
            caseCC.Status = Label.FI_CAS_STATUS_PENDING;
            caseCC.FI_CAS_SEL_resultado__c = null;
            update caseCC;
            canUpdateCIs = false;
        }
        else {
            if (caseCC.FI_CAS_SEL_resultado__c == 'OK' || caseCC.Status == 'Resuelto Motivo: Formalización automática') { //Marcado como OK
                if (oneQuotePteRecover) { //Si esta en pte recuperacion
                    caseCC.Information__c = 'No se puede cerrar el caso como OK porque hay al menos una línea de oferta pendiente de recuperación.';
                    caseCC.Status = Label.FI_CAS_STATUS_PENDING;
                    caseCC.FI_CAS_SEL_resultado__c = null;
                    update caseCC;
                    canUpdateCIs = false;
                }
                else {
                    if (oneQuoteOk){
                        //Se puede cerrar OK porque tiene al menos un CI OK
                        
                        FI_ValidationsService auxVal = new FI_ValidationsService();
                        List<B2C_SummaryKO__c> auxKos = auxVal.validationRules(configItems);
                        
                        Map<Id, NE__OrderItem__c> mapScoringConfigItems = new Map<Id, NE__OrderItem__c>();
                        for(NE__OrderItem__c configItem : configItems){
                            if(configItem.NE__Status__c != Label.FI_CI_STATUS_CANCEL){
                                mapScoringConfigItems.put(configItem.Id, configItem);
                            }
                        }
                        if(!mapScoringConfigItems.isEmpty()){
                            SummaryKOQuote.scoring(mapScoringConfigItems, client, auxKos);
                        }
                        
                        delete [SELECT Id FROM B2C_SummaryKO__c WHERE FI_NEQ_MD_Quote__r.FI_NEQ_LKP_QaCase__c = :caseCC.Id];
                        
                        if (auxKos.isEmpty()){
                            caseCC.Information__c = 'Resuelto ' + caseCC.FI_CAS_SEL_resultado__c;
                            update caseCC;
                        } else if (!auxKos.isEmpty()){
                            caseCC.Information__c = 'No se puede cerrar el caso porque alguna línea de oferta tiene errores automáticos.';
                            caseCC.Status = Label.FI_CAS_STATUS_PENDING;
                            caseCC.FI_CAS_SEL_resultado__c = null;
                            update caseCC;
                            canUpdateCIs = false;
                            insert auxKos;
                        }
                    } else {
                        //No se puede cerrar porque no tiene pendientes ni OK
                        caseCC.Information__c = 'No se puede cerrar el caso como OK porque no tiene ninguna línea de oferta en estado OK.';
                        caseCC.Status = Label.FI_CAS_STATUS_PENDING;
                        caseCC.FI_CAS_SEL_resultado__c = null;
                        update caseCC;                        
                        canUpdateCIs = false;
                    }
                }
            } else { //Marcado KO
                //System.debug('ES KO -------------------------------------------->');
                if (oneQuotePteRecover){ //si esta en pte recuperacion
                    caseCC.Information__c = 'Resuelto ' + caseCC.FI_CAS_SEL_resultado__c;
                    update caseCC;
                    // Permitir formalizar Caso Necesidad para formalizar el KO recuperable
                    if(caseCC.FI_CAS_LKP_CaseNeed__c != null){
                        update (new Case(Id=caseCC.FI_CAS_LKP_CaseNeed__c,FI_CAS_FLG_Formalized__c=false));
                    }
                }
                else {  
                    if (oneQuoteOk){
                        //No se puede cerrar KO porque tiene almenos un CI OK
                        caseCC.Information__c = 'No se puede cerrar el caso como KO porque tiene al menos una línea de oferta OK.';
                        caseCC.Status = Label.FI_CAS_STATUS_PENDING;
                        caseCC.FI_CAS_SEL_resultado__c = null;
                        update caseCC;
                        canUpdateCIs = false;
                    }
                    else {
                        //Se puede cerrar KO porque no tiene pendientes ni OK
                        caseCC.Information__c = 'Resuelto ' + caseCC.FI_CAS_SEL_resultado__c;
                        update caseCC;
                        
                        Set<Id> configItemIds = new Set<Id>();
                        for (NE__OrderItem__c configItem : configItems){
                            configItemIds.add(configItem.Id);
                        }
                        closeControlDocCases(configItemIds, Label.FI_CAS_STATUS_RESOLVED_RELCASE, 'KO No Recuperable en Caso Control de Calidad');
                    }
                }
            }
        }
        
        if(canUpdateCIs && !configItems.isEmpty()){
            for(NE__OrderItem__c ci : configItems){
                ci.FI_CI_FLG_Skip_Validations__c = true;
            }
            update configItems;
        }
    }
    
    /**
* Genera el caso de control de calidad
*/
    @TestVisible
    private static Case generateCQCase(Case casoNecesidad, NE__OrderItem__c configItem, Contact contacto, String casetype, Id entitlementId){
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId = [SELECT id FROM AssignmentRule WHERE SobjectType = 'Case' AND Active = true LIMIT 1].id;
        
        Case casoControlCalidad = new Case();
        //casoControlCalidad.FI_TXT_CAS_Autonomous_community__c = configItem.FI_CI_FOR_Comunidad_Autonoma_User__c;
        casoControlCalidad.FI_TXT_CAS_Autonomous_community__c = configItem.FI_CI_FOR_Autonomous_community__c;        
        casoControlCalidad.FI_CAS_TXT_SubCanalCI__c = configItem.FI_CI_SEL_Subcanal__c;
        casoControlCalidad.FI_CAS_FLG_Critical_movement__c = configItem.FI_CI_FLG_Critical_movement__c ;
        casoControlCalidad.AccountId = casoNecesidad.AccountId;
        casoControlCalidad.ContactId = (contacto != null) ? contacto.Id : null;
        casoControlCalidad.FI_CAS_FLG_Control_de_calidad__c = true;
        casoControlCalidad.Status = Label.FI_CAS_STATUS_PENDING;
        casoControlCalidad.Type = Utilities.lookupValue('Third_matrix__mdt', 'Case_type__c', casetype);
        casoControlCalidad.queue_percentage__c = Integer.valueOf((Math.random() * 100));
        casoControlCalidad.Email__c = (contacto != null) ? contacto.Email : null;
        casoControlCalidad.FII_TXT_Input_Channel__c = casoNecesidad.Access_channel__c;
        casoControlCalidad.FI_CAS_LKP_CaseNeed__c = casoNecesidad.Id;
        casoControlCalidad.Access_channel__c = configItem.FI_CI_SEL_Access_channel__c;
        // Añadido para los recordtype de Firma Digital y SMS + TXL
        if(configItem.FI_CI_SEL_SignatureType__c == 'Digital' || configItem.FI_CI_SEL_SignatureType__c == 'SMS + TXL'){
            casoControlCalidad.TV_Tipo_de_firma__c = configItem.FI_CI_SEL_SignatureType__c;
            casoControlCalidad.TV_Campaign_Name__c = casoNecesidad.FII_CAS_LKP_Campaign__r.Name;
            casoControlCalidad.TV_Partner__c = casoNecesidad.FII_CAS_LKP_Campaign__r.TV_Partner__c;
            casoControlCalidad.TV_Telephone__c = (contacto != null) ? contacto.Phone : null;
            Task j = [SELECT Id, CallObject FROM Task WHERE Id =: casoNecesidad.FII_CAS_TXT_Opened_interaction__c];
            casoControlCalidad.TV_Call_Id__c = j.CallObject;	
        }
        // Tipo de Registro
        Boolean isCommunityAlliance = false;
        User currUser = [SELECT FI_SEL_Community_Type__c FROM User WHERE Id = :UserInfo.getUserId()];
        if (currUser.FI_SEL_Community_Type__c == 'Aliado'){
            casoControlCalidad.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Caso_Aliados').getRecordTypeId();
        } else {
            casoControlCalidad.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(casetype).getRecordTypeId();
        }
        
        // Assignment Rules
        casoControlCalidad.setOptions(dmlOpts);
        if (entitlementId != null) {
            casoControlCalidad.EntitlementId = entitlementId;
        }
        insert casoControlCalidad;
        
        return casoControlCalidad;
    }
    
    
    public static Case generateQACase(Map<Id, NE__OrderItem__c> mapConfigItem, Case casoNecesidad, Contact contacto){
        Case returnCase = null;
        Set<Id> quotesForQACase = new Set<Id>();
        
        // 1. Comprobar si existen otros Casos de Control de Calidad para el Caso Necesidad
        for(Case qCase : [SELECT Id, Status FROM Case WHERE FI_CAS_FLG_Control_de_calidad__c = true AND FI_CAS_LKP_CaseNeed__c = :casoNecesidad.Id]){
            if(!STATUS_QCASE_FINAL.contains(qCase.Status)){
                returnCase = qCase;
                break;
            }
        }
        // 2a. Si existe un Caso de Control de Calidad abierto para el Caso Necesidad: no se genera de nuevo, se devuelve el caso actual
        if (returnCase != null){
            quotesForQACase = getQuotesWithCIRequiredCC(mapConfigItem.values());
        }
        // 2b. Si no existe un Caso de Control de Calidad abierto para el Caso Necesidad: ejecutar lógica de generación Caso de Control de Calidad
        else {
            System.debug('Generar CQ');
            // 2b.1. Verificar CIs contra matriz de terceros
            Third_matrix__mdt matrixResult = checkMatrix(mapConfigItem, contacto);
            quotesForQACase = getQuotesWithCIRequiredCC(mapConfigItem.values());
            System.debug('Generar CQ quotesForQACase ' + quotesForQACase);
            // 2b.2. Si se obtiene un match en la matriz, es decir, se debe generar un Caso de Control de Calidad
            if (matrixResult != null && !quotesForQACase.isEmpty()){
                // 2b.3. Obtener el CI para establecer en el Caso de Control de Calidad los campos "Comunidad Autónoma" y "Mov. Crítico" necesarios para las Case AssignmentRules
                NE__OrderItem__c ciAssignRules = mapConfigItem.values()[0];
                // Si el CI actual no ha disparado la generación del Caso de Control de Calidad o si lo ha hecho pero no es un movimiento crítico, buscamos el primer CI:
                // a) "disparador con mov.crítico"
                // b) "disparador sin mov.crítico" si ninguno de los que ha disparado la generación del Caso de Control de Calidad es un movimiento crítico
                if((!ciAssignRules.FI_CI_FLG_Requiere_CC__c) || (!ciAssignRules.FI_CI_FLG_Critical_movement__c)){
                    for(NE__OrderItem__c ci : mapConfigItem.values()){
                        if(ci.FI_CI_FLG_Requiere_CC__c && (!ciAssignRules.FI_CI_FLG_Requiere_CC__c ||
                                                           (ci.FI_CI_FLG_Critical_movement__c && !ciAssignRules.FI_CI_FLG_Critical_movement__c))){
                                                               ciAssignRules = ci;
                                                               // Si el CI es "disparador con mov.crítico" no es necesario seguir buscando
                                                               if(ciAssignRules.FI_CI_FLG_Critical_movement__c){
                                                                   break;
                                                               }
                                                           }
                    }
                }
                
                // 2b.4. Obtener registro de Entitlement a partir del campo Entitlement_Name del registro de la Matriz de Terceros
                Id entitlementId = null;
                if(matrixResult.Entitlement_Name__c != null){
                    List<Entitlement> entitlementObj = new List<Entitlement>();
                    if(Schema.sObjectType.Entitlement.isAccessible()){
                        entitlementObj = [SELECT Id FROM Entitlement WHERE Name = :matrixResult.Entitlement_Name__c];
                        if(!entitlementObj.isEmpty()){
                            entitlementId = entitlementObj.get(0).Id;
                        }
                    }
                }
                // 2b.5. Generar Caso de Control de Calidad
                System.debug('Generar CQ');
                returnCase = generateCQCase(casoNecesidad, ciAssignRules, contacto, matrixResult.Case_type__c, entitlementId);
            }
        }
        
        // 3. Si hay un Caso de Control de Calidad (recién generado o existente): vinculamos las ofertas a dicho Caso
        if(returnCase != null && !quotesForQACase.isEmpty() && Schema.sObjectType.NE__Quote__c.isUpdateable()){
            List<NE__Quote__c> quotesUpd = new List<NE__Quote__c>();
            for (Id quoteId : quotesForQACase){
                quotesUpd.add(new NE__Quote__c(Id=quoteId, FI_NEQ_LKP_QaCase__c=returnCase.Id));
            }
            update quotesUpd;
        }
        return returnCase;
    }
    
    @TestVisible
    private static Third_matrix__mdt checkMatrix(Map<Id, NE__OrderItem__c> configurationItems, Contact contacto){
        List<Third_matrix__mdt> matrix = [SELECT Id,
                                          Business_line__c,
                                          Case_type__c,
                                          Channel__c,
                                          Company_Holder__c,
                                          Critical_movement__c,
                                          Destination_product__c,
                                          Entitlement_Name__c,
                                          Language__c,
                                          MasterLabel,
                                          TV_Sing_Type__c,
                                          Movement_type__c,
                                          Product_origin__c,
                                          Product_type__c,
                                          Remunerable__c,
                                          SubChannel__c,
                                          Subtype_of_movement__c,
                                          Supplier__c
                                          FROM Third_matrix__mdt];
        Third_matrix__mdt matrixForReturn;
        //Recorremos los CIs
        for (NE__OrderItem__c configItem : configurationItems.values()){
            //System.debug('Matrix CI: ' + configItem);
            Boolean formalized = true;
            //Recorremos la matriz de terceros por cada CI
            for (Third_matrix__mdt matrixEntry : matrix){
                if ((matrixEntry.Channel__c == configItem.FI_CI_SEL_Access_channel__c || matrixEntry.Channel__c == null) &&
                    (matrixEntry.SubChannel__c == configItem.FI_CI_TXT_PDS_Ocaps_TF__c || matrixEntry.SubChannel__c == null) &&
                    (matrixEntry.Business_line__c == configItem.FI_CI_SEL_Business_Line__c || matrixEntry.Business_line__c == null) &&
                    (matrixEntry.Supplier__c == configItem.FI_CI_SEL_Supplier__c || matrixEntry.Supplier__c == null) &&
                    (matrixEntry.Movement_type__c == configItem.FI_CI_SEL_Type__c || matrixEntry.Movement_type__c == null) &&
                    (matrixEntry.Subtype_of_movement__c == configItem.FI_CI_FOR_Subtype_CRM__c || matrixEntry.Subtype_of_movement__c == null) &&
                    (matrixEntry.Company_Holder__c == configItem.FI_CI_SEL_Company_holder__c || matrixEntry.Company_Holder__c == null) &&
                    (matrixEntry.TV_Sing_Type__c == configItem.FI_CI_SEL_SignatureType__c) || (matrixEntry.TV_Sing_Type__c == null) &&																												  
                    ((contacto != null && matrixEntry.Language__c == contacto.Language__c) || matrixEntry.Language__c == null) &&
                    matrixEntry.Critical_movement__c == configItem.FI_CI_FLG_Critical_movement__c) {
                        //para controlar si un CI encaja con algun mdt
                        formalized = false;
                        matrixForReturn = matrixEntry;
                        System.debug('****matrixEntry: ' + matrixEntry);
                    }		
            }
            
            if (formalized){
                configItem.FI_CI_FLG_Control_Calidad_OK__c = true;
            }
            else {
                configItem.FI_CI_FLG_Requiere_CC__c = true;
            }
            configItem.FI_CI_FLG_Skip_Validations__c = true;
        }
        System.debug('****matrixForReturn: ' + matrixForReturn);
        return matrixForReturn;
    }
    
    
    //Metodo que devuelve lista de Quotes que tienen CI's RequiredCC a true
    private static Set<Id> getQuotesWithCIRequiredCC(List<NE__OrderItem__c> configItems){
        Set<Id> returnQuotes = new Set<Id>();
        for(NE__OrderItem__c configItem : configItems){
            if(configItem.FI_CI_FLG_Requiere_CC__c && configItem.FI_CI_LKP_Quote__c != null){
                returnQuotes.add(configItem.FI_CI_LKP_Quote__c);
            }
        }
        return returnQuotes;
    }
    
    
    public static void closeControlDocCases(Set<Id> configItemIds, String closeStatus, String closeReason){
        if(configItemIds != null && !configItemIds.isEmpty()){
            List<Case> documentCaseOfCIs = [SELECT Id, Status, Information__c, FI_CAS_SEL_resultado__c
                                            FROM Case WHERE FI_CAS_LKP_ConfigItem__c IN :configItemIds
                                            AND IsClosed = false];
            
            if(!documentCaseOfCIs.isEmpty()){
                for (Case caseDoc : documentCaseOfCIs){
                    caseDoc.Status = closeStatus;
                    caseDoc.FI_CAS_SEL_resultado__c = null;
                    caseDoc.Information__c = closeReason;
                }
                FII_TRG_Case_Handler.bypassTrigger = true;
                update documentCaseOfCIs;
                FII_TRG_Case_Handler.bypassTrigger = false;
            }
        }
    }
}