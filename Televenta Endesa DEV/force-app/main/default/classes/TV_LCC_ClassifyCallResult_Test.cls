@isTest
public class TV_LCC_ClassifyCallResult_Test {

    // RecordTypeId of the record types used
    private static final Id IDRT_CAS_NEED = FII_Util_MasterData_RecordTypes.getByDevName(Label.FII_RT_CAS_NEED).Id;
    private static final Id IDRT_ACT_INTERACTION = FII_Util_MasterData_RecordTypes.getByDevName(Label.FII_RT_TSK_INTERACTION).Id;
    private static final Id campCCDD = FII_Util_MasterData_RecordTypes.getByDevName('Campaign:FII_RT_CAM_Attention_DigitalChannel_Planned').Id;
    private static final Id campMulti = FII_Util_MasterData_RecordTypes.getByDevName('Campaign:FII_RT_CAM_Attention_Multichannel_Planned').Id;
    private static final Id RTYPEID = Schema.SObjectType.NE__Quote__c.getRecordTypeInfosByDeveloperName().get('Aliados').getRecordTypeId();


    @testSetup
    private static void testSetup(){ 
        
        // Job
        FII_Job__c job = new FII_Job__c(FII_PT_Platform__c ='EU_BCN',
                                        FII_PT_Territory__c ='Andalucía/Extremadura',FII_PT_Canal__c= '1' );
        insert job; 

        FII_UserJobRelation__c userJob = new FII_UserJobRelation__c(FII_UJR_LKP_Job__c =job.Id,FII_UJR_LKP_User__c = UserInfo.getUserId());
        insert userJob;
        // Accounts for the Interaccion
        AccountTriggerHandler.bypassTrigger = true;
        Account acc = new Account(Name = 'Autocalculado', Identifier_Type__c='NIF', NIF_CIF_Customer_NIE__c ='31841666Y', FirstName__c = 'Acc', LastName__c = 'Test', Physical_or_legal_cd__c = 'Physical Customer', Account_language__c = 'Español', Contact_method_cd__c = 'SMS', MobilePhone1__c = '632145987', No_Phone_flg__c  = false, No_Email_flg__c = true,Account_status__c = 'Active');
        insert acc;
        AccountTriggerHandler.bypassTrigger = false;
        
        //Contacts for the Interaction
        Contact cont = new Contact(FirstName = 'Cont', LastName = 'TestsQuery', Contact_method_cd__c = 'SMS',AccountId = acc.Id, MobilePhone = '632145987', No_Phone_flg__c  = false, No_Email_flg__c  = true, Document_Type__c ='NIF', NIF_CIF_Customer_NIE__c ='73009311C');
        ContactTriggerHandler.bypassTrigger = true;
        insert cont;
        ContactTriggerHandler.bypassTrigger = false;

        
        Case ca = new Case( Access_channel__c = '10',
                        AccountId = acc.Id,
                        FII_CAS_SEL_Subtype__c = 'Consulta',
                        FII_CAS_TXT_Closing_comments__c = 'Comments',
                        Origin = 'Website',
                        RecordTypeId = IDRT_CAS_NEED,
                        Status='New', 
                        Subject = 'Subject',
                        Type = 'Atencion al cliente');
        insert ca;
        
        Task t = new Task(Subject = 'TaskTest', WhoId = cont.Id, RecordTypeId = IDRT_ACT_INTERACTION, Type = 'Action',FII_ACT_TXT_Main_interaction__c = null,FII_ACT_TXT_CaseNeed__c=ca.Id);
        t.FII_ACT_LKP_RelatedClient__c = acc.Id;
        insert t;
        
        Opportunity opportunity = new Opportunity();
        opportunity.Name = 'Test Opportunity';
        opportunity.AccountId = acc.Id;
        opportunity.StageName = 'New';
        opportunity.CloseDate = System.today();
        opportunity.FI_OPP_LKP_Case__c= ca.Id;
        insert opportunity;
        
        TV_CallClassification__c callClassification = new TV_CallClassification__c();
		callClassification.TV_CCL_SEL_ContactType__c = 'VENTA CANCELADA';
        callClassification.TV_CCL_SEL_Aggrupation__c = 'NO COLABORA';
        callClassification.TV_CCL_SEL_Typology__c = 'CAMBIO DE DOMICILIO EN BREVE';
        callClassification.TV_CCL_TXT_Code__c = 'Test';
        callClassification.TV_CCL_FLG_NeedAdditionalChannel__c = false;
        callClassification.TV_CCL_FLG_NeedAdditionalDate__c = false;
        callClassification.TV_CCL_FLG_IsRecall__c = false;
        callClassification.TV_CCL_FLG_IsSellOk__c = false;
        insert callClassification;
        
        List<Campaign> campaignsToInsert = new List<Campaign>();
        Campaign com = new Campaign(Name='Campaign test', RecordTypeId = campCCDD, Department__c = 'Atención Canales Digitales', Type = 'Planned', Campaignsubtype__c = 'Multiple', FII_CAM_MSE_CampaignChannel__c = 'CAT');
        campaignsToInsert.add(com);
        Campaign com2 = new Campaign(Name='Campaign test 2', RecordTypeId = campMulti, Department__c = 'Atencion Multicanal', Type = 'Planned', Campaignsubtype__c = 'Multiple', FII_CAM_MSE_CampaignChannel__c = 'CAT');
        campaignsToInsert.add(com2);
        insert campaignsToInsert;
		
        List<CampaignMember> campaignMembersToInsert = new List<CampaignMember>();
        CampaignMember cm = new CampaignMember(CampaignId = com.Id, FII_CAMM_LKP_AccountMember__c = acc.Id, ContactId = cont.Id);
        campaignMembersToInsert.add(cm);
        CampaignMember cm2 = new CampaignMember(CampaignId = com2.Id, FII_CAMM_LKP_AccountMember__c = acc.Id, ContactId = cont.Id);
        campaignMembersToInsert.add(cm2);
        insert campaignMembersToInsert;
        
        NE__Quote__c quote = new NE__Quote__c();
        quote.NE__Status__c = 'Draft';
        quote.RecordTypeId = RTYPEID;
        quote.FI_NEQ_SEL_Movement_Type__c = 'A';
        quote.FI_NEQ_SEL_Flow_Controlling_Action__c = 'A';
        quote.Case__c = ca.Id;
        insert quote;

        NE__Order__c configuration = new NE__Order__c();
        configuration.NE__Configuration_Type__c = 'New';
        configuration.NE__Configuration_SubType__c = 'Standard';
        configuration.NE__AccountId__c = acc.Id;
        configuration.NE__BillAccId__c = acc.Id;
        configuration.NE__ServAccId__c = acc.Id;
        configuration.NE__Quote__c = quote.id;
        insert configuration;
        
        B2C_Address__c address = new B2C_Address__c();
        address.Account__c = acc.Id;
        address.CUPS_13_elec__c = 'ES12345678901';
        address.Name = 'Test Address';
        address.City__c = 'Madrid';
        address.City_cd__c = 'BER38899';
        address.County__c = 'Spain';
        address.Zipcode__c = '28001';
        address.INE_city_cd__c = '280000000';
        address.Formatedd_Address__c = 'Test Address';
        address.Street__c = 'Test Address';
        address.B2C_Quote_Id__c  = quote.Id;
        AddressTriggerHandler.bypassTrigger = true;   
        insert address;
        AddressTriggerHandler.bypassTrigger = false;   

       	NE__OrderItem__c configurationItem = new NE__OrderItem__c();
        configurationItem.NE__OrderId__c = configuration.Id;
        configurationItem.NE__Qty__c = 30;
        configurationItem.NE__Account__c = acc.Id;
        configurationItem.FI_CI_LKP_Address__c = address.Id;
        configurationItem.FI_CI_TXT_FirstName__c = 'CD';
        configurationItem.FI_CI_TXT_LastName__c ='1234';
		configurationItem.NE__Status__c = 'Signature_pending';
        configurationItem.FI_CI_LKP_Quote__c = quote.Id;
        FI_TRG_NE_OrderItem_Handler.bypassTrigger = true;
        insert configurationItem;
        FI_TRG_NE_OrderItem_Handler.bypassTrigger = false;
    }


	@isTest
	private static void getLoginData_Test (){
        Test.startTest();
        WS_Info__mdt result;
        result = TV_LCC_ClassifyCallResult.getLoginData();
        system.assertNotEquals(null, result);
        Test.stopTest();
	}
	@isTest
	private static void getPicklistData_Test (){
        Test.startTest();
        List<TV_CallClassification__c> result = new List<TV_CallClassification__c>();
        result = TV_LCC_ClassifyCallResult.getPicklistData();
        system.assertNotEquals(0, result.size());
        Test.stopTest();
	}
	@isTest
	private static void getTaskData_Test (){
        Test.startTest();
        String accountId = [SELECT Id FROM Account LIMIT 1].Id;
        Task result = TV_LCC_ClassifyCallResult.getTaskData(accountId);
        System.assertNotEquals(null, result);
        Test.stopTest();
	}
    @isTest
	private static void updateSalesforceData_Test (){
        Test.startTest();
        String taskId = [SELECT Id FROM task LIMIT 1].Id;
        String campaignMemberId = [SELECT Id FROM CampaignMember LIMIT 1].Id;
        String callClassificationId = [SELECT Id FROM TV_CallClassification__c LIMIT 1].Id;
        String additionalInfo = 'Test';
        String recallAssignTo = 'Test';
        DateTime recallDatetime = system.now();
        String recallDatetimeString = String.valueOf(recallDatetime);
        String recallPhone = '639879657';
        String additionalDate = 'Test';
        String additionalChannel = 'Test';
        TV_LCC_ClassifyCallResult.updateSalesforceData(taskId,campaignMemberId,callClassificationId,additionalInfo,recallAssignTo,recallDatetimeString,recallPhone,additionalDate,additionalChannel, true);
        Test.stopTest();
	}
    @isTest
	private static void updateTaskResult_Test (){
        Test.startTest();
        String taskId = [SELECT Id FROM task LIMIT 1].Id;
        Boolean updatedToICWS = true;
        Boolean updatedInSF = true;
        Boolean technicalError = true;
        TV_LCC_ClassifyCallResult.updateTaskResult(taskId,updatedToICWS,updatedInSF,technicalError);
        Test.stopTest();
	}
    @isTest
	private static void validateVentaOK_Test (){
        Test.startTest();
        task tarea = [SELECT Id FROM task LIMIT 1];
        String taskId = tarea.ID;
        Case caso = [SELECT Id FROM Case LIMIT 1];
        caso.Type = 'Need_03';
        caso.FII_CAS_TXT_Last_interaction__c = taskId;
        tarea.whatId =caso.Id;
        update tarea;
        update caso;
        Boolean result = TV_LCC_ClassifyCallResult.validateVentaOK(taskId);
        Test.stopTest();
	}
    
    
    
    
    
}