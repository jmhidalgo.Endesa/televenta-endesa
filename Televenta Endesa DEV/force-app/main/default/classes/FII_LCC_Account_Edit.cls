/*
 *============================================================================================================================================
 * @className              FII_LCC_Account_Edit
 * @owner                  Everis
 *============================================================================================================================================
 */
public with sharing class FII_LCC_Account_Edit {
    
    /*
 *============================================================================================================================================
 * @methodName               getInformation
 * @received parameters      Id idAccount
 * @return                   List<sObject> objetos
 *============================================================================================================================================
 */
    @AuraEnabled
    public static List<sObject> getInformation (Id idAccount){

        final List<sObject> objetos = new List<sObject>();
        List<Account> infoAccount = new List<Account>();
        List<Contact> infoContact = new List<Contact>();
        List<Individual> infoIndividual = new List<Individual>();
        Id accId;

        if (string.valueOf(idAccount).substring(0,3)=='500') {
            if (Schema.sObjectType.Account.isAccessible()) {
                accId = [SELECT AccountId FROM Case WHERE Id =: idAccount].AccountId;
            }
        } else {
            accId = idAccount;
        }
        
        if (Schema.sObjectType.Account.isAccessible()) {
            infoAccount = [SELECT FirstName__c, LastName__c, Physical_or_legal_cd__c, Identifier_Type__c, NIF_CIF_Customer_NIE__c, 
                            Duplicate_NIF_flg__c, TitularContactHide__c, Has_gas_flg__c, Employee_of_Company_flg__c, Account_Type__c, 
                            BlockingHolding_cd__c, Exempt_Asnef_flg__c, Exempt_Bureu_credi_flg__c, Account_Vulnerable_Check__c, 
                            FII_ACC_FLG_Electrodependient__c FROM Account WHERE Id =: accId];
            objetos.add(infoAccount[0]);
        }
        
        
        if (Schema.sObjectType.Contact.isAccessible()) {
            infoContact = [SELECT Id, OtherPhone, OtherPhone__c, Phone, MobilePhone, Email, Work_email__c, Contact_method_cd__c, 
                            Nationality_cd__c, Contact_Robinson__c, Sex__c, Profession_cd__c, Vulnerability__c, Language__c,
                            No_Phone_flg__c, No_Email_flg__c, FirstName, LastName, NIF_CIF_Customer_NIE__c, Document_Type__c
                            FROM Contact WHERE Id =: infoAccount[0].TitularContactHide__c];
            objetos.add(infoContact[0]);
        }

        if (Schema.sObjectType.Individual.isAccessible()) {
            infoIndividual = [SELECT FII_IND_SEL_Terceros__c, Contact__c, FII_IND_SEL_Cesion__c, FII_IND_SEL_Clientes__c, FII_IND_SEL_Iden_Evi__c, 
                            FII_IND_SEL_Molestar__c FROM Individual WHERE Contact__c =: infoContact[0].Id];
            if (infoIndividual.size() != 0) {
                objetos.add(infoIndividual[0]); 
            }
        }

        return objetos;
    }

/*
 *============================================================================================================================================
 * @className              ResponseUpdateClientAndRelated
 * @owner                  Everis
 *============================================================================================================================================
 */
    public class ResponseUpdateClientAndRelated {
        @AuraEnabled public String dmlMessage {get; set;}
        @AuraEnabled public String type {get; set;}
    }

/*
 *============================================================================================================================================
 * @methodName               modifyContactHide
 * @received parameters      Account accountItem, Contact contactItem, Individual individualItem
 * @return                   (void method)
 *============================================================================================================================================
 */
    @AuraEnabled
    public static void modifyContactHide(Account accountItem, Contact contactItem, Individual individualItem){
        if (accountItem.TitularContactHide__c != null) {
        
            contactItem.FirstName = accountItem.FirstName__c;
            contactItem.LastName = accountItem.LastName__c;
            contactItem.NIF_CIF_Customer_NIE__c = accountItem.NIF_CIF_Customer_NIE__c;
            contactItem.Document_Type__c = accountItem.Identifier_Type__c;

            if (Schema.sObjectType.Contact.isUpdateable()) {
                update contactItem;
            }

            if(individualItem != null){
                individualItem.FirstName = accountItem.FirstName__c;
                individualItem.LastName = accountItem.LastName__c;

                if(Schema.sObjectType.Individual.isUpdateable()){
                    update individualItem;
                }
            }
        }
    }

    @AuraEnabled 
    public static void deleteNeed(Id needId){
        if (Schema.sObjectType.Case.isAccessible() && Schema.sObjectType.Case.isDeletable()
        && Schema.sObjectType.Task.isAccessible() && Schema.sObjectType.Task.isDeletable()) {
            List<Task> t = [Select Id from task WHERE WhatId=:needId];
            if(t.size()>0){
                Delete t;
            }
            

            List<Case> c = [Select Id From Case WHERE id=:needId];
            if(c.size()>0){
                Delete c;
            }
        }
    } 

/*
 *============================================================================================================================================
 * @methodName               updateClientAndRelated
 * @received parameters      Account accountItem, Contact contactItem, Individual individualItem
 * @return                   ResponseUpdateClientAndRelated response
 *============================================================================================================================================
 */
    @AuraEnabled
    public static ResponseUpdateClientAndRelated updateClientAndRelated(Account accountItem, Contact contactItem, Individual individualItem){
        ResponseUpdateClientAndRelated response = new ResponseUpdateClientAndRelated();

        Individual newIndividual = new Individual();

        if(individualItem != null) {
            newIndividual = individualItem;
        }
        
        try {
            if( Schema.sObjectType.Account.isUpdateable() 
               && Schema.sObjectType.Contact.isUpdateable() 
               && Schema.sObjectType.Individual.isCreateable()
               && Schema.sObjectType.Individual.isUpdateable()
              ) {
                  
                  //START STAGE-8 cambiamos modifyContactHide por esta parte de codigo
                  if (accountItem.Physical_or_legal_cd__c == 'Physical Customer') {
                      if (accountItem.TitularContactHide__c != null) {
                          contactItem.FirstName = accountItem.FirstName__c;
                          contactItem.LastName = accountItem.LastName__c;
                          contactItem.NIF_CIF_Customer_NIE__c = accountItem.NIF_CIF_Customer_NIE__c;
                          contactItem.Document_Type__c = accountItem.Identifier_Type__c;

                          if(individualItem != null){
                              individualItem.FirstName = accountItem.FirstName__c;
                              individualItem.LastName = accountItem.LastName__c;
                          }
                      }
                  }
                  //END cambiamos modifyContactHide por esta parte de codigo
                  
                  /*if (individualItem == null){
                      //newIndividual.LastName = contactItem.LastName;
                      //newIndividual.FirstName = contactItem.FirstName;
                      //newIndividual.Contact__c = contactItem.Id;
                      //insert newIndividual;
                  } else {
                      update newIndividual;
                  }*/
                  if(individualItem != null & Schema.sObjectType.Task.isUpdateable()){
                    update newIndividual;
                  }
                  
                  //STAGE-8 Comentamos esta parte ya que en el trigger de contacto lanzamos la actualizacion del email
                  /*Contact contact1 = [Select id,email,Controller_Email_Account_byP__c,Validate_email__c from contact where id = :contactItem.ID];
                    if(contact1.Email !=  contactItem.Email){
                    System.debug('@@@ Nuevo email: ' + contactItem.Email + ' antiguo email ' + contact1.Email);
                    if(contact1.Email == null){
                    contactItem.Controller_Email_Account_byP__c = 'paraentrarenelcircuito@gmail.com';
                    }
                    if(contact1.Validate_email__c){
                    contactItem.Controller_Email_Account_byP__c = contact1.Email;
                    contactItem.Email = contact1.Email;
                    contactItem.New_email__c = contactItem.Email;
                    }else{
                    contactItem.Controller_Email_Account_byP__c = 'paraentrarenelcircuito@gmail.com';
                    contactItem.Email = contactItem.Email;
                    contactItem.New_email__c = contactItem.Email;
                    contactItem.FII_CON_TXT_LastEmail__c = contact1.Email;
                    }

                    }else{

                    }*/
                    //STAGE-8 Para que ejecute el trigger pero solo la parte de asignar el mail correctamente
                    ContactTriggerHandler.bypassTrigger = true;
                    ContactTriggerHandler.executeValidationEmail = true;
                    avoidRecursive.isStartIndividual = false;
                    update contactItem;
                    avoidRecursive.isStartIndividual = true;
                    ContactTriggerHandler.bypassTrigger = false;
                    ContactTriggerHandler.executeValidationEmail = false;

                    accountItem.MobilePhone1__c = contactItem.MobilePhone;
                    accountItem.Email_con__c = contactItem.Email;
                    accountItem.Phone = contactItem.Phone;
                    accountItem.LicensedNoEmailflg__c = contactItem.No_Email_flg__c;
                    update accountItem;
              }
            response.dmlMessage = 'Se han actualizado los datos correctamente. Espere a que se cierre la ventana.';
            response.type = 'success';
            
        } catch (dmlException e) {
            //response.dmlMessage = e.getMessage().split(',')[1].split(':')[0];
            //response.dmlMessage = e.getMessage();
            if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') && e.getMessage().contains(': [')){
                response.dmlMessage = e.getMessage().substringAfter('FIELD_CUSTOM_VALIDATION_EXCEPTION, ').substringBefore(': [');
            } else{
                response.dmlMessage = e.getMessage();
            }
            response.type = 'error';
        }
        return response;
    }

/*
 *============================================================================================================================================
 * @methodName               standardBehaviorGSM
 * @received parameters      Id recId, String type, String subtype, String subjectNeed, String subjectAct, Id camId, String status, String recordtype
 * @return                   Case need
 *============================================================================================================================================
 */
    @AuraEnabled
    public static Case standardBehaviorGSM(Id recId, String type, String subtype, String subjectNeed, String subjectAct, Id camId, String status, String recordtype){
        final List<SObject> lNeedAct = FII_Util_GSM.standardBehaviorGSM(recId, type, subtype, subjectNeed, subjectAct, camId, status, recordtype);
        
        final Case need = (Case)lNeedAct[0];
        Task act = (Task)lNeedAct[1];
        //actualizamos el campo Contact__c para que MKT envie la comunicacion al contacto titular
        if(Schema.sObjectType.Account.isAccessible() && Schema.sObjectType.Task.isUpdateable()){
            final Account ac = [SELECT Account.TitularContactHide__c From Account WHERE Id =:need.AccountId];
            act.Contact__c = ac.TitularContactHide__c;
            update act;
        }

        return need;
    }

    /*
 *============================================================================================================================================
 * @methodName               getObjectsType
 * @received parameters      --
 * @return                   getDescribedObjects( new List<String>{'Account','Contact','Individual'} )
 *============================================================================================================================================
 */
    @AuraEnabled
    public static String getObjectsType() {
        // Passing the sObject name in the method, it could be multiple objects too
        return getDescribedObjects( new List<String>{'Account','Contact','Individual'} );    
    }

/*
 *============================================================================================================================================
 * @methodName               getDescribedObjects
 * @received parameters      List<String> lstSObjectType
 * @return                   String allObjJSON
 *============================================================================================================================================
 */
    public static String getDescribedObjects( List<String> lstSObjectType ) {
        
        // Globally desribe all the objects 
        final Map<String, SObjectType> globalDescribe = Schema.getGlobalDescribe(); 
        // Create a JSON string with object field labels and picklist values
        String allObjJSON = '{';
        
        // Iterate over the list of objects and describe each object  
        for( String sObjectType : lstSObjectType ) {
            
            if( allObjJSON != '{' ) {
                allObjJSON += ', ';
                }
                
            allObjJSON += '"' + sObjectType + '": ';
            final DescribeSObjectResult describeResult = globalDescribe.get(sObjectType).getDescribe();
            final Map<String, Schema.SObjectField> desribedFields = describeResult.fields.getMap();
            String objJSON = '{';
            
            for( String fieldName :  desribedFields.keySet() ) {
                
                // Descirbe the field 
                final Schema.SObjectField field = desribedFields.get( fieldName );
                final Schema.DescribeFieldResult f = field.getDescribe();    
                if( objJSON != '{' ){
                    objJSON += ', '; 
                    }   
                // Get the field label and append in the JSON string
                objJSON += '"' + f.getName() + '": ' + '{ "label" : "' + f.getLabel() + '"';
                
                // if it's a picklist field then also add the picklist options
                if( field.getDescribe().getType() == Schema.DisplayType.PICKLIST ){
                    
                    final List <Schema.PicklistEntry> picklistValues = field.getDescribe().getPickListValues();
                    final List<String> pickListOptions = new List<String>();
                    pickListOptions.add('{ "label": "-", "value": null }');
                    
                    for (Schema.PicklistEntry pe : picklistValues) { 
                        if(pe.getLabel() != 'CA' && pe.getLabel() != 'ES' && pe.getLabel() != 'EN'){
                            pickListOptions.add('{ "label": "' + pe.getLabel() + '", "value": "' + pe.getValue() + '" }');
                        }
                    }
                    
                    System.debug( '>>>> ' + fieldName + '>>>> ' + String.join(pickListOptions, ', ') );
                    objJSON += ', "picklistOptions": [' + String.join(pickListOptions, ', ') + ']';   
                }
                objJSON += '}';
            }
            objJSON += '}';
            
            allObjJSON += objJSON;
        }
        
        // Close the object in the JSON String
        allObjJSON += '}';
        
        System.debug( ' JSON STRING : ' + allObjJSON );
        
        return allObjJSON;
    }

/*
 *============================================================================================================================================
 * @methodName               checkAdminProfile
 * @received parameters      --
 * @return                   boolean false
 *============================================================================================================================================
 */
    @AuraEnabled 
    public static boolean checkAdminProfile(){
        // query current user information  
        User oUser;
        if(Schema.sObjectType.User.isAccessible()){
            oUser = [SELECT Profile.Name FROM User WHERE id =: userInfo.getUserId()];
        }
        if(oUser.Profile.Name.contains('Admin')) {
            return true;
        }
        else {
            return false;
        }
    }

/*
 *============================================================================================================================================
 * @methodName               isTheEventForMe
 * @received parameters      Id myRecId, Id eventRecId, Task interactionInProgress
 * @return                   Boolean FII_Util_GSM.isTheEventForMe(myRecId, eventRecId, interactionInProgress)
 *============================================================================================================================================
 */
    @AuraEnabled 
    public static Boolean isTheEventForMe (Id myRecId, Id eventRecId, Task interactionInProgress){
        return FII_Util_GSM.isTheEventForMe(myRecId, eventRecId, interactionInProgress);
    }
    
    @AuraEnabled
    public static List<String> getUserQueueName() {
        List<String> userQueueName = new List<String>();
        List<GroupMember> d;
        if(Schema.sObjectType.User.isAccessible()){
            d = [SELECT GroupId FROM GroupMember WHERE Group.Type= 'Queue' AND UserOrGroupId =: UserInfo.getuserid()];
        }
        List<String> colas = new List<String>();
        for(GroupMember g : d){
            colas.add(g.GroupId);
        }
        List<Group> opo = [SELECT Id, DeveloperName FROM Group WHERE Id IN: colas];
        
        if (opo != null) {
            for(Group g1 : opo){
                 userQueueName.add(g1.DeveloperName);
            }       
        }
        return userQueueName;
    } 

}