/**
 * @File Name          : TV_CLS_Test_Factory.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 18/6/2020 13:32:40
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/6/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class TV_CLS_Test_Factory {
    
    public static List<Account> createClientList(){
        /*campos obligatorios hasta el momento:
        	nombre,apellido,recordtype,tipo de identificador,identificador,metodo de contacto
        	email,Idioma,Nacionalidad,Persona Física o Jurídica,Cliente,No Dispone de Teléfono*/
        
        List<Account> accountList = new List<Account>();
        integer numberToCreate 	  = 9;
        List<String>  nieList 	  = new List<String>{'X4454270K','Z8513771G','Y4558275B','Z5883692K',
            										 'X7038064H','Z2647514N','Z7554370W','Y5752550B',
            										 'Y2636531D'};
		Account accountItem;
        
        while( numberToCreate != 0 ){
            
            accountItem = new Account();//Nuevo Account
            
            accountItem.FirstName__c 	   		= 'FirstName__c HTEST-' + numberTocreate;
            accountItem.LastName__c 	   		= 'LastName__c HTEST-' + numberTocreate;
            accountItem.Name					= accountItem.FirstName__c + ' ' + accountItem.LastName__c ;
            accountItem.RecordTypeId 	   		= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente').getRecordTypeId();
            accountItem.Identifier_Type__c 		= 'NIE';
            accountItem.NIF_CIF_Customer_NIE__c = nieList[numberToCreate-1];
            accountItem.Contact_method_cd__c	= 'E-mail';
            accountItem.Email_con__c			= 'Email_con__c' + numberTocreate + '@HTEST.es';
            accountItem.No_Phone_flg__c			= true;
            accountItem.Account_language__c		= 'Español';
            accountItem.Nationality_cd__c		= 'ES';
            accountItem.Physical_or_legal_cd__c = 'Physical Customer';
            
            accountList.add( accountItem );// Añadir a la lista
            
            numberToCreate--;//restar al contador 9 -> 1
        }//Fin del while
        
        return accountList;
    }
    
    public static List<Lead> createLeadList(){
        /*Campos a considerar: LastName, Email, MobilePhone, Phone,  TV_Agent__c, TV_Provider_FV__c, TV_Location__c */
        
        List<Lead> leadList    = new List<Lead>();
        integer numberToCreate = 9;
        Lead leadItem;
        
        while( numberToCreate != 0 ){ 
            
            leadItem = new Lead();//Nuevo Lead
            
            /*leadItem.Account_name__c 	   		= 'FirstName__c HTEST-' + numberTocreate;
            leadItem.Account_Surname_Company__c = 'LastName__c HTEST-' + numberTocreate;
            
            leadItem.LastName  	  	   			= leadItem.Account_Surname_Company__c;
            leadItem.FirstName 					= leadItem.Account_name__c;*/
            
            //leadItem.Name = leadItem.FirstName  + ' ' +leadItem.LastName;
                
            leadItem.LastName  	  	   = 'Name HTEST-' + numberToCreate;
            leadItem.Company	  	   = leadItem.LastName;
            leadItem.RecordTypeId 	   = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Televenta').getRecordTypeId();
            leadItem.Email		  	   = 'Email_con__c' + numberTocreate + '@HTEST.es';
            leadItem.MobilePhone  	   = '678523388';
            leadItem.TV_Agent__c  	   = 'TV_Agent__c HTEST-' + numberTocreate;
            leadItem.TV_Provider_FV__c = 'Bankia';
            leadItem.TV_Location__c    = 'TV_Location__c HTEST-' + numberTocreate;
            
            leadList.add( leadItem );// Añadir a la lista
            
            numberToCreate--;//restar al contador 9 -> 1           
        }//Fin del while
        
        return leadList;
    }
    
    public static Campaign createCampaign(){
        /*Campos a considerar: */
        /*Campos obligatorios: FI_CAM_MSE_SellingChannels__c */

        Campaign campaignItem = new Campaign();
            

            campaignItem.AUDIENCESUBSET__C	=	false	;
            campaignItem.CRM_INSERTSYNCHRONIZED__C	=	false	;
            campaignItem.CRM_SYNCHRONIZED__C	=	false	;
            campaignItem.CAMPAIGNDESCRIPTION__C	=	'Prueba_CAM_TV2'	;
            campaignItem.CAMPAIGNSUBTYPE__C	=	'Catchment'	;
            campaignItem.COMMUNICATIONCODE2__C	=	'S002988'	;
            campaignItem.DEPARTMENT__C	=	'AR00008'	;
            //campaignItem.ENDDATE	=Date.valueOf('30/01/2020');
            campaignItem.ENDDATE	= date.today().addDays(5);
            campaignItem.EXCLUSIONS__C	=	'20'	;
            campaignItem.EXPECTEDRESPONSE	=	0.0	;
            campaignItem.EXPECTEDVOLUME__C	=	0.0	;
            campaignItem.FII_CAM_FLG_ACCOUNTCHANNEL__C	=	false	;
            campaignItem.FII_CAM_FLG_APPACCESS__C	=	false	;
            campaignItem.FII_CAM_FLG_CATCHANNEL__C	=	false	;
            campaignItem.FII_CAM_FLG_CCDDCHANNEL__C	=	false	;
            campaignItem.FII_CAM_FLG_CCDDDEPARTMENT__C	=	false	;
            campaignItem.FII_CAM_FLG_CCPPCHANNEL__C	=	false	;
            campaignItem.FII_CAM_FLG_DIGITALBILL__C	=	false	;
            campaignItem.FII_CAM_FLG_GDPRPRODSALES__C	=	false	;
            campaignItem.FII_CAM_FLG_GDPRNOTDISTURB__C	=	false	;
            campaignItem.FII_CAM_FLG_HASEMAILJOURNEY__C	=	false	;
            campaignItem.FII_CAM_FLG_HASSMSJOURNEY__C	=	false	;
            campaignItem.FII_CAM_FLG_MANUALLYDEACTIVATED__C	=	false	;
            campaignItem.FII_CAM_FLG_REGISTERED__C	=	false	;
            campaignItem.FII_CAM_FLG_REQUIRESALARM__C	=	false	;
            campaignItem.FII_CAM_FLG_SHAREDFROMCCDD__C	=	false	;
            campaignItem.FII_CAM_FLG_SHAREDWITHCAT__C	=	false	;
            campaignItem.FII_CAM_FLG_SHAREDWITHCCDD__C	=	false	;
            campaignItem.FII_CAM_FLG_SHAREDWITHCCPP__C	=	false	;
            campaignItem.FII_CAM_FLG_SHAREDWITHCHANNELS__C	=	false	;
            campaignItem.FII_CAM_FLG_SHAREDWITHMKT__C	=	false	;
            campaignItem.FII_CAM_FLG_INFOENERGYACCESS__C	=	false	;
            campaignItem.FI_CAM_FLG_APPROVED__C	=	false	;
            campaignItem.FI_CAM_FLG_EMAILSENTTOPWC__C	=	false	;
            campaignItem.FI_CAM_FLG_HASCONTROLGROUP__C	=	false	;
            campaignItem.FI_CAM_FLG_NEEDSSYNCHRONIZATION__C	=	false	;
            campaignItem.FI_CAM_FLG_SMSSENTTOPWC__C	=	false	;
            campaignItem.FI_CAM_FLG_SHAREDFROMSALES__C	=	false	;
            campaignItem.FI_CAM_FLG_SHAREDWITHAAFF__C	=	false	;
            campaignItem.FI_CAM_FLG_SHAREDWITHALIADOSYCOLECTIVOS__C	=	false	;
            campaignItem.FI_CAM_FLG_SHAREDWITHCATVENTASRYN__C	=	false	;
            campaignItem.FI_CAM_FLG_SHAREDWITHRETAIL__C	=	false	;
            campaignItem.FI_CAM_FLG_SHAREDWITHSALESCHANNELS__C	=	false	;
            campaignItem.FI_CAM_FLG_SHAREDWITHSTANDS__C	=	false	;
            campaignItem.FI_CAM_FLG_SHAREDWITHTASKFORCE__C	=	false	;
            campaignItem.FI_CAM_MSE_SELLINGCHANNELS__C	=	'CAT Ventas RyN'	;
            campaignItem.FI_CAM_NUM_PO__C	=	0.0	;
            campaignItem.INCLUDEROBINSON__C	=	false	;
            campaignItem.INCLUDEDEFAULTER__C	=	false	;
            campaignItem.ISACTIVE	=	true	;
            //campaignItem.LEAD__C	=		;
            campaignItem.NAME	=	'Prueba_CAM_TV2'	;
            campaignItem.NUMBERSENT	=	0.0	;
            //campaignItem.OFFER__C	=	'a2b1X0000004S5NQAU'	;
            B2C_Offer__c offerItem = createOffer();
            if(Schema.sObjectType.B2C_Offer__c.isCreateable()){
                insert offerItem;
            }
        	campaignItem.OFFER__C	= offerItem.Id;
            //campaignItem.PARENTID	=		;
            //campaignItem.RECORDTYPEID	=	'0121r000000q6suAAA'	;//revisar
            campaignItem.RECORDTYPEID	= Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Campaña').getRecordTypeId();
            //campaignItem.STARTDATE	=	Date.ValueOf('27/01/2020')	;
            campaignItem.STARTDATE	= date.today();
            campaignItem.STATUS	=	'Planned'	;
            campaignItem.SUBMITFORAPPROVAL__C	=	false	;
            campaignItem.TV_COLECTIVO__C	=	'AAFF'	;
            //campaignItem.TV_DATE_LAST_MARKER_SENT__C	=	Datetime.ValueOf('2020-01-27T11:00:00.000Z')	;
            campaignItem.TV_DATE_LAST_MARKER_SENT__C	=	DateTime.now()	;
            //campaignItem.TV_DESCRIPTION_RICH_TEXT__C	=		;
            campaignItem.TV_FREQUENCY__C	=	1.0	;
            campaignItem.TV_LEAD_ORIGIN__C	=	'Colectivos'	;
            campaignItem.TV_PARTNER__C	=	'Alcampo'	;
            campaignItem.TYPE	=	'Events'	;
            campaignItem.VOLUMECRITERIACHANNEL__C	=	0.0	;           
        
        return campaignItem;
    }
    
    public static B2C_Offer__c createOffer(){
        B2C_Offer__c offerItem = new B2C_Offer__c();
        
        offerItem.Department__c = 'AR00008';
        offerItem.Description__c = 'Prueba';
        offerItem.CRM_Synchronized__c= false;
        offerItem.Name = 'Prueba H';
        offerItem.Activationdate__c = Date.today();
        
        return offerItem;       
    }
    
    public static Campaign createComunication(){
        /*Campos a considerar: */
        /*Campos obligatorios: FI_CAM_MSE_SellingChannels__c */

        Campaign campaignItem = new Campaign();


        campaignItem.AUDIENCESUBSET__C	=	false	;
        campaignItem.CRM_INSERTSYNCHRONIZED__C	=	false	;
        campaignItem.CRM_SYNCHRONIZED__C	=	false	;
        campaignItem.CAMPAIGNCODE2__C	=	'C202987'	;
        campaignItem.CAMPAIGNSUBTYPE__C	=	'Catchment'	;
        campaignItem.COMMUNICATION_DESCRIPTION__C	=	'Aaaa'	;
        campaignItem.DEPARTMENT__C	=	'AR00008'	;
        //campaignItem.ENDDATE	=	28/01/2020	;
        campaignItem.ENDDATE	= date.today().addDays(4);
        campaignItem.EXCLUSIONS__C	=	'20'	;
        campaignItem.EXPECTEDRESPONSE	=	0.0	;
        campaignItem.EXPECTEDVOLUME__C	=	0.0	;
        campaignItem.FII_CAM_FLG_ACCOUNTCHANNEL__C	=	false	;
        campaignItem.FII_CAM_FLG_APPACCESS__C	=	false	;
        campaignItem.FII_CAM_FLG_CATCHANNEL__C	=	false	;
        campaignItem.FII_CAM_FLG_CCDDCHANNEL__C	=	false	;
        campaignItem.FII_CAM_FLG_CCDDDEPARTMENT__C	=	false	;
        campaignItem.FII_CAM_FLG_CCPPCHANNEL__C	=	false	;
        campaignItem.FII_CAM_FLG_DIGITALBILL__C	=	false	;
        campaignItem.FII_CAM_FLG_GDPRPRODSALES__C	=	false	;
        campaignItem.FII_CAM_FLG_GDPRNOTDISTURB__C	=	false	;
        campaignItem.FII_CAM_FLG_HASEMAILJOURNEY__C	=	false	;
        campaignItem.FII_CAM_FLG_HASSMSJOURNEY__C	=	false	;
        campaignItem.FII_CAM_FLG_MANUALLYDEACTIVATED__C	=	false	;
        campaignItem.FII_CAM_FLG_REGISTERED__C	=	false	;
        campaignItem.FII_CAM_FLG_REQUIRESALARM__C	=	false	;
        campaignItem.FII_CAM_FLG_SHAREDFROMCCDD__C	=	false	;
        campaignItem.FII_CAM_FLG_SHAREDWITHCAT__C	=	false	;
        campaignItem.FII_CAM_FLG_SHAREDWITHCCDD__C	=	false	;
        campaignItem.FII_CAM_FLG_SHAREDWITHCCPP__C	=	false	;
        campaignItem.FII_CAM_FLG_SHAREDWITHCHANNELS__C	=	false	;
        campaignItem.FII_CAM_FLG_SHAREDWITHMKT__C	=	false	;
        campaignItem.FII_CAM_FLG_INFOENERGYACCESS__C	=	false	;
        campaignItem.FII_CAM_TXT_PARENTCAMPAIGNDEPARTMENT__C	=	'AR00008'	;
        campaignItem.FI_CAM_FLG_APPROVED__C	=	false	;
        campaignItem.FI_CAM_FLG_EMAILSENTTOPWC__C	=	false	;
        campaignItem.FI_CAM_FLG_HASCONTROLGROUP__C	=	false	;
        campaignItem.FI_CAM_FLG_NEEDSSYNCHRONIZATION__C	=	true	;
        campaignItem.FI_CAM_FLG_SMSSENTTOPWC__C	=	false	;
        campaignItem.FI_CAM_FLG_SHAREDFROMSALES__C	=	false	;
        campaignItem.FI_CAM_FLG_SHAREDWITHAAFF__C	=	false	;
        campaignItem.FI_CAM_FLG_SHAREDWITHALIADOSYCOLECTIVOS__C	=	false	;
        campaignItem.FI_CAM_FLG_SHAREDWITHCATVENTASRYN__C	=	false	;
        campaignItem.FI_CAM_FLG_SHAREDWITHRETAIL__C	=	false	;
        campaignItem.FI_CAM_FLG_SHAREDWITHSALESCHANNELS__C	=	false	;
        campaignItem.FI_CAM_FLG_SHAREDWITHSTANDS__C	=	false	;
        campaignItem.FI_CAM_FLG_SHAREDWITHTASKFORCE__C	=	false	;
        campaignItem.INCLUDEROBINSON__C	=	false	;
        campaignItem.INCLUDEDEFAULTER__C	=	false	;
        campaignItem.ISACTIVE	=	true	;
        //campaignItem.LEAD__C	=		;
        campaignItem.NAME	=	'PRUEBA COM TV3'	;
        //campaignItem.OFFER__C	=		;

        //campaignItem.PARENTID	=	7011X000000IdJaQAK	;//revisar
        //campaignItem.RECORDTYPEID	=	0121r000000q6stAAA	;//revisar
        //campaignItem.STARTDATE	=	27/01/2020	;
        Campaign campaignParentItem = createCampaign();
        if(Schema.sObjectType.Campaign.isCreateable()){
            insert campaignParentItem;
        }
        campaignItem.PARENTID = campaignParentItem.id;
        
        campaignItem.STARTDATE	= date.today().addDays(1);
        campaignItem.RECORDTYPEID	= Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Comunicación').getRecordTypeId();

        campaignItem.STATUS	=	'Planned'	;
        campaignItem.SUBMITFORAPPROVAL__C	=	false	;
        campaignItem.TV_FREQUENCY__C	=	24.0	;
        campaignItem.TV_LEAD_ORIGIN__C	=	'No aplica'	;
        campaignItem.TV_PARTNER__C	=	'No aplica'	;
        campaignItem.TYPE	=	'Planned'	;
        campaignItem.VOLUMECRITERIACHANNEL__C	=	0.0	;        
        
        return campaignItem;
    }
    
    Public static List<B2C_Address__c> createAddressList(){
        List<B2C_Address__c> addressList = new List<B2C_Address__c>();
        integer numberToCreate 	  = 9;
        
		B2C_Address__c addressItem;
        while( numberToCreate != 0 ){
            addressItem = new B2C_Address__c();

            addressItem.Country_cd__c ='ES';
            addressItem.Province_cd__c = '28';
            addressItem.INE_state_cd__c = '28';
            addressItem.Province_text__c = 'MADRID';
            addressItem.County__c = 'MADRID';
            addressItem.County_cd__c = '079';
            addressItem.City__c = 'MADRID';
            addressItem.City_cd__c = 'BER38899';
            addressItem.INE_city_cd__c = '28079000101';
            addressItem.Zipcode__c = '28038';
            addressItem.Type_street__c = 'AV';
            addressItem.Type_address_cd__c = 'Normalized';
            addressItem.Street__c = 'FRANCISCO IGLESIAS';
            addressItem.Name = 'FRANCISCO IGLESIAS 2'+numberToCreate+', MADRID';
            addressItem.Formatedd_Address__c = 'FRANCISCO IGLESIAS 2'+numberToCreate;
            addressItem.Street_number__c = '2'+numberToCreate;
            addressItem.Stairs__c = '1';
            addressItem.Floor_cd__c = '8';
            addressItem.Door__c = 'IZQ';
            
            
            addressList.add( addressItem );// Añadir a la lista
            
            numberToCreate--;//restar al contador 9 -> 1           
        }//Fin del while      
        return addressList;
    }
    
    public static B2C_CNAE__c  createCNAEList(){
        B2C_CNAE__c cnaeItem = new B2C_CNAE__c();
        cnaeItem.Description_CNAE__c ='description CNAE';
        cnaeItem.Name = '9820';            
        return cnaeItem;
    }
    public static void updateClientAddress( List<Account> accountList, List<B2C_Address__c> addressList ){
        integer numberToCreate 	  = 9;
        
        while( numberToCreate != 0 ){
            
            accountList[numberToCreate-1].Address__c = addressList[numberToCreate-1].id;
            addressList[numberToCreate-1].Account__c = accountList[numberToCreate-1].id;
            
            numberToCreate--;//restar al contador 9 -> 1  
        }
        if(Schema.sObjectType.Account.isUpdateable() && Schema.sObjectType.B2C_Address__c.isUpdateable()){
            update accountList;
            update addressList;
        }
            
    }
    
    public static List<B2C_Service_Point__c> createServicePointList(){
        List<B2C_Service_Point__c> servicePointList = new List<B2C_Service_Point__c>();
        integer numberToCreate 	  = 9;
        List<String>  cupsList 	  = new List<String>{ 'ES0031405219919006NZ0F','ES0021000004882566CP0F','ES0021000012063992PY0F',
            										  'ES0021000003989329FE0F','ES0021000011696367DJ0F','ES0021000016131798ED0F',
            										  'ES0021000004237438PF0F','ES0021000011933307FF0F','ES0022000005003334DQ1P'};
		
        List<Account> accountList = TV_CLS_Test_Factory.createClientList();
        if(Schema.sObjectType.Account.isCreateable()){
            insert accountList;
        }
        
        List<B2C_Address__c> addressList = TV_CLS_Test_Factory.createAddressList();
        if(Schema.sObjectType.B2C_Address__c.isCreateable()){
            insert addressList;
        }
        updateClientAddress(accountList,addressList);

        B2C_CNAE__c cnaeItem = TV_CLS_Test_Factory.createCNAEList();
        if(Schema.sObjectType.B2C_CNAE__c.isCreateable()){
            insert cnaeItem;
        }
        B2C_Service_Point__c servicePointItem;
        while( numberToCreate != 0 ){
            servicePointItem = new B2C_Service_Point__c();
            
            servicePointItem.Name = cupsList[numberToCreate-1];
            servicePointItem.CUPS_Consulted_SVE__c = cupsList[numberToCreate-1];
            servicePointItem.Business_line_cd__c = '01';
            
            servicePointItem.Power_Ascribed_Access__c =4.6;
            servicePointItem.Power_Ascribed_Ext__c =4.6;
            servicePointItem.Power_Max_CIE__c =	20.785;
            servicePointItem.Power_period_1_Kw__c =4.6;
            servicePointItem.Power_Tranf_kVA__c =1;
            
            servicePointItem.AddrId_Id__c = addressList[numberToCreate-1].id;
            
            servicePointItem.Designer_Power__c = 4.6;
            
            servicePointItem.DistribuitorElec_cd__c = '0024';
            
            servicePointItem.Account_id__c = accountList[numberToCreate-1].id;
            
            servicePointItem.Reason_creation__c = 'Alta 1º ocupación sin cups';
            servicePointItem.Habitual_Residence__c = true;
            
            servicePointItem.Tension_cd__c='02';
            
            servicePointItem.Tariff_cd__c = '004';
            
            servicePointItem.CNAE__c = cnaeItem.id;
                
            servicePointList.add( servicePointItem );// Añadir a la lista
            
            numberToCreate--;//restar al contador 9 -> 1           
        }//Fin del while       
        return servicePointList;
    }
    
    
    public static List<CampaignMember> createCampaignMemberList(){
        
        List<B2C_Service_Point__c> servicePointList   = TV_CLS_Test_Factory.createServicePointList();
        List<CampaignMember> 	   campaignMemberList = new List<CampaignMember>();
        List<Account> 			   accountList    	  = TV_CLS_Test_Factory.createClientList();
        List<Lead> 				   leadList 		  = TV_CLS_Test_Factory.createLeadList();
        Campaign 				   campaignItem 	  = TV_CLS_Test_Factory.createComunication();
        integer 				   numberToCreate 	  = 9;
        if(Schema.sObjectType.B2C_Service_Point__c.isCreateable() &&
           Schema.sObjectType.Campaign.isCreateable() &&
           Schema.sObjectType.Lead.isCreateable()){
            insert servicePointList;
            insert campaignItem;
            //insert accountList;
            insert leadList;
        }
        
		CampaignMember campaignMemberItem;
        while( numberToCreate != 0 ){
            campaignMemberItem = new CampaignMember();
            
            campaignMemberItem.CampaignId                      = campaignItem.id;
            campaignMemberItem.FII_CAMM_FLG_EmailPending__c    = false;
            //campaignMemberItem.FII_CAMM_LKP_AccountMember__c   = accountList[numberToCreate-1].Id;
            campaignMemberItem.LeadId 						   = leadList[numberToCreate-1].Id;
            campaignMemberItem.PS1__c 						   = servicePointList[numberToCreate-1].Name;
       		campaignMemberItem.FII_CAMM_LKP_AccountMember__c   = servicePointList[numberToCreate-1].Account_id__c;
                
            campaignMemberList.add( campaignMemberItem );// Añadir a la lista
            
            numberToCreate--;//restar al contador 9 -> 1           
        }//Fin del while    
        return campaignMemberList;        
    }
    
    public static Task  createInteration( CampaignMember campaignMemberItem){
        Task taskItem = new Task();
        
        //taskItem.FII_ACT_LKP_RelatedClient__c = campaignMemberItem.FII_CAMM_LKP_AccountMember__c;
        taskItem.FII_ACT_LKP_Campaign__c = campaignMemberItem.CampaignId;
        taskItem.WhoId = campaignMemberItem.LeadId;
        taskItem.TV_ACT_LKP_LeadID__c = campaignMemberItem.LeadId;
        taskItem.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Task Interaction').getRecordTypeId();
        taskItem.Subject = 'Call';
        taskItem.FII_ACT_PL_ShippingType__c= 'Email';
        taskItem.FII_ACT_SEL_Channel__c = 'Phone';
        taskItem.FII_ACT_SEL_Interaction_Category__c ='Interaction';
        taskItem.FII_ACT_SEL_Interaction_Type__c ='Input';
        taskItem.Type = 'Type';
        taskItem.Priority = 'Normal';
        taskItem.Status = 'Open';
    	taskItem.CallType = 'Outbound';
        taskItem.TaskSubtype = 'Task';
        taskItem.FII_ACT_TXT_CampaignMemberId__c = campaignMemberItem.Id;
        taskItem.CallObject = '300169145920200316';
        taskItem.FII_ACT_TXT_Type__c = 'Call';
        taskItem.TV_Campana_Marcador__c = 'Test';
        taskItem.TV_Telefono_Marcado__c = '675512277';
        taskItem.TV_ACT_TXT_AgrupadorCampana__c = 'TEST H';
        
        return taskItem;
    }
        
}