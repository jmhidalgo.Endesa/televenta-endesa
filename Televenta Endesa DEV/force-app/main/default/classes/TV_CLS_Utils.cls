/**
 * @File Name          : TV_CLS_Utils.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 24/3/2020 11:20:20
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/3/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class TV_CLS_Utils {
    @AuraEnabled
    public static Boolean getIsTeleventaProfile(){
        Boolean  result = false; 
		String profileId = UserInfo.getProfileId();
		Profile perfil = [SELECT Id, Name FROM Profile WHERE Id =: profileId];
		List<String> televentaProfiles = returnSetValues(Label.TV_Perfiles_Televenta,',');
		if(televentaProfiles.contains(perfil.Name)){
			result = true;
		}   
		system.debug('Perfil result: '+perfil.Name);
		return result;
    }

    private static List<String> returnSetValues(String textToSplit, String character){
        List<String> listValues = new List<String>();
        Set<String> setValues = new Set<String>();
        if(!String.isBlank(textToSplit) && !String.isBlank(character)){
            setValues= new Set<String>(textToSplit.split(character));
        }
        for(String value :setValues){
            listValues.add(value);
        }
        return listValues;
    }

    public static String  obtenerTipoInterlocutor (String recordId){
        String tipoInterlocutor = '';
        List<Asset> contratos = [SELECT Id, NE__Status__c,NE__EndDate__c FROM Asset WHERE AccountId =: recordId AND FI_ASS_SEL_HolderCompany__c = '20' ORDER BY NE__EndDate__c DESC];

        if(contratos.size() > 0){
            for(Asset contrato : contratos){
                if( contrato.NE__Status__c == 'Active'){
                    // Hay contratos en vigor
                    tipoInterlocutor = 'Cliente';
                }
            }
            if(tipoInterlocutor != 'Cliente'){
                Boolean todosLosContratosInactivos = true;
                for(Asset contrato : contratos){
                    if( contrato.NE__Status__c != 'Inactive'){
                        todosLosContratosInactivos = false;
                    }
                }
                if(todosLosContratosInactivos){
                    // Cogemos el ultimo contrato
                    if(contratos[0].NE__EndDate__c != null){
                        // Si se ha dado de baja desde hace más de 17 meses de la fecha actual - Ex cliente Histórico
                        //Update 13 meses
                        Date fechaBaja = date.newinstance(contratos[0].NE__EndDate__c.year(), contratos[0].NE__EndDate__c.month(), contratos[0].NE__EndDate__c.day());
                        if( fechaBaja.monthsBetween(system.today()) > 13 ){
                            tipoInterlocutor = 'Excliente Histórico';
                        }else{
                            tipoInterlocutor = 'Excliente';
                        }
                    }
                }
            }
            
        }else{
            // Nunca ha tenido contratos
            tipoInterlocutor = 'No cliente';
        }

        return tipoInterlocutor;
    }

    public static string estadoRGPDMotivoLicito (String contactId){
        String estadoRGPD = '';
        List<Individual> ind = [SELECT Id, FII_IND_SEL_Molestar__c FROM Individual WHERE contact__c =: contactId ORDER BY lastModifiedDate DESC];

        if(ind.size() > 0){
            if(ind[0].FII_IND_SEL_Molestar__c != null){
                estadoRGPD = (ind[0].FII_IND_SEL_Molestar__c == 'Sí Ofertas' ? 'SI' : 'NO');
            }else{
                estadoRGPD = 'NO';
            }

        }else{
            estadoRGPD = 'NO';
        }
        return estadoRGPD;
    }
}