public with sharing class TV_LCC_ClientSection {
    
    @AuraEnabled
    public static DataWrapper getData(Id recordId){

        DataWrapper dataWrapper = new DataWrapper();

        if(Schema.sObjectType.Account.isAccessible() && 
           Schema.sObjectType.Asset.isAccessible() &&
           Schema.sObjectType.Individual.isAccessible()){

            Account account = [SELECT 	FirstName__c,
                                        LastName__c,
                                        NIF_CIF_Customer_NIE__c,
                                        TOLABEL(Physical_or_legal_cd__c),
                                        Addr_id__c,
                                        TOLABEL(Addr_id__r.Province_cd__c),
                                        MobilePhone1__c,
                                        MobilePhone2__c,
                                        Phone,
                                        Email_con__c,
                                        FII_ACC_FOR_Vulnerable_Val__c,
                                        FII_ACC_FOR_Robinson__c,
                                        Contratos_en_comercializadora__c,
                                        RGPD_Offerings__c,DonotdisturbRGPD__c,
                                        IInternal_payment_quality_index_comerc__c,
                                        FII_ACC_FOR_MoreInfo__c,
                                        TitularContactHide__c
                            FROM Account
                            WHERE Id = :recordId]; 
            
            // Individual (RGPD Information)
            Individual ind;
            if(account.TitularContactHide__c != null){
                List<Individual> individualList = [SELECT Id,FII_IND_SEL_Molestar__c FROM Individual WHERE Contact__c =:  account.TitularContactHide__c Order by LastModifiedDate desc LIMIT 1];
                if(individualList.size() > 0){
                    ind = individualList[0];
                }
            }
            
            
            dataWrapper.account 	= account;
            if(account.IInternal_payment_quality_index_comerc__c >= 0 && account.IInternal_payment_quality_index_comerc__c < 2.01){
                dataWrapper.parsedIndex = 'Buena';
            }else if(account.IInternal_payment_quality_index_comerc__c >= 2.01 && account.IInternal_payment_quality_index_comerc__c < 5.01){
                dataWrapper.parsedIndex = 'Regular';   
            }else if(account.IInternal_payment_quality_index_comerc__c >= 5.01 && account.IInternal_payment_quality_index_comerc__c < 7.51){
                dataWrapper.parsedIndex = 'Mala'; 
            }else if(account.IInternal_payment_quality_index_comerc__c >= 7.51 && account.IInternal_payment_quality_index_comerc__c < 1000){
                dataWrapper.parsedIndex = 'Muy Mala'; 
            }else{
                dataWrapper.parsedIndex	= '';
            }

            List<Asset> listAssetsEnComercializadora = [SELECT Id 
                                                        FROM Asset
                                                        WHERE AccountId = :recordId
                                                            AND Status = 'Active'
                                                            AND FI_ASS_SEL_HolderCompany__c = '20'];
            
            if(!listAssetsEnComercializadora.isEmpty()){
                dataWrapper.contratosComercializadora = 'Si';
            }else{
                dataWrapper.contratosComercializadora = 'No';
            }
            
            // By default
            dataWrapper.RGPD = 'No Informado';
            if(ind != null){
                if(String.IsNotBlank(ind.FII_IND_SEL_Molestar__c)){
                    if(ind.FII_IND_SEL_Molestar__c == 'Sí Ofertas'){
                        dataWrapper.RGPD = 'Sí';
                    }else if(ind.FII_IND_SEL_Molestar__c == 'No Ofertas'){
                        dataWrapper.RGPD = 'No';
                    }
                }
            }
        }


        return dataWrapper;
    }
    
    public class DataWrapper {
        @AuraEnabled
        public Account account;
        @AuraEnabled
        public String parsedIndex;
        @AuraEnabled
        public String contratosComercializadora;
        @AuraEnabled
        public String RGPD;
    }
    
}