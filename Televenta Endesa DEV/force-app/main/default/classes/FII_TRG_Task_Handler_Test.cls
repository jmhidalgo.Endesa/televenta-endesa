/**
    * @author Everis
    * @since November 20, 2018 
    * @desc Test of FII_TRG_Task_Handler
    * @history November 20, 2018 - everis (LJG) - Create Test Apex Class 
    * @history January 16, 2019 - everis (LJG) - Modify Test Apex Class (methods added)
    * @history February 26, 2019 - everis (JVC) - Refact Apex Class 
*/
@isTest

public with sharing class FII_TRG_Task_Handler_Test {
    
    private static final String CONTACT_LASTNAME = 'Test2';
	private static final String ACCOUNT_NIF = '85258421C';

    @testSetup static void setup(){
        Test.setMock(HttpCalloutMock.class, new UtilHttpMock.BasicMock());
        Contact con = UtilTest.createContact();
		AccountTriggerHandler.bypassTrigger = true;
		insert con;
		AccountTriggerHandler.bypassTrigger = false;
        
        Account quendaAccount = new Account();
        quendaAccount.FirstName__c = 'Contacto';
        quendaAccount.LastName__c = 'Pendiente de Informar';
        quendaAccount.Name = 'Contacto Pendiente de Informar';
        quendaAccount.Nationality_cd__c = 'AD';
        quendaAccount.NE__Status__c = 'Prospect';
        quendaAccount.NIF_CIF_Customer_NIE__c = '10144382W';
        quendaAccount.Physical_or_legal_cd__c = 'Physical Customer';
        quendaAccount.Identifier_Type__c = 'NIF';
        quendaAccount.Contact_method_cd__c = 'E-mail';
        quendaAccount.Email_con__c = 'cuentasinoperativa@email.com';
        quendaAccount.No_Email_flg__c = False;
        quendaAccount.No_Phone_flg__c = True;
        AccountTriggerHandler.bypassTrigger = true;
		insert quendaAccount;
		AccountTriggerHandler.bypassTrigger = false;
              
        Contact quendaContact = new Contact();
        quendaContact.AccountId = quendaAccount.Id;
        quendaContact.FirstName = 'Contacto';
        quendaContact.LastName = 'Pendiente de Informar';
        quendaContact.Contact_method_cd__c = 'E-mail';
        quendaContact.Email = 'Contactopendientedeinformar@email.com';
        quendaContact.No_Email_flg__c = false;
        quendaContact.No_Phone_flg__c = true;
        quendaContact.Document_Type__c = 'NIF';
        quendaContact.NIF_CIF_Customer_NIE__c = '10144382W';
        AccountTriggerHandler.bypassTrigger = true;
        insert quendaContact;
        AccountTriggerHandler.bypassTrigger = false;
		

    }

    @isTest static void testfunctionality() {

        Contact con = [SELECT Id, Name, AccountId FROM Contact WHERE LastName = :CONTACT_LASTNAME LIMIT 1];

        // Interaction related with client and contact
		Task interaction1 = FII_Util_GSM.createInteraction(con.AccountId, con.Id, null, 'Phone', 'interaction1');
        insert interaction1;

        Case need = FII_Util_GSM.createNeed(con.AccountId, null, 'Need', null);
        insert need;

        Task actionTask = FII_Util_GSM.createTaskAction(need, 'Modificación de datos', 'Open', Label.FII_RT_TSK_ACTION);

        Test.startTest();
        insert actionTask;
        Task act = [SELECT Id, Status FROM Task WHERE Subject = 'Modificación de datos' LIMIT 1];
        act.Status = 'Completed';
        update act;
        final Task t = [SELECT Id, WhatId FROM Task WHERE Subject = 'Interacción: interaction1' LIMIT 1];
        System.assertEquals(need.Id, t.WhatId);
        Test.stopTest();
    }

    @isTest static void bypass(){
        Test.startTest();
        FII_TRG_Task_Handler.bypassTrigger = true;
        new FII_TRG_Task_Handler().beforeInsert(new List<SObject>());
        new FII_TRG_Task_Handler().beforeUpdate(new Map<Id, SObject>(), new Map<Id, SObject>());
        new FII_TRG_Task_Handler().beforeDelete(new List<SObject>(), new Map<Id,SObject>());
        new FII_TRG_Task_Handler().afterInsert(new List<sObject>(), new Map<Id, SObject>());
        new FII_TRG_Task_Handler().afterUpdate(new Map<Id, SObject>(), new Map<Id, SObject>());
        new FII_TRG_Task_Handler().afterDelete(new Map<Id,SObject>());
        new FII_TRG_Task_Handler().afterUndelete(new List<SObject>(), new Map<Id,SObject>());
        System.assertEquals(true, FII_TRG_Task_Handler.bypassTrigger);
        FII_TRG_Task_Handler.bypassTrigger = false;
        Test.stopTest();
    }
    
    @isTest static void checkQuendaInsert(){
        Id InterId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Task Interaction').getRecordTypeId();
        String updateIndicador = '14';
        Account acc = [SELECT Id, NIF_CIF_Customer_NIE__c FROM Account WHERE NIF_CIF_Customer_NIE__c = :ACCOUNT_NIF LIMIT 1];
        Interaction_LN__mdt tipoInter = [SELECT id, type__c, subType__c, indicador__c FROM Interaction_LN__mdt WHERE indicador__c =: updateIndicador];
        User u = new User(Id = UserInfo.getUserId());
        u.FederationIdentifier = 'ES123456789M';
        update u;
        Task taskItem = new Task();
        taskItem.RecordTypeId = interId;
        taskItem.FII_ACT_TXT_Document__c = acc.NIF_CIF_Customer_NIE__c;
        taskItem.FII_ACT_TXT_FederationId__c = 'ES123456789M';
        taskItem.FII_ACT_SEL_Channel__c = 'CCPP';
        taskItem.FII_ACT_TXT_NLInfo__c = '29';
        Test.startTest();
        insert taskItem;
        taskItem.FII_ACT_TXT_NLInfo__c = updateIndicador;
        update taskItem;
        Task t = [SELECT Id, FII_ACT_TXT_QuendaResult__c, WhoId, OwnerId, FII_ACT_TXT_Document__c, tipo__c FROM Task WHERE FII_ACT_TXT_Document__c =: taskItem.FII_ACT_TXT_Document__c LIMIT 1][0];
        system.assertEquals(t.FII_ACT_TXT_QuendaResult__c, 'La interacción se ha generado correctamente.');
        system.assertEquals(t.tipo__c, tipoInter.type__c);
        Test.stopTest();
    }
    
    @isTest static void createInteration_Test() {
        Test.startTest();
        ServicePointTriggerHandler.bypassTrigger = true;
        List<CampaignMember> campaignMemberList = TV_CLS_Test_Factory.createCampaignMemberList();
        insert campaignMemberList;
        Task taskItem = TV_CLS_Test_Factory.createInteration( campaignMemberList[0] );
        insert taskItem;
        taskItem.Subject = taskItem.Subject +' Update';
        update taskItem;
		Test.stopTest();
        ServicePointTriggerHandler.bypassTrigger = false;
          
        System.assert( taskItem != null );
    }
    
}