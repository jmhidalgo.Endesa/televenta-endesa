({
    doInit: function(component, event, helper){
        
        component.set("v.step", 1);
        component.set("v.loading", true);
        component.set("v.vPagination",1);
        
        helper.doInit(component, event);
        
        helper.completeObjects(component, event, helper);

        var picklist = component.get("v.ObjectType.Contact.Language__c.picklistOptions");
        var resultPicklist = {};
        for(var op in picklist){
            if(op != 'ES' && op != 'CA' && op!='EN'){
                resultPicklist.push(op);
            }
        }
        component.set("v.languagePicklistOptions", resultPicklist);
    },
    
    update: function(component, event, helper){
        var account = component.get("v.Account");

        if(account.Physical_or_legal_cd__c == 'Corporate Customer' && (account.Identifier_Type__c == 'P' || account.Identifier_Type__c == 'NIE')){
            helper.displayToast(component, 'Una Persona Jurídica no puede tener tipo de documente "NIE" ó "Pasaporte"' , 'Warning');
        }else{
            var contacto = component.get("v.Contact");
            if (contacto.No_Email_flg__c) {
                contacto.Email = '';
                contacto.Work_email__c = '';
                contacto.New_email__c = '';
                account.Email_con__c = '';
            }
            if (contacto.No_Phone_flg__c) {
                contacto.Phone = '';
                contacto.MobilePhone = '';
                contacto.OtherPhone = '';
                contacto.OtherPhone__c = '';
            }
            account.Account_language__c = contacto.Language__c;
            component.set("v.Account", account);
            component.set("v.Contact", contacto);
            
            helper.update(component, event);
            component.set("v.isOpenModal", false);
            component.set("v.loading", true);
        }
    },
    
    pathCliente: function(component) {
        component.set("v.step", 1);
        component.set("v.errorValidation",false);
        var changesAccount = component.get("v.accountChanges");
        if (changesAccount) {
            component.set("v.saveButton",false);
        }
        else {
            component.set("v.saveButton",true);
        }
    },
    
    pathContacto: function(component) {
        component.set("v.step", 2);
        component.set("v.errorValidation",false);
        var changesContact = component.get("v.contactChanges");
        if (changesContact) {
            component.set("v.saveButton",false);
        }
        else {
            component.set("v.saveButton",true);
        }
    },
    
    pathDatosInteres: function(component) {
        component.set("v.step", 3);
        component.set("v.errorValidation",false);
        var changesInterest = component.get("v.interestChanges");
        if (changesInterest) {
            component.set("v.saveButton",false);
        }
        else {
            component.set("v.saveButton",true);
        }
    },
    
    pathRGPD: function(component) {
        component.set("v.step", 4);
        component.set("v.errorValidation",false);
        var changesRGPD = component.get("v.RGPDChanges");
        if (changesRGPD) {
            component.set("v.saveButton",false);
        }
        else {
            component.set("v.saveButton",true);
        }
    },
    
    disabledIdentifier: function(component) {
        var adminProfile = component.get("v.isAdmin");
        if (adminProfile) {
            component.set("v.identifierAndType", false);
        }
        else {
            component.set("v.identifierAndType", true);
        }
        
        component.set("v.saveButton",false);
        component.set("v.accountChanges",true);
    },
    
    disabledName: function(component) {
        var adminProfile = component.get("v.isAdmin");
        if (adminProfile) {
            component.set("v.nameAndLastName", false);
        }
        else {
            component.set("v.nameAndLastName", true);
        }
        component.set("v.saveButton",false);
        component.set("v.accountChanges",true);
    },
    
    botonGuardar: function(component) {
        component.set("v.saveButton",false);
        var startStep = component.get("v.step");

        if(component.get("v.Contact.No_Email_flg__c") == true){
            component.set("v.Contact.Email", null);
            component.set("v.Contact.Work_email__c", null);
            component.set("v.Contact.Validate_email__c", false);
        }

        if(component.get("v.Contact.No_Phone_flg__c") == true){
            component.set("v.Contact.Phone", null);
            component.set("v.Contact.MobilePhone", null);
            component.set("v.Contact.OtherPhone", null);
            component.set("v.Contact.OtherPhone__c", null);
        }

        if (startStep == 1) {
            component.set("v.accountChanges",true);
        }
        else if (startStep == 2) {
            component.set("v.contactChanges",true);
        }
        else if (startStep == 3) {
            component.set("v.interestChanges",true);
        }
        else if (startStep == 4) {
            component.set("v.RGPDChanges",true);
        }
    },
    
    personType: function(component) {
        var accountTypePerson = component.get("v.Account.Physical_or_legal_cd__c");
        if (accountTypePerson == 'Physical Customer') {
            component.set("v.Account.FirstName__c",component.get("v.nameAccount"));
            component.set("v.Account.LastName__c",component.get("v.lastNameAccount"));
            component.set("v.Account.Identifier_Type__c",component.get("v.typeIdentifierAccount"));
            component.set("v.Account.NIF_CIF_Customer_NIE__c",component.get("v.identifierAccount"));
            component.set("v.Account.Duplicate_NIF_flg__c",component.get("v.duplicatedNifAccount"));
            component.set("v.nameRequired", true);
            component.set("v.personCorporate", false);
            component.set("v.identifierAndType", false);
            component.set("v.nameRequired", false);
            component.set("v.nameAndLastName", false);
        }
        else if (accountTypePerson == 'Corporate Customer') {
            
            component.set("v.Account.LastName__c",component.get("v.lastNameAccount"));
            component.set("v.Account.Identifier_Type__c",component.get("v.typeIdentifierAccount"));
            component.set("v.Account.NIF_CIF_Customer_NIE__c",component.get("v.identifierAccount"));
            component.set("v.Account.Duplicate_NIF_flg__c",component.get("v.duplicatedNifAccount"));
            component.set("v.personCorporate", true);
            component.set("v.Account.FirstName__c",'');
            

            component.set("v.identifierAndType", false);
            component.set("v.nameAndLastName", false);
            component.set("v.nameRequired", false);
        }
        component.set("v.saveButton",false);
        component.set("v.accountChanges",true);
    },
    
    abrirModal : function(component) {
        component.set("v.isOpenModal", true);
    },
    cerrarModal : function(component) {
        component.set("v.isOpenModal", false);
    },
    
    nextStep : function(component, event, helper) {
        console.log('---- Botón siguiente pulsado ----');
        component.set("v.errorValidation",false);
        
        var changesContact = component.get("v.contactChanges");
        var changesInterest = component.get("v.interestChanges");
        var changesRGPD = component.get("v.RGPDChanges");
        
        var startStep = component.get("v.step");
        if (startStep == 1) {
            component.set("v.step", 2);
            if (changesContact) {
                component.set("v.saveButton",false);
            }
            else {
                component.set("v.saveButton",true);
            }
        }
        else if (startStep == 2) {
            component.set("v.step", 3);
            if (changesInterest) {
                component.set("v.saveButton",false);
            }
            else {
                component.set("v.saveButton",true);
            }
        }
            else if (startStep == 3) {
                component.set("v.step", 4);
                if (changesRGPD) {
                    component.set("v.saveButton",false);
                }
                else {
                    component.set("v.saveButton",true);
                }
            }
        
        
        
    },
    
    prevStep : function(component, event, helper) {
        console.log('---- Botón anterior pulsado ----');
        component.set("v.errorValidation",false);
        var changesAccount = component.get("v.accountChanges");
        var changesContact = component.get("v.contactChanges");
        var changesInterest = component.get("v.interestChanges");
        var startStep = component.get("v.step");
        
        if (startStep == 2) {
            component.set("v.step", 1);
            if (changesAccount) {
                component.set("v.saveButton",false);
            }
            else {
                component.set("v.saveButton",true);
            }
        }
        else if (startStep == 3) {
            component.set("v.step", 2);
            if (changesContact) {
                component.set("v.saveButton",false);
            }
            else {
                component.set("v.saveButton",true);
            }
        }
            else if (startStep == 4) {
                component.set("v.step", 3);
                if (changesInterest) {
                    component.set("v.saveButton",false);
                }
                else {
                    component.set("v.saveButton",true);
                }
            }
        
    },
    
    closeAccountEdit : function(component, event, helper){
        component.find("overlayLib").notifyClose();
    },    
})