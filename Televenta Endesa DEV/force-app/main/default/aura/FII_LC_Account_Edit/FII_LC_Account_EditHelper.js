({
    doInit : function(component, event) {

        var accountId = component.get("v.recordId");

        if (accountId.substr(0,3)=='500') {
            component.set("v.necesidad",accountId);
        }

        var action = component.get("c.checkAdminProfile");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
               // set current user information on userInfo attribute
                component.set("v.isAdmin", storeResponse);
            }
        });
        $A.enqueueAction(action);


        var action = component.get("c.getInformation");
        action.setParams({
            idAccount : accountId
        });
        
        action.setCallback(this, function(response){            
            component.set("v.loading", false);
            var status = response.getState();
            if (status === "SUCCESS" & component.isValid()){
                var result = response.getReturnValue();   
                component.set("v.Account", result[0]);
                if (result[0].Physical_or_legal_cd__c == 'Physical Customer') {
                    component.set("v.nameRequired", true);
                }
                else {
                    component.set("v.personCorporate", true);
                }
                component.set("v.nameAccount",result[0].FirstName__c);
                component.set("v.lastNameAccount",result[0].LastName__c);
                component.set("v.typeIdentifierAccount",result[0].Identifier_Type__c);
                component.set("v.identifierAccount",result[0].NIF_CIF_Customer_NIE__c);
                component.set("v.duplicatedNifAccount",result[0].Duplicate_NIF_flg__c);
                component.set("v.Contact", result[1]);
                component.set("v.Individual", result[2]);

                this.getIsTeleventaProfile(component);
            }
        });
        $A.enqueueAction(action); 
    },

    completeObjects : function(component, event, helper) {

        var actionAccount = component.get("c.getObjectsType");
        actionAccount.setCallback( helper, function( response ) {
        
            if ( component.isValid() && response.getState() === 'SUCCESS' ) {
                // Parse the JSON string into an object
                //component.set( 'v.ObjectType', JSON.parse( response.getReturnValue() ) );
                //console.table( JSON.parse( response.getReturnValue() ) ); 


                // Parse the JSON string into an object
                var obj = JSON.parse( response.getReturnValue() );
                for(var i = 0; i <= 13 ; i++) obj.Contact.Profession_cd__c.picklistOptions.pop();  
                component.set( 'v.ObjectType', obj );
            } else {
                console.error( 'Error calling action "' + actionName + '" with state: ' + response.getState() );
            }
            
        });
        $A.enqueueAction( actionAccount );
    },

    update : function(component, event) {
        var step = component.get("v.step");
        var Account = component.get("v.Account");
        var Contact = component.get("v.Contact");
        var Individual = component.get("v.Individual");
        var accountTypePerson = component.get("v.Account.Physical_or_legal_cd__c");

        var updateClientAndRelated = component.get("c.updateClientAndRelated");
        updateClientAndRelated.setParams({
            accountItem : Account,
            contactItem : Contact,
            individualItem : Individual
        });
        updateClientAndRelated.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.displayToast", true);
                component.set("v.saveButton",true);
                this.displayToast(component, response.getReturnValue().dmlMessage, response.getReturnValue().type);
                $A.get('e.force:refreshView').fire();
                //location.reload();
                // DO SOMETHING WHEN RECORDS ARE SAVED (Lanzar evento refresh, display toast, standarbehaviourGSM)
                if(response.getReturnValue().type === "success"){
                    //component.set("v.loading", false);
                    this.standardBehaviourGSM(component);
                    if (accountTypePerson == 'Physical Customer') {
                        this.modifyContactHideMethod(component);
                    }
                } else {
                    component.set("v.loading", false);
                }
            } else if (state === "ERROR"){
                console.log('---- ERROR ----');
                this.displayToast(component, 'Ha surgido un problema desconocido', 'error');
            }
        });
        $A.enqueueAction(updateClientAndRelated);
    },

    standardBehaviourGSM : function(component){
        var needId;
        var toastMsg;
        if (component.get("v.needSelected") != null) {
            needId = component.get("v.needSelected");
            toastMsg = $A.get("$Label.c.FII_TOAST_ACT");
        }
        else {
            needId = component.get("v.recordId");
            toastMsg = $A.get("$Label.c.FII_TOAST_NEED_ACT");
        }
        var sObjectType = null;
        var standardBehaviorGSM = component.get("c.standardBehaviorGSM");
        standardBehaviorGSM.setParams({
            recId : needId,
            type : 'Atencion al cliente',
            subtype : null,
            subjectNeed : "Modificación de Cliente",
            subjectAct : "Modificación de Cliente",
            camId : null,
            status : 'Completed',
            recordtype : $A.get("{!$Label.c.FII_RT_TSK_ACTION}")
        });
        standardBehaviorGSM.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.loading", false);
            if (state === "SUCCESS") {	
                var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
                needCreateEvent.setParams({"needCreated":response.getReturnValue().Id,"recId":component.get("v.recordId")});
                needCreateEvent.fire();
                this.launchRefreshEvent(component);
                this.displayToast(component, toastMsg, 'success'); 

                component.find("overlayLib").notifyClose();
                component.set("v.loading", false);
            } else {
                this.displayToast(component, sObjectType, "No se ha podido crear una necesidad y una actuación", "error");
            }
        });
        $A.enqueueAction(standardBehaviorGSM);
    },

    modifyContactHideMethod : function (component) {
        var sObjectType = null;
        var action = component.get("c.modifyContactHide");
        action.setParams({
            accountItem : component.get("v.Account"),
            contactItem : component.get("v.Contact"),
            individualItem : component.get("v.Individual")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "ERROR"){
                console.log('----ERROR----');
            }
        });
        $A.enqueueAction(action);
    },

    // CAMBIOS PARA GSM
	launchRefreshEvent : function(component) {
		var FII_LEV_RefreshComponents = $A.get("e.c:FII_LEV_RefreshComponents");
		FII_LEV_RefreshComponents.setParams({
			"interaction" : component.get("v.interactionInCourse"),
			"recId" : component.get("v.recId")
		});
		FII_LEV_RefreshComponents.fire();
	},

	// CAMBIOS PARA GSM
	displayToast: function (component, dmlMessage, type) {
		const toastEvent = $A.get('e.force:showToast');
		
		toastEvent.setParams({
			type: type,
			message:  dmlMessage,
			duration: '8000'
		});
		toastEvent.fire();
    },
    
    getIsTeleventaProfile : function (component) {
        console.log('getIsTeleventaProfile');
		var result = component.get("c.getProfileUser");
		result.setParams({
		});
		result.setCallback(this, function (response) {
			var state = response.getState();
			console.log('perfil: ',response.getReturnValue());
			if (state === "SUCCESS") {
				if (response.getReturnValue() !== '') {
                    if (response.getReturnValue() === 'Asesor Front' || response.getReturnValue() === 'Asesor Experto' ){
                        component.set("v.isTeleventaProfile", true);
                        component.set("v.userProfile", response.getReturnValue());
                        console.log('userProfile: '+component.get("v.userProfile"));
                    }
				}
			}
		});
		$A.enqueueAction(result);
	}
    
})