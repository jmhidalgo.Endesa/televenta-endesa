({
    doInit: function (component, event, helper) {
        if (component.get("v.sobjectType") == 'Case') {
            helper.getAccountLeadId(component, event);
        }
        if (component.get("v.sobjectType") == 'Lead') {
            component.set("v.lead", true);
        }
        if (component.get("v.sobjectType") == 'Account') {
            helper.getAccountName(component, event);
        }
        if (component.get("v.sobjectType") == 'Contact') {
            //helper.getCongaURL(component,event);
            //helper.getTemplate(component, event);
            var updateNeedSelected = $A.get("e.c:FII_LEV_NeedExist");
            updateNeedSelected.setParams({"recId":component.get("v.recordId")});
            updateNeedSelected.fire();
        }
        helper.obtainProfiles(component);
        // console.log('sobjectType',component.get("v.sobjectType"));
        // console.log('isTeleventaProfile', component.get("v.isTeleventaProfile"));
       
        
    },

    openTab: function (component, event, helper) {

        var accountId = component.get("v.recordId");
        var action = component.get("c.getIdCase");

        action.setParams({
            accountId: accountId,
            needId: component.get("v.needSelected")
        });
        action.setCallback(this, function (response) {
            var status = response.getState();
            if (status === "SUCCESS" & component.isValid()) {
                var result = response.getReturnValue();
                component.set("v.idCase", result);
                var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
                needCreateEvent.setParams({ "needCreated": result, "recId": component.get("v.recordId") });
                needCreateEvent.fire();
                helper.newClaim(component);
            }
        });
        $A.enqueueAction(action);
    },

    modificarDatos: function (component, event, helper) {
        var labelbutton = event.getSource().get("v.label");
        component.set("v.labelButton", labelbutton);

        if (component.get("v.sObjectName") != 'Account' && component.get("v.sObjectName") != 'Case') {
            component.set("v.modificar", true);
            setTimeout(function () { component.set("v.modificar", false); }, 3000);
        }

        else {
            var modalBody;
            $A.createComponent("c:FII_LC_Account_Edit",
                {
                    recordId: component.get("v.recordId"),
                    interactionInCourse: component.get("v.interactionInCourse"), // CAMBIOS PARA GSM
                    recId: component.get("v.recId"), // CAMBIOS PARA GSM
                    needSelected: component.get("v.needSelected")
                },
                function (content, status) {
                    if (status === "SUCCESS") {
                        var cuenta = component.get("v.clientName");
                        modalBody = content;
                        component.find('overlayLib').showCustomModal({
                            header: "Modificación de Cliente",
                            body: modalBody,
                            showCloseButton: true,
                            cssClass: "mymodal",
                            closeCallback: function () {
                            }
                        })
                    }
                });
        }

    },

    consultaLecturas: function (component, event, helper) {
        helper.consultaLecturas(component, event);
    },

    close: function (component, event, helper) {
        // CAMBIOS PARA GSM
        component.set("v.consultar", false);
    },

    authorizedGestion: function (component, event, helper) {
        helper.openGestion(component, event);
    },

    rgpd: function (component, event, helper) {
        helper.rgpd(component, event);

    },

    arcopo: function (component, event, helper) {
        helper.arcopo(component, event);

    },

    newAddress: function (component, event, helper) {
        var labelbutton = event.getSource().get("v.label");
        component.set("v.labelButton", labelbutton);

        if (component.get("v.sObjectName") != 'Account' && component.get("v.sObjectName") != 'Case') {
            component.set("v.modificar", true);
            setTimeout(function () { component.set("v.modificar", false); }, 3000);
        }

        else {
            var modalBody;
            $A.createComponent("c:B2C_Address_Add_Controller",
                {
                    recordId: component.get("v.recordId"),
                    sObjectName: component.get("v.sObjectName"),
                    interactionInCourse: component.get("v.interactionInCourse"), // CAMBIOS PARA GSM
                    recId: component.get("v.recId"), // CAMBIOS PARA GSM
                    needSelected: component.get("v.needSelected")
                },
                function (content, status) {
                    if (status === "SUCCESS") {
                        modalBody = content;
                        component.find('overlayLib').showCustomModal({
                            header: "Nueva Dirección ",
                            body: modalBody,
                            showCloseButton: true,
                            cssClass: "mymodal",
                            closeCallback: function () {

                            }
                        })
                    }
                });
        }
    },

    modifyAddress: function (component, event, helper) {
        var labelbutton = event.getSource().get("v.label");
        component.set("v.labelButton", labelbutton);

        if (component.get("v.sObjectName") != 'Account' && component.get("v.sObjectName") != 'Case') {
            component.set("v.modificar", true);
            setTimeout(function () { component.set("v.modificar", false); }, 3000);
        }

        else {
            var modalBody;
            $A.createComponent("c:B2C_Address_Edit_Controller",
                {
                    idContact: component.get("v.contactId"),
                    recordId: component.get("v.recordId"),
                    sObjectName: component.get("v.sObjectName"),
                    interactionInCourse: component.get("v.interactionInCourse"), // CAMBIOS PARA GSM
                    recId: component.get("v.recId"), // CAMBIOS PARA GSM
                    needSelected: component.get("v.needSelected")
                },
                function (content, status) {
                    if (status === "SUCCESS") {
                        modalBody = content;
                        component.find('overlayLib').showCustomModal({
                            header: "Modificar Dirección",
                            body: modalBody,
                            showCloseButton: true,
                            cssClass: "mymodal",
                            closeCallback: function () {

                            }
                        })
                    }
                });
        }
    },

    gotoURLUsuarioEC: function (component, event, helper) {
        helper.getContactName(component);
        helper.gotoURLUsuarioEC(component);
    },

    deferredGestion: function (component, event, helper) {
        helper.deferredGestion(component, event);
    },

    eFact: function (component, event, helper) {
        var obj = component.get("v.sobjectType");
        var modalBody;
        if (obj == 'Account') {
            component.set("v.idAccount", component.get("v.recordId"));
        } else if (obj == 'Account' == 'Case') {
            helper.getAccountLeadId(component, event);
        }
        $A.createComponent("c:FII_LC_GestEInvoice",
            {
                idAccount: component.get("v.idAccount"),
                idContact: component.get("v.contactId"),
                recordId: component.get("v.recordId"),
                oName: component.get("v.sObjectName"),
                interactionInCourse: component.get("v.interactionInCourse"), // CAMBIOS PARA GSM
                recId: component.get("v.recId"), // CAMBIOS PARA GSM
                needSelected: component.get("v.needSelected")
            },
            function (content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find('overlayLib').showCustomModal({
                        header: $A.get("Gestión Factura Digital"),
                        body: modalBody,
                        showCloseButton: true,
                        cssClass: "mymodal slds-modal_large slds-align_absolute-center",
                        closeCallback: function () {
                            helper.refreshFocusedTab(component);
                        }
                    })
                }
            });
    },

    handlerNeedSelected: function (component, event) {

        var action = component.get("c.isTheEventForMe");
        var eventRecId = event.getParam('recId');
        var interactionInProgress = component.get("v.interactionInCourse")
        action.setParams({
            myRecId: component.get("v.recordId"),
            eventRecId: eventRecId,
            interactionInProgress: interactionInProgress
        });
        action.setCallback(this, function (response) {
            var status = response.getState();
            if (status === "SUCCESS" && response.getReturnValue()) {
                component.set("v.needSelected", event.getParam("needSelected"));
            }
        });
        $A.enqueueAction(action);
    },

    handlerNotifyInteractionInCourse: function (component, event, helper) {

        var action = component.get("c.isTheEventForMe");
        var eventRecId = event.getParam('recId');
        var interactionInProgress = event.getParam('interactionInCourse')
        action.setParams({
            myRecId: component.get("v.recordId"),
            eventRecId: eventRecId,
            interactionInProgress: interactionInProgress
        });
        action.setCallback(this, function (response) {
            var status = response.getState();
            if (status === "SUCCESS" && response.getReturnValue()) {
                component.set("v.interactionInCourse", event.getParam("interactionInCourse"));
                component.set("v.recId", event.getParam("recId"));
                helper.disableButton(component, event);
            }
        });
        $A.enqueueAction(action);
    },

    // IBAA-824 MODIFICACIÓN DE CONTACTO 
    openModifyContacto: function (component, event, helper) {
        var labelbutton = event.getSource().get("v.label");
        component.set("v.labelButton", labelbutton);

        var modalBody;
        $A.createComponent("c:FII_LC_ContactEdit",
            {
                recordId: component.get("v.recordId"),
                interactionInCourse: component.get("v.interactionInCourse"), // CAMBIOS PARA GSM
                needSelected: component.get("v.needSelected")
            },
            function (content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find('overlayLib').showCustomModal({
                        header: "Modificación de Contacto",
                        body: modalBody,
                        showCloseButton: false,
                        closeCallback: function () {
                            // DO SOMETHING WHEN CLOSE
                        }
                    })
                }
            }
        );
    },

    //IBAA-860 ALTA DE BONO SOCIAL
    openAltaBono: function (component, event, helper) {
        component.set("v.isSpinner", true);
        helper.createNeedAndFlow(component, event);
    },

    //IBAA-907 CUOTA FIJA
    fixedQuota: function (component, event, helper) {
        component.set("v.isSpinner", true);
        helper.createNeedAndFlowCuotaFija(component, event);
    },
    closeFlowModal: function (component, event, helper) {
        // test
        component.set("v.isOpen", false);
        component.set("v.isOpenGestion", false);
        component.set("v.isOpenCuotaFija", false);
        component.set("v.actBoton", false);
        component.set("v.msgRespuesta", "");
        helper.launchRefreshEvent(component);
        //helper.displayToast(component, null);
    },

    closeFlowBSModal : function (component, event, helper) {
        helper.deleteNeed(component, null, false, component.get("v.newActionBS"), component.get("v.newNeedBS"));
        component.set("v.isOpen", false);
        component.set("v.actBoton", false);
        component.set("v.msgRespuesta", "");
        helper.launchRefreshEvent(component);
    },

    closeFlowCFModal : function (component, event, helper) {
        helper.deleteNeed(component, null, false, component.get("v.newActionCF"), component.get("v.newNeedCF"));
        component.set("v.isOpenCuotaFija", false);
        component.set("v.msgRespuesta", "");
        helper.launchRefreshEvent(component);
    },

    closeModalOnFinishBono: function (component, event, helper) {
        if (event.getParam('status') === "FINISHED" || event.getParam('status') === "FINISHED_SCREEN") {
            //component.set("v.isOpen", false);
            component.set("v.isOpenGestion", false);
            component.set("v.isOpenCuotaFija", false);
            component.set("v.isOpen", false);
            //component.set("v.isOpen", true);

            var outputVariables = event.getParam("outputVariables");
            var outputVar;

            var documentsId;
            var isDocument = false;
            //var isDocumentOK = false;
            var msgRespo;
            //var caseId;
            //var needids;

            for (var i = 0; i < outputVariables.length; i++) {
                outputVar = outputVariables[i];
                if (outputVar.name === "DocumentsId") {
                    documentsId = outputVar.value;
                }

                if (outputVar.name === "MsgResponse"
                    && (outputVar.value == $A.get("$Label.c.FII_LC_Error_Missing_Data")
                        || outputVar.value == $A.get("$Label.c.FII_LC_Error_Validation")
                        || outputVar.value == null)) {

                    component.set("v.msgRespuesta", outputVar.value);
                    msgRespo = outputVar.value;
                    component.set("v.actBoton", true);

                } else if (outputVar.name === "MsgResponse" && outputVar.value == $A.get("$Label.c.FII_LC_Error_Petition_Claim")) {
                    component.set("v.msgRespuesta", $A.get("$Label.c.FII_LC_Error_Petition_Claim_Mod"));
                    component.set("v.actBoton", true);
                    msgRespo = $A.get("$Label.c.FII_LC_Error_Petition_Claim_Mod");

                } else if (outputVar.name === "MsgResponse") {
                    helper.launchRefreshEvent(component);
                    component.set("v.msgRespuesta", outputVar.value);
                    msgRespo = outputVar.value;
                    component.set("v.actBoton", true);
                }

                if (outputVar.name === "typeResponse" && outputVar.value === '00') {
                    isDocument = true;
                }
                /*if (outputVar.name === "ActionItem") {
                    caseId = outputVar.value;

                }
                if (outputVar.name === "CaseId") {
                    if (!component.get("v.needSelected")) {
                        needids = outputVar.value;
                    }
                }*/
            }

            if (isDocument) {
                
                //component.set("v.msgRespuesta", $A.get("$Label.c.FII_CL_Ok_flujo_bono_social"));
                helper.showToast($A.get("$Label.c.FII_CL_Ok_flujo_bono_social"));
            } else {
                //component.set("v.msgRespuesta", msgRespo);
                helper.showErrorToast(msgRespo);
                component.set("v.isDocument", false);
            }

            helper.deleteNeed(component, documentsId, isDocument, component.get("v.newActionBS"), component.get("v.newNeedBS"));
        }
    },

    closeModalOnFinish: function (component, event, helper) {
        if (event.getParam('status') === "FINISHED" || event.getParam('status') === "FINISHED_SCREEN") {
            component.set("v.isOpenCuotaFija", false);

            var outputVariables = event.getParam("outputVariables");
            var validReponse = false;
            var message = '';

            for (var i = 0; i < outputVariables.length; i++) {
                var outputVar = outputVariables[i];
                if (outputVar.name === "typeResponse" && outputVar.value === '00') {
                    validReponse = true;
                }
                if (outputVar.name === "msgResponse"){
                    message = outputVar.value;
                }
            }

            if (validReponse) {
                var message = component.get("v.newNeedCF") ? $A.get("$Label.c.FII_TOAST_NEED_ACT") : $A.get("$Label.c.FII_TOAST_ACT");
                helper.showToast(message);
            } else{
                helper.showErrorToast(message);
            }
            helper.deleteNeed(component, null, validReponse, component.get("v.newActionCF"), component.get("v.newNeedCF"));
            helper.launchRefreshEvent(component);
        }
    },
    closeModalOnFinishGestion: function (component, event, helper) {
        if (event.getParam('status') === "FINISHED") {
            helper.displayToast(component, null);
            var outputVariables = event.getParam("outputVariables");
            var outputVar;
            var result;
            for (var i = 0; i < outputVariables.length; i++) {
                outputVar = outputVariables[i];
                if (outputVar.name === "needId") {
                    component.set("v.needSelected", outputVar.value);
                    result = outputVar.value;
                }
            }
            var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
            needCreateEvent.setParams({ "needCreated": result, "recId": component.get("v.recordId") });
            needCreateEvent.fire();

            component.set("v.isOpenGestion", false);
            helper.launchRefreshEvent(component);
        }
    },

    openIframeSFW: function (component, event, helper) {
        var modalBody;
        var name = "c:FII_LC_Iframe";
        var data =
        {
            iframeUrl: component.get("v.salesfolderWeb"),
            width: '100%',
            height: '100%',
            frameBorder: 0,
            scrolling: 'auto'
        };
        var modalBody;
        var modalHeader = "Salesforlder Web";
        var cssClass = "slds-modal_large slds-align_absolute-center";

        helper.createComponent(name, data)
            .then(
                body => {
                    modalBody = body;
                },

                error => {
                    helper.showToast(component, "Error", error, "error");
                }
            ).then(
                modalFooter => {
                    return helper.createModal(component, modalHeader, modalBody, modalFooter, true, cssClass);
                }
            ).then(
                () => {
                    //$A.get('e.force:refreshView').fire();
                }
            ).catch(
                error => {
                    helper.showToast(component, error.name, error.message, "error");
                }
            )
    },

    statusChange: function (cmp, event) {
        if (event.getParam('status') === "FINISHED") {
            //Do something
        }
    },

    defferedGestionFlow: function (component, event, helper) {
        helper.defferedGestionFlow(component);
    },
})