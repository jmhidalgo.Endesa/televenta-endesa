({
	getAccountLeadId: function (component, event) {
		component.set("v.isSpinner", true);
		var getAccountOrLead = component.get("c.getAccountOrLead");
		getAccountOrLead.setParams({
			caseId: component.get("v.recordId")
		});
		getAccountOrLead.setCallback(this, function (response) {
			component.set("v.isSpinner", false);
			var state = response.getState();
			if (state === "SUCCESS") {
				if (response.getReturnValue()) {
					if (response.getReturnValue().substr(0, 3) == '00Q') {
						component.set("v.lead", true);
					}
					component.set("v.idAccount", response.getReturnValue());

				}
			} else {
				console.log('Error');
				console.log(response.getError());
			}
		});
		$A.enqueueAction(getAccountOrLead);


	},

	getAccountName: function (component) {
		component.set("v.isSpinner", true);
		var client = component.get("c.getClient");
		client.setParams({
			accountId: component.get("v.recordId")
		});
		client.setCallback(this, function (response) {
			component.set("v.isSpinner", false);
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.clientName", response.getReturnValue().Name);
			} else {
				console.log('Error');
				console.log(response.getError());
			}
		});
		$A.enqueueAction(client);
	},
	getContactName: function (component) {
		component.set("v.isSpinner", true);
		var client = component.get("c.getContact");
		client.setParams({
			idContact: component.get("v.recordId")
		});
		client.setCallback(this, function (response) {
			component.set("v.isSpinner", false);
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.contactName", response.getReturnValue().Name);
			} else {
				console.log('Error');
				console.log(response.getError());
			}
		});
		$A.enqueueAction(client);
	},

	consultaLecturas: function (component, event) {
		component.set("v.isSpinner", true);
		var labelbutton = event.getSource().get("v.label");
		var standardBehaviorGSM = component.get("c.standardBehaviorGSM");
		var sObjectType = null;
		standardBehaviorGSM.setParams({
			recId: component.get("v.recordId"),
			type: 'Atencion al cliente',
			subtype: 'Consulta',
			subjectNeed: labelbutton,
			subjectAct: labelbutton,
			camId: null,
			status: 'Completed',
			recordtype: $A.get("{!$Label.c.FII_RT_TSK_ACTION}")
		});
		standardBehaviorGSM.setCallback(this, function (response) {
			component.set("v.isSpinner", false);
			var state = response.getState();
			if (state === "SUCCESS") {
				// CAMBIOS PARA GSM
				this.launchRefreshEvent(component);
				if (component.get("v.sObjectName") == 'Case') {
					sObjectType = 'Case';
				}
				this.displayToast(component, sObjectType);
			} else {
				console.log('Error');
				console.log(response.getError());
			}
		});
		$A.enqueueAction(standardBehaviorGSM);
	},

	openGestion: function (component, event) {
		component.set("v.isSpinner", true);
		var labelbutton = event.getSource().get("v.label");
		var standardBehaviorGSM = component.get("c.standardBehaviorGSM");
		var sObjectType = null;
		var needId = null;
		if (component.get("v.needSelected") != null) {
			needId = component.get("v.needSelected");
		}
		else {
			needId = component.get("v.recordId");
		}
		standardBehaviorGSM.setParams({
			recId: needId,
			type: 'Atencion al cliente',
			subtype: null,
			subjectNeed: labelbutton,
			subjectAct: labelbutton,
			camId: null,
			status: 'Completed',
			recordtype: $A.get("{!$Label.c.FII_RT_TSK_ACTION}")
		});
		standardBehaviorGSM.setCallback(this, function (response) {
			component.set("v.isSpinner", false);
			var state = response.getState();
			if (state === "SUCCESS") {
				var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
				needCreateEvent.setParams({ "needCreated": response.getReturnValue()[0].Id, "recId": component.get("v.recordId") });
				component.set("v.idNeed", response.getReturnValue()[0].Id);
				needCreateEvent.fire();
				this.bodyCallback(component, response);

				// CAMBIOS PARA GSM
				this.launchRefreshEvent(component);
				if (component.get("v.sObjectName") == 'Case') {
					sObjectType = 'Case';
				}
				this.displayToast(component, sObjectType);
			} else {
				console.log('Error');
				console.log(response.getError());
			}
		});
		$A.enqueueAction(standardBehaviorGSM);
	},

	createNeedAndFlow: function (component, response) {
		component.set("v.isSpinner", true);
		var needId;

		if (component.get("v.needSelected") != null && component.get("v.needSelected") != "") {
			needId = component.get("v.needSelected");
		}
		else {
			needId = component.get("v.recordId");
		}

		var standardBehaviorGSM = component.get("c.standardBehaviorGSM");
		standardBehaviorGSM.setParams({
			recId: needId,
			type: 'TBS',
			subtype: 'SBS',
			subjectNeed: 'Gestión Administrativa',
			subjectAct: 'Alta de Bono Social',
			camId: null,
			status: 'New',
			recordtype: $A.get("{!$Label.c.FII_RT_CAS_ADMINGESTION}")
		});
		standardBehaviorGSM.setCallback(this, function (response) {
			var state = response.getState();
			component.set("v.isSpinner", false);
			if (state === "SUCCESS") {
				component.set("v.isOpen", true);
				var newNeedBS = response.getReturnValue()[0].Id;
				if (!component.get("v.needSelected")) {
					component.set("v.newNeedBS", newNeedBS);
				}

				var newActionBS = response.getReturnValue()[1].Id;
				component.set("v.newActionBS", newActionBS);
				var flow = component.find("flow");

				var inputVariables = [{
					name: "AccountId",
					type: "String",
					value: component.get("v.recordId")
				},
				{
					name: "CaseId",
					type: "String",
					value: newNeedBS
				},
				{
					name: "TaskId",
					type: "String",
					value: newActionBS
				}];
				flow.startFlow('ATC_Alta_de_Bono_Social', inputVariables);
				var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
				needCreateEvent.setParams({ "needCreated": response.getReturnValue()[0].Id, "recId": component.get("v.recordId") });
				needCreateEvent.fire();

			} else {
				console.log('Error');
				console.log(response.getError());
			}
		});
		$A.enqueueAction(standardBehaviorGSM);
	},

	//IBAA-907 Cuota Fija INICIO
	createNeedAndFlowCuotaFija: function (component, response) {
		var recId;
		component.set("v.isSpinner", true);
		if (component.get("v.needSelected") != null) {
			recId = component.get("v.needSelected");
		}
		else {
			recId = component.get("v.recordId");
		}
		var standardBehaviorGSM = component.get("c.standardBehaviorGSM");
		standardBehaviorGSM.setParams({
			recId: recId,
			type: 'Fixed Fee',
			subtype: 'Alta Cuota Fija',
			subjectNeed: 'Gestión de Cuota Fija',
			subjectAct: 'Gestión de Cuota Fija',
			camId: null,
			status: 'X11',
			recordtype: $A.get("{!$Label.c.FII_RT_CAS_ADMINGESTION}")
		});
		standardBehaviorGSM.setCallback(this, function (response) {
			component.set("v.isSpinner", false);
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.isOpenCuotaFija", true);
				var newNeedCF = response.getReturnValue()[0].Id;

				if (!component.get("v.needSelected")) {
					component.set("v.newNeedCF", newNeedCF);
				}
				var newActionCF = response.getReturnValue()[1].Id;
				component.set("v.newActionCF", newActionCF);

				var flow = component.find("flowCuotaFija");

				var inputVariables = [{
					name: "AccountId",
					type: "String",
					value: component.get("v.recordId")
				},
				{
					name: "recId",
					type: "String",
					value: newActionCF
				}];
				flow.startFlow('Cuota_Fija', inputVariables);
				var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
				needCreateEvent.setParams({ "needCreated": newNeedCF, "recId": component.get("v.recordId") });
				needCreateEvent.fire();

			} else {
				console.log('Error');
				console.log(response.getError());
			}
		});
		$A.enqueueAction(standardBehaviorGSM);

	},

	//IBAA-907 Cuota Fija FIN


	bodyCallback: function (component, response) {
		component.set("v.isSpinner", true);
		var accountId = response.getReturnValue()[0].AccountId;
		var workspaceAPI = component.find("workspace");
		workspaceAPI.getEnclosingTabId().then(function (tabId) {
			workspaceAPI.getTabInfo({
				tabId: tabId
			}).then(function (response) {
				workspaceAPI.openSubtab({
					parentTabId: tabId,
					pageReference: {
						type: "standard__component",
						attributes: {
							componentName: "c__FII_LC_AutorizedContactsContracts"
						},
						state: {
							c__recordId: accountId,
							c__sObjectName: "Case",
							c__idNeed: component.get("v.idNeed"),
							c__interactionInCourse: component.get("v.interactionInCourse"),
						}
					},
					focus: true
				}).then(function (response) {
					workspaceAPI.setTabLabel({
						tabId: response,
						label: "Gestión de Autorizados"
					});
					workspaceAPI.setTabIcon({
						tabId: response,
						icon: "standard:contact_list",
						iconAlt: "Gestión de Autorizados"
					});
					component.set("v.isSpinner", false);
				}).catch(function (error) {
					console.log(error);
					component.set("v.isSpinner", false);
				});
			});
		})
	},

	refreshFocusedTab: function (component) {
		var workspaceAPI = component.find("workspace");
		var sObjName = component.get("v.sobjectType");
		var parentId;

		workspaceAPI.getFocusedTabInfo().then(function (response) {

			if (sObjName == 'Account' && response.parentTabId == null) {

				parentId = response.tabId;
				var sObjectEvent = $A.get("e.force:navigateToSObject");
				sObjectEvent.setParams({
					"recordId": response.recordId.substring(0, 15),
					"slideDevName": "related"
				});
				sObjectEvent.fire();
			}
			else {

				parentId = response.parentTabId;
				//Obtenemos la info del tab padre
				workspaceAPI.getTabInfo({
					tabId: parentId
				}).then(function (infoTabParent) {
					//Iteramos las subtabs, aquellas que no sean Case se les hace un refreshTab y a los Case se simulará
					//un refresh abriendo y cerrando la misma pestaña y estableciendo el focus sobre la que lo tubiera
					for (let tab of infoTabParent.subtabs) {
						if (tab.pageReference.attributes.objectApiName == "Case" || tab.pageReference.attributes.objectApiName == "Account") {
							//Volvemos a abrir la subtab con los mismos datos
							workspaceAPI.openSubtab({
								parentTabId: tab.parentTabId,
								url: tab.url,
								focus: tab.focused
							}).then(function (response) {
								if (tab.focused) {
									workspaceAPI.focusTab({ tabId: response });
								}
							});
							//Nos aseguramos que la tab anterior se cierra
							workspaceAPI.closeTab({
								tabId: tab.tabId
							});
						} else {
							//Hacemos un refresh de todo aquello que no sea un Case
							workspaceAPI.refreshTab({
								tabId: tab.tabId,
								includeAllSubtabs: true
							});
						}
					}
				})
			}
		})
			.catch(function (error) {
				console.log('ERROR');
				console.log(error);
			});
	},

	gotoURLUsuarioEC: function (component) {
		component.set("v.isSpinner", true);
		var action = component.get("c.getUrlGestionEC");
		action.setParams({
			idContact: component.get("v.recordId"),
		});
		action.setCallback(this, function (response) {
			component.set("v.isSpinner", true);
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.isSpinner", false);
				var URLUsuarioEC = response.getReturnValue();
				var contactName = component.get("v.contactName");
				console.log('URLUsuarioEC: '+URLUsuarioEC);
				this.openURLModal(component, URLUsuarioEC, 'Gestión de usuario EC: ' + contactName);
			} else if (state == "ERROR") {
				var errors = response.getError();
				component.set("v.isSpinner", false);
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log("Error message: " + errors[0].message);
					} else {
						console.log("Unknown error");
					}
				}
			}

		});

		$A.enqueueAction(action);
	},

	openURLModal: function (component, url, titulo) {
		var modalBody;
		$A.createComponent("c:FII_LC_Iframe",
			{
				iframeUrl: url,
				width: '100%',
				height: 700,
				frameBorder: 0,
				scrolling: 'auto'
			},
			function (content, status) {
				if (status === "SUCCESS") {
					modalBody = content;
					component.find('overlayLib').showCustomModal({
						header: titulo,
						body: modalBody,
						showCloseButton: true,
						cssClass: "popoverclass",
						closeCallback: function () {
							console.log('Cierro modal');
						}
					})
				}
			});
	},

	deferredGestion: function (component, event) {
		component.set("v.isSpinner", true);
		var labelbutton = event.getSource().get("v.label");
		var standardBehaviorGSM = component.get("c.standardBehaviorGSM");
		var sObjectType = null;
		standardBehaviorGSM.setParams({
			recId: component.get("v.recordId"),
			type: 'Atencion al cliente',
			subtype: 'ATC Diferida',
			subjectNeed: labelbutton,
			subjectAct: labelbutton,
			camId: null,
			status: 'New',
			recordtype: $A.get("{!$Label.c.FII_RT_CAS_ACTION}")
		});

		standardBehaviorGSM.setCallback(this, function (response) {
			component.set("v.isSpinner", false);
			var state = response.getState();
			if (state === "SUCCESS") {
				var need = response.getReturnValue()[0];
				var workspaceAPI = component.find("workspace");
				workspaceAPI.openSubtab({
					recordId: need.Id,
					focus: true,
				});

				this.launchRefreshEvent(component);
				if (component.get("v.sObjectName") == 'Case') {
					sObjectType = 'Case';
				}
				this.displayToast(component, sObjectType);
			} else {
				console.log('Error');
				console.log(response.getError());
			}
		});
		$A.enqueueAction(standardBehaviorGSM);
	},

	newClaim: function (component) {
		component.set("v.isSpinner", true);
		var workspaceAPI = component.find("workspace");
		var pageReference = {
			"type": "standard__component",
			"attributes": {
				"componentName": "c__FII_LC_LaunchFlow"
			},
			"state": {
				"c__flowname": "FII_FLW_Claim_Case",
				"c__flowinput": JSON.stringify([{ name: "caseId", type: "String", value: component.get('v.idCase') }])
			}
		};
		workspaceAPI.openTab({
			pageReference: pageReference,
			focus: true
		}).then(function (tabId) {
			workspaceAPI.setTabIcon({
				tabId: tabId,
				icon: "standard:default",
				iconAlt: "Nueva Reclamacion"
			});
			component.set("v.isSpinner", false);

			workspaceAPI.setTabLabel({
				tabId: tabId,
				label: $A.get("$Label.c.Create_Claim")
			}).then(function (tabInfo) {
				workspaceAPI.disableTabClose({
					tabId: tabId,
					disabled: true
				});
				component.set("v.isSpinner", false);
			});
		}).catch(function (error) {
			component.set("v.isSpinner", false);
			console.log(error);
		});
	},

	// CAMBIOS PARA GSM
	launchRefreshEvent: function (component) {
		var FII_LEV_RefreshComponents = $A.get("e.c:FII_LEV_RefreshComponents");
		FII_LEV_RefreshComponents.setParams({
			"interaction": component.get("v.interactionInCourse"),
			"recId": component.get("v.recId")
		});
		FII_LEV_RefreshComponents.fire();
	},

	// CAMBIOS PARA GSM
	displayToast: function (component, sObjectType) {
		const toastEvent = $A.get('e.force:showToast');
		var messageToast;
		if (component.get("v.needSelected") != null) {
			messageToast = $A.get("$Label.c.FII_TOAST_ACT");
		}
		else {
			messageToast = $A.get("$Label.c.FII_TOAST_NEED_ACT");
		}
		/*if(sObjectType == "Case"){
			  messageToast = $A.get("$Label.c.FII_TOAST_ACT");
		  } else if (sObjectType == null) {
			  messageToast = $A.get("$Label.c.FII_TOAST_NEED_ACT");
		  }*/
		toastEvent.setParams({
			type: 'success',
			message: messageToast,
			duration: '8000'
		});
		toastEvent.fire();
	},

	getCongaURL: function (component, event) {
		component.set("v.isSpinner", true);
		var idContact = component.get("v.recordId");
		var action = component.get("c.getCongaURL");
		var acuse = component.get("v.isAcusedeRecibo");
		action.setParams({
			idContact: component.get("v.recordId"),
			acuse: acuse
		});
		action.setCallback(this, function (response) {
			component.set("v.isSpinner", false);
			var resp = response.getReturnValue();
			component.set("v.urlConga", resp);

			this.generateCongaFile(component, event);
		});
		$A.enqueueAction(action);
	},

	rgpd: function (component, event) {
		component.set("v.isAcusedeRecibo", false);
		if (component.get("v.congaWindowsIsOpen")) {
			this.showToast('Por favor, espere.');
		} else {
			component.set("v.congaWindowsIsOpen", true);
			this.showToast('Se previsualizará el documento una vez esté generado. Por favor, espere.');

			this.getCongaURL(component, event);
		}

	},

	arcopo: function (component, event) {
		component.set("v.isAcusedeRecibo", true);
		if (component.get("v.congaWindowsIsOpen")) {
			this.showToast('Por favor, espere.');
		} else {
			component.set("v.congaWindowsIsOpen", true);
			this.showToast('Se previsualizará el documento una vez esté generado. Por favor, espere.');

			this.getCongaURL(component, event);
		}
	},

	generateCongaFile: function (component, event) {
		if (component.get("v.urlConga") != "") {
			this.showToast('Se va a generar el justificante, no cierre la ventana de generación');
			var my_window = window.open(component.get("v.urlConga"),
				"Generando PDF... NO CERRAR", "status=1,width=750,height=200");
			component.set("v.congaWindowsIsOpen", true);
			component.set("v.sinCambios", false);
			var mihelper = this;
			setTimeout(function () {
				my_window.close();
				component.set("v.congaWindowsIsOpen", false);
				mihelper.showToast('Plantilla generada. Espere a que se muestre.');
				mihelper.getLastFileBase64(component, event);
			}, 30000);
		} else {
			this.showToast('No ha habido cambios en el justificante desde la última generación.');
			component.set("v.sinCambios", true);

			this.getLastFileBase64(component, event);
		}

	},

	getLastFile: function (component, event) {
		var lastfile = component.get("c.getLastFile");
		lastfile.setParams({
			idContact: component.get('v.recordId'),
			acuse: component.get("v.isAcusedeRecibo")
		});
		lastfile.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.pdf", response.getReturnValue());
				this.previewPDF(component, event);
				/*$A.get('e.lightning:openFiles').fire({
					  recordId: response.getReturnValue()
				  }); */
				//
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.log(errors);
			}
		});
		if (component.get("v.urlConga") && component.get("v.urlConga") != "") {
			setTimeout(function () {
				$A.enqueueAction(lastfile);
			}, 30000);
		} else {
			$A.enqueueAction(lastfile);
		}


	},


	previewPDF: function (component, event) {
		var url = component.get("v.urlConga");
		var urlbegin = url.split("/apex/", 1);
		var pdfId = component.get("v.pdf");
		url = urlbegin + "/lightning/r/ContentDocument/" + pdfId;

		this.openURLModal(component, url, 'RGPD');
		/*var urlEvent = $A.get("e.force:navigateToURL");
		  urlEvent.setParams({
		  "url": url
		  });
		  urlEvent.fire();*/

	},

	disableButton: function (component, event) {
		var itInCourse = event.getParam("interactionInCourse");

		var disableButton = component.get("c.disableButton");
		disableButton.setParams({
			myRecId: component.get("v.recordId"),
			eventRecId: event.getParam("recId"),
			itInCourse: event.getParam("interactionInCourse")
		});
		disableButton.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var newDisabled = response.getReturnValue();
				if (newDisabled != null) {
					if (itInCourse != null && itInCourse.Who && itInCourse.FII_ACT_LKP_RelatedClient__c != null) {
						component.set("v.contactId", itInCourse.WhoId);
						component.set("v.contactName", itInCourse.Who.Name);
					} else {
						component.set("v.contactId", null);
						component.set("v.contactName", null);
					}
					component.set("v.disabledButton", newDisabled);
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.log(errors);
			}
		});
		$A.enqueueAction(disableButton);
	},

	openURLModal: function (component, url, titulo) {
		var modalBody;
		$A.createComponent("c:FII_LC_Iframe",
			{
				iframeUrl: url,
				width: '100%',
				height: 700,
				frameBorder: 0,
				scrolling: 'auto'
			},
			function (content, status) {
				if (status === "SUCCESS") {
					modalBody = content;
					component.find('overlayLib').showCustomModal({
						header: titulo,
						body: modalBody,
						showCloseButton: true,
						cssClass: "slds-modal_large slds-align_absolute-center popoverclass",
						closeCallback: function () {
							console.log('Cierro modal');
						}
					})
				}
			});
	},

	getLastFileBase64: function (component, event) {
		var lastfile = component.get("c.getLastFileBase64");
		lastfile.setParams({
			idContact: component.get('v.recordId'),
			acuse: component.get("v.isAcusedeRecibo")
		});
		lastfile.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.PDFcontent", response.getReturnValue());
				this.getLastContentDocumentLink(component, event);
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.log(errors);
			}
		});
		$A.enqueueAction(lastfile);

	},

	getLastContentDocumentLink: function (component, event) {
		var getLastContentDocumentLink = component.get("c.getLastContentDocumentLink");
		getLastContentDocumentLink.setParams({
			idContact: component.get('v.recordId'),
			acuse: component.get('v.isAcusedeRecibo')
		});
		getLastContentDocumentLink.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				if (response.getReturnValue() && response.getReturnValue().ContentDocumentId) {
					component.set("v.documentId", response.getReturnValue().ContentDocumentId);
					this.visorJustificantePDF(component);
				} else {
					this.showErrorToast('Error. No ha sido posible obtener el fichero.');
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.log(errors);
			}
		});
		$A.enqueueAction(getLastContentDocumentLink);
	},

	visorJustificantePDF: function (component) {
		//this.getAccountIdIfContact(component);

		var accountId = component.get("v.idParent");
		if (accountId == null || accountId == '') {
			//"ws=/lightning/r/Account/0011X000005rl0EQAQ/view"
			var sUri = decodeURIComponent(window.location.search.substring(1));
			if (sUri.includes('Account/')) {
				var iniStr = sUri.indexOf('Account/') + 8;
				accountId = sUri.substring(iniStr, iniStr + 18);
			}
		}

		var contactId = component.get("v.recordId");
		var name = "c:FII_LC_Iframe";
		var acuse = component.get("v.isAcusedeRecibo");
		var fileName = "Justificante RGPD.pdf";
		if (acuse == true) {
			fileName = "Acuse de Recibo.pdf";
		}
		var data =
		{
			width: '100%',
			height: '100%',
			frameBorder: 0,
			scrolling: 'auto',
			type: "PDF",
			iframeUrl: $A.get('$Resource.PDF_Render') + "/web/viewer.html",
			fileName: fileName,
			fileData: component.get("v.PDFcontent"),
			fileType: "application/pdf",
			accountId: accountId,
			contactId: contactId,
			documentId: component.get("v.documentId"),
			needId: component.get("v.recordNeedAccount"),
		};

		var modalBody;
		var modalHeader = "Justificante RGPD";
		if (acuse == true) {
			modalHeader = "Acuse de recibo";
		}
		var cssClass = "slds-modal_large slds-align_absolute-center";

		this.createComponent(name, data)
			.then(
				body => {
					modalBody = body;
					var buttons =
						[
							[
								"lightning:button",
								{
									label: "Descargar",
									variant: "brand",
									onclick: body.getReference("c.onDownload")
								}
							],
							[
								"lightning:button",
								{
									label: "Enviar Correo",
									variant: "brand",
									disabled: false,
									onclick: body.getReference("c.onSubmit")
								}
							],
							[
								"lightning:button",
								{
									label: "Firmar",
									variant: "brand",
									disabled: false,
									onclick: body.getReference("c.firma")
								}
							],
							[
								"lightning:button",
								{
									label: "Cancelar",
									onclick: body.getReference("c.onCancel")
								}
							],
						];
					if (acuse == true) {
						buttons =
							[
								/*[
									"lightning:button",
									{
											label : "Enviar Correo",
											variant: "brand",
											disabled: false,
											onclick : body.getReference("c.onSubmit")
									}
								],*/
								[
									"lightning:button",
									{
										label: "Descargar",
										variant: "brand",
										onclick: body.getReference("c.onDownload")
									}
								],
								[
									"lightning:button",
									{
										label: "Cancelar",
										onclick: body.getReference("c.onCancel")
									}
								],
							];
					}

					return this.createComponentList(buttons);
				},

				error => {
					this.showToast(component, "Error", error, "error");
				}
			).then(
				modalFooter => {
					return this.createModal(component, modalHeader, modalBody, modalFooter, true, cssClass);
				}
			).then(
				() => {
					//$A.get('e.force:refreshView').fire();
				}
			).catch(
				error => {
					this.showToast(component, error.name, error.message, "error");
				}
			)
	},

	showToast: function (texto) {
		const toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
			type: 'success',
			message: texto,
			duration: '8000'
		});
		toastEvent.fire();;
	},

	showErrorToast : function(texto) {
        const toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
			type: 'error',
			message: texto,
			duration: '8000'
		});
		toastEvent.fire();
    },





	createModal: function (component, header, body, footer, showCloseButton, cssClass) {
		return new Promise(
			$A.getCallback(
				resolve => {
					component.find('overlayLib').showCustomModal({
						header: header,
						body: body,
						footer: footer,
						showCloseButton: showCloseButton,
						cssClass: cssClass,
						closeCallback: function () {
							component.set("v.congaWindowsIsOpen", false);
							resolve();
						}
					});
				}
			)
		);
	},
	createComponent: function (name, data) {
		return new Promise(
			$A.getCallback(
				(resolve, reject) => {
					$A.createComponent(name, data,
						(component, status, error) => {
							if (status == "SUCCESS") {
								resolve(component);
							}
							else {
								reject(error.message);
							}
						});
				}
			)
		);
	},
	createComponentList: function (data) {
		return new Promise(
			$A.getCallback(
				(resolve, reject) => {
					$A.createComponents(data,
						(componentList, status, error) => {
							if (status == "SUCCESS") {
								resolve(componentList);
							}
							else {
								reject(error.message);
							}
						});
				}
			)
		);
	},

	defferedGestionFlow: function (component) {

		var accountId = component.get("v.recordId");
		var contactId = component.get("v.contactId");
		if (contactId == null) contactId = '';

		var flowinput = JSON.stringify([{ name: "FII_TXT_Accountid_Input", type: "String", value: accountId },
		{ name: "FII_TXT_ContactId_Input", type: "String", value: contactId }]);


		component.set("v.isOpenGestion", true);
		var needId;
		if (component.get("v.needSelected") != null) {
			needId = component.get("v.needSelected");
		}
		else {
			needId = component.get("v.recordId");
		}
		//alert(accountId);
		var flowGestion = component.find("flowGestion");
		var inputVariables = [{
			name: "FII_TXT_Accountid_Input",
			type: "String",
			value: accountId
		},
		{
			name: "FII_TXT_ContactId_Input",
			type: "String",
			value: contactId
		},
		{
			name: "recordId",
			type: "String",
			value: needId
		}];
		flowGestion.startFlow('FII_FLW_DeferredGestionCreation', inputVariables);
	},

	deleteNeed: function (component, documentsId, isDocument, caseId, needids) {

		var deleteNeed = component.get("c.deleteNeed");
		deleteNeed.setParams({
			//documentsId
			needId: needids,
			documentids: documentsId,
			isDocument: isDocument,
			caseId: caseId
		});

		deleteNeed.setCallback(this, function (response) {
			var state = response.getState();

			if (state === "SUCCESS") {
				component.set("v.newNeedBS", null);
				component.set("v.newActionBS", null);
				component.set("v.newNeedCF", null);
				component.set("v.newActionCF", null);
				
				if (response.getReturnValue() == 'OK') {
					component.set("v.idNeed2", null);
					component.set("v.idNeed", null);

					var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
					needCreateEvent.setParams({ "needCreated": null, "recId": component.get("v.recordId") });
					needCreateEvent.fire();
				}
			}
            this.launchRefreshEvent(component);
		});
		$A.enqueueAction(deleteNeed);
	},
	obtainProfiles : function (component) {
		var result = component.get("c.getProfileUser");
		result.setParams({
		});
		result.setCallback(this, function (response) {
			var state = response.getState();
			console.log('perfil: ',response.getReturnValue());
			if (state === "SUCCESS") {
				if (response.getReturnValue() !== '') {
                    if (response.getReturnValue() === 'Asesor Front' || response.getReturnValue() === 'Asesor Experto' ){
                        component.set("v.isTeleventaProfile", true);
                        component.set("v.userProfile", response.getReturnValue());
                        console.log('userProfile: '+response.getReturnValue());
                    }
				}
			}
		});
		$A.enqueueAction(result);
	}
})