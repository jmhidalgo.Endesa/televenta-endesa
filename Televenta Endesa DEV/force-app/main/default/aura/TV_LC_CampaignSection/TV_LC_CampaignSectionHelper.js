({
    getDataDos : function(component, event, helper) {
		var url = new URL(window.location.href);

        var campaignMemberId = url.searchParams.get("SF__MemberID");
        var recordId = component.get("v.recordId");
        var actionGetData = component.get("c.getDataDos");

        campaignMemberId = null;
        
        actionGetData.setParams({
            'campaignMemberId' : campaignMemberId,
            'recordId' :component.get("v.recordId"),
            'url': url
        });
            
            actionGetData.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set('v.data', response.getReturnValue());
                    component.set( 'v.callInProgressBool', component.get('v.data').callInProgressBool );
                    /*console.log('HCONSOLE.LOG: v.callInProgressBool -> ' + component.get('v.data').callInProgressBool );
                    console.log('HCONSOLE.LOG: v.urlDocument -> ' + component.get('v.data').urlDocument );
                    console.log('HCONSOLE.LOG: SUCCESS ' );*/
                }else if(state === "ERROR"){ 
                    component.set("v.error", "Error obteniendo datos");
                    console.log('HCONSOLE.LOG: ERROR ' );

                }
            });
            $A.enqueueAction(actionGetData);

	}
})