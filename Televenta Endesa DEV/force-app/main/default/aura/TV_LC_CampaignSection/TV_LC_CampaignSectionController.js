({
	showTooltip : function(component, event, helper) {
		var tooltip = component.find('tooltipClientType');
        $A.util.removeClass(tooltip, 'slds-hide');
	},
    hideTooltip : function(component, event, helper) {
		var tooltip = component.find('tooltipClientType');
        $A.util.addClass(tooltip, 'slds-hide');
	},
    init : function(component, event, helper){
        //component.set( 'v.callInProgressBool', false );
        //helper.callInProgress(component, event, helper);
        helper.getDataDos(component, event, helper);       
    }
    ,
    naviteToCaseRecord : function(component, event, helper) {

        var recordId = component.get("v.data").campaignMember.TV_LKP_Caso_Control_Calidad__c;
        var event = $A.get( 'e.force:navigateToSObject' );
        if ( event ) {
            event.setParams({
                'recordId' : recordId
            }).fire();
        }

    },
    refreshComponent : function(component, event, helper){
        component.set( 'v.callInProgressBool', false );
    }
})