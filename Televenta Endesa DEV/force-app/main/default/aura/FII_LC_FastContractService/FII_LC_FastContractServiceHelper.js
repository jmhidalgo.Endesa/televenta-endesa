({
	helperMethod : function() {
		
	},
    
    showInteractionToast: function () {

        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            "title": "No hay interacción en curso",
            "type": "Error",
            "message": "Debe crearse una interacción antes de crear una necesidad",
            "mode": "dismissible"
        });

        toastEvent.fire();
    },

      // 03/12/19
      disableButton: function (component, event) {
		//var itInCourse = event.getParam("interactionInCourse");

		var disableButton = component.get("c.disableButton");
		disableButton.setParams({
			myRecId: component.get("v.recordId"),
			eventRecId: event.getParam("recId"),
			itInCourse: event.getParam("interactionInCourse")
		});
		disableButton.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var newDisabled = response.getReturnValue();
				if (newDisabled != null) {
					component.set("v.disabledButton", newDisabled);
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.log(errors);
			}
		});
		$A.enqueueAction(disableButton);
	},
	// TELEVENTA
	obtainProfiles : function(component){
		var result = component.get("c.getProfileUser");
		result.setParams({
		});
		result.setCallback(this, function (response) {
			var state = response.getState();
			console.log('perfil: ',response.getReturnValue());
			if (state === "SUCCESS") {
				if(response.getReturnValue() !== ''){
					component.set("v.isTeleventaProfile",true);
				}
			}
		});
		$A.enqueueAction(result);
	},
	showRGPDToast: function (mensaje) {

		var toastEvent = $A.get("e.force:showToast");

		toastEvent.setParams({
			"title": "No se cumplen los criterios de RGPD",
			"type": "Error",
			"message": mensaje,
			"mode": "dismissible"
		});

		toastEvent.fire();
	}
	// END TELEVENTA

})