({	 
    doInit : function(component, event, helper) {
      
        var action = component.get("c.getInteraction");
		action.setParams({ myRecId : component.get("v.recordId") });        
        action.setCallback(this, function(response) {
            var state = response.getState();
			if (state === "SUCCESS") {
                var newDisabled = response.getReturnValue();

				if (newDisabled[1] == 'true') {
					component.set("v.disabledButton", true);
                }else{
                    component.set("v.disabledButton", false);
                }
                if(newDisabled[0] != null){
                    component.set("v.interactionInCourse", newDisabled[0]);
                }
                console.log('interactionInCourse: ',JSON.stringify(newDisabled[0]));
			} else if (state === "ERROR") {
				var errors = response.getError();
			
			}
		});
        $A.enqueueAction(action);
        // TELEVENTA
		helper.obtainProfiles(component);
		// END TELEVENTA
    },
    createObjNeed : function(component, event, helper) {   
        var interaction = component.get("v.interactionInCourse");
        if(interaction == null || interaction === 'false' ){
            helper.showInteractionToast();
        } else{
            // TELEVENTA
            var isTVProfile = component.get('v.isTeleventaProfile');
            console.log('isTVProfile: ',isTVProfile);
            if(isTVProfile){
                console.log('Input Account: ',component.get("v.recordId"));
                console.log('Input interaction: ',interaction);
                // Check RGPD
                var checkRGPD = component.get("c.getIsContractAllowed");
                checkRGPD.setParams({ 
                    accountId : component.get("v.recordId"),
                    interactionId : interaction });        
                checkRGPD.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        //console.log('checkRGPD response: ',JSON.stringify(JSON.parse(response.getReturnValue())));
                        if(response.getReturnValue() !== 'OK'){
                            var mensaje = '';
                            if(response.getReturnValue() === ''){
                                mensaje = 'En la RGPD el cliente indica que no quiere que le molesten.';
                            }else if(response.getReturnValue() === 'RolNoValido'){
                                mensaje ='El contacto no es ni titular del contrato o autorizado de la cuenta';
                            }else{
                                mensaje = response.getReturnValue();
                            }
                            
                            helper.showRGPDToast(mensaje); 
                        }else{
                            console.log('inteaction OK');
                            var action = component.get("c.createNeed");
                            action.setParams({ accountId : component.get("v.recordId") });        
                            action.setCallback(this, function(response) {
                                var state = response.getState();
                                if (state === "SUCCESS") {
                                    if(response.getReturnValue() == null){
                                        helper.showInteractionToast();
                                    }else{
                                        var resultNeed = response.getReturnValue();
                                        var navigateEvt = $A.get("e.force:navigateToSObject");
                                        navigateEvt.setParams({
                                            "recordId": resultNeed.Id,
                                            "slideDevName": "Detail"
                                        });
                    
                                        var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
                                        needCreateEvent.setParams({"needCreated":response.getReturnValue().Id,"recId":component.get("v.recordId")});
                                        needCreateEvent.fire();
                                        var workspaceAPI = component.find("workspace");
                                        workspaceAPI.getFocusedTabInfo().then(function(response) {
                                            var focusedTabId = response.tabId;
                                            workspaceAPI.refreshTab({
                                                tabId: focusedTabId,
                                                includeAllSubtabs: false
                                            });
                                            
                                        })
                                        navigateEvt.fire();
                                        
                                    }
                                }
                                else if (state === "INCOMPLETE") {
                                }
                                else if (state === "ERROR") {
                                    var errors = response.getError();
                                    if (errors) {
                                        if (errors[0] && errors[0].message) {
                                            console.log("Error message: " + 
                                                    errors[0].message);
                                        }
                                    } else {
                                        console.log("Unknown error");
                                    }
                                }
                            });
                            $A.enqueueAction(action);
                        }

                    } else if (state === "ERROR") {
                        var errors = response.getError();
                        console.log('Check RGPD - ERRORS: ',errors);
                    }
                });
                $A.enqueueAction(checkRGPD);
            }else{
                var action = component.get("c.createNeed");
                action.setParams({ accountId : component.get("v.recordId") });        
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        if(response.getReturnValue() == null){
                            helper.showInteractionToast();
                        }else{
                            var resultNeed = response.getReturnValue();
                            var navigateEvt = $A.get("e.force:navigateToSObject");
                            navigateEvt.setParams({
                                "recordId": resultNeed.Id,
                                "slideDevName": "Detail"
                            });
        
                            var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
                            needCreateEvent.setParams({"needCreated":response.getReturnValue().Id,"recId":component.get("v.recordId")});
                            needCreateEvent.fire();
                            var workspaceAPI = component.find("workspace");
                            workspaceAPI.getFocusedTabInfo().then(function(response) {
                                var focusedTabId = response.tabId;
                                workspaceAPI.refreshTab({
                                    tabId: focusedTabId,
                                    includeAllSubtabs: false
                                });
                                
                            })
                            navigateEvt.fire();
                        }
                    }
				 
                    else if (state === "INCOMPLETE") {
                    }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                        errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }

                });
                $A.enqueueAction(action);
            }

            // END TELEVENTA 
        }
    },
    createNeedMethod: function(component, event, helper) {
        var action = component.get("c.createNeed");
        action.setParams({ accountId : component.get("v.recordId") });        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() == null){
                    helper.showInteractionToast();
                }else{
                    var resultNeed = response.getReturnValue();
                    var navigateEvt = $A.get("e.force:navigateToSObject");
                    navigateEvt.setParams({
                        "recordId": resultNeed.Id,
                        "slideDevName": "Detail"
                    });

                    var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
                    needCreateEvent.setParams({"needCreated":response.getReturnValue().Id,"recId":component.get("v.recordId")});
                    needCreateEvent.fire();
                    var workspaceAPI = component.find("workspace");
                    workspaceAPI.getFocusedTabInfo().then(function(response) {
                        var focusedTabId = response.tabId;
                        workspaceAPI.refreshTab({
                            tabId: focusedTabId,
                            includeAllSubtabs: false
                        });
                        
                    })
                    navigateEvt.fire();
                    
                }
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    // 03/12/19
    handlerNotifyInteractionInCourse: function (component, event, helper) {

        var action = component.get("c.isTheEventForMe");
        var eventRecId = event.getParam('recId');
        var interactionInProgress = event.getParam('interactionInCourse')
        action.setParams({
            myRecId: component.get("v.recordId"),
            eventRecId: eventRecId,
            interactionInProgress: interactionInProgress
        });
        action.setCallback(this, function (response) {
            var status = response.getState();
            if (status === "SUCCESS" && response.getReturnValue()) {
                component.set("v.interactionInCourse", event.getParam("interactionInCourse"));
                component.set("v.recId", event.getParam("recId"));
                helper.disableButton(component, event);
            }
        });
        $A.enqueueAction(action);
    },
})