({
	getData : function(component, event, helper) {
		var recordId = component.get('v.recordId');
        
        var actionGetData = component.get("c.getData");
        actionGetData.setParams({
            'recordId': recordId
        });
        
        actionGetData.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.data', response.getReturnValue());
            }else if(state === "ERROR"){
                component.set("v.error", "Error obteniendo datos");
            }
        });
        $A.enqueueAction(actionGetData);  
	}
})