({
    init : function(component, event, helper){
        helper.getData(component, event, helper);
    },
    
    refresh : function(component, event, helper){
        var recId = event.getParam("recId");
        if(recId == component.get('v.recordId')){
            helper.getData(component, event, helper);
        }
    }
})