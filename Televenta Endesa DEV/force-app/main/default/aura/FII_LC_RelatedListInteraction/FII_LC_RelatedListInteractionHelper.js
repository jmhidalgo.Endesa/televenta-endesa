({
	getAllInteractionsRelated : function(component) {
		var interaction = component.get("c.getInteractionsRelatedList");
		
		interaction.setParams({
            "recId" : component.get("v.recordId")
		});
	
		interaction.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				let sObjs = response.getReturnValue();
				component.set("v.allMyData", sObjs);
				this.fetchData(component, component.get("v.filData"));//component.get("v.fetchData"));
			} else {
				var errors = response.getError();
				console.log(errors);
				component.set("v.noResults", $A.get("$Label.c.FII_TABLE_EMPTY_ERROR") + " "+component.get("v.keyField"));
			}
		});
		$A.enqueueAction(interaction);
	},

    getOwnerID : function(component) {
        var currentUserId = $A.get("$SObjectType.CurrentUser.Id");
        component.set("v.currentUserId", currentUserId);
    },
    
	fetchData : function (component, filterData) {
        component.set("v.filData", filterData);
		var allMyDatas = component.get("v.allMyData");
		var data = [];
        var dataCreated = [];
        var dataOwerId = [];

		if(filterData){
			for(var mydata of allMyDatas){
				if( filterData == "Created" ){
					var date1 = new Date(Date.now());
					var date2 = new Date(mydata.CreatedDate);

					if(	date1.getDate() == date2.getDate() 
						&& date1.getFullYear() == date2.getFullYear() 
						&& date1.getMonth() == date2.getMonth() ) {
							data.push(mydata);
                        	dataCreated.push(mydata);
					}
					
				} else if( filterData == "OwnerId" ) {
					var currentUserId = $A.get("$SObjectType.CurrentUser.Id");
					if( currentUserId == mydata.OwnerId ) {
						data.push(mydata);
                        dataOwerId.push(mydata);
					}
				}
			}
		}else {
			data = allMyDatas;
            component.set("v.myData",data);
        }
        //alert('filterData'+filterData)
        if(filterData == "All"){
            data = allMyDatas;
        }
        
        /*if(filterData == "Created"){
            data = dataCreated;
        }
        
        if(filterData == "OwnerId"){
            data = dataOwerId;
        }*/
        
        component.set("v.myData",data);
        
        
        for(let row of data){
			var channel = row.FII_ACT_SEL_Channel__c.split(':')[0];
			row.channelLabel = row.FII_ACT_SEL_Channel__c.split(':')[1];
			if(channel == "null"){
				channel = null;
				row.channelLabel = null;
			}
			var type = row.FII_ACT_SEL_Interaction_Type__c.split(':')[0];
			row.typeLabel = row.FII_ACT_SEL_Interaction_Type__c.split(':')[1];
			if(type == "null"){
				type = null;
				row.typeLabel = null;
			}
			var category = row.FII_ACT_SEL_Interaction_Category__c.split(':')[0];
			row.categoryLabel = row.FII_ACT_SEL_Interaction_Category__c.split(':')[1];
			if(category == "null"){
				category = null;
				row.categoryLabel = null;
			}
			
			row.Type_text = row.typeLabel + ', ' + row.categoryLabel;
			
			row.Identifier = row.Subject;
			// row.Identifier_ico ='';
			row.IdentifierURL = '/'+row.Id;
			if(row.WhatId){
				row.WhatIdURL = '/'+row.WhatId;
				row.WhatName = row.What.Name;
				row.related_ico = 'standard:case';
			} else if(row.FII_ACT_LKP_WhatId__c){
				row.WhatIdURL = '/'+row.FII_ACT_LKP_WhatId__r.Id;
				row.WhatName = row.FII_ACT_LKP_WhatId__r.CaseNumber;
				row.related_ico = 'standard:case';
			} else if(row.FII_ACT_TXT_Recruitment_interaction__c){
				row.WhatIdURL = '/'+row.FII_ACT_TXT_Recruitment_interaction__c;
				row.WhatName = row.FII_ACT_TXT_RecIt_Name__c;
				row.related_ico = 'custom:custom111';
			}

			if(row.Identifier.includes('Interacción: ')){
				row.Identifier = row.Identifier.replace("Interacción: ", "");
				row.Subject_ico ='standard:messaging_user';
				
			}else if(row.Identifier.includes('Comunicación: ')){
				row.Identifier = row.Identifier.replace("Comunicación: ", "");
				row.Subject_ico2 ='standard:messaging_conversation';
				esComunicacion = true;
			}

			if(channel != null){
				if(channel == $A.get("$Label.c.FII_TaskChannel_Phone")){
					row.Identifier_ico ='custom:custom22';
				 }
				 else if(channel == $A.get("$Label.c.FII_TaskChannel_Email")){
					row.Identifier_ico ='standard:email_chatter';
				 }
				 else if(channel == $A.get("$Label.c.FII_TaskChannel_Website") || channel == $A.get("$Label.c.FII_TaskChannel_Web")){
					row.Identifier_ico ='standard:screen';
				 }
				 else if(channel == $A.get("$Label.c.FII_TaskChannel_Community") || channel == $A.get("$Label.c.FII_TaskChannel_Instagram") || channel == $A.get("$Label.c.FII_TaskChannel_Twitter") || channel == $A.get("$Label.c.FII_TaskChannel_Facebook") || channel == $A.get("$Label.c.FII_TaskChannel_LinkedIn")){
					row.Identifier_ico ='standard:social';
				 }
				 else if(channel == $A.get("$Label.c.FII_TaskChannel_SMS")){
					row.Identifier_ico ='standard:quick_text';
				 }
				 else if(channel == $A.get("$Label.c.FII_TaskChannel_CCPP")){
					row.Identifier_ico ='custom:custom31';
				 } 
				 else if(channel == $A.get("$Label.c.FII_TaskChannel_Chat")){
					row.Identifier_ico ='standard:live_chat';
				 } 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_ALEX")){
					row.Identifier_ico ='standard:post';
					row.IconClass_ico="slds-icon-standard-post1";
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_E21M")){
					row.Identifier_ico ='standard:custom_notification';
					row.IconClass_ico="slds-icon-standard-custom_notification1";
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_E21W")){
					row.Identifier_ico ='standard:custom_notification';
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_EAPP")){
					row.Identifier_ico ='standard:apex_plugin';
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_ESER")){
					row.Identifier_ico ='standard:channel_programs';
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_EWEB")){
					row.Identifier_ico ='standard:apex_plugin';
					row.IconClass_ico="slds-icon-standard-apex_plugin1";
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_GHOM")){
					row.Identifier_ico ='standard:home';
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_IVR")){
					row.Identifier_ico ='standard:log_a_call';
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_WAT1")){
					row.Identifier_ico ='standard:bot';
					row.IconClass_ico="slds-icon-standard-bot1";
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_WAT2")){
					row.Identifier_ico ='standard:bot';
					row.IconClass_ico="slds-icon-standard-bot2";
				} 
				else if(channel == $A.get("$Label.c.FII_TaskChannel_WAT3")){
					row.Identifier_ico ='standard:bot';
					row.IconClass_ico="slds-icon-standard-bot3";
				}
			}
			if(type!=null){
				if(type == $A.get("$Label.c.FII_TaskInteractionType_Input")){
					if(category == $A.get("$Label.c.FII_TaskCategory_Communication")){
						row.Type_ico ='standard:rtc_presence';
					}else if(category == $A.get("$Label.c.FII_TaskCategory_Interaction")){
						row.Type_ico ='standard:marketing_actions';
					}
				 }
				 else if(type == $A.get("$Label.c.FII_TaskInteractionType_Output")){
					if(category == $A.get("$Label.c.FII_TaskCategory_Communication")){
						row.Type_ico ='standard:feedback';
					}else if(category == $A.get("$Label.c.FII_TaskCategory_Interaction")){
						row.Type_ico ='standard:outcome';
					}
				 }
			}
			else{
				row.Type_ico ='utility:breadcrumbs';
				row.Type_text = 'Entrada, Interacción';
			}
			row.SubjectUrl = '/'+row.Id;
			if(data.length == 0) {
				component.set("v.noResults", $A.get("$Label.c.FII_TABLE_EMPTY"));
			}

		}

		this.sortData(component,component.get("v.sortedBy"),component.get("v.sortedDirection"));
		if(data.length > component.get("v.numberOfInteractions")){
			component.set("v.interactionsToShow", " (" + component.get("v.numberOfInteractions") + "+)");
		}
		else{
			component.set("v.interactionsToShow", " (" + component.get("v.myData.length") + ")");
		}
		
		component.set("v.title",component.get("v.titleAux"));

		if(data.length == 0) {
			component.set("v.noResults", $A.get("$Label.c.FII_TABLE_EMPTY"));
		}
	},


	getColumnDefinitions: function () {
        var columns = [
			{label: $A.get("$Label.c.FII_TABLE_COL_TYPE"), fieldName: 'TipeIcon', type: 'text', sortable: true, initialWidth: 70,
             typeAttributes: {title: { fieldName: 'Type_text'}},
             cellAttributes: {alignment: "center", iconName : {fieldName:'Type_ico'}, iconAlternativeText: { fieldName: 'Type_text'}, class: 'icn'}},
			{label: $A.get("$Label.c.FII_TABLE_COL_CHANNEL"), fieldName: 'IdentifierURL', type: 'url', sortable: true,
				typeAttributes: {label: { fieldName: 'channelLabel'}, target: '_self',  tooltip: {fieldName: 'channelLabel'} }, 
				cellAttributes: { class: {fieldName:'IconClass_ico'},  iconName : {fieldName:'Identifier_ico'} }},
            {label: $A.get("$Label.c.FII_TABLE_COL_DATE"), fieldName: 'CreatedDate', type: 'date', sortable: true, initialWidth: 140,
                typeAttributes: { day: 'numeric', month: 'short', year: 'numeric', hour: '2-digit', minute: '2-digit' }},
			{label: $A.get("$Label.c.FII_TABLE_COL_RELATED"), fieldName: 'WhatIdURL', type: 'url', sortable: true, 
				typeAttributes: {label: {fieldName:'WhatName'} , target: '_self', tooltip: {fieldName: 'WhatName'}},
				cellAttributes: { iconName : {fieldName:'related_ico'}}
			},

        ];
        return columns;
	},

	sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.myData");
        var reverse = sortDirection !== 'asc';

        data = Object.assign([],
            data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        component.set("v.myDataTruncated", data.slice(0,component.get("v.numberOfInteractions")));
	},
	
    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function(x) { return primer(x[field]) }
            : function(x) { return x[field] };

        return function (a, b) {
            var A = key(a);
			var B = key(b);
			if(!isNaN(A) && !isNaN(B)){
                var AA = parseFloat(A);
                var BB = parseFloat(B);
                return reverse * ((AA > BB) - (BB > AA));
            }
            return reverse * ((A > B) - (B > A));
        };
    },
    
    //CAMBIOS PARA TV
    hiddenButton: function(component, event, helper){
        var action = component.get("c.hiddenButton");
        action.setCallback(this, function(response){
            var status = response.getState();
            if (status === "SUCCESS"){
                var responseBool = response.getReturnValue();
                console.log('HCONSOLE.LOG-> responseBool='+responseBool);
                component.set("v.marcadorView", responseBool);
            } else {
                console.log('HCONSOLE.LOG-> status='+status);
                component.set("v.marcadorView", true);
            }
        });
        $A.enqueueAction(action);
        
    }
})