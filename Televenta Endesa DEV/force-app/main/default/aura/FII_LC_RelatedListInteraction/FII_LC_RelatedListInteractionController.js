({
	doInit : function(component, event, helper) {
		component.set('v.columns', helper.getColumnDefinitions()); //caller
		helper.getAllInteractionsRelated(component);
        helper.hiddenButton(component, event, helper);
	},
    
	openListView : function(component, event, helper) {
		var workspaceAPI = component.find("workspace");
		workspaceAPI.getEnclosingTabId().then(function(tabId) {
			workspaceAPI.getTabInfo({
				tabId: tabId
			}).then(function(response){
				workspaceAPI.openSubtab({
					parentTabId: tabId, 
					pageReference: {
						type: "standard__component",
						attributes: {
							componentName: "c__FII_LC_InteractionListView"
						},
						state: {
							c__recId : component.get("v.recordId"),
							c__objType : component.get("v.sObjectName")
						}
					},
					focus: true
				}).then(function(response) {
					workspaceAPI.setTabLabel({
					   tabId: response,
					   label: $A.get("$Label.c.FII_INTERACTIONS")
					});
					workspaceAPI.setTabIcon({
						tabId: response,
						icon: "standard:messaging_user",
						iconAlt: $A.get("$Label.c.FII_INTERACTIONS")
					});
			 	}).catch(function(error) {
					console.log(error);
				});
			});
		})
	},

	//--------------Datatable--------------------
	updateColumnSorting: function (component, event, helper) {
		component.set('v.isLoading', true);
		// We use the setTimeout method here to simulate the async
		// process of the sorting data, so that user will see the
		// spinner loading when the data is being sorted.
		setTimeout(function() {
			var fieldName = event.getParam('fieldName');
			var sortDirection = event.getParam('sortDirection');
			component.set("v.sortedBy", fieldName);
			component.set("v.sortedDirection", sortDirection);
			helper.sortData(component, fieldName, sortDirection);
			component.set('v.isLoading', false);
		}, 0);
	},

	handleSelect: function (component, event, helper) {
		// This will contain the index (position) of the selected lightning:menuItem
		var selectedMenuItemValue = event.getParam("value");
		var filterData;
		// Find all menu items
		var menuItems = component.find("menuItems");
		menuItems.forEach(function (menuItem) {
			// For each menu item, if it was checked, un-check it. This ensures that only one
			// menu item is checked at a time
			if (menuItem.get("v.checked")) {
				menuItem.set("v.checked", false);
			}
			// Check the selected menu item
			if (menuItem.get("v.value") === selectedMenuItemValue) {
				menuItem.set("v.checked", true);
			}
		});
		
		if (selectedMenuItemValue == "WithoutFilters") {
			component.set("v.filterLabel", $A.get("$Label.c.FII_TABLE_FILTER_WITHOUT_FILTERS"))
			component.set("v.titleAux", $A.get("$Label.c.FII_INTERACTIONS"));
			filterData = "All";
		}
		if (selectedMenuItemValue == "CreatedToday") {
			component.set("v.filterLabel",  $A.get("$Label.c.FII_TABLE_FILTER_CREATED_TODAY"));
			component.set("v.titleAux", $A.get("$Label.c.FII_INTERACTIONS") +" "+ $A.get("$Label.c.FII_TABLE_FILTER_CREATED_TODAY"));
			filterData = "Created";
		}
		if (selectedMenuItemValue == "MyActivities") {
			component.set("v.filterLabel", $A.get("$Label.c.FII_TABLE_FILTER_MYINTERACTIONS"));
			component.set("v.titleAux", $A.get("$Label.c.FII_TABLE_FILTER_MYINTERACTIONS"));
			filterData = "OwnerId";
		}
		component.set("v.filter", selectedMenuItemValue);
		component.set("v.filterData", filterData);
		helper.fetchData(component, filterData);
	},
    
    alertRefresh: function(component, event, helper){
        var obj = component.get("v.sObjectName");
        if(obj == "Account") helper.getOwnerID(component);  
		component.set('v.columns', helper.getColumnDefinitions()); //caller
		helper.getAllInteractionsRelated(component);
    },
	
	simulateInteraction : function(component, helper, event){

		var obj = component.get("v.sobjectType");
		var modalBody;
        
		$A.createComponent("c:FII_LC_InteractionSimulator", 
		{
			recordId : component.get("v.recordId"),
			sObjectName : obj
		},
		   function(content, status) {
			   if (status === "SUCCESS") {
				   modalBody = content;
				   component.find('overlayLib').showCustomModal({
					   header: 'Simular interacción',
					   body: modalBody, 
					   showCloseButton: true,
					   cssClass: "mymodal",
					   closeCallback: function() {
						   
					   }
				   })
			   }                               
		});
		
	}, 

	// CAMBIOS PARA GSM
	handlerRefreshComponents : function(component, event, helper){
		var action = component.get("c.isTheEventForMe");
        var eventRecId = event.getParam('recId');
        var oldInteraction = event.getParam('interaction')
        action.setParams({
			myRecId : component.get("v.recordId"), 
			eventRecId : eventRecId, 
			oldInteraction : oldInteraction
        });
        action.setCallback(this, function(response){            
            var status = response.getState();
			if (status === "SUCCESS" && response.getReturnValue()){
				var alertRefresh = component.get('c.alertRefresh');
				$A.enqueueAction(alertRefresh);
			}
		});
		$A.enqueueAction(action);
	},
    refreshComponent : function(component, event, helper){
        var alertRefresh = component.get('c.alertRefresh');
		$A.enqueueAction(alertRefresh);
    }
})