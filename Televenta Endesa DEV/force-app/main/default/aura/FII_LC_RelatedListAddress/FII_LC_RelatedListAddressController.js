({
	doInit: function (component, event, helper) {
		// TELEVENTA
		// helper.obtainProfiles(component);
		// END TELEVENTA
		helper.initMainAddress(component, event);
		//helper.init(component, event);
		component.set('v.columns', helper.getColumnDefinitions(component, event));
	},

	handlerNeedSelected: function (component, event, helper) {

		var action = component.get("c.isTheEventForMe");
		var eventRecId = event.getParam('recId');
		var interactionInProgress = component.get("v.interactionInCourse");
		action.setParams({
			myRecId: component.get("v.recordId"),
			eventRecId: eventRecId,
			interactionInProgress: interactionInProgress
		});
		action.setCallback(this, function (response) {
			var status = response.getState();
			//console.log('---- status: ' + status);
			if (status === "SUCCESS" && response.getReturnValue()) {
				component.set("v.needSelected", event.getParam("needSelected"));
			}
		});
		$A.enqueueAction(action);
	},

	updateColumnSorting: function (component, event, helper) {
		component.set('v.isLoading', true);
		// We use the setTimeout method here to simulate the async
		// process of the sorting data, so that user will see the
		// spinner loading when the data is being sorted.
		setTimeout(function () {
			var fieldName = event.getParam('fieldName');
			var sortDirection = event.getParam('sortDirection');
			component.set("v.sortedBy", fieldName);
			component.set("v.sortedDirection", sortDirection);
			helper.sortData(component, fieldName, sortDirection);

			if (component.get("v.idContact")) {
				var cmp = component.find("contactTable");
				var idcontact = component.get("v.idContact");
				cmp.set("v.selectedRows", {
					idcontact
				});
			}

			component.set('v.isLoading', false);
		}, 0);
	},


	updateSelectedText: function (component, event) {
		var selectedRows = event.getParam('selectedRows');
		if (selectedRows[0] != undefined)
			component.set("v.selectedAddress", selectedRows[0].IdentifierURL);
		else selectedRows = undefined;
	},

	deleteAddressComp: function (component, event, helper) {
		helper.deleteAddress(component, event, helper);
	},

	mainAddress: function (component, event, helper) {
		helper.mainAddress(component, event, helper);
	},

	modifyAddress: function (component, event, helper) {
		if (component.get("v.selectedAddress")) {
			$A.createComponent("c:B2C_Address_Edit_Controller", {
					idContact: component.get("v.contactId"),
					recordId: component.get("v.selectedAddress").slice(1),
					sObjectName: "B2C_Address__c",
					interactionInCourse: component.get("v.interactionInCourse"), // CAMBIOS PARA GSM
					recId: component.get("v.recId"), // CAMBIOS PARA GSM
					needSelected: component.get("v.needSelected")
				},
				function (content, status) {
					if (status === "SUCCESS") {
						component.find('overlayLib').showCustomModal({
							header: "Modificar Dirección",
							body: content,
							showCloseButton: true,
							cssClass: "mymodal",
							closeCallback: function () {

							}
						})
					}
				});
		} else {
			var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
				"title": "Direcciones",
				"message": "No se ha seleccionado dirección para modificar",
				"type": "error",

			});
			toastEvent.fire();
		}
	},

	newAddress: function (component, event, helper) {
		//console.log('--- Necesidad en el botón: ' + component.get("v.needSelected"));
		var labelbutton = event.getSource().get("v.label");
		component.set("v.labelButton", labelbutton);

		if (component.get("v.sObjectName") != 'Account' && component.get("v.sObjectName") != 'Case') {
			component.set("v.modificar", true);
			setTimeout(function () {
				component.set("v.modificar", false);
			}, 3000);
		} else {
			var modalBody;
			$A.createComponent("c:B2C_Address_Add_Controller", {
					recordId: component.get("v.recordId"),
					sObjectName: component.get("v.sObjectName"),
					needSelected: component.get("v.needSelected")
				},
				function (content, status) {
					if (status === "SUCCESS") {
						modalBody = content;
						component.find('overlayLib').showCustomModal({
							header: "Nueva Dirección ",
							body: modalBody,
							showCloseButton: true,
							cssClass: "mymodal",
							closeCallback: function () {

							}
						})
					}
				});
		}
	},

	openListView: function (component, event, helper) {
		var workspaceAPI = component.find("workspace");
		workspaceAPI.getEnclosingTabId().then(function (tabId) {
			workspaceAPI.getTabInfo({
				tabId: tabId
			}).then(function (response) {
				workspaceAPI.openSubtab({
					parentTabId: tabId,
					pageReference: {
						type: "standard__component",
						attributes: {
							componentName: "c__FII_LC_AddressListView"
						},
						state: {
							c__recordId: component.get("v.recordId"),
							c__objType: component.get("v.sObjectName")
						}
					},
					focus: true
				}).then(function (response) {
					workspaceAPI.setTabLabel({
						tabId: response,
						label: "Direcciones"
					});
					workspaceAPI.setTabIcon({
						tabId: response,
						icon: "standard:location",
						iconAlt: "Direcciones"
					});
				}).catch(function (error) {
					console.log(error);
				});
			});
		})
	},
	handlerNotifyInteractionInCourse: function (component, event, helper) {

		var action = component.get("c.isTheEventForMe");
		var eventRecId = event.getParam('recId');
		var interactionInProgress = event.getParam('interactionInCourse')
		action.setParams({
			myRecId: component.get("v.recordId"),
			eventRecId: eventRecId,
			interactionInProgress: interactionInProgress
		});
		action.setCallback(this, function (response) {
			var status = response.getState();
			//console.log('---- status: ' + status);
			if (status === "SUCCESS" && response.getReturnValue()) {
				component.set("v.interactionInCourse", event.getParam("interactionInCourse"));
				component.set("v.recId", event.getParam("recId"));
				helper.disableButton(component, event);
			}
		});
		$A.enqueueAction(action);
	},

	handlerNeedSelected: function (component, event) {

		var action = component.get("c.isTheEventForMe");
		var eventRecId = event.getParam('recId');
		var interactionInProgress = component.get("v.interactionInCourse")
		action.setParams({
			myRecId: component.get("v.recordId"),
			eventRecId: eventRecId,
			interactionInProgress: interactionInProgress
		});
		action.setCallback(this, function (response) {
			var status = response.getState();
			//console.log('---- status: ' + status);
			if (status === "SUCCESS" && response.getReturnValue()) {
				component.set("v.needSelected", event.getParam("needSelected"));
			}
		});
		$A.enqueueAction(action);
	},

	// CAMBIOS PARA GSM
	handlerRefreshComponents: function (component, event, helper) {
		//console.log('handle refresh need');
		var action = component.get("c.isTheEventForMe");
		var eventRecId = component.get("v.recordId");
		var oldInteraction = event.getParam('interaction');
		action.setParams({
			myRecId: component.get("v.recordId"),
			eventRecId: eventRecId,
			oldInteraction: oldInteraction
		});
		action.setCallback(this, function (response) {
			var status = response.getState();
			if (status === "SUCCESS" && response.getReturnValue()) {
				//component.set("v.firstExecution", false);
				helper.initMainAddress(component, event);
				component.set('v.columns', helper.getColumnDefinitions(component, event));
			}
		});
		$A.enqueueAction(action);
	},

})