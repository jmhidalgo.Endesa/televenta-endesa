({
    init : function(component, event) {
        var action;
        
            action = component.get("c.returnListAddress");
            action.setParams({
                clientId : component.get("v.recordId"),
                obs : false
            });
        

        action.setCallback(this, function (response) {
            var state = response.getState();
            //console.log('state: '+state);

            if (state === "SUCCESS") {
                var sObjs = response.getReturnValue();
                //console.log('sobj: '+sObjs);                
                var main = component.get("v.mainAddress");
                component.set("v.addresses", sObjs);

                var listSize = component.get("v.addresses.length");
                component.set("v.addressToShow", "(" + listSize + ")");
                //console.log('listSize: '+listSize);
                if(listSize > 0){
                    component.set("v.showTableAddress", true);
                    for (let row of sObjs){
                            row.Identifier_ico ='standard:location';                        
                            //row.Name = row.ContactId__r.Name;
                            if(row.Id == main){
                                row.colorBG='color';
                                console.log('Direccion principal color ='+main);
                            }       
                            row.IdentifierURL = '/'+row.Id;                        
                            row.Pais = row.Country_desc__c;
                            row.Obsoleto = row.Deprecated__c;
                        
                    }

                    component.set("v.addresses", sObjs);
                    component.set('v.isLoading', false);
                    this.sortData(component,component.get("v.sortedBy"),component.get("v.sortedDirection"));

                }
            } else {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
    },

    initMainAddress: function (component, event){
        var action;
        
            action = component.get("c.returnMainAddress");
            action.setParams({
                id : component.get("v.recordId"),
            });
            

            action.setCallback(this, function (response) {
                var state = response.getState();
    
                if (state === "SUCCESS") {
                    var mainAddress = response.getReturnValue();
                    component.set("v.mainAddress", mainAddress);
                    this.init(component, event);
                   
                } else {
                    console.log('Error');
                }
            });
            $A.enqueueAction(action);

    },
    
    getColumnDefinitions: function (component, event) {
        //var identicador = ;
        var columns = [
            {label: 'Dirección', fieldName: 'IdentifierURL', type: 'url', sortable: true,
             typeAttributes: {label: { fieldName: 'Name' }, tooltip : {fieldName: 'Name'}, target: '_self'},
             cellAttributes: { alignment: 'left', iconName : {fieldName:'Identifier_ico'},  class: {fieldName : 'colorBG'} }},
            {label: 'País', fieldName: 'Pais', type: 'text', sortable: true,cellAttributes: { class: {fieldName : 'colorBG'} }},
            {label: 'Provincia', fieldName: 'Province_cd__c', type: 'text', sortable: true,cellAttributes: { class: {fieldName : 'colorBG'} }},
            {label: '¿Tiene Suministro?', fieldName: 'Premise_flg__c', type: 'boolean', sortable: true,cellAttributes: { alignment: 'center', class: {fieldName : 'colorBG'} }}
        ];
        return columns;
    },

    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.addresses");
        var reverse = sortDirection !== 'asc';
        if(fieldName == "IdentifierURL"){
            fieldName = "Name";
        }
        data = Object.assign([],
            data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        //component.set("v.myDataTruncated", data.slice(0,component.get("v.numberOfContracts")));
        component.set("v.addresses", data);
	},
	
    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function(x) { return primer(x[field]) }
            : function(x) { return x[field] };

        return function (a, b) {
            var A = key(a);
            var B = key(b);
            if(!isNaN(A) && !isNaN(B)){
                var AA = parseFloat(A);
                var BB = parseFloat(B);
                return reverse * ((AA > BB) - (BB > AA));
            }
            return reverse * ((A > B) - (B > A));
        };
    },
    mainAddress: function(component,event,helper){
        var selectedAddress=component.get("v.selectedAddress");
        console.log('selected address'+selectedAddress);
        if(selectedAddress!=undefined){
            var action = component.get("c.mainAddressApex");
            var address = component.get("v.selectedAddress").slice(1);
            var account =  component.get("v.recordId");
            console.log('Account '+account);
            console.log('address '+address);
            action.setParams({
                accountid : account,
                addressid : address
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    var error = response.getReturnValue();                    
                    if(error=='0'){ 
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                        "title": "Dirección Principal",
                        "message": "Se ha cambiado la dirección con éxito",
                        "type": "success",
                       
                    });
                    toastEvent.fire();                   
                    //function refresh(){
                        component.set('v.isLoading', true);
                        $A.get('e.force:refreshView').fire();
                        var FII_LEV_RefreshComponents = $A.get("e.c:FII_LEV_RefreshComponents");
                        FII_LEV_RefreshComponents.setParams({
                            "interaction" : component.get("v.interactionInCourse"),
                            "recId" : component.get("v.recordId")
                        });
                        FII_LEV_RefreshComponents.fire();
                    //}                   
                    //setTimeout(refresh(),30000);
                    }else{    
                        console.log('Error'+state);                
                        var toastEvent = $A.get("e.force:showToast");
                             toastEvent.setParams({
                            "title": "Ocurrio un error inesperado: ",
                            "message": error,
                            "type": "error",                            
                         });
                         toastEvent.fire();    
                    }
                }
            });
        $A.enqueueAction(action);
        }else{
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                "title": "Dirección Principal",
                "message": "Se ha de seleccionar una dirección",
                "type": "warning",                       
                });
            toastEvent.fire(); 
        }
    },
    
    deleteAddress: function(component,event,helper){
        
        var selectedAddress=component.get("v.selectedAddress");
        
        if(selectedAddress!=undefined){
        //console.log('slice: '+component.get("v.selectedAddress").slice(1));
        var action = component.get("c.deleteAddress");
            action.setParams({
                id : component.get("v.selectedAddress").slice(1)
            });
        action.setCallback(this, function (response) {
            var state = response.getState();
            //console.log('state: '+state);

            if (state === "SUCCESS") {
                //console.log("error -->"+error);
               var error = response.getReturnValue();
                
                if(error=='0'){                    
                    
                        var toastEvent = $A.get("e.force:showToast");
                 		toastEvent.setParams({
                 		"title": "Eliminar dirección",
                 		"message": "Se ha eliminado la dirección con éxito",
                 		"type": "success",
                        
                 	});
                     toastEvent.fire(); 
                     component.set('v.isLoading', true);                  
                    //$A.get('e.force:refreshView').fire();
                    var FII_LEV_RefreshComponents = $A.get("e.c:FII_LEV_RefreshComponents");
                    FII_LEV_RefreshComponents.setParams({
                        "interaction" : component.get("v.interactionInCourse"),
                        "recId" : component.get("v.recordId")
                    });
                    FII_LEV_RefreshComponents.fire();
                }else{                    
                    var toastEvent = $A.get("e.force:showToast");
                 		toastEvent.setParams({
                        "title": "La dirección no se ha podido eliminar, tiene estas dependencias:",
                 		"message": error,
                 		"type": "error",
                        
                 	});
                 	toastEvent.fire();                   
                    
                }
                
            }
            
            
        });
        $A.enqueueAction(action);
        
        
        }else{
    		var toastEvent = $A.get("e.force:showToast");
                 		toastEvent.setParams({
                 		"title": "Eliminar dirección",
                 		"message": "Se ha de seleccionar una dirección",
                 		"type": "warning",
                        
                 	});
                 	toastEvent.fire(); 
    
    
    
        }},
    // CAMBIOS PARA GSM
	launchRefreshEvent : function(component) {
		var FII_LEV_RefreshComponents = $A.get("e.c:FII_LEV_RefreshComponents");
		FII_LEV_RefreshComponents.setParams({
			"interaction" : component.get("v.interactionInCourse"),
			"recId" : component.get("v.recId")
		});
		FII_LEV_RefreshComponents.fire();
	},

	// CAMBIOS PARA GSM
	displayToast: function (component, sObjectType) {
		const toastEvent = $A.get('e.force:showToast');
		var messageToast;
		if(sObjectType == "Case"){
			messageToast = $A.get("$Label.c.FII_TOAST_ACT");
		} else if (sObjectType == null) {
			messageToast = $A.get("$Label.c.FII_TOAST_NEED_ACT");
		}
		toastEvent.setParams({
			type: 'success',
			message:  messageToast,
			duration: '8000'
		});
		toastEvent.fire();
    },
    
    disableButton : function(component, event) {
		var itInCourse = event.getParam("interactionInCourse");
		
		var disableButton = component.get("c.disableButton");
		disableButton.setParams({
			myRecId : component.get("v.recordId"),
			eventRecId : event.getParam("recId"),
			itInCourse : event.getParam("interactionInCourse")
		});
		disableButton.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var newDisabled = response.getReturnValue();
				if(newDisabled != null){
					if( itInCourse != null && itInCourse.Who && itInCourse.FII_ACT_LKP_RelatedClient__c != null){
					  component.set("v.contactId", itInCourse.WhoId);
					  component.set("v.contactName", itInCourse.Who.Name);
					} else{
					  component.set("v.contactId", null);
					  component.set("v.contactName", null);
					}
					component.set("v.disabledButton", newDisabled);
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				console.log(errors);
			}
		});
		$A.enqueueAction(disableButton);
	},
    
    refreshTab : function(component, event) {
        //console.log('refreshTab');
		var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            //console.log('focusedTabId '+focusedTabId);
            workspaceAPI.refreshTab({
                      tabId: focusedTabId,
                      includeAllSubtabs: true
             });
        })
        .catch(function(error) {
            console.log('errorRefreshTab: '+error);
        });
	},
    // TELEVENTA
    /*
    obtainProfiles : function(component){
        var result = component.get("c.getIsTeleventaProfile");
        result.setParams({
        });
        result.setCallback(this, function (response) {
            var state = response.getState();
            console.log('perfil: ',response.getReturnValue());
            if (state === "SUCCESS") {
                if (response.getReturnValue()) {
                    component.set("v.disabledButton",false);
                }
            }
        });
        $A.enqueueAction(result);
    }*/
    // END TELEVENTA
})