({
    buildMethod: function(component) {
        var methodName = component.get("v.methodName");
        component.set("v.dataLoaded", false);
        
        var spinner = component.find("spinner");
        if (spinner != undefined) {
            $A.util.removeClass(spinner, "slds-hide");
        }
        
        var action = component.get("c." + methodName);
        
        var parameters =
            methodName === "getRecordsByAccountId" ||
            methodName === "getAddressesByAccountId"
        ? { accountId: component.get("v.accountId") }
        : { searchText: component.get("v.searchText") };
        
        action.setParams(parameters);
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (methodName == "getAddressesByUSPC" && state == "SUCCESS" && response.getReturnValue().length == 0) {
                this.populateCups(component, response);
            } else {
                
                this.getResultCallback(component, response);
                
                if (methodName === "getRecordsByAccountId") {
                    component.set("v.accountRecords", component.get("v.resultsData"));
                }
                
                component.set("v.dataLoaded", true);
                $A.util.addClass(spinner, "slds-hide");
            }
        });
        $A.enqueueAction(action);        
    },
    
    populateCups: function(component) {

        var action = component.get("c.getRecordsByCUPS");
        var parameters = { searchText: component.get("v.searchText") };
        
        action.setParams(parameters);
        
        action.setCallback(this, function(response) {
			var state = response.getState();
            if (state == "SUCCESS") {
				var messtr = '';
                if (response.getReturnValue().length == 0) {
					messtr = "Punto de suministro no encontrado. Aseguresé de que el CUPS es correcto.";
				} else {
                    messtr = "Debe asignar una dirección al punto de suministro para poder continuar.";
				}
                if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message: messtr,
                variant: "warning"
            };
            component.find("notifLib").showToast(toastProperties);
        }
                	component.set("v.newElectricSupplyPointButtonDisabled", false);
					component.set("v.newGasSupplyPointButtonDisabled", false);
        			component.set("v.selectedRows", {});
        			component.find("next").set("v.class" , "slds-hide");
        			component.set("v.dataLoaded", true);
        			component.set("v.mainSection", false);
        			component.set("v.resultsSection", true);
        			var spinner = component.find("spinner");        
					//var selectedAddressLookup = component.get("v.selectedAddressLookup"); 
					//component.set("v.selectedAddress", selectedAddressLookup);
        			$A.util.addClass(spinner, "slds-hide");
					this.setTableRows(component, response.getReturnValue());
			}	
        });
        $A.enqueueAction(action);
    },
    
    getResultCallback: function(component, response) {
        var state = response.getState();
        switch (state) {
            case "NEW":
                console.info("The action was created but is not in progress yet.");
                break;
            case "RUNNING":
                console.info("The action is in progress.");
                break;
            case "SUCCESS":
                console.info("The action executed successfully.");
                this.getResultOnSuccess(component, response);
                break;
            case "ERROR":
                this.getResultOnError(response);
                break;
            case "INCOMPLETE":
                console.warn(
                    "The server didn’t return a response." +
                    "The server might be down or the client might be offline."
                );
                break;
            case "ABORTED":
                // This action state is deprecated.
                console.error("The action was aborted.");
                break;
            default:
                console.log(state + " not recognized as an action state.");
        }
    },
    
    getResultOnError: function(response) {
        console.group("The server returned an error.");
        this.showErrors(response.getError());
        this.showParameters(response.getParams());
        this.showReturnedValue(response.getReturnValue());
        console.groupEnd();
    },
    
    getResultOnSuccess: function(component, response) {
        var addresses = response.getReturnValue();
        var methodName = component.get("v.methodName");
        if (methodName === "getRecordsByAccountId") {
            this.setTableRows(component, addresses);
        } else {
            if (addresses.length === 0) {
                this.showAddressesWereNotFoundToast(component);
                if (!component.get("v.usingFromUtilityBar")) {
                    if (component.find("newAddress") != undefined) {
                        $A.util.removeClass(component.find("newAddress"), "slds-hide");
                    }
                }
            } else {
                if (component.find("newAddress") != undefined) {
                    $A.util.addClass(component.find("newAddress"), "slds-hide");
                }
            }
            component.set("v.data", addresses);
        }
    },
    

    checkDistributorCreate: function(component, cups, sveState) {

        var checkDistributor = component.get("c.checkDistributor");
        var busLine = component.get("v.businessLine");
        if (busLine == null || busLine == '') {
            var busLinecomp = component.find("Business_line_cd__c_id");
            if(busLinecomp != null) {
                busLine = busLinecomp.get("v.value");
            }
        }
        checkDistributor.setParams({
            cups: cups,
            bussinesLine: busLine
        });
        checkDistributor.setCallback(this, function(response) {
            var state = response.getState();
            var check = response.getReturnValue();
            if (state === "SUCCESS") {
                if (check == true) {
                   component.set('v.cupsValidated',cups);
                    if (sveState == "SUCCESS") {
                            this.showAvailablePSToast(component);
                        } else {
                            this.showNoSVEConnectionToast(component);
                        }
                } else {
                    this.showWrongCUPSToast(component);
                    component.set('v.cupsValidated','');
                }
            } else {
               this.showWrongCUPSToast(component);
                component.set('v.cupsValidated','');
            }
        });
        $A.enqueueAction(checkDistributor);
    },

    callSVEPScreate: function(component) {
                //Name_id
        var cupscomp = component.find("Name_id");
        if(cupscomp == null) {
            if (component.find("notifLib") != undefined) {
                var toastProperties = {
                    message: "Debe especificar un CUPS",
                    variant: "error"
                };
                component.find("notifLib").showToast(toastProperties);
            }
            return;
        }
        var cups = cupscomp.get("v.value");
        if(cups == null || cups == '') {
            if (component.find("notifLib") != undefined) {
                var toastProperties = {
                    message: "Debe especificar un CUPS",
                    variant: "error"
                };
                component.find("notifLib").showToast(toastProperties);
            }
            return;
        }
        if ("PENDIENTE_INFORMAR" == cups) {
        	return;
        }
        console.log("consulta SVE");
        var callSVEPS = component.get("c.callSVEPS");
        callSVEPS.setParams({
            cups: cups
        });
        callSVEPS.setCallback(this, function(response) {
            var state = response.getState();
            var cupsResponse = response.getReturnValue().replace(/\s/g, "");
            //var cupsResponse = 'ERROR';
            if (
                state === "SUCCESS" &&
                cupsResponse != "" &&
                (cupsResponse.includes("ERROR") || cupsResponse =="ERROR")
            ) {
        //falla la llamada al WS y NO debemos bloquear
        
        this.checkDistributorCreate(component, cups, state);
                
        } else {
            if (state === "SUCCESS" && cupsResponse != "") {
                this.showSVECallErrorToast(component, cupsResponse);
                //rellenar la variable que no se pueda generar
                component.set('v.cupsValidated','');
            }
            
            if (
                (state === "SUCCESS" && cupsResponse === "") ||
                ["INCOMPLETE", "ERROR"].includes(state)
            ) {
                this.checkDistributorCreate(component, cups, state);
            }
            
            if (!["SUCCESS", "INCOMPLETE", "ERROR"].includes(state)) {
                this.showWrongCUPSLengthToast(component);
                //rellenar la variable que no se pueda generar
                component.set('v.cupsValidated','');
            }
        }
        });
            $A.enqueueAction(callSVEPS);
        },
    
    callSVEPS: function(component) {
        console.log("cups a consultar: " + component.get("v.cups"));
        console.log("consulta SVE");
        var callSVEPS = component.get("c.callSVEPS");
        callSVEPS.setParams({
            cups: component.get("v.cups")
        });
        callSVEPS.setCallback(this, function(response) {
            var state = response.getState();
            var cupsResponse = response.getReturnValue().replace(/\s/g, "");
            //var cupsResponse = 'ERROR';
            if (
                state === "SUCCESS" &&
                cupsResponse != "" &&
                cupsResponse.includes("ERROR")
            ) {
                //ARV Start
                /*this.showNoSVEToast(component);
component.set('v.searchText', '');
component.set('v.value', $A.get("$Label.c.Address"));
component.find("searchBy").set("v.value", $A.get("$Label.c.Address"));
this.getAddresses(component);*/
    this.checkDistributor(component, "ERROR");
    //ARV End
    //everis (ART) ahora no se ejecuta el NEXT hasta que acabe el callback de SVE --SVE NO es bloqueante
    var navigate = component.get("v.navigateFlow");
    navigate("NEXT");
} else {
    if (state === "SUCCESS" && cupsResponse != "") {
        this.showSVECallErrorToast(component, cupsResponse);
        //everis (ART) ahora no se ejecuta el NEXT hasta que acabe el callback de SVE --SVE NO es bloqueante
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    }
    
    if (
        (state === "SUCCESS" && cupsResponse === "") ||
        ["INCOMPLETE", "ERROR"].includes(state)
    ) {
        this.checkDistributor(component, state);
        //everis (ART) ahora no se ejecuta el NEXT hasta que acabe el callback de SVE --SVE NO es bloqueante
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    }
    
    if (!["SUCCESS", "INCOMPLETE", "ERROR"].includes(state)) {
        this.showWrongCUPSLengthToast(component);
        //everis (ART) ahora no se ejecuta el NEXT hasta que acabe el callback de SVE --SVE NO es bloqueante
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    }
}
});
    $A.enqueueAction(callSVEPS);
},
    checkDistributor: function(component, sveState) {
        var checkDistributor = component.get("c.checkDistributor");
    var busLine = component.get("v.businessLine");
    if (busLine == null || busLine == '') {
        var busLinecomp = component.find("Business_line_cd__c_id");
        if(busLinecomp != null) {
            busLine = busLinecomp.get("v.value");
        }
    }
    var cups = component.get("v.cups");
    if (cups == null || cups == '') {
        var cupscomp = component.find("Name_id");
        if(cupscomp != null) {
            cups = cupscomp.get("v.value");
        }
    }
        checkDistributor.setParams({
        cups: cups,
        bussinesLine: busLine
        });
        checkDistributor.setCallback(this, function(response) {
            var state = response.getState();
            var check = response.getReturnValue();
            if (state === "SUCCESS") {
                if (check == true) {
                    if (sveState == "NOTOAST") {
                        //
                    } else if (sveState === "SEARCHTOAST") {
                        this.showCreatedPS(component, "Búsqueda realizada.");
                    }
                    //Si venimos de crear un PS no muetra el toast de SVE
                        else if (sveState === "NOSVETOAST") {
                            this.showCreatedPS(
                                component,
                                "Punto de Suministro creado correctamente."
                            );
                        } else if (sveState === "SUCCESS") {
                            this.showAvailablePSToast(component);
                        } else {
                            this.showNoSVEConnectionToast(component);
                        }
                    if (component.get("v.resultsSection")) {
                        this.showSpinner(component);
                        var fields = component.get("v.recordEditFormFields");
                        if (component.find("servicePointCreationForm") != undefined) {
                            component.find("servicePointCreationForm").submit(fields);
                        }
                    }
                } else {
                    this.showWrongCUPSToast(component);
                }
                if (component.get("v.mainSection")) {
                    this.getAddresses(component);
                }
            }
        });
        $A.enqueueAction(checkDistributor);
    },
    
    checkServicePointAvailability: function(component, activateSVE) {
        console.log('checkServicePointAvailability');
        var cups = component.get("v.cups");
        if (
            cups != "" &&
            cups != "PENDIENTE_INFORMAR" &&
            cups.length != 20 &&
            cups.length != 22
        ) {
            this.showWrongCUPSLengthToast(component);
        } else {
            this.setBusinessLine(component);
            if (component.get("v.doSVE") == false) {
                this.checkDistributor(component, "NOTOAST");
                var navigate = component.get("v.navigateFlow");
                navigate("NEXT");
            } else if (activateSVE) {
                console.log('callSVEPS');
                this.callSVEPS(component);
            }
        }
    },
    
    clearAdresses: function(component) {
        component.set("v.selectedRows", {});
        if (component.find("next") != undefined) {
            $A.util.addClass(component.find("next"), "slds-hide");
        }
        component.set("v.resultsSection", false);
        component.set("v.mainSection", true);
        component.set("v.data", null);
        component.set("v.resultsData", null);
    },
    
    getAddresses: function(component) {
        if (component.find("searchBy") != undefined) {
            var searchBy = component.find("searchBy").get("v.value");
            switch (searchBy) {
                case $A.get("$Label.c.All"):
                    var searchLength = component.get("v.searchText").replace(/\s/g, "")
                    .length;
                    if (
                        !component.get("v.supplyPointChecked") &&
                        (searchLength === 20 || searchLength === 22)
                    ) {
                        component.set(
                            "v.cups",
                            component.get("v.searchText").replace(/\s/g, "")
                        );
                        component.set("v.supplyPointChecked", true);
                        //everis (ART) Esto ya se comprueba cuando seleccionas el ps y haces el next step
                        this.checkServicePointAvailability(component, false);
                    } else {
                        component.set("v.methodName", "getAddressesByAnything");
                        this.buildMethod(component);
                    }
                    break;
                case $A.get("$Label.c.CUPS"):
                    var searchLength = 0;
                    if (component.get("v.searchText") != null) {
                        searchLength = component.get("v.searchText").replace(/\s/g, "")
                        .length;
                    }
                    if ("PENDIENTE_INFORMAR" === component.get("v.searchText")) {
                        //component.set('v.supplyPointChecked', true);
                        //this.checkDistributor(component, "ERROR");
                        component.set("v.supplyPointChecked", false);
                        this.showPendienteInformarToast(component);
                        component.set("v.searchText", "");
                        component.set("v.value", $A.get("$Label.c.Address"));
                        component
                        .find("searchBy")
                        .set("v.value", $A.get("$Label.c.Address"));
                        this.getAddresses(component);
                    } else if (searchLength != 20 && searchLength != 22) {
                        if (component.find("notifLib") != undefined) {
                            var toastProperties = {
                                message: "Formato de CUPS incorrecto",
                                variant: "info"
                            };
                            component.find("notifLib").showToast(toastProperties);
                        }
                    } else if (
                        !component.get("v.supplyPointChecked") &&
                        (searchLength === 20 || searchLength === 22)
                    ) {
                        component.set(
                            "v.cups",
                            component.get("v.searchText").replace(/\s/g, "")
                        );
                        component.set("v.supplyPointChecked", true);
                        //everis (ART) Esto ya se comprueba cuando seleccionas el ps y haces el next step
                        this.checkServicePointAvailability(component, false);
                        this.checkDistributor(component, "SEARCHTOAST");
                    } else {
                        component.set("v.methodName", "getAddressesByUSPC");
                        this.buildMethod(component);
                        component.set("v.supplyPointChecked", false);
                    }
                    break;
                case $A.get("$Label.c.Address"):
                    component.set("v.methodName", "getAddressesByName");
                    this.buildMethod(component);
                    break;
                case $A.get("$Label.c.Contract"):
                    component.set("v.methodName", "getAddressesByAssetName");
                    this.buildMethod(component);
                    break;
                default:
                    // no code
            }
        }
    },
    
    showModal: function(component) {
        var populatedFields = component.get("v.modalPopulatedFields");
        
        var fields = component.get("v.formFields");
        
        var componentList = [
            [
                "lightning:spinner",
                {
                    "aura:id": "spinner",
                    class: "slds-hide",
                    alternativeText: $A.get("$Label.c.Loading"),
                    size: "large"
                }
            ],
            [
                "lightning:recordEditForm",
                {
                    "aura:id": "servicePointCreationForm",
                    class: "",
                    recordTypeId: component.get("v.recordTypeObject"),
                    objectApiName: "B2C_Service_Point__c",
                    onsubmit: component.getReference("c.handleSupplyPointSubmit"),
                    onsuccess: component.getReference("c.handleSupplyPointSuccess"),
                    onerror: component.getReference("c.handleSupplyPointError")
                }
            ]
        ];
        var i;
        for (i = 0; i < fields.length; i++) {
            var properties = { fieldName: fields[i].name };
            
            if (populatedFields.hasOwnProperty([fields[i].name])) {
                properties["value"] = populatedFields[fields[i].name];
                if (properties["value"]) {
                    properties["disabled"] = true;
                }
            }
             properties["aura:id"] = fields[i].name+"_id";
            componentList.push(["lightning:inputField", properties]);
        }
        componentList.push([
                "aura:HTML",
                {
                    "tag": "div",
                    "class": "slds-button-group",
                    "role": "group",
                }
            ]);
        componentList.push([
                "lightning:button",
                {
                    "aura:id": "validationButton",
                    //class: "slds-align_absolute-center",
                    variant: "brand",
                    type: "button",
                    name: "validation",
                    label: "Validar CUPS",
                    onclick: component.getReference("c.validateCUPS")
                }
            ]);
        componentList.push([
            "lightning:button",
            {
                "aura:id": "saveButton",
                //class: "slds-align_absolute-center",
                variant: "brand",
                type: "submit",
                name: "save",
                label: $A.get("$Label.c.Save")
            }
        ]);
        $A.createComponents(componentList, function(content, status) {
            if (status === "SUCCESS") {
                var spinner = content[0];
                var recordEditForm = content[1];
                var recordEditFormBody = content[1].get("v.body");
                var i;
                var lastdivbody = null;
                var lastdiv = null;
                for (i = 2; i < content.length; i++) {
                    if (lastdivbody != null) {
                        //añadimos los botones al final
                        lastdivbody.push(content[i]);
                    } else {
                    recordEditFormBody.push(content[i]);
                       
                        if(content[i].get("v.tag") != null && content[i].get("v.tag") == "div") {
                            lastdivbody = content[i].get("v.body");
                            lastdiv = content[i];
                        }
                    }
                }
                if (lastdiv != null) {
                        lastdiv.set("v.body", lastdivbody);
                }
                recordEditForm.set("v.body", recordEditFormBody);
                if (component.find("overlayLib") != undefined) {
                    var modalPromise = component.find("overlayLib").showCustomModal({
                        body: [spinner, recordEditForm],
                        showCloseButton: true
                    });
                    component.set("v.modalPromise", modalPromise);
                }
            }
        });
    },
    
    createServicePoint: function(component, recordDeveloperName) {
        var actionRecordType = component.get("c.getRecordTypeId");
        
        actionRecordType.setParams({
            DeveloperName: recordDeveloperName,
            ObjectName: "B2C_Service_Point__c"
        });
        actionRecordType.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var recordTypeId = response.getReturnValue();
                
                component.set("v.recordTypeObject", recordTypeId);
                
                var actionLayout = component.get("c.readFieldSet");
                
                actionLayout.setParams({
                    fieldSetName: component.get("v.fieldSetLayout"),
                    ObjectName: "B2C_Service_Point__c"
                });
                actionLayout.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var resultMap = response.getReturnValue();
                        component.set("v.formFields", resultMap);
                        this.showModal(component);
                    } else {
                        console.error(
                            "Error " +
                            JSON.stringify(response.getError()) +
                            " calling action with state: " +
                            response.getState()
                        );
                    }
                });
                $A.enqueueAction(actionLayout);
            } else {
                console.error(
                    "Error " +
                    JSON.stringify(response.getError()) +
                    " calling action with state: " +
                    response.getState()
                );
            }
        });
        $A.enqueueAction(actionRecordType);
    },
    
    closeModal: function(component) {
        component.get("v.modalPromise").then(function(modal) {
            modal.close();
        });
    },
    
    getHighestPowerPeriod: function(powerPeriods) {
        var result;
        powerPeriods = powerPeriods.filter(item => item != null && item > 0);
        if (powerPeriods.length > 0) {
            if (powerPeriods.length > 1) {
                powerPeriods = powerPeriods.sort(function(a, b) {
                    return b - a;
                });
            }
            result = powerPeriods[0];
        }
        return result;
    },
    
    getNewRow: function(component, event) {
        var createdServicePoint = event.getParams().response;
        var createdServicePointFields = createdServicePoint.fields;
        
        var newRow = this.createNewRow();
        
        newRow.Id = newRow.SupplyPoint.Id = createdServicePoint.id;
        if (createdServicePointFields.Name != null) {
            newRow.SupplyPoint.Name = newRow.SupplyPointName =
                createdServicePointFields.Name.value;
        }
        
        if (
            createdServicePoint.fields.AddrId_Id__r != undefined &&
            createdServicePoint.fields.AddrId_Id__r.value.fields.Name.value !=
            undefined
        ) {
            newRow.AddressName =
                createdServicePoint.fields.AddrId_Id__r.value.fields.Name.value != null
            ? createdServicePoint.fields.AddrId_Id__r.value.fields.Name.value
            : null;
        } else {
            newRow.AddressName = component.get("v.addrName");
        }
        
        newRow.SupplyPoint.Account_id__c =
            createdServicePointFields.Account_id__c != null
        ? createdServicePointFields.Account_id__c.value
        : null;
        
        newRow.SupplyPointAccountName =
            createdServicePointFields.Account_id__r.displayValue != null
        ? createdServicePointFields.Account_id__r.displayValue
        : null;
        newRow.SupplyPoint.CNAE__c = createdServicePointFields.CNAE__c != null ? createdServicePointFields.CNAE__c.value : null;      
        newRow.SupplyPoint.Business_line_cd__c =
            createdServicePointFields.Business_line_cd__c != null
        ? createdServicePointFields.Business_line_cd__c.value
        : null;
        newRow.BusinessLineName =
            createdServicePointFields.Business_line_cd__c != null
        ? createdServicePointFields.Business_line_cd__c.displayValue
        : null;
        
        const icons = component.get("v.ICONS");
        if (newRow.SupplyPoint.Business_line_cd__c === "01") {
            newRow.ButtonIconName = icons.electric;
            newRow.SupplyPoint.Power_period_1_Kw__c =
                createdServicePointFields.Power_period_1_Kw__c != null
            ? createdServicePointFields.Power_period_1_Kw__c.value
            : null;
            newRow.SupplyPoint.Power_period_2_Kw__c =
                createdServicePointFields.Power_period_2_Kw__c != null
            ? createdServicePointFields.Power_period_2_Kw__c.value
            : null;
            newRow.SupplyPoint.Power_period_3_Kw__c =
                createdServicePointFields.Power_period_3_Kw__c != null
            ? createdServicePointFields.Power_period_3_Kw__c.value
            : null;
        } else if (newRow.SupplyPoint.Business_line_cd__c === "02") {
            newRow.ButtonIconName = icons.gas;
            newRow.SupplyPoint.Estimated_annual_consumption_kWh__c =
                createdServicePointFields.Estimated_annual_consumption_kWh__c != null
            ? createdServicePointFields.Estimated_annual_consumption_kWh__c.value
            : null;
        }
        newRow.Power = this.getSupplyPointPower(newRow.SupplyPoint);
        return newRow;
    },
    
    createNewRow: function() {
        return {
            AssetAccountName: "",
            AssetDisabled: false,
            BusinessLineName: "",
            ButtonDisabled: false,
            ButtonVariant: "brand",
            Id: "",
            Power: "",
            SupplyPoint: {
                Name: "",
                Business_line_cd__c: "",
                Power_period_1_Kw__c: 0,
                Power_period_2_Kw__c: 0,
                Power_period_3_Kw__c: 0,
                AddrId_Id__c: "",
                Id: ""
            },
            SupplyPointAccountName: "",
            AddressName: "",
            SupplyPointName: ""
        };
    },
    
    getResultsOnError: function(response) {
        console.group("The server returned an error.");
        this.showErrors(response.getError());
        this.showParameters(response.getParams());
        this.showReturnedValue(response.getReturnValue());
        console.groupEnd();
    },
    
    hideSpinner: function(component) {
        if (component.find("spinner") != undefined) {
            $A.util.addClass(component.find("spinner"), "slds-hide");
        }
        if (component.find("servicePointCreationForm") != undefined) {
            $A.util.removeClass(
                component.find("servicePointCreationForm"),
                "slds-hide"
            );
        }
        //component.find("servicePointCreationForm").set("v.class","slds-show");
        if (component.find("saveButton") != undefined) {
            component.find("saveButton").set("v.class", "slds-align_absolute-center");
        }
    },
    
    viewDetails: function(component, rowId) {
        var workspaceAPI = component.find("workspace");
        if (workspaceAPI != undefined) {
            if (!component.get("v.usingFromUtilityBar")) {
                workspaceAPI.getEnclosingTabId().then(function(tabId) {
                    workspaceAPI.openSubtab({
                        parentTabId: tabId,
                        recordId: rowId
                    });
                });
            } else {
                workspaceAPI.openTab({
                    recordId: rowId
                });
                if (component.find("utilityBar") != undefined) {
                    component.find("utilityBar").minimizeUtility();
                }
            }
        }
    },
    
    setBusinessLine: function(component) {
        var businessLine;
        var cupsLength = component.get("v.cups").length;
        if (cupsLength == 22) {
            businessLine = "01";
        } else if (cupsLength == 20) {
            businessLine = "02";
        }
        if (businessLine) {
            component.set("v.businessLine", businessLine);
        }
    },
    
    setDataTable: function(component, event) {
        var columns = [];
        if (!component.get("v.usingFromUtilityBar")) {
            columns.push({
                cellAttributes: {
                    alignment: "center"
                },
                fixedWidth: 50,
                type: "button-icon",
                typeAttributes: {
                    class: { fieldName: "ButtonClass" },
                    disabled: { fieldName: "ButtonDisabled" },
                    iconName: { fieldName: "ButtonIconName" },
                    name: "selectRow",
                    variant: { fieldName: "ButtonVariant" }
                }
            });
        }
        columns = columns.concat([
            {
                cellAttributes: {
                    alignment: "center"
                },
                label: "Solo No Energético",
                type: "button-icon",
                fixedWidth: 100,
                typeAttributes: {
                    variant: { fieldName: "notEnerVariant" },
                    disabled: { fieldName: "notEnerDisabled" },
                    label: { fieldName: " " },
                    title: " ",
                    name: "checkNotEnergetic",
                    iconName: { fieldName: "notEnergetic" }
                }
            },
            {
                cellAttributes: {
                    alignment: "center"
                },
                label: $A.get("$Label.c.Address"),
                type: "button",
                typeAttributes: {
                    label: { fieldName: "AddressName" },
                    name: "viewAddress",
                    variant: "base"
                }
            },
            {
                cellAttributes: {
                    alignment: "center"
                },
                fixedWidth: 190,
                label: $A.get("$Label.c.Service_Point"),
                type: "button",
                typeAttributes: {
                    label: { fieldName: "SupplyPointName" },
                    name: "viewSupplyPoint",
                    variant: "base"
                }
            },
            {
                cellAttributes: {
                    alignment: "center"
                },
                fixedWidth: 90,
                label: $A.get("$Label.c.Business_line"),
                fieldName: "BusinessLineName",
                type: "text"
            },
            {
                cellAttributes: {
                    alignment: "center"
                },
                fixedWidth: 110,
                label: $A.get("$Label.c.PowEAC"),
                fieldName: "Power",
                type: "text"
            },
            {
                cellAttributes: {
                    alignment: "center"
                },
                label: $A.get("$Label.c.Account"),
                type: "button",
                typeAttributes: {
                    label: { fieldName: "AssetAccountName" },
                    name: "viewAssetAccount",
                    variant: "base"
                }
            },
            {
                cellAttributes: {
                    alignment: "center"
                },
                fixedWidth: 190,
                label: $A.get("$Label.c.Contract"),
                type: "button",
                typeAttributes: {
                    class: { fieldName: "AssetClass" },
                    disabled: { fieldName: "AssetDisabled" },
                    label: { fieldName: "AssetName" },
                    name: "viewAsset",
                    variant: "base"
                }
            },
            {
                cellAttributes: {
                    alignment: "center"
                },
                fixedWidth: 100,
                label: $A.get("$Label.c.Contract_Type"),
                fieldName: "AssetTypeName",
                type: "text"
            },
            {
                cellAttributes: {
                    alignment: "center"
                },
                label: $A.get("$Label.c.Product"),
                type: "button",
                typeAttributes: {
                    label: { fieldName: "ProductName" },
                    name: "viewProduct",
                    variant: "base"
                }
            },
            {
                cellAttributes: {
                    alignment: "center"
                },
                fixedWidth: 100,
                label: $A.get("$Label.c.Status"),
                fieldName: "AssetStatusName",
                type: "text"
            }
        ]);
        component.set("v.resultsColumns", columns);
    },
    
    getAssetPower: function(item) {
        var result;
        switch (item.FI_ASS_SEL_BusinessLine__c) {
            case "01":
                result = this.getAssetPowerPeriod(item);
                break;
            case "02":
                result = this.getAssetPowerConsumption(item);
                break;
            default:
                // no code
        }
        return result;
    },
    
    getSupplyPointPower: function(item) {
        var result;
        switch (item.Business_line_cd__c) {
            case "01":
                result = this.getSupplyPointPowerPeriod(item);
                break;
            case "02":
                result = this.getSupplyPointPowerConsumption(item);
                break;
            default:
                // no code
        }
        return result;
    },
    
    getAssetPowerConsumption: function(item) {
        // var result = null; // Asset has no Estimated_annual_consumption_kWh__c field
        var result = item.FI_ASS_SEL_Rate__c;
        /*if (result) {
result += ' ' + $A.get("$Label.c.Consumption_Unit");
}*/
    return result;
},
    
    getSupplyPointPowerConsumption: function(item) {
        var result = item.Tariff_cd__c;
        //var result = item.Estimated_annual_consumption_kWh__c;
        /*if (result) {
result += ' ' + $A.get("$Label.c.Consumption_Unit");
}*/
        return result;
    },
    
    getAssetPowerPeriod: function(item) {
        var result = this.getHighestPowerPeriod([
            item.FI_ASS_NUM_Power_T_Hire_1__c,
            item.FI_ASS_NUM_Power_T_Hire_2__c,
            item.FI_ASS_NUM_Power_T_Hire_3__c
        ]);
        if (result) {
            result += " " + $A.get("$Label.c.Power_Unit");
        }
        return result;
    },
    
    getSupplyPointPowerPeriod: function(item) {
        var result = this.getHighestPowerPeriod([
            item.Power_period_1_Kw__c,
            item.Power_period_2_Kw__c,
            item.Power_period_3_Kw__c
        ]);
        if (result) {
            result += " " + $A.get("$Label.c.Power_Unit");
        }
        return result;
    },
    
    handleRowSelection: function(component, row) {
        var selectedRows = component.get("v.selectedRows");
        var rowIsSelected = !selectedRows.hasOwnProperty(row.Id);
        row.ButtonVariant = rowIsSelected ? "brand" : "border";
        this.selectRow(component, row, rowIsSelected);
        
        if (row.Asset) {
            if (rowIsSelected) this.showAssetSelectionToast(component);
            this.setAssetSelection(component, row, rowIsSelected);
        } else if (row.SupplyPoint) {
            if (rowIsSelected) this.showSupplyPointSelectionToast(component);
            this.setSupplyPointSelection(component, row, rowIsSelected);
        }
        this.setNextButtonDisplay(component);
    },
    
    handleRowNoEnergetic: function(component, row) {
        var selectedRows = component.get("v.selectedRows");
        var rowIsSelected = !selectedRows.hasOwnProperty(row.Id);
        row.ButtonVariant = rowIsSelected ? "brand" : "border";
        this.selectRow(component, row, rowIsSelected);
        
        if (row.notEnergetic == "utility:check") {
            row.ButtonDisabled = false;
            row.notEnergetic = "utility:steps";
            row.notEnerVariant = "border";
            //row.modifiedEnergetic = false;
        } else {
            row.notEnergetic = "utility:check";
            row.notEnerVariant = "brand";
            row.ButtonDisabled = true;
            
            //row.modifiedEnergetic = true;
        }
        this.setSupplyPointSelection(component, row, rowIsSelected);
        
        //Quitamos el id del asset si solo queremos un no energético
        component.set("v.assetId", null);
        this.setNextButtonDisplay(component);
    },
    
    selectRow: function(component, row, isSelected) {
        var selectedRows = component.get("v.selectedRows");
        
        if (isSelected) {
            selectedRows[row.Id] = row;
        } else {
            delete selectedRows[row.Id];
        }
        component.set("v.selectedRows", selectedRows);
        component.set("v.resultsSelectedRows", Object.keys(selectedRows));
    },
    
    setAssetSelection: function(component, row, isSelected) {
        var allRows = component.get("v.resultsData");
        var i;
        var rowIndex;
        var selectedRows = component.get("v.selectedRows");
        var rowId;
        
        for (i = 0; i < allRows.length; i++) {
            if (allRows[i].Id == row.Id) {
                rowIndex = i;
            } else {
                rowId = allRows[i].Id;
                if (selectedRows[rowId] == null) {
                    allRows[i].ButtonDisabled = isSelected;
                }
            }
        }
        
        allRows[rowIndex] = row;
        component.set("v.resultsData", allRows);
    },
    
    setNextButtonDisplay: function(component) {
        var selectedRows = component.get("v.selectedRows");
        if (Object.keys(selectedRows).length > 0) {
            if (component.find("restoreAccountRecords") != undefined) {
                $A.util.addClass(component.find("restoreAccountRecords"), "slds-hide");
            }
            if (component.find("addressClient") != undefined) {
                $A.util.addClass(component.find("addressClient"), "slds-hide");
            }
            if (component.find("next") != undefined) {
                $A.util.removeClass(component.find("next"), "slds-hide");
            }
        } else {
            if (component.find("next") != undefined) {
                $A.util.addClass(component.find("next"), "slds-hide");
            }
            if (component.find("restoreAccountRecords") != undefined) {
                $A.util.removeClass(
                    component.find("restoreAccountRecords"),
                    "slds-hide"
                );
            }
            if (component.find("addressClient") != undefined) {
                $A.util.removeClass(component.find("addressClient"), "slds-hide");
            }
        }
    },
    
    setNextStepVariables: function(component) {
        component.set("v.assetId", null);
        component.set("v.gasServicePointId", null);
        component.set("v.electricServicePointId", null);
        var selectedRows = component.get("v.selectedRows");
        
        for (const rowId in selectedRows) {
            if (
                selectedRows[rowId].Asset &&
                selectedRows[rowId].notEnergetic != "utility:check"
            ) {
                component.set("v.assetId", selectedRows[rowId].Id);
            } else {
                if (selectedRows[rowId].SupplyPoint.Business_line_cd__c === "01") {
                    component.set(
                        "v.electricServicePointId",
                        selectedRows[rowId].SupplyPoint.Id
                    );
                    if (selectedRows[rowId].notEnergetic == "utility:check")
                        component.set("v.isElectricityContract", true);
                }
                if (selectedRows[rowId].SupplyPoint.Business_line_cd__c === "02") {
                    component.set(
                        "v.gasServicePointId",
                        selectedRows[rowId].SupplyPoint.Id
                    );
                    if (selectedRows[rowId].notEnergetic == "utility:check")
                        component.set("v.isGasContract", true);
                }
            }
        }
    },
    
    setRecordEditFormFields: function(component, event) {
        var fields = event.getParam("fields");
        
        fields.Business_line_cd__c = component.get(
            "v.modalPopulatedFields"
        ).Business_line_cd__c;
        
        var isFirstOccupation = !this.getSupplyPointPower(fields);
        
        fields.X1_Ocupaci_n__c = isFirstOccupation ? "Si" : "No";
        var cups = fields["Name"] ? fields["Name"] : "";
        component.set("v.cups", cups);
        if (cups === "PENDIENTE_INFORMAR") {
            /*fields.Reason_creation__c = isFirstOccupation
            ? "Alta 1º ocupación sin cups"
            : "No localizado";*/
        } else {
            fields.CUPS_Consulted_SVE__c = cups;
        }
        
        component.set("v.recordEditFormFields", fields);
    },
    setSupplyPointSelection: function(component, row, isSelected) {
        var allRows = component.get("v.resultsData");
        var i;
        var rowIndex;
        var selectedRows = component.get("v.selectedRows");
        var rowId;
        var toastCNAEIsOpen = false;
        for (i = 0; i < allRows.length; i++) {
            if (allRows[i].Id == row.Id) {
                rowIndex = i;
            } else {
                var asset = allRows[i].Asset;
                var supplyPoint = allRows[i].SupplyPoint;
                var addressId;
                var businessLine;
                if (supplyPoint) {
                    addressId = supplyPoint.AddrId_Id__c;
                    businessLine = supplyPoint.Business_line_cd__c;
                }
                if (
                    asset ||
                    (businessLine === row.SupplyPoint.Business_line_cd__c ||
                     addressId !== row.SupplyPoint.AddrId_Id__c)
                ) {
                    rowId = allRows[i].Id;
                    if (selectedRows[rowId] == null) {
                        allRows[i].ButtonDisabled = isSelected;
                    }
                }
            }
        }
        component.find("next").set("v.class" , "");
        allRows[rowIndex] = row;
        component.set("v.resultsData", allRows);
        if(row.SupplyPoint.AddrId_Id__c === undefined){
            if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message: "Debe asignar una dirección al punto de suministro para poder continuar.",
                variant: "error"
            };
            component.find("notifLib").showToast(toastProperties);
                component.set("v.allowNextStep", false);
        }
        } else if(row.SupplyPoint.CNAE__c === undefined){
            console.log('Entrada if CNAE vacio');
            this.showNoCNAEToast(component);
            component.set("v.allowNextStep", false);
        } else {
            component.set("v.allowNextStep", true);
        }
    },
    showNoCNAEToast: function(component, buttonPSIsSelected) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message: $A.get("{!$Label.c.FI_Cl_No_CNAE_Alert}")
                ,
                variant: "error"
            };
            if(!component.get("v.buttonPSIsSelected")){
                component.find("notifLib").showToast(toastProperties);
                component.set("v.buttonPSIsSelected", true);
            } else {
                component.set("v.buttonPSIsSelected", false);
            }
        }
    },
    
    setTableRows: function(component, tableRowWrapperList) {
        if (
            !component.get("v.usingFromUtilityBar") ||
            tableRowWrapperList.length > 0
        ) {
            const icons = component.get("v.ICONS");
            var tableRows = [];
            var i;
            
            for (i = 0; i < tableRowWrapperList.length; i++) {
                var tableRow = {
                    AssetDisabled: false,
                    ButtonDisabled: false,
                    ButtonVariant: "border",
                    Id: tableRowWrapperList[i].Id,
                    SupplyPointInProgress: tableRowWrapperList[i].SupplyPointInProgress
                };
                tableRow.doSVE = true;
                
                var asset = tableRowWrapperList[i].Asset;
                if (asset) {
                    tableRow.doSVE = false;
                    var assetProperties = {
                        Asset: asset,
                        AssetName: asset.Name,
                        AssetType: asset.FI_ASS_SEL_CRM_Agree_Type__c,
                        AssetTypeName: asset.AssetTypeName,
                        AssetStatusName: asset.AssetStatusName,
                        BusinessLineName: asset.BusinessLineName,
                        ButtonIconName: icons.asset,
                        Power: this.getAssetPower(asset)
                    };
                    
                    if (asset.AccountId) {
                        assetProperties.AssetAccountName = asset.Account.Name;
                    }
                    if (asset.NE__ProdId__c) {
                        assetProperties.ProductName = asset.NE__ProdId__r.Name;
                    }
                    Object.assign(tableRow, assetProperties);
                } else if (tableRowWrapperList[i].AssetInB2B) {
                    Object.assign(tableRow, {
                        AssetDisabled: true,
                        AssetTypeName: "B2B",
                        AssetStatusName: $A.get("{!$Label.c.Active}"),
                        ButtonClass: "slds-hide",
                        ButtonDisabled: true
                    });
                }
                
                var supplyPoint = tableRowWrapperList[i].SupplyPoint;
                if (supplyPoint) {
                    var BusinessLineNameVar;
                    switch (supplyPoint.Business_line_cd__c) {
                        case "01":
                            component.set("v.newElectricSupplyPointButtonDisabled", true);
                            BusinessLineNameVar = component.get("v.pickListValuesBusinessLine")['01'];
                            break;
                        case "02":
                            component.set("v.newGasSupplyPointButtonDisabled", true);
                            BusinessLineNameVar = component.get("v.pickListValuesBusinessLine")['02'];
                            break;
                        default:
                            // no code
                    }
                    supplyPoint.Business_line_cd__c
                    var supplyPointProperties = {
                        BusinessLineName: BusinessLineNameVar,
                        //'Power': this.getSupplyPointPower(supplyPoint),
                        SupplyPoint: supplyPoint,
                        SupplyPointName: supplyPoint.Name
                    };
                    
                    if (!asset) {
                        if(supplyPoint.BusinessLineName != undefined){
                            supplyPointProperties.BusinessLineName = supplyPoint.BusinessLineName;
                        }
                        supplyPointProperties.Power = this.getSupplyPointPower(supplyPoint);
                        switch (supplyPoint.Business_line_cd__c) {
                            case "01":
                                supplyPointProperties.ButtonIconName = icons.electric;
                                supplyPointProperties.BusinessLineName = BusinessLineNameVar = component.get("v.pickListValuesBusinessLine")['01'];
                                break;
                            case "02":
                                supplyPointProperties.ButtonIconName = icons.gas;
                                supplyPointProperties.BusinessLineName = BusinessLineNameVar = component.get("v.pickListValuesBusinessLine")['02'];
                                
                                break;
                            default:
                                // no code
                        }
                    }else{
                        console.log('FJDM ntrada if CNAE vacio'+asset.BusinessLineName+'  '+component.get("v.pickListValuesBusinessLine")['03'] );
                        
                                supplyPointProperties.BusinessLineName = asset.BusinessLineName;
                            
                    }
                    
                    supplyPointProperties.AddressName = supplyPoint.AddrId_Id__c
                    ? supplyPoint.AddrId_Id__r.Name
                    : supplyPoint.Adress__c;
                    Object.assign(tableRow, supplyPointProperties);
                }
                tableRows.push(tableRow);
            }
            
            tableRows.sort(function(a, b) {
                var sortBy = [
                    { column: "AddressName", sort: "ASC" },
                    { column: "AssetType", sort: "ASC" },
                    { column: "BusinessLineName", sort: "ASC" }
                ];
                var result = 0;
                var i = 0;
                while (result === 0 && i < sortBy.length) {
                    var column = sortBy[i]["column"];
                    var sort = sortBy[i]["sort"];
                    
                    if (a[column] > b[column]) {
                        switch (sort) {
                            case "ASC":
                                result = 1;
                                break;
                            case "DESC":
                                result = -1;
                                break;
                            default:
                                result = 1;
                        }
                    } else if (a[column] < b[column]) {
                        switch (sort) {
                            case "ASC":
                                result = -1;
                                break;
                            case "DESC":
                                result = 1;
                                break;
                            default:
                                result = -1;
                        }
                    } else {
                        i++;
                    }
                }
                return result;
            });
            
            component.set("v.resultsData", tableRows);
        } else {
            component.set("v.dataLoaded", false);
        }
    },
    
    showErrors: function(errors) {
        console.group("Errors:");
        for (const error of errors) {
            console.log(error.stackTrace);
            console.log(error.message);
        }
        console.groupEnd();
    },
    
    showParameters: function(parameters) {
        console.group("Parameters:");
        var name;
        for (name in parameters) {
            console.log(name + ": " + parameters[name]);
        }
        console.groupEnd();
    },
    
    showReturnedValue: function(value) {
        console.group("Returned value:");
        console.log(value);
        console.groupEnd();
    },
    
    showAddressesWereNotFoundToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message: $A.get(
                    "$Label.c.AddressesWereNotFoundPleaseCreateANewAddress"
                ),
                variant: "info"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showAddressNotNormalizedToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message: $A.get("$Label.c.AddressNotNormalized"),
                variant: "info"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showAvailablePSToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                title: $A.get("$Label.c.SP").replace("{0}", component.get("v.cups")),
                message: $A.get("$Label.c.TheSPDoesNotHaveAnActiveContractInSVE"),
                variant: "success"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showNoSVEConnectionToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                title: $A.get("$Label.c.InternalError"),
                message: $A.get(
                    "$Label.c.ThereIsNoConnectionWithSVECouldNotQueryTheSystemWouldYouLikeToContinue"
                ),
                variant: "error"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showSpinner: function(component) {
        if (component.find("saveButton") != undefined) {
            component.find("saveButton").set("v.class", "slds-hide");
        }
        if (component.find("servicePointCreationForm") != undefined) {
            component.find("servicePointCreationForm").set("v.class", "slds-hide");
        }
        if (component.find("spinner") != undefined) {
            $A.util.removeClass(component.find("spinner"), "slds-hide");
        }
    },
    
    showSupplyPointHasRequestInProgressToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message: $A.get("$Label.c.Requests_in_flight"),
                variant: "info"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    verifySupplyPointInProgress: function (component, event, helper, row){
        var psId = row.SupplyPoint.Id;
        var psBussinessLine;
        if(row.Asset)
            psBussinessLine = row.Asset.BusinessLineName;   
        else
            psBussinessLine = row.BusinessLineName;
        
        var action = component.get("c.getConfigurationItemInProgress");
        action.setParams({
            psId: psId,
            businessLineName : psBussinessLine
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log(' JVGG -- Entra verifySupplyPointInProgress, PSiD '+psId+' BussinnesLine '+psBussinessLine);
            if (state === "SUCCESS" && response.getReturnValue()) {
                
                this.showSupplyPointHasRequestInProgressToast(component);
            } else if(state === "SUCCESS" && !response.getReturnValue()){                
                this.handleRowSelection(component, row);
            } else if (["INCOMPLETE", "ERROR"].includes(state)) {
                if (component.find("notifLib") != undefined) {
                    toastProperties = {
                        title: cupsResponse,
                        message: 'Ocurrio un error al consultar sobre el registro. Por favor contacte con un administrador',
                        variant: "error"
                    };
                    component.find("notifLib").showToast(toastProperties);
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    
    showSVECallErrorToast: function(component, cupsResponse) {
        var toastProperties;
        if (!cupsResponse.includes("ERROR")) {
            toastProperties = {
                title: $A.get("$Label.c.SP").replace("{0}", cupsResponse),
                message: $A.get(
                    "$Label.c.ItWasNotPossibleToCreateTheSupplyPointBecauseItHasAnActiveContractInSVE"
                ),
                variant: "warning"
            };
        } else {
            toastProperties = {
                title: cupsResponse,
                message: $A.get(
                    "$Label.c.AnErrorHasOccurredQueryingSVEPleaseContactYourSystemAdministrator"
                ),
                variant: "error"
            };
        }
        if (component.find("notifLib") != undefined) {
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showtoast: function(toastProperties) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastProperties);
        toastEvent.fire();
    },
    
    showUnexpectedErrorToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                title: "Error",
                message: $A.get(
                    "$Label.c.AnUnexpectedErrorHasOccurredPleaseContactYourSystemAdministrator"
                ),
                variant: "error"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showWrongCUPSToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                title: $A.get("$Label.c.SP").replace("{0}", "vacio"),
                message: $A.get("$Label.c.TheSPMustBelongToAValidDistributor"),
                variant: "error"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showWrongCUPSLengthToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                title: $A.get("$Label.c.FormatError"),
                message: $A.get(
                    "$Label.c.TheEnteredUSPCLengthIsWrongForTheSelectedType"
                ),
                variant: "error"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showAssetSelectionToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message: $A.get("$Label.c.YouHaveSelectedAContract"),
                variant: "success"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showSupplyPointSelectionToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message: $A.get("$Label.c.YouHaveSelectedASupplyPoint"),
                variant: "success"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showNoSVEToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message:
                "Llamada a SVE no disponible. Seleccione o cree una direccion para continuar el proceso",
                variant: "warning"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    showPendienteInformarToast: function(component) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                message:
                "No se puede realizar la busqueda para el valor PENDIENTE_INFORMAR, seleccione o cree una direccion para continuar con el proceso.",
                variant: "warning"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    //everis (ART) -- Community adaptation for community users
    getAddressName: function(component, psId) {
        var action = component.get("c.getAddrName");
        action.setParams({
            psId: psId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.addrName", response.getReturnValue());
            } else {
                console.log(state);
            }
        });
        $A.enqueueAction(action);
    },
    
    showCreatedPS: function(component, message) {
        if (component.find("notifLib") != undefined) {
            var toastProperties = {
                title: $A.get("$Label.c.SP").replace("{0}", component.get("v.cups")),
                //message: $A.get('$Label.c.TheSPDoesNotHaveAnActiveContractInSVE'),
                message: message,
                variant: "success"
            };
            component.find("notifLib").showToast(toastProperties);
        }
    },
    
    getPicklistValuesB2CServicePointBusinessLine: function(component, event) {
        var action = component.get("c.getPicklistValues");
        action.setParams({ 
            objectApiname : 'B2C_Service_Point__c',
            fieldName: 'Business_line_cd__c'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.pickListValuesBusinessLine", response.getReturnValue());
                console.log(' JVGG '+ JSON.stringify(component.get("v.pickListValuesBusinessLine")));
                var usingFromUtilityBar = component.get("v.usingFromUtilityBar");
                if(!usingFromUtilityBar)
                {
                     this.buildMethod(component);
                }               
                this.setDataTable(component, event);
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action); 
    }
});