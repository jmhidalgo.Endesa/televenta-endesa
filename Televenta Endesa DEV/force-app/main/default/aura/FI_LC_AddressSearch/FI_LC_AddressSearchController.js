({
    doInit: function(component, event, helper) {
        
        if (typeof component.get("v.accountId") === "undefined") {
            component.set("v.usingFromUtilityBar", true);
            component.set("v.dataLoaded", true);
            component.find("spinner").set("v.class", "slds-hide");
        } else {
            component.set("v.recId", component.get("v.accountId"));
            component.set("v.selectedRows", {});
            component.set("v.methodName", "getRecordsByAccountId");
          
        }
        component.set("v.columns", [
            {
                label: $A.get("$Label.c.Address"),
                type: "button",
                typeAttributes: {
                    label: { fieldName: "Name" },
                    name: "selectRow",
                    variant: "base"
                }
            }
        ]);
        helper.getPicklistValuesB2CServicePointBusinessLine(component, event);
      
        
    },
    
     getResultsOfPS : function(component, event, helper) {
        
        component.set("v.newElectricSupplyPointButtonDisabled", false);
		component.set("v.newGasSupplyPointButtonDisabled", false);
        component.set("v.selectedRows", {});
        component.find("next").set("v.class" , "slds-hide");
        component.set("v.dataLoaded", true);
        component.set("v.mainSection", false);
        component.set("v.resultsSection", true);
   
        var spinner = component.find("spinner");        
		var selectedAddressLookup = component.get("v.selectedAddressLookup"); 
		component.set("v.selectedAddress", selectedAddressLookup);
        $A.util.removeClass(spinner, "slds-hide");
        var action = component.get("c.getRecordsByAddressName");        
        action.setParams({
            searchText: selectedAddressLookup.Name
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();            
            if (state === "SUCCESS") {
                helper.setTableRows(component, response.getReturnValue());                 
            } else{
                helper.getResultsOnError(response);
                helper.showUnexpectedErrorToast(component);
            }
            var spinner = component.find("spinner");        
        	$A.util.addClass(spinner, "slds-hide");
        });
        $A.enqueueAction(action);
    },
    
    createSupplyPoint: function(component, event, helper) {
        var caseIsValid = true;
        var modalPopulatedFields = {};
        var recordDeveloperName;
        switch (event.getSource().getLocalId()) {
            case "Electric":
                modalPopulatedFields.Business_line_cd__c = "01";
                recordDeveloperName = "Electricidad";
                component.set(
                    "v.fieldSetLayout",
                    "ContractingServicePointElectricCreate"
                );
                break;
            case "Gas":
                modalPopulatedFields.Business_line_cd__c = "02";
                recordDeveloperName = "Gas";
                component.set("v.fieldSetLayout", "ContractingServicePointGasCreate");
                break;
            default:
                caseIsValid = false;
        }
        if (caseIsValid) {
            modalPopulatedFields.Account_id__c = component.get("v.accountId");
            modalPopulatedFields.AddrId_Id__c = component.get("v.selectedAddress").Id;
            component.set("v.modalPopulatedFields", modalPopulatedFields);
            helper.createServicePoint(component, recordDeveloperName);
        }
    },
    
    handleRowAction: function(component, event, helper) {
        var row = event.getParam("row");
        var action = event.getParam("action");
        console.log(' row '+JSON.stringify(row));
        switch (action.name) {
            case "selectRow":
                if (row.SupplyPointInProgress) {
                    helper.verifySupplyPointInProgress(component, event, helper, row);
                    //helper.showSupplyPointHasRequestInProgressToast(component);
                } else {
                    component.set("v.cups", row.SupplyPointName);
                    component.set("v.doSVE", row.doSVE);
                    helper.handleRowSelection(component, row);
                }
                break;
            case "checkNotEnergetic":
                    var selectedRows = component.get("v.selectedRows");
                    var rowIsSelected = !selectedRows.hasOwnProperty(row.Id);
                    if (
                        row.Asset &&
                        row.Asset.AccountId != "" &&
                         // Se comenta condición para permitir "Solo energético" con diferente cliente
                         /*row.Asset.AccountId == component.get("v.account")["Id"]) &&*/
                        (row.Asset.FI_ASS_SEL_BusinessLine__c == "01" ||
                         row.Asset.FI_ASS_SEL_BusinessLine__c == "02") &&
                        (rowIsSelected || row.notEnergetic == "utility:check") &&
                        row.Asset.FI_ASS_SEL_HolderCompany__c != "70"
                    ) {
                        helper.handleRowNoEnergetic(component, row);
                    }
                    if(row.Asset){
                        if(row.Asset.FI_ASS_SEL_HolderCompany__c == "70"){
                            var toastProperties = {
                                message:
                                "No se puede contratar un STP o PSVA en un Punto Suministro con un producto de Energía XXI",
                                variant: "error"
                            };
                            component.find("notifLib").showToast(toastProperties);
                        }
                    }
                    
                break;
            case "viewAddress":
                var addressId = row.SupplyPoint.AddrId_Id__c;
                if (addressId) {
                    helper.viewDetails(component, addressId);
                } else {
                    helper.showAddressNotNormalizedToast(component);
                }
                break;
            case "viewAssetAccount":
                helper.viewDetails(component, row.Asset.AccountId);
                break;
            case "viewAsset":
                helper.viewDetails(component, row.Asset.Id);
                break;
            case "viewSupplyPoint":
                helper.viewDetails(component, row.SupplyPoint.Id);
                break;
            case "viewProduct":
                helper.viewDetails(component, row.Asset.NE__ProdId__c);
                break;
            default:
                // no code
        }
    },
    
    newAddress: function(component, event, helper) {
        if (component.get("v.isCommunity")) {
            component.set("v.showNewDir", true);
        } else {
            var evt = $A.get("e.force:navigateToComponent");
            console.log("EVENT", evt);
            evt.setParams({
                componentDef: "c:B2C_Address_Add_Controller",
                componentAttributes: {
                    recordId: component.get("v.accountId"),
                    sObjectName: "Account"
                }
            });
            evt.fire();
        }
    },
    
    getAddresses: function(component, event, helper) {
        if (
            event.getParams().keyCode === component.get("v.ENTER_KEY_CODE") ||
            event.getSource().get("v.name") == "search"
        ) {
            component.set("v.supplyPointChecked", false);
            helper.clearAdresses(component);
            helper.getAddresses(component);
        }
    },
    
    getResults: function(component, event, helper) {
        component.set("v.newElectricSupplyPointButtonDisabled", false);
        component.set("v.newGasSupplyPointButtonDisabled", false);
        component.set("v.selectedRows", {});
        component.find("next").set("v.class", "slds-hide");
        component.set("v.dataLoaded", true);
        component.set("v.mainSection", false);
        component.set("v.resultsSection", true);
        
        var spinner = component.find("spinner");
        var row = event.getParam("row");
        component.set("v.selectedAddress", row);
        
        var spinner = component.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");
        
        var action = component.get("c.getRecordsByAddressId");
        action.setParams({
            AddressId: row.Id
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.setTableRows(component, response.getReturnValue());
            } else {
                helper.getResultsOnError(response);
                helper.showUnexpectedErrorToast(component);
            }
            var spinner = component.find("spinner");
            $A.util.addClass(spinner, "slds-hide");
        });
        $A.enqueueAction(action);
    },
    
    handleSupplyPointError: function(component, event, helper) {
        helper.hideSpinner(component);
        var payload = event.getParams().error.body.output.errors[0];
        var fieldErrors = event.getParams().error.body.output.fieldErrors;
        
        if (payload) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: payload.message,
                type: "error"
            });
            toastEvent.fire();
        } else if (fieldErrors) {
            for (var fieldName in fieldErrors) {
                console.log(fieldName);
                for (var fieldError of fieldErrors[fieldName]) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        message: fieldError.message,
                        type: "error"
                    });
                    toastEvent.fire();
                }
            }
        } else {
            helper.showUnexpectedErrorToast(component);
        }
    },
    
    handleSupplyPointSubmit: function(component, event, helper) {
        //ART
        helper.getAddressName(component, event.getParams().fields.AddrId_Id__c);
        //--
        var reason = event.getParams().fields.Reason_creation__c;
        var power = event.getParams().fields.Power_period_1_Kw__c;
        var linea = event.getParams().fields.Business_line_cd__c;
        var cupsname = event.getParams().fields.Name;
        if("PENDIENTE_INFORMAR" != cupsname && 
           cupsname != component.get('v.cupsValidated')){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": $A.get("$Label.c.ERROR_TOAST_TITLE"),
                "message": "Debe validar el CUPS antes de guardar.", //
                "type": "error",
                "mode": "sticky"
            });
            toastEvent.fire();
            event.preventDefault();
            return false;
        } else if(reason=='Alta 1º ocupación sin cups' && power && linea=='01'){
            if(power!=0){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.ERROR_TOAST_TITLE"),
                    "message": "No se puede informar la potencia si es una primera ocupacion.", //
                    "type": "error",
                    "mode": "sticky"
                });
                toastEvent.fire();
                event.preventDefault();
                return false;
            }
        }else if(reason=='No localizado' && (!power || power==0 )&& linea=='01'){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": $A.get("$Label.c.ERROR_TOAST_TITLE"),
                "message": "Se debe informar la primera potencia si no es una primera ocupacion.",
                "type": "error",
                "mode": "sticky"
            });
            toastEvent.fire();
            event.preventDefault();
            return false;
        }else if((reason=='' || reason==null) && linea=='01'){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": $A.get("$Label.c.ERROR_TOAST_TITLE"),
                "message": "El campo Primera Ocupación es obligatorio.",
                "type": "error",
                "mode": "sticky"
            });
            toastEvent.fire();
            event.preventDefault();
            return false;
        }
        helper.setRecordEditFormFields(component, event);
        //event.preventDefault();
        
        //everis (ART) Se añade el NOSVETOAST para que se salte la validación de SVE en el callback de checkDistributor y no muestre el toast de SVE
        //helper.checkDistributor(component, "NOTOAST");
        //everis (ART) Ahora esta comprobación se hace al seleccionar un PS y darle a "Siguiente".
        //helper.checkServicePointAvailability(component, false);
    },

    validateCUPS: function(component, event, helper){
        console.log('validateCUPS');
        //helper.setRecordEditFormFields(component, event);
        helper.callSVEPScreate(component);
    },
    
    handleSupplyPointSuccess: function(component, event, helper) {
        var rows = component.get("v.resultsData");
        var newRow = helper.getNewRow(component, event);
        component.set("v.resultsData", [newRow].concat(rows));
        helper.handleRowSelection(component, newRow);
        
        switch (newRow.SupplyPoint.Business_line_cd__c) {
            case "01":
                component.set("v.newElectricSupplyPointButtonDisabled", true);
                break;
            case "02":
                component.set("v.newGasSupplyPointButtonDisabled", true);
                break;
            default:
                // no code
        }
        
        helper.closeModal(component);
    },
    
    nextStep: function(component, event, helper) {
        helper.checkServicePointAvailability(component, true);
        helper.setNextStepVariables(component);
    },
    
    restoreAccountRecords: function(component, event, helper) {
        component.set("v.selectedAddress", null);
        component.set("v.resultsData", component.get("v.accountRecords"));
    },
    
    addressClient: function(component, event, helper) {
        component.set("v.supplyPointChecked", false);
        component.set("v.methodName", "getAddressesByAccountId");
        helper.clearAdresses(component);
        helper.buildMethod(component);
    }
});