({
  getAllMethods: function(component, event, helper) {
    var action = component.get("c.getAllMethods");
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var data = [];
        var staticLabel = $A.get("$Label.c.TV_CL_SelectMethod");
        data.push({ label: staticLabel, value: "" });
        response.getReturnValue().forEach(function(arrayItem) {
          var arr = arrayItem.split("#");
          data.push({ label: arr[1], value: arr[0] });
        });
        component.set("v.selectionList", data);
      }
    });
    $A.enqueueAction(action);
  },

  getPermissionProfile:function(component,event,helper){
    var action = component.get("c.checkProfile");
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var profileOk = response.getReturnValue();
        if(profileOk){
          component.set("v.permissionProfile",true);
        }else{
          component.set("v.permissionProfile",false);
        }
      }
    });
    $A.enqueueAction(action);
  },

  reSign: function(component,event,helper){
    var action = component.get("c.reSigned");
    var dateSelected = component.find("dateResigned").get("v.value");
    action.setParams({
      caseId: component.get("v.recordId"),
      dateSelected:dateSelected
    });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var isUpdated = response.getReturnValue();
        if(isUpdated == 'TRUE'){
          this.showToast(
            "Reconfirmar firma",
            "\n" +
              "Se ha reconfirmado la firma.",
            "success"
          );
          component.set("v.pressedButtonRS",false);
        }else{
          if(isUpdated == 'FALSE'){
            this.showToast(
              "Reconfirmar firma",
              "\n" +
                "No hay CIs que reconfirmar o la necesidad no se ha firmado con texto legal.",
              "error"
            );
          }else{
            if(isUpdated == 'EMPTY'){
              this.showToast(
                "Reconfirmar firma",
                "\n" +
                  "Debes añadir una fecha.",
                "warning"
              );
            }
          }
        }
      }
    });
    $A.enqueueAction(action);
  },

  updateTypeSign: function(component, event, helper) {
    var action = component.get("c.updTypeSign");
    action.setParams({
      caseId: component.get("v.recordId")
    });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        this.showToast(
          "Actualización necesidad",
          "\n" +
            "Se ha actualizado el tipo de firma de la necesidad y de los CIs correctamente.",
          "success"
        );
      }
    });
    $A.enqueueAction(action);
  },

  getTextLegal: function(component, event, helper) {
    var action = component.get("c.getLegalTextConga");
    action.setParams({
      caseId: component.get("v.recordId")
    });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var wrapper = response.getReturnValue();
        console.log("ABS wrapper.legalText: " + wrapper.legalText);
        console.log("ABS wrapper.urlConga: " + wrapper.urlCongaPDF);
        component.set("v.textLegal", wrapper.legalText);
        component.set("v.urlConga",wrapper.urlCongaPDF);
        if(wrapper.legalText != ''  && wrapper.urlCongaPDF != ''){
          var interactionInCourse = component.get("v.interaction");
          if (interactionInCourse != null) {
            var subtipoTarea = interactionInCourse.FII_ACT_SEL_SubTipoTarea__c;
            if(subtipoTarea != $A.get("$Label.c.TV_Label_ContingencyMode")){

              var initGrabacion = component.get("c.initGrabacion");
              $A.enqueueAction(initGrabacion);
            }
            else{
              this.showToast(
                "Error",
                "No se puede iniciar la grabación bajo demanda con interacciones simuladas",
                "error"
              );
            }
          }

          component.set("v.isRecordEnd", false);
          component.set("v.disableStart", true);
          component.set("v.disableCancelEnd", false);
        }else{ 
          this.showToast(
            "Error en TXL",
            "\n" +
              "Ha ocurrido un problema al obtener los textos legales, por favor seleccione otro metodo de firma.",
            "error"
          );
          component.set("v.isReadingLegalTextModalOpen", false);
          component.set("v.disableCancelEnd", true);
          component.set("v.textLegal", "");
        }
      }
    });
    $A.enqueueAction(action);
  },

  sign: function(component,event,helper){
    var selectedSign = component.find("signatureMethods").get("v.value");
    var action = component.get("c.signCaseCIs");
    action.setParams({
      caseId: component.get("v.recordId"),
      selectedSign: selectedSign
    });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var status = response.getReturnValue();
        if(status == 'SIGNED'){
          this.showToast(
            "CIs Firmados",
            "\n" +
              "Al menos hay un CI firmado o pendiente de firma. No se puede utilizar la firma desde la necesidad.",
            "warning"
          );
        }else{
          if(status == 'SUCCESS'){
            this.showToast(
              "CIs Firmados",
              "\n" +
                "Se han firmado la necesidad y todos los CIs.",
              "success"
            );
          }else{
            if(status == 'ERROR'){
              this.showToast(
                "Error en la firma",
                "\n" +
                  "No se ha podido firmar desde la necesidad, pruebelo desde los CIs.",
                "success"
              );
            }
          }
        }
      }
    });
    $A.enqueueAction(action);
  },

  checkBeforeSign: function(component, event, helper) {
    var action = component.get("c.checkCIAndAddress");
    action.setParams({
      recordId: component.get("v.recordId")
    });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var wrapper = response.getReturnValue();
        if (wrapper.statusKo) {
          var statusKo = $A.get("$Label.c.TV_CL_SignPending");
          this.showToast("Estados CIs", "\n" + statusKo, "warning");
        }

        if (wrapper.differentAddress == 0) {
          this.showToast(
            "CIs Incorrectos",
            "\n" +
              "Los CIs no tienen direcciones o los CIs no estan en el estado correcto.",
            "warning"
          );
        }

        if (wrapper.differentAddress > 1) {
          var multi = $A.get("$Label.c.TV_CL_MultiAddress");
          this.showToast("CIs multipunto", "\n" + multi, "warning");
        }
        if (wrapper.validationOk != "") {
          this.showToast(
            "Validaciones OK CIs",
            "\n" +
              "Los CIs que no estan pasando las validaciones son: " +
              wrapper.validationOk +
              ".",
            "warning"
          );
        }
        if (
          wrapper.statusKo == false &&
          wrapper.differentAddress == 1 &&
          wrapper.validationOk == ""
        ) {
          if (selectedSign != "") {
            var selectedSign = component.find("signatureMethods").get("v.value");
            component.set("v.selectedSignature", selectedSign);
            component.set("v.disableStart", false);
            component.set("v.isReadingLegalTextModalOpen", true);
          } else {
            component.set("v.blankTypeFirm", true);
          }
        }
      }
    });
    $A.enqueueAction(action);
  },
  updateNeedAndCI: function(component, event, helper, selectedSign) {
    var action = component.get("c.updateNeedCI");
    action.setParams({
      recordId: component.get("v.recordId"),
      selectedSign: selectedSign
    });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
      }
    });
    $A.enqueueAction(action);
  },
  permissionOk: function(component, event, helper) {
    var action = component.get("c.permissionOK");
    action.setParams({
      caseId: component.get("v.recordId")
    });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var wrapper = response.getReturnValue();
        if (!wrapper.interactionInProgress) {
          this.showToast(
            "Interacción en curso",
            "\n" + "No hay ninguna interacción en curso.",
            "error"
          );
        }
        if (!wrapper.channelOk) {
          this.showToast(
            "Canales de televenta",
            "\n" +
              "El canal que tiene tu puesto de trabajo no es válido para televenta.",
            "error"
          );
        }

        if (wrapper.interactionInProgress && wrapper.channelOk){
        //if (wrapper.channelOk) {
          component.set("v.permissionOk", true);
        }
      }
    });
    $A.enqueueAction(action);
  },
  showToast: function(title, message, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      title,
      message,
      mode: "dismissible", // 'pester', 'sticky', 'dismissible'
      type, // 'error', 'warning', 'success', 'info'
      duration: "5000"
    });
    toastEvent.fire();
  },
  getLoginInfo: function(component, event, helper) {
    var actionGetLoginInfo = component.get("c.getLoginData");
    actionGetLoginInfo.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS" && response.getReturnValue() != null) {
        component.set("v.loginUser", response.getReturnValue().UserName__c);
        component.set("v.loginPassword", response.getReturnValue().Password__c);
        component.set("v.endpointIcws", response.getReturnValue().EndPoint__c);
      } else if (state === "ERROR") {
        helper.showErrorToast(
          "Error",
          "Error al obtener datos de inicio de sesión para conectar con ICWS"
        );
        console.log(
          "Error: Error obteniendo metadata de inicio de sesión en ICWS"
        );
      }
    });
    $A.enqueueAction(actionGetLoginInfo);
  },


  record: function(component, event, helper) {
    //var from = helper.getParameterByName(component , event, 'ws', window.location.href);
    //var interactionId = helper.getParameterByName(component , event, 'c__interactionid', from);
    //var interactionId = helper.getParameterByName(component , event, 'GNS__callidkey', from);
    //console.log('HCONSOLE.LOG: from= ' + from);
    //no Sirven las anteriores

    /*var urli = new URL(window.location.href);
    var interactionId = urli.searchParams.get("GNS__callidkey");
    console.log(
      "HCONSOLE.LOG: interactionId= " + interactionId.substring(0, 10)
    );*/

    //tienes que consultar el objeto de la vista llamado interaccion y sacar el campo CallObject para hacerle el substring.
    let sObjs = component.get("v.interaction");
    if (sObjs.CallObject != null) {
      var interactionId = sObjs.CallObject;

      var handlerParams = {
        objectId: "ROD_CallScripter",
        eventId: "ROD_CallScripter",
        data: [interactionId.substring(0, 10)]
      };

      console.log(
        "HCONSOLE.LOG: genSessionId= " + component.get("v.genSessionId")
      );

      //var url = "https://svc-csf-cert/client/api/svc-cic-cert.inin-endesa-v4.local/icws/" + component.get('v.genSessionId') + "/system/handler-notification";
      /*var url =
        "https://svc-csf-cert.inin-endesa-v4.com/client/api/SVC-CIC-CERT.ININ-ENDESA-V4.local/icws/" +
        component.get("v.genSessionId") +
        "/system/handler-notification";*/
        
        var url = component.get("v.endpointIcws") +
        "client/api/SVC-CIC-CERT.ININ-ENDESA-V4.local/icws/" +
        component.get("v.genSessionId") +
        "/system/handler-notification";

      console.log("HCONSOLE.LOG: url= " + url);

      console.log("HCONSOLE.LOG: genToken= " + component.get("v.genToken"));

      var xhr = new XMLHttpRequest();
      xhr.withCredentials = true;
      xhr.open("post", url, true);
      xhr.setRequestHeader("ININ-ICWS-CSRF-Token", component.get("v.genToken"));
      xhr.setRequestHeader("Content-Type", "application/vnd.inin.icws+JSON");

      xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
          if (xhr.status == 200 || xhr.status == 201 || xhr.status == 202) {
            console.log("Grabacion bajo demanda iniciada");
            if (component.get("v.isRecordEnd"))
              helper.showError(
                "Grabacion bajo demanda finalizada",
                "Success",
                "success"
              );
            else
              helper.showError(
                "Grabacion bajo demanda iniciada",
                "Success",
                "success"
              );
          } else {
            helper.showError(
              "Error al iniciar la grabación bajo demanda",
              "Error",
              "error"
            );
          }
        }
      };

      xhr.send(JSON.stringify(handlerParams));
    }
  },
  showError: function(message, title, type) {
    var toastEvent = $A.get("e.force:showToast");

    toastEvent.setParams({
      title: title,
      duration: 2000,
      type: type,
      message: message,
      mode: "pester"
    });

    toastEvent.fire();
  },
  getParameterByName: function(component, event, paramName, url) {
    paramName = paramName.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + paramName + "(=([^&#]*)|&|#|$)");
    var results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return "";
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  },
  getAllInteractionsOpened: function(component) {
    var interaction = component.get("c.getInteractions");
    //conseguir el id del cliente
    interaction.setParams({
      accountId: component.get("v.accountId")
    });

    interaction.setCallback(this, function(response) {
      var taskInteraction = response.getState();
      if (taskInteraction === "SUCCESS") {
        let sObjs = response.getReturnValue();
        if (sObjs != null) {
          console.log("AB HCONSOLE.LOG: sObjs.CallObject= " + sObjs.CallObject);
          component.set("v.interaction", sObjs);
        }
        //alojarlo en una variable en la vista
        //component.set("v.isLoading", false);
      } else {
        var errors = response.getError();
        console.log(errors);
      }
    });
    $A.enqueueAction(interaction);
  },
  getAccountId: function(component, event, helper) {
    var aId = component.get("c.getAccountId");
    //conseguir el id del cliente
    console.log("AB HCONSOLE" + component.get("v.recordId"));
    aId.setParams({
      caseId: component.get("v.recordId")
    });
    aId.setCallback(this, function(response) {
      var taskInteraction = response.getState();
      if (taskInteraction === "SUCCESS") {
        var sObjs = response.getReturnValue();
        console.log("AB HCONSOLE" + sObjs);
        component.set("v.accountId", sObjs);
        if (sObjs != null) {
          this.getAllInteractionsOpened(component);
        }
      } else {
        var errors = response.getError();
        console.log(errors);
      }
    });
    $A.enqueueAction(aId);
  }

});