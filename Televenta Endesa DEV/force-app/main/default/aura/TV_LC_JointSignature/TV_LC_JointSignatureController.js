({
  init: function(component, event, helper) {
    helper.getAllMethods(component, event, helper);
    helper.permissionOk(component, event, helper);
    helper.getLoginInfo(component, event, helper);
    helper.getAccountId(component, event, helper);
    helper.getPermissionProfile(component,event,helper);
  },
  sign: function(component, event, helper) {
    helper.checkBeforeSign(component, event, helper);
  },
  aceptRS:function(component,event,helper){
    helper.reSign(component,event,helper);
  },
  reSign:function(component,event,helper){
    component.set("v.pressedButtonRS",true);
  },

  handleCloseRejectModal: function(component, event, helper) {
    component.set("v.isReadingLegalTextModalOpen", false);
    component.set("v.disableCancelEnd", true);
    component.set("v.textLegal", "");
    
    if(component.get("v.disableStart")){

      var interactionInCourse = component.get("v.interaction");
      if (interactionInCourse != null) {
        var subtipoTarea = interactionInCourse.FII_ACT_SEL_SubTipoTarea__c;
        if(subtipoTarea != $A.get("$Label.c.TV_Label_ContingencyMode")){

          var initGrabacion = component.get("c.initGrabacion");
          $A.enqueueAction(initGrabacion);
        }
      }
      component.set("v.isRecordEnd", true);
    }
  },
  start: function(component, event, helper) {
    helper.getTextLegal(component, event, helper);
  },
  cancel: function(component, event, helper) {
    component.set("v.disableStart", false);
    component.set("v.disableCancelEnd", true);
    component.set("v.textLegal", "");
    component.set("v.isRecordEnd", true);
    var interactionInCourse = component.get("v.interaction");
      if (interactionInCourse != null) {
        var subtipoTarea = interactionInCourse.FII_ACT_SEL_SubTipoTarea__c;
        if(subtipoTarea != $A.get("$Label.c.TV_Label_ContingencyMode")){

          var initGrabacion = component.get("c.initGrabacion");
          $A.enqueueAction(initGrabacion);
        }
      }
  },
  finalize: function(component, event, helper) {
    component.set("v.isReadingLegalTextModalOpen", false);
    component.set("v.textLegal", "");
    component.set("v.disableCancelEnd", true);
    var interactionInCourse = component.get("v.interaction");
      if (interactionInCourse != null) {
        var subtipoTarea = interactionInCourse.FII_ACT_SEL_SubTipoTarea__c;
        if(subtipoTarea != $A.get("$Label.c.TV_Label_ContingencyMode")){

          var initGrabacion = component.get("c.initGrabacion");
          $A.enqueueAction(initGrabacion);
        }
      }
    component.set("v.isRecordEnd", true);
    var urlConga = component.get("v.urlConga");
    var my_window = window.open(
      urlConga,
      "Conga",
      "status=1,width=20,height=20"
    );
    var timer = setInterval(
      $A.getCallback(function() {
        if (my_window == null) {
          clearInterval(timer);
        }
        if (my_window != null && my_window.closed) {
          helper.sign(component,event,helper);
          clearInterval(timer);
        }
      }),
      2000
    );
  },
  handleCloseBlankModal: function(component, event, helper) {
    component.set("v.blankTypeFirm", false);
  },

  handleCloseRSModal:function(component,event,helper){
    component.set("v.pressedButtonRS",false);
  },

  rejectRS:function(component,event,helper){
    component.set("v.pressedButtonRS",false);
  },

  acept: function(component, event, helper) {
    helper.updateTypeSign(component, event, helper);
    component.set("v.blankTypeFirm", false);
  },
  reject: function(component, event, helper) {
    component.set("v.blankTypeFirm", false);
  },

  // Start David Homero code
  initGrabacion: function(component, event, helper) {
    /*var loginUrl =
      "https://svc-csf-cert.inin-endesa-v4.com/client/api/SVC-CIC-CERT.ININ-ENDESA-V4.local/icws/connection";*/
    var loginUrl = component.get("v.endpointIcws") +"client/api/SVC-CIC-CERT.ININ-ENDESA-V4.local/icws/connection";
      
      var connectionParams = {
      __type: "urn:inin.com:connection:icAuthConnectionRequestSettings",
      applicationName: "ICWS JavaScript Direct Usage Example",
      userID: component.get("v.loginUser"),
      password: component.get("v.loginPassword")
    };
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.open("post", loginUrl, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {
        if (xhr.status == 200 || xhr.status == 201 || xhr.status == 202) {
          console.log("Conectado al ICWS");
          var json = JSON.parse(xhr.responseText);
          component.set("v.genToken", json.csrfToken);
          component.set("v.genSessionId", json.sessionId);
          helper.record(component, event, helper);
        } else {
          helper.showError("Error al conectar con ICWS", "Error", "error");
        }
      }
    };
    xhr.send(JSON.stringify(connectionParams));
  }
  // End David Homer code
});