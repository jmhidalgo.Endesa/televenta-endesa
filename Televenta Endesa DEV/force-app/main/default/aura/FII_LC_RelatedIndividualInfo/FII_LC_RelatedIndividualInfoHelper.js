({
    init : function(component) {
        var action = component.get("c.getIndividual");
        var idRecord = component.get("v.idRecord");
        console.log('--- SVV. idRecord: ' + idRecord);
        this.getInteractionsInProgress(component);
        action.setParams({
            idRecord : idRecord
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('Esta es la respuesta:' + response);
            console.log('Esta es la respuesta 2:' + state);
            if (state === "SUCCESS") {
                var ind = response.getReturnValue();
                console.log('--- IND. : ' + ind);
                if (ind != null) {
                    console.log('Individual existe');
                    //alert('Individual existe');
                    component.set("v.individualObj", ind[0]);
                    component.set("v.disableHistory", false);
                }
                else {
                    var createIndi = component.get("c.createdIndividual");
                    createIndi.setParams({
                        contactId : idRecord
                    });
                    createIndi.setCallback(this, function (response) {
                        var state = response.getState();
                        if(state === 'SUCCESS'){
                            if(response.getReturnValue()){
                                console.log("Calling INIT Again");
                               // this.init(component);
                            }
                        } else{
                            var errors = response.getError();
                            this.displayToast(component, "Se ha intentado crear el individual error: " +errors[0].message, "error");
                            console.log("Error message: "+ errors[0].message);
                            
                        }
                    });
                    $A.enqueueAction(createIndi);
                }
            } else if(state == "ERROR"){
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error message: "+ errors[0].message);
                    }else{  
                        console.log("Unknown error");
                    }
                }
            }
        });
        
        $A.enqueueAction(action);
        
        var action2 = component.get("c.getPhysical");
        action2.setParams({
            idRecord : idRecord
        });
        action2.setCallback(this, function (response) {
            var state = response.getState();
            console.log('Esta es la respuesta:' + response);
            if (state === "SUCCESS") {
                var booleanJuridico = response.getReturnValue();
                component.set("v.contactStatus", booleanJuridico);
            } else if(state == "ERROR"){
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error message: "+ errors[0].message);
                    }else{  
                        console.log("Unknown error");
                    }
                }
            }
        });
        
        $A.enqueueAction(action2);
        
        var actionPhy = component.get("c.getPhysicalOrLegal");
        actionPhy.setParams({
            idRecord : idRecord
        });
        actionPhy.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var typePerson = response.getReturnValue();
                component.set("v.typePhysicalOrLegal", typePerson);
            } 
        });
        
        $A.enqueueAction(actionPhy);
        
        
    },
    saveIndividualHelper : function(component) {

        //if (component.get("v.identifierPress")) {
            var action = component.get("c.save");
            var individual = component.get("v.individualObj");
            console.log('--- SVV. terceros al guardar: ' + individual.FII_IND_SEL_Terceros__c);
            console.log('--- SVV. terceros al guardar FII_IND_SEL_Iden_Evi__c: ' + individual.FII_IND_SEL_Iden_Evi__c);
            console.log('--- SVV. terceros al guardar recId: ' + component.get("v.recId"));
            action.setParams({
                ind: individual,
            });
            
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('success');
                    this.launchTeleventaEvent(component); 
                    this.displayToast(component, "Se han actualizado los datos correctamente", "success");
                    this.standardBehaviourGSM(component);
                } else if(state == "ERROR"){
                    var errors = response.getError();
                    this.displayToast(component, errors, "error");
                    console.log('++++----' + errors[0].message);
                    if(errors){
                        if(errors[0] && errors[0].message){
                            console.log("Error message: "+ errors[0].message);
                        }else{  
                            console.log("Unknown error");
                        }
                    }
                }
            });
            
            $A.enqueueAction(action);
        /*}
        else {
            component.set("v.errorValidation",true);
            component.set("v.errorValidationTitle","El campo identificador de Evidencia es obligatorio si se modifica algún consentimiento RGPD.");
            component.set("v.severity","error");
            //component.set("v.arrayMensajeError",response.getReturnValue().msg);
        }*/

        
    },

    // CAMBIOS GSM
    standardBehaviourGSM : function(component){
        var needId;
        var toastMsg;
        if (component.get("v.needSelected") != null) {
            needId = component.get("v.needSelected");
            toastMsg = $A.get("$Label.c.FII_TOAST_ACT");
        }
        else {
            needId = component.get("v.recordId");
            toastMsg = $A.get("$Label.c.FII_TOAST_NEED_ACT");
        }
        var standardBehaviorGSM = component.get("c.standardBehaviorGSM");
        standardBehaviorGSM.setParams({
            recId : needId,
            type : 'Atencion al cliente',
            subtype : null,
            subjectNeed : "Modificación RGPD",
            subjectAct : "Modificación RGPD",
            camId : null,
            status : 'Completed',
            recordtype : $A.get("{!$Label.c.FII_RT_TSK_ACTION}")
        });
        standardBehaviorGSM.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {	
                var needCreateEvent = $A.get("e.c:FII_LEV_NeedCreate");
                needCreateEvent.setParams({"needCreated":response.getReturnValue().Id,"recId":component.get("v.recordId")});
                needCreateEvent.fire();
                this.launchRefreshEvent(component);
                this.displayToast(component, toastMsg, 'success');
                 
                component.find("overlayLib").notifyClose();
            } else {
                this.displayToast(component, "No se ha podido crear una necesidad y una actuación", "error");
            }
        });
        $A.enqueueAction(standardBehaviorGSM);
    },
    launchTeleventaEvent : function(component){
        console.log('launchTeleventaEvent: ',component.get("v.recId"));
        var event = $A.get("e.c:TV_LEV_RefreshClientSection");
        event.setParams({
			"recId" : component.get("v.recId")
		});
		event.fire();
    },
    launchRefreshEvent : function(component) {
        var FII_LEV_RefreshComponents = $A.get("e.c:FII_LEV_RefreshComponents");
		FII_LEV_RefreshComponents.setParams({
			"interaction" : component.get("v.interactionInCourse"),
			"recId" : component.get("v.recId")
		});
		FII_LEV_RefreshComponents.fire();
    },

    displayToast : function(component, message, type){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "message": message,
            "type": type,
        });
        toastEvent.fire();
    },

    getInteractionsInProgress : function(component){
        console.log(' getInteractionsInProgress recId: ');
        var getInteractionsInProgress = component.get("c.getInteractionsInProgress");
        getInteractionsInProgress.setParams({
            recId : component.get("v.recordId")
        });
        getInteractionsInProgress.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.interactionInCourse", response.getReturnValue());
                component.set("v.recId", component.get("v.recordId"));
                console.log('recId: ',component.get("v.recordId"));
            }
        });
        $A.enqueueAction(getInteractionsInProgress);
    },

    completeObjects : function(component, event, helper) {

        var actionAccount = component.get("c.getObjectsType");
        actionAccount.setCallback( helper, function( response ) {
        
            if ( component.isValid() && response.getState() === 'SUCCESS' ) {
                // Parse the JSON string into an object
                component.set( 'v.ObjectType', JSON.parse( response.getReturnValue() ) );
                console.table( JSON.parse( response.getReturnValue() ) ); 
            } else {
                console.error( 'Error calling action "' + actionName + '" with state: ' + response.getState() );
            }
            
        });
        $A.enqueueAction( actionAccount );
    },
	obtainProfiles : function (component) {
		var result = component.get("c.getIsTeleventaProfile");
		result.setParams({
		});
		result.setCallback(this, function (response) {
			var state = response.getState();
			console.log('perfil: ',response.getReturnValue());
			if (state === "SUCCESS") {
				if (response.getReturnValue()) {
                    component.set("v.isTeleventaProfile", true);
                    this.obtainTextInfo(component);
				}
			}
		});
		$A.enqueueAction(result);
    },
    obtainTextInfo : function(component) {
        component.set('v.isLoading',true);
        var recordId = component.get('v.recordId');
        console.log('recordId: ',recordId);
        var textsRGPDInfo = component.get("c.obtainTextsRGPD");
        textsRGPDInfo.setParams({
        	'recordId': recordId
        });
        textsRGPDInfo.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() !== null && response.getReturnValue() !== undefined){
                    console.log('response.getReturnValue(): ',response.getReturnValue());
                    var result = response.getReturnValue();
                    component.set("v.textoInicial",result.textoInicial);
                    component.set("v.NoMolestar",result.textoNoMolestar);
                    component.set("v.Ofrecimientos",result.textoOfrecimientos);
                    component.set("v.Cesion",result.textoCesion);
                    component.set("v.NoCliente",result.textoNoCliente);
                    component.set('v.isLoading',false);
                }else{
                   console.log('ERROR - response.getReturnValue(): ',response.getReturnValue());
                }

            }else if(state === "ERROR"){
                console.log('ERROR - response.getReturnValue(): ',response.getReturnValue());
            }
        });
    	$A.enqueueAction(textsRGPDInfo); 
    }

})