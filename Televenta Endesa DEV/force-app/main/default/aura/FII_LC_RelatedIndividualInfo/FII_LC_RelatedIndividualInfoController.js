({
	doInit : function(component, event, helper) {
        helper.init(component);
        helper.completeObjects(component, event, helper);
        helper.obtainProfiles(component);
	},
    
    saveIndividual : function (component, event, helper) {
        helper.saveIndividualHelper(component);
        component.set("v.isOpen", false);
    },
    abrirModal : function(component) {
        component.set("v.isOpen", true);
    },
    cerrarModal : function(component) {
        component.set("v.isOpen", false);
    },
    refreshRGPD : function(component, event, helper) {
        var action = component.get("c.isTheEventForMe");
        var eventRecId = event.getParam('recId');
        var oldInteraction = event.getParam('interaction');
        action.setParams({
            myRecId : component.get("v.recordId"), 
            eventRecId : eventRecId, 
            oldInteraction : oldInteraction
        });
        action.setCallback(this, function(response){            
            var status = response.getState();
            if (status === "SUCCESS" && response.getReturnValue()){
                component.set("v.firstExecution", false);
                helper.init(component);
            }
        });
        $A.enqueueAction(action);
       
    },
    
    showHistory: function(component, rowId) {
        var workspaceAPI = component.find("workspace");    
        var indivId = component.get('v.individualObj.Id');
            workspaceAPI.getEnclosingTabId().then(function(tabId) {
                workspaceAPI.openSubtab({
                    parentTabId: tabId,
                    recordId: indivId
                });
            });
    },

    doneChanges : function (component, event, helper) {

        var idEvidencia = component.get("v.individualObj.FII_IND_SEL_Iden_Evi__c");
        var displayValidation = component.get("v.displayValidation");
        if(idEvidencia == null || idEvidencia == ""){
            component.set("v.displayValidation", true);
            component.set("v.disableButton", true);
        } else {
            component.set("v.displayValidation", false);
            component.set("v.disableButton", false);
        }
    },

    doneChangesIdEvidencia : function (component, event, helper) {
     
        component.set("v.identifierPress",true);
        component.set("v.errorValidation",false);
        var idEvidencia = component.get("v.individualObj.FII_IND_SEL_Iden_Evi__c");
        var displayValidation = component.get("v.displayValidation");
        if(idEvidencia == null || idEvidencia == ""){
            component.set("v.displayValidation", true);
            component.set("v.disableButton", true);
        } else {
            component.set("v.displayValidation", false);
            component.set("v.disableButton", false);
        }
    },

    handlerNeedSelected: function(component, event) {

        var action = component.get("c.isTheEventForMe");
        var eventRecId = event.getParam('recId');
        var interactionInProgress = component.get("v.interactionInCourse")
        action.setParams({
			myRecId : component.get("v.recordId"), 
			eventRecId : eventRecId, 
			interactionInProgress : interactionInProgress
        });
        action.setCallback(this, function(response){            
            var status = response.getState();
			console.log('---- status: ' + status);
            if (status === "SUCCESS" && response.getReturnValue()){
				console.log('--- Evento Need ---');
                component.set("v.needSelected",event.getParam("needSelected"));
			}
		});
		$A.enqueueAction(action);
    },

    handlerNotifyInteractionInCourse: function(component, event){
        
        var action = component.get("c.isTheEventForMe");
        var eventRecId = event.getParam('recId');
        var interactionInProgress = event.getParam('interactionInCourse')
        action.setParams({
			myRecId : component.get("v.recordId"), 
			eventRecId : eventRecId, 
			interactionInProgress : interactionInProgress
        });
        action.setCallback(this, function(response){            
            var status = response.getState();
			console.log('---- status: ' + status);
            if (status === "SUCCESS" && response.getReturnValue()){
				component.set("v.interactionInCourse", event.getParam("interactionInCourse"));
                component.set("v.recId", event.getParam("recId"));
                //Get the event InteractionInCourse attribute
                var itInCourse = event.getParam("interactionInCourse");
                if(itInCourse == null){
                    component.set("v.disabledEditButton", true);
                } else{
                    var recId = component.get("v.recordId");
                    if(itInCourse.FII_ACT_LKP_RelatedClient__c == recId && itInCourse.WhoId != null){
                        if (itInCourse.FII_ACT_TXT_Role__c == 'Titular de contrato' || itInCourse.FII_ACT_TXT_Role__c == 'Guardian') {
                            component.set("v.disabledEditButton", false);
                        }
                        else {
                            component.set("v.disabledEditButton", true);
                        }
                    } 
                    else {
                        component.set("v.disabledEditButton", true);
                    }
                }
			}
		});
		$A.enqueueAction(action);
    },
    openFlowRGPD : function(component, event, helper) {
        console.log('openFlowRGPD Loaded');

        component.set("v.isFlowRunning", true);
        component.set("v.isLoading", true);
        var flow = component.find("flowData");
		var inputVariables = [{
            name: "Texto_No_Molestar",
            type: "String",
            value: component.get("v.NoMolestar")
        },{
            name: "Texto_Ofrecimientos",  
            type: "String",
            value: component.get("v.Ofrecimientos")
        },{
            name: "Texto_Cesion", 
            type: "String",
            value: component.get("v.Cesion")
        },{
            name: "Texto_No_Cliente", 
            type: "String",
            value: component.get("v.NoCliente")
        }];
        component.set("v.isLoading", false);
        flow.startFlow("TV_FLW_RGPDSection",inputVariables);
        
    },
    handleStatusChange : function (component, event,helper) {
        console.log(event.getParam("status"));
        var individual = component.get('v.individualObj');

        if(event.getParam("status") === "FINISHED" ) {
            component.set("v.isFlowRunning", false);
            component.set("v.isLoading", true);
        
           // Get the output variables and iterate over them
           var outputVariables = event.getParam("outputVariables");
           var outputVar;
           console.log('outputVariables: ',outputVariables.length);
           console.log('event.getParams().response',event.getParams().response);
            var goToSave = false;
            for(var i = 0; i < outputVariables.length; i++) {
                outputVar = outputVariables[i];
                console.log('outputVar: ',outputVar);
                if(!goToSave){
                    if(outputVar.name === 'Valor_Identificador_Evidencia'){
                        console.log('Valor_Identificador_Evidencia: ',outputVar.value);
                        individual.FII_IND_SEL_Iden_Evi__c = outputVar.value;
                    }
                    if(outputVar.name === 'Valor_No_Molestar'){
                    
                        if(outputVar.value == 'true'){
                            component.set('v.NoMolestarValue','Sí Ofertas');
                        }else if(outputVar.value == 'false'){
                            component.set('v.NoMolestarValue','No Ofertas');
                            individual.FII_IND_SEL_Molestar__c = 'No Ofertas';
                            individual.FII_IND_SEL_Terceros__c = 'No';
                            individual.FII_IND_SEL_Cesion__c = 'No';
                            individual.FII_IND_SEL_Clientes__c = 'No';
                            goToSave = true;
                        }else{
                            component.set('v.NoMolestarValue','');   
                        }
                        console.log('Valor_No_Molestar es: '+component.get('v.NoMolestarValue'));
                        individual.FII_IND_SEL_Molestar__c = component.get('v.NoMolestarValue');
                        
                    }
                    if(outputVar.name === 'Valor_Ofrecimientos'){
                        if(outputVar.value === 'true'){
                            component.set('v.OfrecimientosValue','Si');
                        }else if(outputVar.value === 'false'){
                            component.set('v.OfrecimientosValue','No');
                        }else{
                            component.set('v.OfrecimientosValue','');
                        }
                        console.log('Valor_Ofrecimientos es: '+component.get('v.OfrecimientosValue'));
                            individual.FII_IND_SEL_Terceros__c = component.get('v.OfrecimientosValue');
                    }
                    if(outputVar.name === 'Valor_Cesion'){
                        console.log('Valor_Cesion es: '+outputVar.value);
                        if(outputVar.value === 'true'){
                            component.set('v.CesionValue','Si');
                        }else  if(outputVar.value === 'false'){ 
                            component.set('v.CesionValue','No');
                        }else{
                            component.set('v.CesionValue','');
                        }
                        console.log('Valor_Cesion es: '+component.get('v.CesionValue'));
                            individual.FII_IND_SEL_Cesion__c = component.get('v.CesionValue');
                    }    
                    if(outputVar.name === 'Valor_No_Cliente'){
                        console.log('Valor_No_Cliente es: '+outputVar.value);
                        if(outputVar.value === 'true'){
                            component.set('v.NoClienteValue','Si');
                        }else  if(outputVar.value === 'false'){ 
                            component.set('v.NoClienteValue','No');
                        }else{
                            component.set('v.NoClienteValue','');
                        }
                        console.log('Valor_No_Cliente es: '+component.get('v.NoClienteValue'));
                        individual.FII_IND_SEL_Clientes__c = component.get('v.NoClienteValue');
                    }    
                }
            }

            component.set("v.identifierPress",true);
            component.set('v.individualObj',individual);
            component.set("v.isLoading", false);
            component.set("v.isOpen", true);
        }
    }
})