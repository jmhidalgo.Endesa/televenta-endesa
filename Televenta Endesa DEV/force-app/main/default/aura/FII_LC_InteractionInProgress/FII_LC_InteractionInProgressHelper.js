({
	getAllInteractionsOpened : function(component) {
		var interaction = component.get("c.getInteractionsInProgress");
		interaction.setParams({
						"recId" : component.get("v.recordId")
		});
	
		interaction.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				let sObjs = response.getReturnValue();
				component.set("v.openInteraction", sObjs);                
				
				if(sObjs != null){
					if(sObjs.Who != null && sObjs.Who.Name == 'CONTACTO PENDIENTE DE INFORMAR'){
						component.set("v.openInteraction.Who", null);
					}

					this.getChannelIcon(component);
					this.isLead(component);
					 
					//Start, it validates that object does not come null
					var nlInfo = sObjs.FII_ACT_TXT_NLInfo__c;
					component.set("v.indicatorTypeSubType", sObjs.FII_ACT_TXT_NLInfo__c);
					var event = $A.get("e.c:FII_LEV_InteractionIdentification");
					event.setParams({
						"targetInterIdentification" : nlInfo
					});
					event.fire();
					//End, it validates that object does not come null
				}
				
				component.set("v.interactionInCourse", sObjs);
				component.set("v.recId", component.get("v.recordId"));
				var event = $A.get("e.c:FII_LEV_NotifyInteractionInCourse");
				event.setParams({
					"interactionInCourse" : sObjs,
					"recId" : component.get("v.recordId")
				});
				
				event.fire();
				//if(component.get("v.sObjectName") === 'Account'){
					if(component.get("v.openInteraction.FII_ACT_TXT_Role__c")){
						
						if(component.get("v.openInteraction.FII_ACT_TXT_Role__c").includes("Aut") ){
							this.getAutorizedOperations(component);
						}else{
							this.getTitularOperations(component);
						}
					}
					
					this.getContactRelated(component);
				//}
				
                component.set("v.isLoading", false);
			} else {
				var errors = response.getError();
				console.log(errors);
			}
			
		});
		$A.enqueueAction(interaction);    
	},

	getChannelIcon : function(component) {
		var channel = component.get("v.openInteraction").FII_ACT_SEL_Channel__c.split(':')[0];
		var channelLabel = component.get("v.openInteraction").FII_ACT_SEL_Channel__c.split(':')[1];
				
		if(channel == $A.get("$Label.c.FII_TaskChannel_Phone")){ 
			component.set("v.opertionActionPhone", true);
		}
		
		var channelIco;
		var iconClass;
		if(channel != null){
			
			if(channel == $A.get("$Label.c.FII_TaskChannel_Phone")) {
				channelIco ='custom:custom22';
			}
			else if(channel == $A.get("$Label.c.FII_TaskChannel_Email")){
				channelIco ='standard:email_chatter';
			}
			else if(channel == $A.get("$Label.c.FII_TaskChannel_Website") || channel == $A.get("$Label.c.FII_TaskChannel_Website")){
				channelIco ='standard:screen';
			}
			else if(channel == $A.get("$Label.c.FII_TaskChannel_Community") || channel == $A.get("$Label.c.FII_TaskChannel_Instagram") 
					|| channel == $A.get("$Label.c.FII_TaskChannel_Twitter") || channel == $A.get("$Label.c.FII_TaskChannel_Facebook") 
					|| channel == $A.get("$Label.c.FII_TaskChannel_LinkedIn")){
				channelIco ='standard:social';
			}
			else if(channel == $A.get("$Label.c.FII_TaskChannel_SMS")){
				channelIco ='standard:quick_text';
			}
			else if(channel == $A.get("$Label.c.FII_TaskChannel_CCPP")){
				channelIco ='custom:custom31';
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_Chat")){
				channelIco ='standard:live_chat';
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_ALEX")){
				channelIco ='standard:post';
				iconClass="slds-icon-standard-post1";
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_E21M")){
				channelIco ='standard:custom_notification';
				iconClass="slds-icon-standard-custom_notification1";
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_E21W")){
				channelIco ='standard:custom_notification';
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_EAPP")){
				channelIco ='standard:apex_plugin';
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_ESER")){
				channelIco ='standard:channel_programs';
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_EWEB")){
				channelIco ='standard:apex_plugin';
				iconClass="slds-icon-standard-apex_plugin1";
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_GHOM")){
				channelIco ='standard:home';
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_IVR")){
				channelIco ='standard:log_a_call';
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_WAT1")){
				channelIco ='standard:bot';
				iconClass="slds-icon-standard-bot1";
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_WAT2")){
				channelIco ='standard:bot';
				iconClass="slds-icon-standard-bot2";
			} 
			else if(channel == $A.get("$Label.c.FII_TaskChannel_WAT3")){
				channelIco ='standard:bot';
				iconClass="slds-icon-standard-bot3";
			}
		}
		component.set("v.channelIcon", channelIco);
		component.set("v.iconClass", iconClass);
		component.set("v.channelLabel",channelLabel);
	},

	isLead : function(component) {
		var interact = component.get("v.openInteraction");
		if(interact != null){
			var contactOrLead = interact.WhoId;
			if(contactOrLead != null && contactOrLead.startsWith('00Q')){
				component.set("v.isLead", true);
			}
		}
	},
	getContactRelated: function(component){

		var contacts = component.get("c.getContactRelated");
		contacts.setParams({
			accountId : component.get("v.openInteraction.FII_ACT_LKP_RelatedClient__c")
		});
		contacts.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {

				var state = response.getState();
				if (state === "SUCCESS") {
					if(response.getReturnValue()){
						var rawContactList = JSON.parse(response.getReturnValue());
						var contactList = [];
						rawContactList.forEach(function(rawContact) {
							var contact = {
								id : rawContact.cont_id,
								title : rawContact.cont_Name,
								subtitle : rawContact.cont_Role,
								icon : 'standard:contact'
							};
							contactList.push(contact);
						});
						component.set("v.relatedContact", contactList);
					} else{
						var lista = [];
						component.set("v.relatedContact", lista);
					}
				} else {
					console.log('Error');
				}

			} else {
				console.log('Error');
			}
		});
		$A.enqueueAction(contacts);
	},

	setContactToInt : function(component, helper) {
		var action = component.get('c.setContactToInteraction');
		
		action.setParams({
			"interactionId" : component.get("v.openInteraction.Id"),
			"contactId" : component.get("v.contactId"),
		});
		
		action.setCallback(this, function (response) {
			var state = response.getState();
			
			if (state === 'SUCCESS') {
				this.getAllInteractionsOpened(component);
			
				component.set("v.valuePreChange", component.get("v.openInteraction.Who"));

				this.launchRefreshEvent(component);
			}
		})   
		$A.enqueueAction(action);
	},

	getAutorizedOperations : function(component, helper) {
		var action = component.get('c.getAutorizedOperations');     
		action.setCallback(this, function (response) {
			var state = response.getState();
			
			if (state === 'SUCCESS') {
				if(response.getReturnValue()){
					component.set("v.operations", response.getReturnValue())
				}
			}
		})   
		$A.enqueueAction(action);
		
	},

	getTitularOperations : function(component) {
		var action = component.get('c.getTitularOperations');     
		action.setCallback(this, function (response) {
			var state = response.getState();
			
			if (state === 'SUCCESS') {
				if(response.getReturnValue()){
					component.set("v.operations", response.getReturnValue())
				}
			}
		})   
		$A.enqueueAction(action);
	},

	// CAMBIOS PARA GSM
	launchRefreshEvent : function(component) {
		var FII_LEV_RefreshComponents = $A.get("e.c:FII_LEV_RefreshComponents");
		FII_LEV_RefreshComponents.setParams({
			"interaction" : component.get("v.interactionInCourse"),
			"recId" : component.get("v.recId")
		});
		FII_LEV_RefreshComponents.fire();
	},
    
    //CAMBIOS PARA TV
    hiddenButton: function(component, event, helper){
        var action = component.get("c.hiddenButton");
        action.setCallback(this, function(response){
            var status = response.getState();
            if (status === "SUCCESS"){
                var responseBool = response.getReturnValue();
                console.log('HCONSOLE.LOG-> responseBool='+responseBool);
                component.set("v.marcadorView", responseBool);
            } else {
                console.log('HCONSOLE.LOG-> status='+status);
                component.set("v.marcadorView", true);
            }
        });
        $A.enqueueAction(action);        
    }
 
})