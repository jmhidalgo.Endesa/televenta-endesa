({
    handlerInteractonIdent : function(component, event, helper) {
        var valueInter = event.getParam('targetInterIdentification');
        var typeSubtype = component.get("c.getTypeAndSubType");
        typeSubtype.setParams({
            idMotive : component.get("v.indicatorTypeSubType")
        });
        typeSubtype.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                if(resp[0]!=null){
                    component.set("v.typeMotive", resp[0]);
                    component.set("v.subTypeMotive", resp[1]);   
                }
            } 
        });
        $A.enqueueAction(typeSubtype);
    },
    openAltaContacto: function(component, event, helper){
        var modalBody;
        $A.createComponent("c:FII_LC_AltaContacto",
        {
            recordId : component.get("v.openInteraction.FII_ACT_LKP_RelatedClient__c"),
            interactionInCourse : component.get("v.openInteraction"), // CAMBIOS PARA GSM
            recId : component.get("v.recId"), // CAMBIOS PARA GSM
            idNeed : component.get("v.idNeed"),
            desdeInteraccion : true
        },
        function(content, status) {
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: "Alta de Contacto",
                    body: modalBody, 
                    showCloseButton: false,
                    closeCallback: function() {                                               
                        // DO SOMETHING WHEN CLOSE
                    }
                })
            }                               
        });
    },

    handlerNeedSelected: function(component, event, helper) {
        
        var action = component.get("c.isTheEventForMe");
        var interactionInProgress = component.get("v.interactionInCourse");
        var recId = event.getParam("recId");
        action.setParams({
			myRecId : recId, 
			eventRecId : component.get('v.recordId'), 
			interactionInProgress : interactionInProgress
        });
        action.setCallback(this, function(response){            
            var status = response.getState();
			console.log('---- status: ' + status);
            if (status === "SUCCESS" && response.getReturnValue()){
                component.set("v.needSelected",event.getParam("needSelected"));
			}
		});
		$A.enqueueAction(action);
    },
    openAltaContacto: function(component, event, helper){
        var modalBody;
        $A.createComponent("c:FII_LC_AltaContacto",
        {
            recordId : component.get("v.openInteraction.FII_ACT_LKP_RelatedClient__c"),
            interactionInCourse : component.get("v.openInteraction"), // CAMBIOS PARA GSM
            recId : component.get("v.recId"), // CAMBIOS PARA GSM
            needSelected : component.get("v.needSelected"),
            desdeInteraccion : true
        },
        function(content, status) {
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: "Alta de Contacto",
                    body: modalBody, 
                    showCloseButton: false,
                    closeCallback: function() {                                               
                        // DO SOMETHING WHEN CLOSE
                    }
                })
            }                               
        });
    },

    doInit : function(component, event, helper) {
        helper.getAllInteractionsOpened(component);
		helper.hiddenButton(component, event, helper);        
    },
    
    closeInteraction : function(component, event, helper) {
        component.set("v.isLoading", true);
        var flow = component.find("flowCloseInteraction");
        var inputVariables = [
            {
                name : 'interactionId',
                type : 'String',
                value : component.get("v.openInteraction.Id")
            }
        ];
        flow.startFlow("FII_FLW_Close_InteractionInCourse", inputVariables);
    },
    
    setContactToInt: function(component, event, helper) {
        helper.setContactToInt(component);
    },
    
    handleStatusChange : function (component, event, helper) {
        if (event.getParam('status') === "FINISHED_SCREEN") {
            helper.launchRefreshEvent(component);
        }
    },
    navigateToContact : function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToSObject");
        if(navEvent){
            navEvent.setParams({
                recordId: component.get("v.openInteraction.Who.Id"),
                slideDevName: "detail"
            });
            navEvent.fire();   
        }
    },
    navigateToObject : function(component, event, helper) {
        var record = "";
        if(component.get("v.openInteraction.WhatId")){
            record = component.get("v.openInteraction.WhatId");
        }else if(component.get("v.openInteraction.FII_ACT_LKP_WhatId__c")){
            record = component.get("v.openInteraction.FII_ACT_LKP_WhatId__c");
        }else if(component.get("v.openInteraction.FII_ACT_TXT_Recruitment_interaction__c")){
            record = component.get("v.openInteraction.FII_ACT_TXT_Recruitment_interaction__c");
        }
        
        var navEvent = $A.get("e.force:navigateToSObject");
        if(navEvent){
            navEvent.setParams({
                recordId: record,
                slideDevName: "detail"
            });
            navEvent.fire();   
        }
    },
    navigateToAgent : function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToSObject");
        if(navEvent){
            navEvent.setParams({
                recordId: component.get("v.openInteraction.OwnerId"),
                slideDevName: "detail"
            });
            navEvent.fire();   
        }
        
    },
    navigateToAccount : function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToSObject");
        if(navEvent){
            navEvent.setParams({
                recordId: component.get("v.openInteraction.FII_ACT_LKP_RelatedClient__c"),
                slideDevName: "detail"
            });
            navEvent.fire();   
        }
        
    },
    navigateToInteraction: function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToSObject");
        if(navEvent){
            navEvent.setParams({
                recordId: component.get("v.openInteraction.Id"),
                slideDevName: "detail"
            });
            navEvent.fire();   
        }
    },
    checkContact: function(component, event, helper){
        
        function isValidContact(){
            var isContactoClient = false;
            
            let sObjs = component.get("v.relatedContact");
            
            for(let row of sObjs){
                if(row.ContactId == component.find("contactField").get("v.value")) {
                    isContactoClient = true;
                    break;
                }
            }
            
            return isContactoClient;
        }
        
        var validContact = isValidContact();
        if(!validContact){
            helper.showToast(component, "error", $A.get("$Label.c.FII_MSG_ERROR_INVALID_CONTACT"), $A.get("$Label.c.FII_MSG_ERROR_SELECT_ACCOUNT_CONTACT"));
        }
    },
    setContact : function(component, event, helper) {

        var inputField = component.find("contactField");
        if (Array.isArray(inputField) && inputField) {
            inputField[0].set('v.value', event.getParam('sObjectId'));
        } else {
            inputField.set('v.value', event.getParam('sObjectId'));
        }
        component.set("v.contactId",component.find("contactField").get('v.value'));
    
        let sObjs = component.get("v.relatedContact");
        var role = '';
        for(let row of sObjs){ 
            if(row.id == component.find("contactField").get("v.value")) {
                role = row.subtitle;
                break;
            }
        }
        component.set("v.selectedRole", role);
    
   
    },
    changeContact : function(component, event, helper) {
        if(!component.get("v.valuePreChange") || component.get("v.valuePreChange") == ""){
            component.set("v.valuePreChange", component.get("v.openInteraction.Who"));
            component.set("v.openInteraction.Who", null);
        }else{
            component.set("v.openInteraction.Who", component.get("v.valuePreChange") );
            component.set("v.valuePreChange", "");
        }
    },
    openPop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-show');
        $A.util.removeClass(cmpTarget, 'slds-hide');
        
    },
    
    closePop : function(component, event, helper) {
        var cmpTarget = component.find('pop');
        $A.util.addClass(cmpTarget, 'slds-hide');
        $A.util.removeClass(cmpTarget, 'slds-show');
        
    },
    
    // CAMBIOS PARA GSM
    handlerRefreshComponents : function(component, event, helper){
        var action = component.get("c.isTheEventForMe");
        var eventRecId = event.getParam('recId');
        var interactionInProgress = event.getParam('interaction')
        action.setParams({
            myRecId : component.get("v.recordId"), 
            eventRecId : eventRecId, 
            interactionInProgress : interactionInProgress
        });
        action.setCallback(this, function(response){            
            component.set("v.loading", false);
            var status = response.getState();
            if (status === "SUCCESS" && response.getReturnValue()){
                helper.getAllInteractionsOpened(component);
            }
        });
        $A.enqueueAction(action);
    },
    refreshComponent : function(component, event, helper){
        helper.getAllInteractionsOpened(component);
    }
})