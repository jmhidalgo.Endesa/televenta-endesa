({
    doInit : function(component, event, helper){
        component.set('v.assignToAgent', true);
        helper.getPicklistData(component, event, helper);
        helper.getChannelData(component, event, helper);
        helper.getLoginInfo(component, event, helper);
        helper.getTaskInfo(component, event, helper);
        
        var taskMonitor = window.setInterval(
            $A.getCallback(function() {          
                helper.getTaskInfo(component, event, helper);
            }), 5000
        );   

        var limitCheckerMonitor = window.setInterval(
            $A.getCallback(function() {          
                helper.timeDownChecker(component, event, helper);
            }), 1000
        );   

        component.set("v.taskMonitorId", taskMonitor); 
        component.set("v.limitCheckerMonitorId", limitCheckerMonitor); 
        
        /*component.set('v.subscription', null);
        // Get empApi component.
        const empApi = component.find('empApi');
        // Define an error handler function that prints the error to the console.
        const errorHandler = function (message) {
            console.error('Received error ', JSON.stringify(message));
        };
        // Register empApi error listener and pass in the error handler function.
        empApi.onError($A.getCallback(errorHandler));
        
        const channel = component.get('v.channel');
        // Subscription option to get only new events.
        const replayId = -1;
        // Callback function to be passed in the subscribe call.
        // After an event is received, this callback prints the event
        // payload to the console. A helper method displays the message
        // in the console app.
        const callback = function (message) {
            alert('Event Received : ' + JSON.stringify(message));
        };
        // Subscribe to the channel and save the returned subscription object.
        empApi.subscribe(channel, replayId, $A.getCallback(callback)).then($A.getCallback(function (newSubscription) {
            console.log('Subscribed to channel ' + channel);
            component.set('v.subscription', newSubscription);
        }));*/
        
    },
    
    onSelectContactType : function(component, event, helper) {
        
        helper.visibilityReset(component, event, helper, 1);
        var selectedContactType = component.find('selectContactType').get('v.value');
                
        if(helper.isReprogramacion(component)){
            component.set('v.resultId', component.get('v.specialCaseReprogramacion').Id);
            component.set('v.reasonCode', component.get('v.specialCaseReprogramacion').TV_CCL_TXT_Code__c);
            helper.displayReprogramacion(component, event, helper);
        }else if(helper.isVentaOK(component)){
            component.set('v.resultId', component.get('v.specialCaseVentaOk').Id);
            component.set('v.reasonCode', component.get('v.specialCaseReprogramacion').TV_CCL_TXT_Code__c);
            helper.displayVentaOk(component, event, helper);
        }else if (!$A.util.isEmpty(selectedContactType)) { 
            component.set('v.agrupationDisabled', false);
            component.set('v.agrupationVisibleStyle', 'slds-m-bottom--medium slds-show');
        	helper.setAgrupationOptions(component, event, helper, selectedContactType);
        }
    },
    
    onSelectAgrupation : function(component, event, helper) {
		
        helper.visibilityReset(component, event, helper, 2);   
        
        var selectedAgrupation = component.find('selectAgrupation').get('v.value');
        
        if (!$A.util.isEmpty(selectedAgrupation)) {
        	helper.setTypeOptions(component, event, helper, component.get('v.selectedContactType'), selectedAgrupation);         
            
            component.set('v.typeVisibleStyle', 'slds-show slds-m-bottom--medium');
            component.set('v.typeDisabled', false);
        }
    },
    
    onSelectType : function(component, event, helper){
        helper.visibilityReset(component, event, helper, 3); 
        var selectedType = component.find('selectType').get('v.value');
        if (!$A.util.isEmpty(selectedType)) {  
            component.set('v.buttonDisabled', false);
            component.set('v.additionalInformationDisabled', false);
        	helper.displayAdditionalFields(component, event, helper);
        }else{ 
            component.set('v.additionalInformationDisabled', true);
            component.set('v.buttonDisabled', true);
        }
    },
    
    onSelectDateTime : function(component, event, helper){
            
        var selectedDateTime = component.get('v.dateTimeRecall');
        
        if (!$A.util.isEmpty(selectedDateTime)) {  
            
            var currentDate = new Date(new Date().getTime());
            var selectedDate = helper.sfStringToDateTime(selectedDateTime);
            selectedDate.setMinutes(selectedDate.getMinutes() - selectedDate.getTimezoneOffset());
            var validDate = true;
            
            // Comprobar si la fecha es anterior
            var differenceInTime = selectedDate.getTime() - currentDate.getTime(); 
            if(differenceInTime <= 0){
                validDate = false;
                helper.showErrorToast('Fecha de reprogramación','La fecha de reprogramación es anterior a la actual');
            }
            
            // Eliminamos la parte de horas, minutos, etc
            selectedDate = helper.toDate(selectedDate);
            currentDate = helper.toDate(currentDate);
            
            differenceInTime = selectedDate.getTime() - currentDate.getTime();
			var differenceInDays = differenceInTime / 1000 / 3600 / 24; 
            
            if(differenceInDays > 90){
                validDate = false;
                helper.showErrorToast('Fecha de reprogramación','La fecha de reprogramación es superior a 3 meses');
            } else if(differenceInDays > 7){
                helper.showToast('Fecha de reprogramación', 'warning', 'La fecha de reprogramación es superior a una semana');
            }
            if(validDate == true){	
            	component.set('v.buttonDisabled', false);                
            }else{
            	component.set('v.buttonDisabled', true);
            }
            
        }else{
            component.set('v.buttonDisabled', true);
        }
        
    },
    
    onClickRegister : function(component, event, helper){  
        
        if(helper.isVentaOK(component) ){
            helper.validateVentaOK(component, event, helper);
        }else{
            if(helper.isReprogramacion(component) && !helper.validatePhone(component)){
                helper.showErrorToast('Error', 'El telefono que ha introducido para la reprogramación no es correcto');	
            }else{
                helper.startRegister(component, event, helper);
            }
        }
    },
    
    testReady: function(component, event, helper){
        
        var loginUrl = "https://svc-csf-cert.ININ-ENDESA-V4.com/client/api/svc-cic-cert.inin-endesa-v4.local/icws/connection";
        
        var connectionParams = {
            "__type": "urn:inin.com:connection:icAuthConnectionRequestSettings",
            "applicationName": "ICWS JavaScript Direct Usage Example",
            "userID": component.get('v.loginUser'),
            "password": component.get('v.loginPassword')
        };
        
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open("post", loginUrl, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status > 200 && xhr.status < 299) {
                    console.log('Conectado al ICWS');
                    var json = JSON.parse(xhr.responseText);
                    component.set('v.genToken', json.csrfToken);
                    component.set('v.genSessionId', json.sessionId);
                    component.set('v.loginSuccess', true);
                    
        			var campaignId = '%7B8A7AAA9D-09B9-430B-BC41-5B906E938901%7D';
                    
                    var handlerParams = { 
                        "campaignIds": "%7B8A7AAA9D-09B9-430B-BC41-5B906E938901%7D"
                    };
                    
                    var url = "https://svc-csf-cert.ININ-ENDESA-V4.com/client/api/svc-cic-cert.inin-endesa-v4.local/icws/" + component.get('v.genSessionId') + "/dialer/ready-for-calls";
                    var xhr2 = new XMLHttpRequest();
                    xhr2.withCredentials = true;
                    xhr2.open("post", url, true);     
                    xhr2.setRequestHeader('ININ-ICWS-CSRF-Token', component.get('v.genToken'));
                    xhr2.setRequestHeader('Content-Type', "application/vnd.inin.icws+JSON");
                    xhr2.setRequestHeader('sessionId', component.get('v.genSessionId'));
                    xhr2.onreadystatechange = function() {
                        if (xhr2.readyState == 4) {
                            if ( xhr2.status == 200 || xhr2.status == 201 || xhr2.status == 202) {
                                helper.showSuccessToast('Prueba enviada', 'Prueba enviada');
                            } else {
                                helper.showErrorToast('Error', 'Error al enviar información al ICWS');
                            }
                        }
                    };
                    
                    xhr2.send(JSON.stringify(handlerParams));
                } else {
                    helper.showErrorToast('Error', 'Error al conectar con ICWS');
                    console.log('Loaded: ' + xhr.status + ' - ' + xhr.response);
                    component.set('v.loginSuccess', false);
                }
            }
        };
        xhr.send(JSON.stringify (connectionParams));  
    }
})