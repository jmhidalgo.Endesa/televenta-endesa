({
    loginAndRegister: function(component, event, helper) {
        console.log('Iniciando sesión en ICWS');
        component.set('v.integracionMarcadorOK', false);
        var loginUrl = component.get("v.endpointIcws") + "client/api/svc-cic-cert.inin-endesa-v4.local/icws/connection";
        
        var connectionParams = {
            "__type": "urn:inin.com:connection:icAuthConnectionRequestSettings",
            "applicationName": "ICWS JavaScript Direct Usage Example",
            "userID": component.get('v.loginUser'),
            "password": component.get('v.loginPassword')
        };
        
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open("post", loginUrl, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status > 200 && xhr.status < 299) {
                    console.log('Conectado al ICWS');
                    var json = JSON.parse(xhr.responseText);
                    component.set('v.genToken', json.csrfToken);
                    component.set('v.genSessionId', json.sessionId);
                    component.set('v.loginSuccess', true);
                    helper.createDisposition(component, event, helper);
                } else {
        			component.set('v.integracionMarcadorOK', true);	
                    helper.showErrorToast('Error', 'Error al conectar con ICWS');
                    helper.updateTaskResult(component, event, helper, null, null, true);
                    console.log('Error al conectar con ICWS. Status: ' + xhr.status + ' | Error: ' + xhr.response);
                    component.set('v.loginSuccess', false);
                }
            }
        };
        xhr.send(JSON.stringify (connectionParams));        
    },
    
    createDisposition : function (component, event, helper) {
        
        var task = component.get('v.task');
        var callId = task.CallObject.substring(0, 10);
        var callIdKey = task.CallObject;
        var campaignName = task.TV_Campana_Marcador__c;
        var campaignId = task.FII_ACT_LKP_Campaign__c;
        var agentId = task.TV_Login_Agent_Marcador__c;
        var assignToAgent = '';
        
        // Datos genericos
        var reason = component.get('v.reasonCode');
        
        // Datos reprogramacion
        var recallPhone = null;
        var assignAgent = null;
        var recallDate = component.get('v.dateTimeRecall');
        if(helper.isReprogramacion(component)){
            recallPhone = component.get('v.phoneRecall');
            assignAgent = component.get('v.assignToAgent');
        }
        
        // Datos adicionales
        var additionalDate = component.get('v.additionalDate');
        var additionalChannel = component.get('v.selectedAdditionalChannel');
        
        if(assignAgent){
            assignToAgent=agentId;
        }
        
        var formattedDate;
        if(recallDate){
            formattedDate = this.formatDate(recallDate); 
        }
        
        var handlerParams = { 
            "objectId": "EN_SF_Disposition",
            "eventId": "EN_SF_Disposition",
            "data": [callId, campaignName, campaignId, callIdKey, agentId, reason, formattedDate, recallPhone, assignToAgent]
        };
        
        var url = component.get("v.endpointIcws") + "client/api/svc-cic-cert.inin-endesa-v4.local/icws/" + component.get('v.genSessionId') + "/system/handler-notification";
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open("post", url, true);     
        xhr.setRequestHeader('ININ-ICWS-CSRF-Token', component.get('v.genToken'));
        xhr.setRequestHeader('Content-Type', "application/vnd.inin.icws+JSON");
        
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if ( xhr.status == 200 || xhr.status == 201 || xhr.status == 202) {
                    console.log('Información enviada al marcador correctamente');
                    helper.updateTaskResult(component, event, helper, true, null, null);
                } else {
                    helper.showErrorToast('Error', 'Error al enviar información al ICWS');
                    console.log('Error al enviar información al ICWS');
                    helper.updateTaskResult(component, event, helper, null, null, true);
                }
                component.set('v.integracionMarcadorOK', true);	
            }
        };
        
        xhr.send(JSON.stringify(handlerParams));
    },
    
    updateTaskResult: function(component, event, helper, updatedIWCS, updatedSf, technicalError){
        console.log('Guardando resultado de actualizaciones en la Task');
        var actionUpdateTaskResult = component.get("c.updateTaskResult");
        actionUpdateTaskResult.setParams({
            taskId: component.get('v.task').Id,
            updatedToICWS: updatedIWCS,
            updatedInSF: updatedSf,
            technicalError: technicalError
        });
        actionUpdateTaskResult.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('Guardado resultado actualizaciones Marcador: ' + updatedIWCS + ' | Task: ' + updatedSf + ' | Technical Error: ' + technicalError);
            }else if(state === "ERROR"){
                console.log('Error al guardar el resultado actualizaciones Marcador: ' + updatedIWCS + ' | Task: ' + updatedSf + ' | Technical Error: ' + technicalError);
                helper.showErrorToast('Error', 'Error técnico inesperado');
            }
        });
        $A.enqueueAction(actionUpdateTaskResult);
    },
    
    updateSalesforceData: function(component, event, helper){
        component.set('v.salesforceUpdated', false);
        console.log('Guardando resultado en Salesforce');
        var recallPhone = null;
        var recallAssignTo = null;
        if(helper.isReprogramacion(component)){
            recallPhone = component.get('v.phoneRecall');
            recallAssignTo = component.get('v.assignToAgent');
        }
        var actionUpdateSalesforceData = component.get("c.updateSalesforceData");
        actionUpdateSalesforceData.setParams({
            taskId: 			component.get('v.task').Id,
            campaignMemberId: 	component.get('v.task').FII_ACT_TXT_CampaignMemberId__c, 
            resultId: 			component.get('v.resultId'),
            additionalInfo: 	component.get('v.additionalInformation'),
            recallAssignTo: 	recallAssignTo,
            recallDatetime: 	component.get('v.dateTimeRecall'),
            recallPhone: 		recallPhone,
            additionalDate:		component.get('v.additionalDate'),
            additionalChannel: 	component.get('v.selectedAdditionalChannel')
        });
        actionUpdateSalesforceData.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('Guardado resultado en Salesforce');
                helper.updateTaskResult(component, event, helper, null, true, null);
            }else if(state === "ERROR"){
                console.log('Error al guardar el resultado en Salesforce');
                helper.showErrorToast('Error', 'Error al guardar el resultado en Salesforce');
                helper.updateTaskResult(component, event, helper, null, null, true);
            }
        	component.set('v.salesforceUpdated', true);
        });
        $A.enqueueAction(actionUpdateSalesforceData);
    },
    
    isVentaOK: function(component){
        var specialCaseVentaOk = component.get('v.specialCaseVentaOk');
        var selectedContactType = component.find('selectContactType').get('v.value');
        if(specialCaseVentaOk != null && selectedContactType == specialCaseVentaOk.TV_CCL_SEL_ContactType__c){
            return true;
        }
        return false;
    },
    
    isReprogramacion: function(component){
        var specialCaseReprogramacion = component.get('v.specialCaseReprogramacion');
        var selectedContactType = component.get('v.selectedContactType');
        if(specialCaseReprogramacion != null && selectedContactType == specialCaseReprogramacion.TV_CCL_SEL_ContactType__c){
            return true;
        }
        return false;
    },

    getLoginInfo: function(component, event, helper){
        var actionGetLoginInfo = component.get("c.getLoginData");
        actionGetLoginInfo.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
            	component.set('v.loginUser', response.getReturnValue().UserName__c);
                component.set('v.loginPassword', response.getReturnValue().Password__c);
                component.set('v.endpointIcws', response.getReturnValue().EndPoint__c);
            }else if(state === "ERROR"){
                helper.showErrorToast('Error', 'Error al obtener datos de inicio de sesión para conectar con ICWS');
                console.log('Error: Error obteniendo metadata de inicio de sesión en ICWS');
            }
        });
        $A.enqueueAction(actionGetLoginInfo);
    },
    
    getPicklistData: function(component, event, helper){
    	var actionGetPicklistData = component.get("c.getPicklistData");
        actionGetPicklistData.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue() != null) {
               this.buildPicklistMap(component, event, helper, response.getReturnValue());
            }else if(state === "ERROR"){
                helper.showErrorToast('Error', 'Error al obtener datos de inicio de sesión para conectar con ICWS');
                console.log('Error: Error obteniendo metadata de inicio de sesión en ICWS');
            }
        });
        $A.enqueueAction(actionGetPicklistData);
        
	},
        
    buildPicklistMap: function(component, event, helper, metadataList){
        var mapPicklists = component.get('v.mapPicklists');
        var mapPicklistsProperties = {};
		for(var metadata in metadataList){
            var key = metadataList[metadata].TV_CCL_SEL_ContactType__c + ';' + metadataList[metadata].TV_CCL_SEL_Aggrupation__c + ';' + metadataList[metadata].TV_CCL_SEL_Typology__c;
            if(mapPicklists[metadataList[metadata].TV_CCL_SEL_ContactType__c] === undefined){
                mapPicklists[metadataList[metadata].TV_CCL_SEL_ContactType__c] = {};
            }
            if(mapPicklists[metadataList[metadata].TV_CCL_SEL_ContactType__c][metadataList[metadata].TV_CCL_SEL_Aggrupation__c] === undefined){
                mapPicklists[metadataList[metadata].TV_CCL_SEL_ContactType__c][metadataList[metadata].TV_CCL_SEL_Aggrupation__c] = [];
            }
            
            // Casos especiales Reprogramacion y Venta OK
            if(metadataList[metadata].TV_CCL_FLG_IsRecall__c){
                component.set('v.specialCaseReprogramacion', metadataList[metadata]);
            }
            if(metadataList[metadata].TV_CCL_FLG_IsSellOk__c){
                component.set('v.specialCaseVentaOk', metadataList[metadata]);
            }

            mapPicklists[metadataList[metadata].TV_CCL_SEL_ContactType__c][metadataList[metadata].TV_CCL_SEL_Aggrupation__c].push(metadataList[metadata].TV_CCL_SEL_Typology__c);
			mapPicklistsProperties[key] = metadataList[metadata];     
        }
        component.set('v.mapPicklistsProperties', mapPicklistsProperties);
        component.set('v.mapPicklists', mapPicklists);
        helper.setContactTypeOptions(component, event, helper);
    },
    
    setContactTypeOptions: function(component, event, helper){
        var contactTypeOptions = [];
        var mapPicklists = component.get('v.mapPicklists');
        for(var contactType in mapPicklists){
         	var contactTypeSelectOption = {value: contactType, label:contactType};
            contactTypeOptions.push(contactTypeSelectOption);
        }
        component.set("v.contactTypeOptions", contactTypeOptions);
    },
    
    setAgrupationOptions: function(component, event, helper, contactType) {
        var agrupationOptions = [];
        var mapPicklists = component.get('v.mapPicklists');
        var listAgruptation = mapPicklists[contactType];
        for(var agrupation in listAgruptation){
            var agrupationSelectOption = {value: agrupation, label:agrupation};
            agrupationOptions.push(agrupationSelectOption);
        }
        component.set("v.agrupationOptions", agrupationOptions);
    },
    
    setTypeOptions: function(component, event, helper, contactType, agrupation) {
        var typeOptions = [];
        var mapPicklists = component.get('v.mapPicklists');
        var listTypes = mapPicklists[contactType][agrupation];
        for(var typ in listTypes){
            var typeSelectOption = {value: listTypes[typ], label:listTypes[typ]};
            typeOptions.push(typeSelectOption);
        }
        component.set("v.typeOptions", typeOptions);
    },
    
    getChannelData: function(component, event, helper){
        var channelOptions = [];
        channelOptions.push({value: 'Punto de venta',   label: 'Punto de venta'});
        channelOptions.push({value: 'Llamada', 			label: 'Llamada'});
        channelOptions.push({value: 'Digital', 			label: 'Digital'});
        channelOptions.push({value: 'Visita', 			label: 'Visita'});
        component.set("v.channelOptions", channelOptions);
    },
    
    displayAdditionalFields: function(component, event, helper){
        var key = component.get('v.selectedContactType') + ';' + component.get('v.selectedAgrupation') + ';' + component.get('v.selectedType');
    	var resultInfo = component.get('v.mapPicklistsProperties')[key];
        if(resultInfo != null){
            component.set('v.reasonCode', resultInfo.TV_CCL_TXT_Code__c);
            component.set('v.resultId', resultInfo.Id);
            if(resultInfo.TV_CCL_FLG_NeedAdditionalChannel__c){
            	component.set('v.additionalChannelVisibleStyle', 'slds-m-bottom--medium slds-show');
            }
            if(resultInfo.TV_CCL_FLG_NeedAdditionalDate__c){
            	component.set('v.additionalDateVisibleStyle', 'slds-m-bottom--medium slds-show');
            }
        }else{
            helper.showErrorToast('Error', 'Error obteniendo datos para la combinación de picklists');
            console.log('Error: Error obteniendo datos para la combinación de picklists');
        }
    },
    
    displayReprogramacion: function(component, event, helper){
        
    	var showClass = helper.getShowClass();
        var hideClass = helper.getHideClass();
        
        component.set('v.additionalInformationDisabled', false);
        component.set('v.agrupationVisibleStyle', hideClass);
        component.set('v.typeVisibleStyle', hideClass);
        component.set('v.additionalInformationVisibleStyle', showClass);
        component.set('v.dateTimeVisibleStyle', showClass);
        component.set('v.phoneRecallVisibleStyle', showClass);
        component.set('v.assignToAgentVisibleStyle', showClass);
        
	},
    
    displayVentaOk: function(component, event, helper){
        
    	var showClass = helper.getShowClass();
        var hideClass = helper.getHideClass();
        
        component.set('v.agrupationVisibleStyle', hideClass);
        component.set('v.typeVisibleStyle', hideClass);
        component.set('v.additionalInformationVisibleStyle', hideClass);
        component.set('v.buttonDisabled', false);
        
	},
    
    visibilityReset: function(component, event, helper, level){
        
        var showClass = helper.getShowClass();
        var hideClass = helper.getHideClass();
        
        // Input vars
        if(level < 2)
        	component.set('v.selectedAgrupation', '');
        
        if(level < 3)
        	component.set('v.selectedType', '');
        
        component.set('v.dateTimeRecall', '');
        component.set('v.assignToAgent', true);
        component.set('v.phoneRecall', component.get('v.task').TV_Telefono_Marcado__c);
        component.set('v.additionalInformation', '');
        component.set('v.selectedAdditionalChannel', '');
        component.set('v.additionalDate', '');
        
        // Disable vars
        if(level < 2){
        	component.set('v.agrupationDisabled', true);
        }
        
        if(level < 3){
        	component.set('v.typeDisabled', true);
            component.set('v.additionalInformationDisabled', true);
        }
        
        component.set('v.buttonDisabled', true);
        
        // Style vars
        component.set('v.agrupationVisibleStyle', showClass);
        component.set('v.typeVisibleStyle', showClass);
        component.set('v.additionalInformationVisibleStyle', showClass);
        component.set('v.dateTimeVisibleStyle', hideClass);
        component.set('v.phoneRecallVisibleStyle', hideClass);
        component.set('v.assignToAgentVisibleStyle', hideClass);
        component.set('v.additionalChannelVisibleStyle', hideClass);
        component.set('v.additionalDateVisibleStyle', hideClass);
    },
    
    getShowClass: function(){
        return 'slds-show slds-m-bottom--medium';
    },
    
    getHideClass: function(){
        return 'slds-hide';
    },
    
    showSuccessToast: function (title, message) {
        this.showToast(title, "success", message);
    },    
    
    showErrorToast: function (title, message) {
        this.showToast(title, "error", message);
    },
    
    showToast: function (title, type, message, mode) {
        
        if (!mode)
            mode = "dismissible";
        
        if (!type)
            type = "other";
        
        if (!title)
            title = "";
        
        var toastEvent = $A.get("e.force:showToast");
        
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message,
            "mode": mode
        });
        
        toastEvent.fire();
    },
    
    formatDate : function (unformattedDate){
        var formattedDate;
        var index = unformattedDate.indexOf('T');
        var timeInfo = unformattedDate.substring(index + 1, unformattedDate.length-5);
        var dateInfo = unformattedDate.substring(0,index);
        
        dateInfo = dateInfo.split('-');
        
        var day = dateInfo[2];
        var month = dateInfo[1];
        var year = dateInfo[0];
        
        formattedDate = day + '/' + month + '/' + year + ' ' + timeInfo;
        return formattedDate;
    },
    
    toDate : function(datetime){
        datetime.setHours(0);
        datetime.setMinutes(0);
        datetime.setSeconds(0);
        datetime.setMilliseconds(0);
        return datetime;
    },

    getParameterByName: function(component, event, paramName, url) {
        paramName = paramName.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + paramName + "(=([^&#]*)|&|#|$)");
        var results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },

    getTaskInfo: function (component, event, helper){
        var actionGetTaskData = component.get("c.getTaskData");
        var recordId = component.get("v.recordId");
        if(actionGetTaskData != null){
            actionGetTaskData.setParams({
                recordId: recordId
            });
            actionGetTaskData.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && response.getReturnValue() != null) {
                    var currentTask = response.getReturnValue();
                    if(component.get('v.task') != null){
                        var previousTask = component.get('v.task');
                        if(previousTask.Status == 'Open' && currentTask.Status == 'Completed'){
                            helper.callEnded(component, event, helper);
                        }
                    }
                    component.set('v.task', currentTask);
                    if(currentTask.TV_Result_Tip_Sf__c == true || currentTask.TV_Result_Tip_Mrc__c == true){
                		component.set('v.resultInformed', true);
                    }
                    if(currentTask.Status == 'Completed'){
                        window.clearInterval(component.get('v.taskMonitorId'));
                    }
                }else if(state === "ERROR"){ 
                    // Si no se ha encontrado ninguna task
                	component.set('v.componentEnabled', false);
                }
            });
            $A.enqueueAction(actionGetTaskData);
        }
    },

    callEnded: function(component, event, helper){
        console.log("Ha terminado la llamada");
        window.clearInterval(component.get('v.taskMonitorId'));
        
        var interactionEndedEvent = $A.get("e.c:TV_LEV_InteractionEnded");
        interactionEndedEvent.setParams(
            {"taskId" : component.get('v.task').Id}
        );
		interactionEndedEvent.fire();

    },
    
    timeDownChecker: function(component, event, helper){
        var currentTask = component.get('v.task');
        if(currentTask != null){
            if(currentTask.Status == 'Completed'){
                var currentDate = new Date(new Date().getTime());
                var limitTime = helper.sfStringToDateTime(currentTask.CompletedDateTime);
                limitTime.setMinutes(limitTime.getMinutes() + 5 - limitTime.getTimezoneOffset());
                limitTime.setMilliseconds(0);
                currentDate.setMilliseconds(0);        
                var differenceInTime = limitTime.getTime() - currentDate.getTime();
                var differenceInSeconds = differenceInTime / 1000;
                
                var minutes = Math.floor(differenceInSeconds / 60).toString().padStart(2,'0');
                var seconds = (differenceInSeconds - minutes * 60).toString().padStart(2,'0');
                component.set('v.timeLeft', minutes + ':' + seconds);
                if(differenceInSeconds <= 0){
                    // Si la task esta COMPLETED y ha pasado el tiempo administrativo
                    component.set('v.componentEnabled', false);
                    window.clearInterval(component.get('v.limitCheckerMonitorId'));
                }else{
                    // Si la task esta COMPLETED pero no ha pasado el tiempo administrativo
                    component.set('v.componentEnabled', true);
                }
            }else{
                // Si la task esta OPEN 
            	component.set('v.componentEnabled', true);
        	}
        }
    },
    
    sfStringToDateTime: function(strDatetime){
        return new Date(strDatetime.substring(0, 4), 
                                        parseInt(strDatetime.substring(5, 7))-1, 
                                        strDatetime.substring(8, 10), 
                                        strDatetime.substring(11, 13), 
                                        strDatetime.substring(14, 16), 
                                        strDatetime.substring(17, 19), 
                                        strDatetime.substring(20, 23));
    },
    
    validatePhone: function(component){
        var phoneNumber = component.get('v.phoneRecall');
        if(phoneNumber !== undefined){
            var str = phoneNumber.toString().replace(/\s/g, '');
            var result = ( str.length === 9 && /^[679]{1}[0-9]{8}$/.test(str) || str.length === 12 && /^[+]{1}[0-9]{2}?[-\s\.]?[0-9]{9}$/.test(str) );
            return result;
        }else{
            return false;
        }

    },
    
    validateVentaOK: function(component, event, helper){
        var actionValidateVentaOK = component.get("c.validateVentaOK");
        actionValidateVentaOK.setParams(
            {"taskId" : component.get('v.task').Id}
        );
        actionValidateVentaOK.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" ) {
                if( response.getReturnValue()){
                	helper.startRegister(component, event, helper);
                }else{
                	helper.showErrorToast('Error', 'No se puede tipificar la llamada como Venta OK, revisa las contrataciones en curso.');	
                }
            }else if(state === "ERROR"){ 
                helper.showErrorToast('Error', 'Error al validar la venta OK.');	
            }
        });
        $A.enqueueAction(actionValidateVentaOK);
       
    },
    
    startRegister: function(component, event, helper){
        helper.loginAndRegister(component, event, helper);
        helper.updateSalesforceData(component, event, helper);
        component.set('v.resultInformed', true);
    }
 
})